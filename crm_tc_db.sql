-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2018 at 04:50 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm_tc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `ID_CUSTOMER` int(11) NOT NULL,
  `COMPANY_NAME` varchar(100) NOT NULL,
  `COMPANY_EMAIL` varchar(100) NOT NULL,
  `COMPANY_ADDRESS` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`ID_CUSTOMER`, `COMPANY_NAME`, `COMPANY_EMAIL`, `COMPANY_ADDRESS`) VALUES
(1, 'GARUDA INDONESIA', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dt_request_status_order_log`
--

CREATE TABLE `dt_request_status_order_log` (
  `ID_REQUEST_STATUS_ORDER_LOG` int(11) NOT NULL,
  `REFERENCE` varchar(20) NOT NULL,
  `ID_STATUS_ORDER` int(11) NOT NULL,
  `MAINTENANCE` varchar(20) NOT NULL,
  `REQUEST_STATUS_ORDER_LOG_DATE` datetime NOT NULL,
  `DUE_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dt_request_status_order_log`
--

INSERT INTO `dt_request_status_order_log` (`ID_REQUEST_STATUS_ORDER_LOG`, `REFERENCE`, `ID_STATUS_ORDER`, `MAINTENANCE`, `REQUEST_STATUS_ORDER_LOG_DATE`, `DUE_DATE`) VALUES
(1, '99022191', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(2, '99022192', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(3, '99022193', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(4, '99022194', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(5, '99022191', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(9, '99022191', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(10, '99022192', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(11, '99022193', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(12, '99022194', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(13, '99022191', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(14, '99022192', 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(17, '99022195', 0, 'B check', '0000-00-00 00:00:00', '2018-02-06'),
(19, '132', 0, 'C check', '0000-00-00 00:00:00', '2018-02-02'),
(20, '1223', 0, 'C check', '0000-00-00 00:00:00', '2018-02-01'),
(21, '1313', 0, 'C check', '0000-00-00 00:00:00', '2018-02-08');

-- --------------------------------------------------------

--
-- Table structure for table `dt_request_status_progress_log`
--

CREATE TABLE `dt_request_status_progress_log` (
  `ID_REQUEST_STATUS_PROGRESS_LOG` int(11) NOT NULL,
  `ID_STATUS_PROGRESS` int(11) NOT NULL,
  `MAINTENANCE` varchar(20) NOT NULL,
  `REQUEST_STATUS_PROGRESS_LOG_DATE` datetime NOT NULL,
  `DUE_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dt_request_status_progress_log`
--

INSERT INTO `dt_request_status_progress_log` (`ID_REQUEST_STATUS_PROGRESS_LOG`, `ID_STATUS_PROGRESS`, `MAINTENANCE`, `REQUEST_STATUS_PROGRESS_LOG_DATE`, `DUE_DATE`) VALUES
(1, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(2, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(3, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(4, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(5, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(9, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(10, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(11, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(12, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(13, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(14, 0, 'C check', '0000-00-00 00:00:00', '2017-07-10'),
(17, 0, 'B check', '0000-00-00 00:00:00', '2018-02-06'),
(19, 0, 'C check', '0000-00-00 00:00:00', '2018-02-02'),
(20, 0, 'C check', '0000-00-00 00:00:00', '2018-02-01'),
(21, 0, 'C check', '0000-00-00 00:00:00', '2018-02-08');

-- --------------------------------------------------------

--
-- Table structure for table `ms_customer`
--

CREATE TABLE `ms_customer` (
  `ID_CUSTOMER` int(11) NOT NULL,
  `CUSTOMER_NAME` varchar(50) NOT NULL,
  `ADDRESS` varchar(50) DEFAULT NULL,
  `WEBSITE` varchar(100) DEFAULT NULL,
  `PHONE` varchar(100) DEFAULT NULL,
  `MOBILE` varchar(100) NOT NULL,
  `FAX` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_customer`
--

INSERT INTO `ms_customer` (`ID_CUSTOMER`, `CUSTOMER_NAME`, `ADDRESS`, `WEBSITE`, `PHONE`, `MOBILE`, `FAX`, `EMAIL`) VALUES
(1, 'GARUDA INDONESIA', NULL, NULL, NULL, '', NULL, ''),
(2, 'CITYLINK INDONESIA', NULL, NULL, NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `ms_part_number`
--

CREATE TABLE `ms_part_number` (
  `ID_PN` int(11) NOT NULL,
  `PN_NAME` varchar(50) NOT NULL,
  `PN_DESC` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_part_number`
--

INSERT INTO `ms_part_number` (`ID_PN`, `PN_NAME`, `PN_DESC`) VALUES
(1, '12664', 'VALVE-CHECK'),
(2, '1263A0000-03', 'MACHINE-AIR-CYCLE');

-- --------------------------------------------------------

--
-- Table structure for table `ms_request_category`
--

CREATE TABLE `ms_request_category` (
  `ID_REQ_CAT` int(11) NOT NULL,
  `REQ_CAT_NAME` varchar(50) NOT NULL,
  `REQ_CAT_DESC` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_request_category`
--

INSERT INTO `ms_request_category` (`ID_REQ_CAT`, `REQ_CAT_NAME`, `REQ_CAT_DESC`) VALUES
(1, 'BOEING 737', 'IND'),
(2, 'AIRBUS A330', 'IND');

-- --------------------------------------------------------

--
-- Table structure for table `ms_request_part_number`
--

CREATE TABLE `ms_request_part_number` (
  `ID_REQ_CAT` int(11) NOT NULL,
  `ID_PN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_status_order`
--

CREATE TABLE `ms_status_order` (
  `ID_STATUS_ORDER` int(1) NOT NULL,
  `STATUS_ORDER_NAME` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_status_order`
--

INSERT INTO `ms_status_order` (`ID_STATUS_ORDER`, `STATUS_ORDER_NAME`) VALUES
(1, 'New Order'),
(2, 'Accepted'),
(3, 'In Progress'),
(4, 'Close'),
(5, 'Cancel Order');

-- --------------------------------------------------------

--
-- Table structure for table `ms_status_progress`
--

CREATE TABLE `ms_status_progress` (
  `ID_STATUS_PROGRESS` int(2) NOT NULL,
  `STATUS_PROGRESS_NAME` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_status_progress`
--

INSERT INTO `ms_status_progress` (`ID_STATUS_PROGRESS`, `STATUS_PROGRESS_NAME`) VALUES
(1, 'Waiting Approved'),
(2, 'Waiting Material'),
(3, 'Under Maintenance'),
(4, 'Waiting Repair\r\n'),
(5, 'Finished');

-- --------------------------------------------------------

--
-- Table structure for table `progress_status`
--

CREATE TABLE `progress_status` (
  `ID_PROGRESS_STATUS` int(12) NOT NULL,
  `PROGRESS_STATUS` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `progress_status`
--

INSERT INTO `progress_status` (`ID_PROGRESS_STATUS`, `PROGRESS_STATUS`) VALUES
(1, 'Waiting Approved'),
(2, 'Waiting Material'),
(3, 'Under Maintenance'),
(4, 'Waiting Repair'),
(5, 'Finished');

-- --------------------------------------------------------

--
-- Table structure for table `req_pn`
--

CREATE TABLE `req_pn` (
  `ID_REQ_CAT` int(11) NOT NULL,
  `ID_PN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_order`
--

CREATE TABLE `status_order` (
  `ID_STATUS` int(11) NOT NULL,
  `NO_ORDER` varchar(20) NOT NULL,
  `STATUS` varchar(50) NOT NULL,
  `CUSTOMER` varchar(100) NOT NULL,
  `MAINTENANCE` varchar(20) NOT NULL,
  `DUE_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_order`
--

INSERT INTO `status_order` (`ID_STATUS`, `NO_ORDER`, `STATUS`, `CUSTOMER`, `MAINTENANCE`, `DUE_DATE`) VALUES
(1, '99022191', 'Waiting Approved', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(2, '99022192', 'Waiting Approved', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(3, '99022193', 'Waiting Approved', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(4, '99022194', 'Waiting Approved', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(5, '99022191', 'Waiting Material', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(9, '99022191', 'Under Maintenance', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(10, '99022192', 'Under Maintenance', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(11, '99022193', 'Under Maintenance', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(12, '99022194', 'Under Maintenance', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(13, '99022191', 'Waiting Repair', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(14, '99022192', 'Waiting Repair', 'Garuda Maintenance Facility', 'C check', '2017-07-10'),
(17, '99022195', 'Waiting Approved', 'Saudi Airlines', 'B check', '2018-02-06'),
(19, '99022189', 'Finished', 'Garuda Indonesia', 'C check', '2018-02-02'),
(20, '99022190', 'Finished', 'Garuda Indonesia', 'C check', '2018-02-01'),
(21, '99022194', 'Finished', 'Garuda Indonesia', 'C check', '2018-02-08'),
(22, '99022191', 'Finished', 'Garuda Maintenance Facility', 'C-Check', '2018-02-27'),
(23, '99022191', 'Finished', 'Garuda Indonesia', 'C-check', '2018-02-27'),
(24, '99022192', 'Finished', 'Garuda Indonesia', 'C-check', '2018-02-27'),
(25, '99022193', 'Finished', 'Garuda Indonesia', 'C-check', '2018-02-27'),
(26, '99022194', 'Finished', 'Garuda Indonesia', 'C-check', '2018-02-27'),
(27, '99022195', 'Finished', 'Garuda Indonesia', 'C-check', '2018-02-27'),
(28, '99022196', 'Finished', 'Garuda Indonesia', 'C-check', '2018-02-27'),
(29, '99022196', 'Finished', 'Garuda Indonesia', 'C-check', '2018-02-27');

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `ID_CUSTOMER` int(11) NOT NULL,
  `CUSTOMER_NAME` varchar(50) NOT NULL,
  `ADDRESS` varchar(50) DEFAULT NULL,
  `WEBSITE` varchar(100) DEFAULT NULL,
  `PHONE` varchar(100) DEFAULT NULL,
  `MOBILE` varchar(100) NOT NULL,
  `FAX` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_customer`
--

INSERT INTO `tb_customer` (`ID_CUSTOMER`, `CUSTOMER_NAME`, `ADDRESS`, `WEBSITE`, `PHONE`, `MOBILE`, `FAX`, `EMAIL`) VALUES
(1, 'GARUDA INDONESIA', NULL, NULL, NULL, '', NULL, ''),
(2, 'CITYLINK INDONESIA', NULL, NULL, NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_part_number`
--

CREATE TABLE `tb_part_number` (
  `ID_PN` int(11) NOT NULL,
  `PN_NAME` varchar(50) NOT NULL,
  `PN_DESC` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_part_number`
--

INSERT INTO `tb_part_number` (`ID_PN`, `PN_NAME`, `PN_DESC`) VALUES
(1, '12664', 'VALVE-CHECK'),
(2, '1263A0000-03', 'MACHINE-AIR-CYCLE');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pool_order`
--

CREATE TABLE `tb_pool_order` (
  `ID_ORDER_POOL` int(11) NOT NULL,
  `ID_USER_POOL` int(100) NOT NULL,
  `ID_CUSTOMER` int(100) NOT NULL,
  `SERVICE_LEVEL` varchar(50) NOT NULL,
  `ID_PART_NUMBER` int(100) NOT NULL,
  `COMPONENT_DESCRIPTION` varchar(100) NOT NULL,
  `ID_REQ_CAT` int(100) NOT NULL,
  `REQUESTED_DELIVERY_DATE` datetime NOT NULL,
  `MAINTENANCE` varchar(5) NOT NULL,
  `POOL` varchar(5) NOT NULL,
  `OFF_WING` varchar(5) NOT NULL,
  `ATA_CHAPTER` varchar(50) NOT NULL,
  `AIRCRAFT_REGISTRATION` varchar(100) NOT NULL,
  `RELEASE_TYPE` varchar(50) NOT NULL,
  `3_DIGIT_AIRPORT_CODE` varchar(25) NOT NULL,
  `DESTINATION_ADDRESS` varchar(100) NOT NULL,
  `CUSTOMER_PO` varchar(50) NOT NULL,
  `CUSTOMER_RO` varchar(50) DEFAULT NULL,
  `REMAKE_OF_CUSTOMER` varchar(100) DEFAULT NULL,
  `ATTENTION_NAME` varchar(100) NOT NULL,
  `ATTENTION_PHONE` varchar(100) NOT NULL,
  `COMPANY_CONSIGNE_NAME` varchar(100) NOT NULL,
  `COMPANY_CONSIGNE_ADDRESS` varchar(100) NOT NULL,
  `STATUS_ORDER` varchar(100) NOT NULL,
  `STATUS_ORDER_CUSTOMER` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pool_order`
--

INSERT INTO `tb_pool_order` (`ID_ORDER_POOL`, `ID_USER_POOL`, `ID_CUSTOMER`, `SERVICE_LEVEL`, `ID_PART_NUMBER`, `COMPONENT_DESCRIPTION`, `ID_REQ_CAT`, `REQUESTED_DELIVERY_DATE`, `MAINTENANCE`, `POOL`, `OFF_WING`, `ATA_CHAPTER`, `AIRCRAFT_REGISTRATION`, `RELEASE_TYPE`, `3_DIGIT_AIRPORT_CODE`, `DESTINATION_ADDRESS`, `CUSTOMER_PO`, `CUSTOMER_RO`, `REMAKE_OF_CUSTOMER`, `ATTENTION_NAME`, `ATTENTION_PHONE`, `COMPANY_CONSIGNE_NAME`, `COMPANY_CONSIGNE_ADDRESS`, `STATUS_ORDER`, `STATUS_ORDER_CUSTOMER`) VALUES
(1, 3, 2, 'Pre-Defined', 2, 'VALVE-CHECK', 1, '2018-02-23 00:00:00', 'CHECK', '1', '', '', '', '', '', '', '', NULL, NULL, '', '', '', '', '', NULL),
(2, 1, 2, '', 2, '', 0, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', '', '', '', NULL),
(3, 1, 1, '', 2, '', 0, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_project_list`
--

CREATE TABLE `tb_project_list` (
  `ID_LSN` varchar(50) NOT NULL,
  `LO` varchar(100) NOT NULL,
  `WORKSCOPE` varchar(100) NOT NULL,
  `CONTRACTUAL_TAT` varchar(50) NOT NULL,
  `INDUCTION_DATE` date NOT NULL,
  `CURRENT_TAT` varchar(100) NOT NULL,
  `ID_CUSTOMER` int(100) NOT NULL,
  `TYPE` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_project_list`
--

INSERT INTO `tb_project_list` (`ID_LSN`, `LO`, `WORKSCOPE`, `CONTRACTUAL_TAT`, `INDUCTION_DATE`, `CURRENT_TAT`, `ID_CUSTOMER`, `TYPE`) VALUES
('1234AA', 'Andri', 'Performance', '34', '2018-02-26', '12', 1, ''),
('1244BD', 'Arya', 'Performance', '23', '2018-02-26', '11', 1, ''),
('2224RS', 'Inggi', 'Overhaul', '22', '2018-02-22', '24', 1, ''),
('2289CG', 'Shiro', 'Medium', '54', '2018-02-26', '45', 1, ''),
('3217KU', 'Joko', 'Minimum', '11', '2018-03-19', '21', 1, ''),
('3312HJ', 'Karin', 'Minimum', '13', '2018-02-19', '43', 1, ''),
('4462IK', 'Fajar', 'Minimum', '33', '2018-01-20', '13', 1, ''),
('4489DH', 'Kasim', 'Overhaul', '17', '2018-01-09', '14', 1, ''),
('5513WH', 'Ayu', 'Medium', '13', '2018-02-10', '9', 1, ''),
('5567PO', 'Michael', 'Performance', '15', '2018-03-20', '18', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reqcat`
--

CREATE TABLE `tb_reqcat` (
  `ID_REQ_CAT` int(11) NOT NULL,
  `REQ_CAT_NAME` varchar(50) NOT NULL,
  `REQ_CAT_DESC` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_reqcat`
--

INSERT INTO `tb_reqcat` (`ID_REQ_CAT`, `REQ_CAT_NAME`, `REQ_CAT_DESC`) VALUES
(1, 'BOEING 737', 'IND'),
(2, 'AIRBUS A330', 'IND');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tat`
--

CREATE TABLE `tb_tat` (
  `NO_ACCOUNT` int(11) NOT NULL,
  `ACCOUNT` varchar(50) NOT NULL,
  `REFERENCE` varchar(20) NOT NULL,
  `ASSIGNMENT` varchar(20) NOT NULL,
  `DOCUMENT_DATE` date NOT NULL,
  `CCY` varchar(10) DEFAULT NULL,
  `VALUE` decimal(20,2) DEFAULT NULL,
  `MHRS` decimal(20,2) DEFAULT NULL,
  `MAT` decimal(20,2) DEFAULT NULL,
  `OTHER` decimal(20,2) DEFAULT NULL,
  `PPN` decimal(20,2) DEFAULT NULL,
  `PPH` decimal(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tat`
--

INSERT INTO `tb_tat` (`NO_ACCOUNT`, `ACCOUNT`, `REFERENCE`, `ASSIGNMENT`, `DOCUMENT_DATE`, `CCY`, `VALUE`, `MHRS`, `MAT`, `OTHER`, `PPN`, `PPH`) VALUES
(1, '', '2018022700001', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '', '2018022700002', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '', '2018022700003', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '', '2018022700004', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '', '2018022700005', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '', '2018022700006', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '', '2018022700007', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '', '2018022700008', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '', '2018022700009', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '', '2018022700010', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '', '2018022700011', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '', '2018022700012', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '', '2018022700013', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '', '2018022700014', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '', '2018022700015', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '', '2018022700016', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '', '2018022700017', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '', '2018022700018', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '', '2018022700019', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '', '2018022700020', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '', '2018030100020', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '', '2018030100021', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '', '2018030100022', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '', '2018030200023', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_pooling`
--

CREATE TABLE `tr_pooling` (
  `NO_ACCOUNT` int(11) NOT NULL,
  `ACCOUNT` varchar(50) NOT NULL,
  `REFERENCE` varchar(20) NOT NULL,
  `ASSIGNMENT` varchar(20) NOT NULL,
  `DOCUMENT_DATE` date NOT NULL,
  `CCY` varchar(10) DEFAULT NULL,
  `VALUE` decimal(20,2) DEFAULT NULL,
  `MHRS` decimal(20,2) DEFAULT NULL,
  `MAT` decimal(20,2) DEFAULT NULL,
  `OTHER` decimal(20,2) DEFAULT NULL,
  `PPN` decimal(20,2) DEFAULT NULL,
  `PPH` decimal(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_pooling`
--

INSERT INTO `tr_pooling` (`NO_ACCOUNT`, `ACCOUNT`, `REFERENCE`, `ASSIGNMENT`, `DOCUMENT_DATE`, `CCY`, `VALUE`, `MHRS`, `MAT`, `OTHER`, `PPN`, `PPH`) VALUES
(1, '', '2018022700001', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '', '2018022700002', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '', '2018022700003', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '', '2018022700004', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '', '2018022700005', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '', '2018022700006', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '', '2018022700007', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '', '2018022700008', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '', '2018022700009', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '', '2018022700010', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '', '2018022700011', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '', '2018022700012', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '', '2018022700013', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '', '2018022700014', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '', '2018022700015', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '', '2018022700016', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '', '2018022700017', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '', '2018022700018', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '', '2018022700019', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '', '2018022700020', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '', '2018030100020', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '', '2018030100021', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '', '2018030100022', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '', '2018030200023', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_pooling_order`
--

CREATE TABLE `tr_pooling_order` (
  `ID_ORDER_POOL` int(11) NOT NULL,
  `REFERENCE` varchar(20) NOT NULL,
  `ID_USER_POOL` int(100) NOT NULL,
  `ID_CUSTOMER` int(100) NOT NULL,
  `SERVICE_LEVEL` varchar(50) NOT NULL,
  `ID_PART_NUMBER` int(100) NOT NULL,
  `COMPONENT_DESCRIPTION` varchar(100) NOT NULL,
  `ID_REQ_CAT` int(100) NOT NULL,
  `REQUESTED_DELIVERY_DATE` datetime NOT NULL,
  `MAINTENANCE` varchar(5) NOT NULL,
  `POOL` varchar(5) NOT NULL,
  `OFF_WING` varchar(5) NOT NULL,
  `ATA_CHAPTER` varchar(50) NOT NULL,
  `AIRCRAFT_REGISTRATION` varchar(100) NOT NULL,
  `RELEASE_TYPE` varchar(50) NOT NULL,
  `3_DIGIT_AIRPORT_CODE` varchar(25) NOT NULL,
  `DESTINATION_ADDRESS` varchar(100) NOT NULL,
  `CUSTOMER_PO` varchar(50) NOT NULL,
  `CUSTOMER_RO` varchar(50) DEFAULT NULL,
  `REMARKS_OF_CUSTOMER` varchar(100) DEFAULT NULL,
  `ATTENTION_NAME` varchar(100) NOT NULL,
  `ATTENTION_PHONE` varchar(100) NOT NULL,
  `COMPANY_CONSIGNE_NAME` varchar(100) NOT NULL,
  `COMPANY_CONSIGNE_ADDRESS` varchar(100) NOT NULL,
  `STATUS_ORDER` varchar(100) NOT NULL,
  `STATUS_ORDER_CUSTOMER` varchar(100) DEFAULT NULL,
  `REQUEST_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_pooling_order`
--

INSERT INTO `tr_pooling_order` (`ID_ORDER_POOL`, `REFERENCE`, `ID_USER_POOL`, `ID_CUSTOMER`, `SERVICE_LEVEL`, `ID_PART_NUMBER`, `COMPONENT_DESCRIPTION`, `ID_REQ_CAT`, `REQUESTED_DELIVERY_DATE`, `MAINTENANCE`, `POOL`, `OFF_WING`, `ATA_CHAPTER`, `AIRCRAFT_REGISTRATION`, `RELEASE_TYPE`, `3_DIGIT_AIRPORT_CODE`, `DESTINATION_ADDRESS`, `CUSTOMER_PO`, `CUSTOMER_RO`, `REMARKS_OF_CUSTOMER`, `ATTENTION_NAME`, `ATTENTION_PHONE`, `COMPANY_CONSIGNE_NAME`, `COMPANY_CONSIGNE_ADDRESS`, `STATUS_ORDER`, `STATUS_ORDER_CUSTOMER`, `REQUEST_DATE`) VALUES
(1, '2018022700001', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(2, '2018022700002', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(3, '2018022700003', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(4, '2018022700004', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(5, '2018022700005', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(6, '2018022700006', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(7, '2018022700007', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(8, '2018022700008', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(9, '2018022700009', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(10, '2018022700010', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(11, '2018022700011', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(12, '2018022700012', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(13, '2018022700013', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(14, '2018022700014', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(15, '2018022700015', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(16, '2018022700016', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(17, '2018022700017', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(18, '2018022700018', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(19, '2018022700019', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(20, '2018022700020', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '0000-00-00 00:00:00', 'Yes', 'No', 'Yes', 'Ata chapter isi', 'Aircraft reg isi', 'EASA', '3 Digit', 'BEKASI', '2897349', '8973894324', 'Parah nih  meleduk gede banget', 'Herdy', '088210563381', 'ASC Technology', 'Gondo Sari', '1', NULL, '2018-02-27'),
(21, '2018030100020', 0, 0, 'Pre-Defined', 2, 'MACHINE-AIR-CYCLE', 2, '2018-03-01 00:00:00', 'Yes', 'No', 'Yes', 'sada', 'aS', 'FAA', '123', '', '', '', '', '', '', '', '', '1', NULL, '2018-03-01'),
(22, '2018030100021', 0, 0, '0', 1, 'VALVE-CHECK', 1, '0000-00-00 00:00:00', '0', '0', '0', '', '', '0', '', '', '', '', '', '', '', '', '', '1', NULL, '2018-03-01'),
(23, '2018030100022', 0, 0, '0', 2, 'MACHINE-AIR-CYCLE', 1, '2018-03-02 00:00:00', 'Yes', 'Yes', 'Yes', 'qew', 'wqew', 'FAA', 'eqw', 'qwe', '12', '21', '', '', '', '', '', '1', NULL, '2018-03-01'),
(24, '2018030200023', 0, 0, '0', 2, 'MACHINE-AIR-CYCLE', 1, '2018-03-02 00:00:00', '0', 'Yes', '0', '', '', '0', '', '', '', '', '', '', '', '', '', '1', NULL, '2018-03-02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID_USER` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `GROUP_ID` int(1) NOT NULL,
  `CUSTOMER_ID` int(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `STATUS` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID_USER`, `NAME`, `USERNAME`, `PASSWORD`, `EMAIL`, `GROUP_ID`, `CUSTOMER_ID`, `last_login`, `STATUS`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', 1, 0, '2018-01-07 11:00:01', 1),
(4, 'TC LANDING GEAR', 'tc_landing', '5a696606bb478571d726c727d3386dd3', 'tc_landing@gmf.com', 4, 1, '2018-02-26 14:21:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `ID_USER_GROUP` int(11) NOT NULL,
  `USER_GROUP` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`ID_USER_GROUP`, `USER_GROUP`) VALUES
(1, 'Administrator'),
(2, 'Customers');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID_CUSTOMER`);

--
-- Indexes for table `dt_request_status_order_log`
--
ALTER TABLE `dt_request_status_order_log`
  ADD PRIMARY KEY (`ID_REQUEST_STATUS_ORDER_LOG`);

--
-- Indexes for table `dt_request_status_progress_log`
--
ALTER TABLE `dt_request_status_progress_log`
  ADD PRIMARY KEY (`ID_REQUEST_STATUS_PROGRESS_LOG`);

--
-- Indexes for table `ms_customer`
--
ALTER TABLE `ms_customer`
  ADD PRIMARY KEY (`ID_CUSTOMER`);

--
-- Indexes for table `ms_part_number`
--
ALTER TABLE `ms_part_number`
  ADD PRIMARY KEY (`ID_PN`);

--
-- Indexes for table `ms_request_category`
--
ALTER TABLE `ms_request_category`
  ADD PRIMARY KEY (`ID_REQ_CAT`);

--
-- Indexes for table `progress_status`
--
ALTER TABLE `progress_status`
  ADD PRIMARY KEY (`ID_PROGRESS_STATUS`);

--
-- Indexes for table `status_order`
--
ALTER TABLE `status_order`
  ADD PRIMARY KEY (`ID_STATUS`);

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`ID_CUSTOMER`);

--
-- Indexes for table `tb_part_number`
--
ALTER TABLE `tb_part_number`
  ADD PRIMARY KEY (`ID_PN`);

--
-- Indexes for table `tb_pool_order`
--
ALTER TABLE `tb_pool_order`
  ADD PRIMARY KEY (`ID_ORDER_POOL`);

--
-- Indexes for table `tb_project_list`
--
ALTER TABLE `tb_project_list`
  ADD PRIMARY KEY (`ID_LSN`);

--
-- Indexes for table `tb_reqcat`
--
ALTER TABLE `tb_reqcat`
  ADD PRIMARY KEY (`ID_REQ_CAT`);

--
-- Indexes for table `tb_tat`
--
ALTER TABLE `tb_tat`
  ADD PRIMARY KEY (`NO_ACCOUNT`);

--
-- Indexes for table `tr_pooling`
--
ALTER TABLE `tr_pooling`
  ADD PRIMARY KEY (`NO_ACCOUNT`);

--
-- Indexes for table `tr_pooling_order`
--
ALTER TABLE `tr_pooling_order`
  ADD PRIMARY KEY (`ID_ORDER_POOL`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID_USER`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`ID_USER_GROUP`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `ID_CUSTOMER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dt_request_status_order_log`
--
ALTER TABLE `dt_request_status_order_log`
  MODIFY `ID_REQUEST_STATUS_ORDER_LOG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `dt_request_status_progress_log`
--
ALTER TABLE `dt_request_status_progress_log`
  MODIFY `ID_REQUEST_STATUS_PROGRESS_LOG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `ms_customer`
--
ALTER TABLE `ms_customer`
  MODIFY `ID_CUSTOMER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_part_number`
--
ALTER TABLE `ms_part_number`
  MODIFY `ID_PN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ms_request_category`
--
ALTER TABLE `ms_request_category`
  MODIFY `ID_REQ_CAT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `progress_status`
--
ALTER TABLE `progress_status`
  MODIFY `ID_PROGRESS_STATUS` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `status_order`
--
ALTER TABLE `status_order`
  MODIFY `ID_STATUS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tb_customer`
--
ALTER TABLE `tb_customer`
  MODIFY `ID_CUSTOMER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_part_number`
--
ALTER TABLE `tb_part_number`
  MODIFY `ID_PN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_pool_order`
--
ALTER TABLE `tb_pool_order`
  MODIFY `ID_ORDER_POOL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_reqcat`
--
ALTER TABLE `tb_reqcat`
  MODIFY `ID_REQ_CAT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_tat`
--
ALTER TABLE `tb_tat`
  MODIFY `NO_ACCOUNT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tr_pooling`
--
ALTER TABLE `tr_pooling`
  MODIFY `NO_ACCOUNT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tr_pooling_order`
--
ALTER TABLE `tr_pooling_order`
  MODIFY `ID_ORDER_POOL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `ID_USER_GROUP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
