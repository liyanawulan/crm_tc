<?php

class Project_list extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('dashboard_model', '', TRUE);
        //$this->load->model('notification_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "4") {
            redirect('index.php/login/logout', 'refresh');
        }
        $this->data["open_menu"] = true;
    }

    function overview() {
        $data["session"]          = $this->session->userdata('logged_in');
        $data['content']          = 'customerslandinggear/project_list/overview';
        $data['overview']         = true;
        $this->load->view('template_landinggear', $data);
    }
    function profit_analysis() {
        $data["session"]          = $this->session->userdata('logged_in');
        $data['content']          = 'customerslandinggear/project_list/profit_analisyst/';
        $data['profit_analysis']  = true;
        $this->load->view('template_landinggear', $data);
    }
    function lo_report() {
        $data["session"]          = $this->session->userdata('logged_in');
        $data['content']          = 'customerslandinggear/project_list/lo_report';
        $data['eo_report']        = true;
        $this->load->view('template_landinggear', $data);
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */

