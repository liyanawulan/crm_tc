<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {
  
  public function __construct(){
    parent::__construct();
    
    $this->load->model('order_model'); // Load order_model ke controller ini
  }
  
  public function index(){
    $data['tb_order'] = $this->order_model->view();
    $this->load->view('tb_order/index', $data);
  }
  
  public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->order_model->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->order_model->save(); // Panggil fungsi save() yang ada di order_model.php
        redirect('tb_order');
      }
    }
    
    $this->load->view('tb_order/form_tambah');
  }
  
  public function ubah($order){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->order_model->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->order_model->edit($order); // Panggil fungsi edit() yang ada di order_model.php
        redirect('tb_order');
      }
    }
    
    $data['tb_order'] = $this->order_model->view_by($nis);
    $this->load->view('tb_order/form_ubah', $data);
  }
  
  public function hapus($order){
    $this->order_model->delete($nis); // Panggil fungsi delete() yang ada di order_model.php
    redirect('tb_order');
  }
}