<?php defined('BASEPATH') OR exit('No direct script access allowed');

class loan_control extends CI_Controller {

    // Construct

    function __construct() {
        parent::__construct();      
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('services/m_loan/loan_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('login/logout', 'refresh');
        }
        $this->data["open_menu"] = TRUE;
    }    

    // ./Construct

    function loan_index() {
        $data["session"]         = $this->session->userdata('logged_in');
        //$data['status_loan']    = $this->loan_model->tampil_data();
        $data['content']         = 'administrator/services/loan/loan';
        $data['loan_index']  = TRUE;
        $this->load->view('template',$data);
    }


    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'LOAN / EXCHANGE',
        'ldl1'              => '#',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'LOAN / EXCHANGE',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'administrator/services/pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data



    // View

    function index() 
    {
        redirect('administrator/Pooling_control/dashboard');
    }

    function dashboard() 
    {
        $this->data['title']                        = 'Dashboard';
        $this->data['icon']                         = 'fa fa-dashboard';
        $this->data['content']                      = 'administrator/services/pooling/dashboard';
        $this->load->view('template',$this->data);
    }

    function create_order() 
    {
        $this->data['part_number_list']             = $this->pooling_model->get_part_number_list();
        $this->data['requirement_category_list']    = $this->pooling_model->get_requirement_category_list();
        $this->data['aircraft_registration_list']   = $this->pooling_model->get_aircraft_registration_list();
        $this->data['delivery_point_list']          = $this->pooling_model->get_delivery_point_list();

        $this->data['title']                        = 'Create Order';
        $this->data['icon']                         = 'fa fa-edit';
        $this->data['content']                      = 'administrator/services/pooling/create_order';
        $this->load->view('template', $this->data);
    }

    function list_order() 
    {
        $page=$this->input->get('per_page');
        $batas=10;                          //jlh data yang ditampilkan per halaman
        if(!$page):                         //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page;                 // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;
        
        // print_r($batas);
        // echo '<br>';
        // print_r($offset);
        // echo '<br>';
        // echo '<pre>';
        // print_r($this->pooling_model->select_request($batas,$offset));
        // die;
        
        $config['page_query_string'] = TRUE;                                                            
        //mengaktifkan pengambilan method get pada url default
        $config['base_url']          = base_url().'index.php/administrator/Pooling_control/list_order?';       
        //url yang muncul ketika tombol pada paging diklik
        $config['total_rows']        = $this->pooling_model->count_request();                
        // jlh total barang
        $config['per_page']          = $batas;                                                                   
        //batas sesuai dengan variabel batas
        $config['uri_segment']       = $page;                                                                
         //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
 
        $config['full_tag_open']     = '<ul class="pagination" style="margin-top:0px;">';
        $config['full_tag_close']    = '</ul>';
        $config['first_link']        = '&laquo; First';
        $config['first_tag_open']    = '<li class="prev page">';
        $config['first_tag_close']   = '</li>';
 
        $config['last_link']         = 'Last &raquo;';
        $config['last_tag_open']     = '<li class="next page">';
        $config['last_tag_close']    = '</li>';
 
        $config['next_link']         = 'Next &rarr;';
        $config['next_tag_open']     = '<li class="next page">';
        $config['next_tag_close']    = '</li>';
 
        $config['prev_link']         = '&larr; Prev';
        $config['prev_tag_open']     = '<li class="prev page">';
        $config['prev_tag_close']    = '</li>';
 
        $config['cur_tag_open']      = '<li class="current"><a href="">';
        $config['cur_tag_close']     = '</a></li>';
 
        $config['num_tag_open']      = '<li class="page">';
        $config['num_tag_close']     = '</li>';
        $this->pagination->initialize($config);
        $this->data['paging']=$this->pagination->create_links();
        if($page=='') 
        {
            $this->data['jlhpage']=0;
        }
        else
        {
            $this->data['jlhpage']=$page;
        }
        $this->data['total_rows']                   = $this->pooling_model->count_request();

        $this->data['part_number_list']             = $this->pooling_model->get_part_number_list();
        $this->data['requirement_category_list']    = $this->pooling_model->get_requirement_category_list();
        $this->data['aircraft_registration_list']   = $this->pooling_model->get_aircraft_registration_list();
        $this->data['delivery_point_list']          = $this->pooling_model->get_delivery_point_list();
        
        $this->data['data_table']                   = $this->pooling_model->select_request($batas,$offset);

        $this->data['title']                        = 'List Order';
        $this->data['icon']                         = 'fa fa-list';
        $this->data['content']                      = 'administrator/services/pooling/list_order';
        $this->load->view('template', $this->data);
    }

    function update_order($NO_ACCOUNT) 
    {
        $this->data['ldl2']                         = '<?php echo base_url(); ?>index.php/administrator/Pooling_control/list_order';
        $this->data['ldt2']                         = 'List Order';
        $this->data['ldi2']                         = 'fa fa-list';

        $this->data['part_number_list']             = $this->pooling_model->get_part_number_list();
        $this->data['requirement_category_list']    = $this->pooling_model->get_requirement_category_list();
        $this->data['aircraft_registration_list']   = $this->pooling_model->get_aircraft_registration_list();
        $this->data['delivery_point_list']          = $this->pooling_model->get_delivery_point_list();

        $this->data['order']                        = $this->pooling_model->select_request_where_no_account($NO_ACCOUNT);
        // echo '<pre>';
        // print_r($this->pooling_model->select_request_where_no_account($NO_ACCOUNT));
        // die;

        $this->data['title']                        = 'Update Order';
        $this->data['icon']                         = 'fa fa-edit';
        $this->data['content']                      = 'administrator/services/pooling/update_order';
        $this->load->view('template', $this->data);
    }

        // Report

        function report_serviceable_level() 
        {
            $this->data['ldl2']                         = '#';
            $this->data['ldt2']                         = 'Report';
            $this->data['ldi2']                         = 'fa fa-bar-chart';

            // $this->data['data_table']                   = $this->pooling_model->select_data_pooling();
            
            $this->data['title']                        = 'Report Serviceable Level';
            $this->data['icon']                         = 'fa fa-bar-chart';
            $this->data['content']                      = 'administrator/services/pooling/report_serviceable_level';
            $this->load->view('template', $this->data);
        }

        function report_unserviceable_level() 
        {
            $this->data['ldl2']         = '#';
            $this->data['ldt2']         = 'Report';
            $this->data['ldi2']         = 'fa fa-bar-chart';

            // $this->data['data_table']                   = $this->pooling_model->select_data_pooling();
            
            $this->data['title']                        = 'Report Unerviceable Level';
            $this->data['icon']                         = 'fa fa-bar-chart';
            $this->data['content']                      = 'administrator/services/pooling/report_unserviceable_level';
            $this->load->view('template', $this->data);
        }

        // ./Report

        // Configuration

        function part_number() 
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'administrator/services/pooling/part_number';
            $this->load->view('template',$this->data);
        }

        function contract() 
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'administrator/services/pooling/contract';
            $this->load->view('template',$this->data);
        }

        function requirement_category() 
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'administrator/services/pooling/requirement_category';
            $this->load->view('template',$this->data);
        }

        function user() 
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'administrator/services/pooling/user';
            $this->load->view('template',$this->data);
        }

        function customer() 
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'administrator/services/pooling/customer';
            $this->load->view('template',$this->data);
        }

        // ./Configuration

    // ./ View



    // Get Data

    public function get_part_number_description()
    {
        $ID_PN                          = $this->input->post('ID_PN');                                   //mengambil post data yang dijabarkan pada javascript yaitu type: "POST" 
        $part_number_description        = $this->pooling_model->get_part_number_description($ID_PN);                      //mengambil data dari database melalui model mcombox 
        echo json_encode($part_number_description); // Encode to json data
    }

    public function get_delivery_point_address()
    {
        $ID_DP                          = $this->input->post('ID_DP');                                   //mengambil post data yang dijabarkan pada javascript yaitu type: "POST" 
        $delivery_point_address         = $this->pooling_model->get_delivery_point_address($ID_DP);                      //mengambil data dari database melalui model mcombox 
        echo json_encode($delivery_point_address); // Encode to json data
    }

    // ./ Get Data



    // Actions

    public function insert_create_order() 
    {
        if(isset($_POST['insert'])) 
        {
            $getcounter                                 = $this->pooling_model->get_counter();
            $referencenumber                            = date("Y").'-'.sprintf("%03d",$getcounter);
            // print_r($referencenumber);
            // die;

            $query_insert_pooling                       = $this->pooling_model->insert_pooling($referencenumber);
            if($query_insert_pooling)
            {
                $query_insert_pooling_order             = $this->pooling_model->insert_pooling_order($referencenumber);
                redirect('administrator/Pooling_control');
            }
        }
        else
        {
            redirect('administrator/Pooling_control/create_order');
        }
    }

    // ./Actions
}
?>
