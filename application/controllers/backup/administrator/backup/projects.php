<?php
defined('BASEPATH') OR exit ('No direct script access allowed') ;

class Projects extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('services/', '', TRUE);
         /*$this->load->model('tickets_model', '', TRUE);*/
        //$this->load->model('notification_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('login/logout', 'refresh');
        }
    }

    function index() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/services/landinggear/index';
        $data['sevices/landinggear/projects'] = TRUE;
        
        $this->load->view('template', $data);
       
    }

    function project_tabs() {
       
        $data['content'] = 'administrator/services/landinggear/project_tabs';
        $data['projects'] = true;
          $data['total_jc_open'] = $this->dashboard_model->getTotalJCOpen();
        $data['total_mdr_open'] = $this->dashboard_model->getTotalMDROpen();
        
        $data['total_jc_hangar'] = $this->dashboard_model->getTotalJChangar();
        $data['total_mdr_hangar'] = $this->dashboard_model->getTotalMDRhangar();
        
        $data['total_jc_wsss'] = $this->dashboard_model->getTotalJCwsss();
        $data['total_mdr_wsss'] = $this->dashboard_model->getTotalMDRwsss();
        
        $data['total_jc_wsse'] = $this->dashboard_model->getTotalJCwsse();
        $data['total_mdr_wsse'] = $this->dashboard_model->getTotalMDRwsse();
        
        $data['total_jc_wscb'] = $this->dashboard_model->getTotalJCwscb();
        $data['total_mdr_wscb'] = $this->dashboard_model->getTotalMDRwscb();
        
        $data['total_jc_wsls'] = $this->dashboard_model->getTotalJCwsls();
        $data['total_mdr_wsls'] = $this->dashboard_model->getTotalMDRwsls();
        
        $data['total_jc_close'] = $this->dashboard_model->getTotalJCclose();
        $data['total_mdr_close'] = $this->dashboard_model->getTotalMDRclose();
        $total_close = $this->dashboard_model->getTotalClose();
        $total_open = $this->dashboard_model->getTotalOpen();
        $total_progress = $this->dashboard_model->getTotalProgress();
        $data['total_jc_progress'] = $this->dashboard_model->getTotalJCProgress();
        $data['total_mdr_progress'] = $this->dashboard_model->getTotalMDRProgress();
        $total_jc = $this->dashboard_model->getTotaljc();
        $total_mdr = $this->dashboard_model->getTotalMDR();
        
        
        //$total_open = $this->dashboard_model->getTotalOpen();
        $data['percentage_jc_close'] = round(($data['total_jc_close'] / $total_close) * 100);
        $data['percentage_mdr_close'] = round(($data['total_mdr_close'] / $total_close) * 100);
        
        $data['percentage_mdr_open'] = round(($data['total_mdr_open'] / $total_open) * 100);
        $data['percentage_jc_open'] = round(($data['total_jc_open'] / $total_open) * 100);
        
        $data['percentage_mdr_progress'] = round(($data['total_mdr_progress'] / $total_progress) * 100);
        $data['percentage_jc_progress'] = round(($data['total_jc_progress'] / $total_progress) * 100);
        
        $data['percentage_jc_open_pie'] = round(($data['total_jc_open'] / $total_jc) * 100);
        $data['percentage_jc_progress_pie'] = round(($data['total_jc_progress'] / $total_jc) * 100);
        $data['percentage_jc_close_pie'] = round(($data['total_jc_close'] / $total_jc) * 100);
        
        $data['percentage_mdr_open_pie'] = round(($data['total_mdr_open'] / $total_mdr) * 100);
        $data['percentage_mdr_progress_pie'] = round(($data['total_mdr_progress'] / $total_mdr) * 100);
        $data['percentage_mdr_close_pie'] = round(($data['total_mdr_close'] / $total_mdr) * 100);
//for ticket
         $data["listticket"] = $this->dashboard_model->getListticket();

         //for production_planning
         $data["listProject"] = $this->dashboard_model->getListDataProject();
        $data["getPlant"] = $this->dashboard_model->getIDPlant();
        $data["getProject"] = $this->dashboard_model->getIDProject();
        $this->load->view('template', $data);
    }
     function modal_daily_day() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/projects/modal_daily_day';
       
        $this->load->view('administrator/projects/modal_daily_day', $data);
    }
     function save_modaldaily_day() {
        $area = $_POST['area'];
        $task_progress = $_POST['task_progress'];
        $followup_result = $_POST['followup_result'];
        $remarks = $_POST['remarks'];
    

        
        $back = base_url() . "administrator/services/landinggear/project_tabs";
        
            for ($i = 0; $i < count($area); $i++) {
                $post_data = array(
                   
                    'create_date_daily' => date('Y-m-d H:i:s'),
                    'area' => $area[$i],
                    'task_progress' => $task_progress[$i],
                    'followup_result' => $followup_result[$i],
                    'remarks' => $remarks[$i],
                    'update_date_daily' => "",
                    'createdBy_daily' => $this->data["session"]["username"],
                    'updateBy_daily' => "",
                    'id_project' => ""
                );
                if ($area[$i] != "" || $task_progress[$i] != "" || $followup_result[$i] != "" || $remarks[$i] != "")
                $this->db->insert('daily_day', $post_data);
            }
            echo "
                        <script type='text/javascript'>
                        alert('Sucess');
                        window.location='$back';
                        </script>
            ";
        
    }


 function save_modal_tickets() {
        $subject = $_POST['subject'];
        $department = $_POST['department'];
        $service = $_POST['service'];
        $priority = $_POST['priority'];
         $status = $_POST['status'];
    

        
        $back = base_url() . "administrator/services/landinggear/project_tabs";
        
            for ($i = 0; $i < count($subject); $i++) {
                $post_data = array(
                   
                    'create_date_ticket' => date('Y-m-d H:i:s'),
                    'subject' => $subject[$i],
                    'department' => $department[$i],
                    'project' => "Wings Pesawat",
                    'service' => $service[$i],
                    'priority' => $priority[$i],
                    'status' => $status[$i],
                    'last_replay' => date('Y-m-d H:i:s')
                    
                );
                if ($subject[$i] != "" || $department[$i] != "" || $service[$i] != "" || $priority[$i] != "" || $status[$i] != "")
                $this->db->insert('ticket', $post_data);
            }
            echo "
                        <script type='text/javascript'>
                        alert('Sucess');
                        window.location='$back';
                        </script>
            ";
        
    }
    function modal_tickets() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/projects/modal_tickets';
       
        $this->load->view('administrator/projects/modal_tickets', $data);
    }



    //for production_planning
   function modal_planning($revision) {
        $data["revision"] = $revision;
        $data["cekPlanning"] = $this->dashboard_model->cekPlanning($revision);
        $getProdPlanning = $this->dashboard_model->getProdPlanning($revision);
        if ($getProdPlanning) {
            $data["getArea"] = $getProdPlanning;
        } else {
            $data["getArea"] = $this->dashboard_model->getListArea();
        }
        $this->load->view('administrator/projects/modal_planning', $data);
    }

    function save_planning() {
        $revision = $_POST['revision'];
        $area = $_POST['area'];
        $phase = $_POST['phase'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $udpate = array(
            'STATUS_PLANNING' => "1"
        );
        $back = base_url() . "administrator/services/landinggear/project_tabs";
        if ($this->db->update('order_list', $udpate, "REVISION = '$revision'")) {
            for ($i = 0; $i < count($area); $i++) {
                $post_data = array(
                    'REVISION' => $revision,
                    'AREA' => $_POST['area'][$i],
                    'PHASE' => $_POST['phase'][$i],
                    'START_DATE' => $start_date[$i],
                    'END_DATE' => $end_date[$i]
                );
                $this->db->insert('project_planning', $post_data);
            }
            echo "
                        <script type='text/javascript'>
                        alert('Sucess');
                        window.location='$back';
                        </script>
            ";
        }
    }

    function update_planning() {
        $revision = $_POST['revision'];
        $id_project = $_POST['id'];
        $area = $_POST['area'];
        $phase = $_POST['phase'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $updateArray = array();

        for ($x = 0; $x < count($area); $x++) {

            $updateArray[] = array(
                'ID_PROJECT' => $id_project[$x],
                'START_DATE' => $start_date[$x],
                'END_DATE' => $end_date[$x]
            );
        }
        $this->db->update_batch('project_planning', $updateArray, "ID_PROJECT");
        $back = base_url() . "administrator/projects/project_tabs";
        echo "
                        <script type='text/javascript'>
                        alert('Sucess');
                        window.location='$back';
                        </script>
            ";
    }



	// public function tampil() {
	// 	$data['dataCustomers'] = $this->customer->select_all_customer();
	// 	$this->load->view('administrator/Customers/list_data', $data);
	// }
}