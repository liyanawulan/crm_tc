<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Order_control extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('dashboard_model', '', TRUE);
        //$this->load->model('notification_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('login/logout', 'refresh');
        }
        $this->data["open_menu"] = TRUE; 
   }

    function status() {
        $data["session"]    = $this->session->userdata('logged_in');
        $data['content']    = 'administrator/order/status/index';
        $data['status']     = TRUE;
        $this->load->view('template', $data);
    }
    function preorder() {
        $data["session"]    = $this->session->userdata('logged_in');
        $data['content']    = 'administrator/order/preorder/index';
        $data['preorder']   = TRUE;
        $this->load->view('template', $data);
    }
    function finance() {
        $data["session"]    = $this->session->userdata('logged_in');
        $data['content']    = 'administrator/order/finance/index';
        $data['finance']    = TRUE;
        $this->load->view('template', $data);
    }
    function fullotoritas() {
        $data["session"]    = $this->session->userdata('logged_in');
        $data['content']    = 'administrator/order/fullotoritas/index';
        $data['full']       = TRUE;
        $this->load->view('template', $data);
    }
}