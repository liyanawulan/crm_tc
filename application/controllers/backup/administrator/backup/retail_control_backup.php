<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Retail_control_backup extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('services/retail_model_backup', '', TRUE);
        $this->load->library('pagination');
        // $this->load->model('notification_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('index.php/login/logout', 'refresh');
        }
        $this->data["open_menu"] = TRUE; 
    }

    function index() 
    {
        $this->data['session']        				= $this->session->userdata('logged_in');

        $this->data['title']                  		= 'Project List Retail';
        $this->data['content']        				= 'administrator/services/retail/index';
        $this->data['listProject']    				= $this->retail_model_backup->getlistProject();
        $this->load->view('template', $this->data);
    }

    function project_list() 
    {
        $this->data['session']        				= $this->session->userdata('logged_in');

        $this->data['title']                  		= 'Project List Retail';
        $this->data['content']        				= 'administrator/services/retail/index';
        $this->data['listProject']    				= $this->retail_model_backup->getlistProject();
        $this->load->view('template', $this->data);
    }

    function overview($ID_LSN) 
    {               
        $data['total_waiting_approved']  = $this->retail_model_backup->getTotalWaitingApproved();
        $data['total_waiting_material']  = $this->retail_model_backup->getTotalWaitingMaterial();
        $data['total_under_maintenance'] = $this->retail_model_backup->getTotalUnderMaintenance();
        $data['total_waiting_repair']    = $this->retail_model_backup->getTotalWaitingRepair();
        $data['total_finished']          = $this->retail_model_backup->getTotalFinished();

        $total_wa    = $this->retail_model_backup->getTotalWA();
        $total_wm    = $this->retail_model_backup->getTotalWM();
        $total_um    = $this->retail_model_backup->getTotalUM();
        $total_wr    = $this->retail_model_backup->getTotalWR();
        $total_fin   = $this->retail_model_backup->getTotalFIN();
        
        $data['percentage_waiting_approved']     = round(($data['total_waiting_approved']  / 
                                                        ($total_wa + $total_wm + $total_um + $total_wr + $total_fin))  * 100);
        $data['percentage_waiting_material']     = round(($data['total_waiting_material']  / 
                                                        ($total_wa + $total_wm + $total_um + $total_wr + $total_fin))  * 100);
        $data['percentage_under_maintenance']    = round(($data['total_under_maintenance'] / 
                                                        ($total_wa + $total_wm + $total_um + $total_wr + $total_fin))  * 100);
        $data['percentage_waiting_repair']       = round(($data['total_waiting_repair']    / 
                                                        ($total_wa + $total_wm + $total_um + $total_wr + $total_fin))  * 100);
        $data['percentage_finished']             = round(($data['total_finished']  / 
                                                        ($total_wa + $total_wm + $total_um + $total_wr + $total_fin))  * 100);

        $data['session']    = $this->session->userdata('logged_in');
        $data['ID_LSN']    	= $ID_LSN;

        $data['title']    	= 'Overview';
        $data['content']    = 'administrator/services/retail/overview'; //view home  
        $this->load->view('template', $data);   
    }

   	function list_order($ID_LSN) 
    {
        $page=$this->input->get('per_page');
        $batas=10;                          //jlh data yang ditampilkan per halaman
        if(!$page):                         //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page;                 // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;
        
        // print_r($page);
        // die;
        
        $config['page_query_string'] = TRUE;                                                            
        //mengaktifkan pengambilan method get pada url default
        $config['base_url']          = base_url().'index.php/administrator/Retail_control/list_order/'.$ID_LSN.'?';       
        //url yang muncul ketika tombol pada paging diklik
        $config['total_rows']        = $this->retail_model_backup->count_retail();                
        // jlh total barang
        $config['per_page']          = $batas;                                                                   
        //batas sesuai dengan variabel batas
        $config['uri_segment']       = $page;                                                                
         //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
 
        $config['full_tag_open']     = '<ul class="pagination" style="margin-top:0px;">';
        $config['full_tag_close']    = '</ul>';
        $config['first_link']        = '&laquo; First';
        $config['first_tag_open']    = '<li class="prev page">';
        $config['first_tag_close']   = '</li>';
 
        $config['last_link']         = 'Last &raquo;';
        $config['last_tag_open']     = '<li class="next page">';
        $config['last_tag_close']    = '</li>';
 
        $config['next_link']         = 'Next &rarr;';
        $config['next_tag_open']     = '<li class="next page">';
        $config['next_tag_close']    = '</li>';
 
        $config['prev_link']         = '&larr; Prev';
        $config['prev_tag_open']     = '<li class="prev page">';
        $config['prev_tag_close']    = '</li>';
 
        $config['cur_tag_open']      = '<li class="current"><a href="">';
        $config['cur_tag_close']     = '</a></li>';
 
        $config['num_tag_open']      = '<li class="page">';
        $config['num_tag_close']     = '</li>';
        $this->pagination->initialize($config);
        $this->data['paging']=$this->pagination->create_links();
        if($page=='') 
        {
            $this->data['jlhpage']=0;
        }
        else
        {
            $this->data['jlhpage']=$page;
        }
        $this->data['total_rows']   		= $this->retail_model_backup->count_retail();

        $this->data['ID_LSN']    			= $ID_LSN;
        $this->data['data_table']   		= $this->retail_model_backup->select_retail($batas,$offset);

        $this->data['title']                        = 'List Order';
        $this->data['content']                      = 'administrator/services/retail/list_order';
        $this->load->view('template', $this->data);
    }

    function order_filter($ID_LSN)
    {
        $parameter_status_order = $_GET['status_order'];
        $parameter_status_paid  = $_GET['status_paid'];
        $status_order           = explode('-', $parameter_status_order);

        $total_status_order     = count($status_order);
        if($total_status_order==1) 
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = '';
            $this->data['status_order3']    = '';
            $this->data['status_order4']    = '';
            $this->data['status_order5']    = '';
        }
        elseif($total_status_order==2)
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = $status_order[1];
            $this->data['status_order3']    = '';
            $this->data['status_order4']    = '';
            $this->data['status_order5']    = '';
        }
        elseif($total_status_order==3)
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = $status_order[1];
            $this->data['status_order3']    = $status_order[2];
            $this->data['status_order4']    = '';
            $this->data['status_order5']    = '';
        }
        elseif($total_status_order==4)
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = $status_order[1];
            $this->data['status_order3']    = $status_order[2];
            $this->data['status_order4']    = $status_order[3];
            $this->data['status_order5']    = '';
        }
        elseif($total_status_order==5)
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = $status_order[1];
            $this->data['status_order3']    = $status_order[2];
            $this->data['status_order4']    = $status_order[3];
            $this->data['status_order5']    = $status_order[4];
        }
        $this->data['status_order']         = $status_order;
        $this->data['status_paid']          = $parameter_status_paid;




        $page=$this->input->get('per_page');
        $batas=10;                          //jlh data yang ditampilkan per halaman
        if(!$page):                         //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page;                 // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;
        
        // print_r($page);
        // die;
        
        $config['page_query_string'] = TRUE;                                                            
        //mengaktifkan pengambilan method get pada url default
        $config['base_url']          = base_url().'index.php/administrator/Retail_control/order_filter?status_order='.$parameter_status_order.'&status_paid='.$parameter_status_paid.'';
        //url yang muncul ketika tombol pada paging diklik
        $config['total_rows']        = $this->retail_model_backup->count_retail_where_filter($status_order, $parameter_status_paid);                
        // jlh total barang
        $config['per_page']          = $batas;                                                                   
        //batas sesuai dengan variabel batas
        $config['uri_segment']       = $page;                                                                
         //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
 
        $config['full_tag_open']     = '<ul class="pagination" style="margin-top:0px;">';
        $config['full_tag_close']    = '</ul>';
        $config['first_link']        = '&laquo; First';
        $config['first_tag_open']    = '<li class="prev page">';
        $config['first_tag_close']   = '</li>';
 
        $config['last_link']         = 'Last &raquo;';
        $config['last_tag_open']     = '<li class="next page">';
        $config['last_tag_close']    = '</li>';
 
        $config['next_link']         = 'Next &rarr;';
        $config['next_tag_open']     = '<li class="next page">';
        $config['next_tag_close']    = '</li>';
 
        $config['prev_link']         = '&larr; Prev';
        $config['prev_tag_open']     = '<li class="prev page">';
        $config['prev_tag_close']    = '</li>';
 
        $config['cur_tag_open']      = '<li class="current"><a href="">';
        $config['cur_tag_close']     = '</a></li>';
 
        $config['num_tag_open']      = '<li class="page">';
        $config['num_tag_close']     = '</li>';
        $this->pagination->initialize($config);
        $this->data['paging']=$this->pagination->create_links();
        if($page=='') 
        {
            $this->data['jlhpage']=0;
        }
        else
        {
            $this->data['jlhpage']=$page;
        }
        $this->data['total_rows']   				= $this->retail_model_backup->count_retail_where_filter($status_order, $parameter_status_paid);

        $this->data['ID_LSN']    					= $ID_LSN;
        $this->data['data_table']   				= $this->retail_model_backup->select_retail_where_filter($status_order, $parameter_status_paid, $batas,$offset);

        $this->data['title']                        = 'Order Filter';
        $this->data['content']                      = 'administrator/services/retail/order_filter';
        $this->load->view('template', $this->data);
    }

    function order_status($ID_LSN)
    {
        $parameter_status_order = $_GET['status_order'];
        // $parameter_status_paid  = $_GET['status_paid'];
        $parameter_status_paid  = '';
        $status_order           = explode('-', $parameter_status_order);

        $total_status_order     = count($status_order);
        if($total_status_order==1) 
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = '';
            $this->data['status_order3']    = '';
            $this->data['status_order4']    = '';
            $this->data['status_order5']    = '';
        }
        elseif($total_status_order==2)
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = $status_order[1];
            $this->data['status_order3']    = '';
            $this->data['status_order4']    = '';
            $this->data['status_order5']    = '';
        }
        elseif($total_status_order==3)
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = $status_order[1];
            $this->data['status_order3']    = $status_order[2];
            $this->data['status_order4']    = '';
            $this->data['status_order5']    = '';
        }
        elseif($total_status_order==4)
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = $status_order[1];
            $this->data['status_order3']    = $status_order[2];
            $this->data['status_order4']    = $status_order[3];
            $this->data['status_order5']    = '';
        }
        elseif($total_status_order==5)
        {
            $this->data['status_order1']    = $status_order[0];
            $this->data['status_order2']    = $status_order[1];
            $this->data['status_order3']    = $status_order[2];
            $this->data['status_order4']    = $status_order[3];
            $this->data['status_order5']    = $status_order[4];
        }
        $this->data['status_order']         = $status_order;
        $this->data['status_paid']          = $parameter_status_paid;




        $page=$this->input->get('per_page');
        $batas=10;                          //jlh data yang ditampilkan per halaman
        if(!$page):                         //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page;                 // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;
        
        // print_r($page);
        // die;
        
        $config['page_query_string'] = TRUE;                                                            
        //mengaktifkan pengambilan method get pada url default
        $config['base_url']          = base_url().'index.php/administrator/Retail_control/order_status?status_order='.$parameter_status_order.'&status_paid='.$parameter_status_paid.'';
        //url yang muncul ketika tombol pada paging diklik
        $config['total_rows']        = $this->retail_model_backup->count_retail_where_filter($status_order, $parameter_status_paid);                
        // jlh total barang
        $config['per_page']          = $batas;                                                                   
        //batas sesuai dengan variabel batas
        $config['uri_segment']       = $page;                                                                
         //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
 
        $config['full_tag_open']     = '<ul class="pagination" style="margin-top:0px;">';
        $config['full_tag_close']    = '</ul>';
        $config['first_link']        = '&laquo; First';
        $config['first_tag_open']    = '<li class="prev page">';
        $config['first_tag_close']   = '</li>';
 
        $config['last_link']         = 'Last &raquo;';
        $config['last_tag_open']     = '<li class="next page">';
        $config['last_tag_close']    = '</li>';
 
        $config['next_link']         = 'Next &rarr;';
        $config['next_tag_open']     = '<li class="next page">';
        $config['next_tag_close']    = '</li>';
 
        $config['prev_link']         = '&larr; Prev';
        $config['prev_tag_open']     = '<li class="prev page">';
        $config['prev_tag_close']    = '</li>';
 
        $config['cur_tag_open']      = '<li class="current"><a href="">';
        $config['cur_tag_close']     = '</a></li>';
 
        $config['num_tag_open']      = '<li class="page">';
        $config['num_tag_close']     = '</li>';
        $this->pagination->initialize($config);
        $this->data['paging']=$this->pagination->create_links();
        if($page=='') 
        {
            $this->data['jlhpage']=0;
        }
        else
        {
            $this->data['jlhpage']=$page;
        }
        $this->data['total_rows']   = $this->retail_model_backup->count_retail_where_filter($status_order, $parameter_status_paid);

        $this->data['ID_LSN']    					= $ID_LSN;
        $this->data['data_table']   				= $this->retail_model_backup->select_retail_where_filter($status_order, $parameter_status_paid, $batas,$offset);

        $this->data['title']                        = 'Order Status';
        $this->data['content']                      = 'administrator/services/retail/order_status';
        $this->load->view('template', $this->data);
    }

    public function view($id_pelamar)
    {
        $data = $this->MSeleksi->get_seleksi_where_id_pelamar($id_pelamar);
        echo json_encode($data);
    }

    public function view_log($id_pelamar)
    {
        $data = $this->MSeleksi->get_seleksi_log_where_id_pelamar($id_pelamar);
        echo json_encode($data);
    }

    public function order_detail($ID_LSN,$id_status)
    {
        $this->data['ID_LSN']    					= $ID_LSN;
        $this->data['data_order_detail'] 			= $this->retail_model_backup->get_retail_where_id($id_status);

        $this->data['title']                        = 'Order Detail';
        $this->data['content']                      = 'administrator/services/retail/order_detail';
        $this->load->view('template', $this->data);
        // print_r($data);
        // die;
        // echo json_encode($data);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */