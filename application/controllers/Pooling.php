<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pooling extends CI_Controller{

    // Construct

	function __construct() {
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Pooling_model', '', TRUE);
	}

    // ./Construct

		public function check_session(){
			print_r($this->session->userdata('log_sess_id_user_role'));

		}

    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'Pooling',
        'ldl1'              => 'index.php/Pooling',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'POOLING',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data



    // View

    function index()
    {
        redirect('Pooling/list_order');
    }

		function dashboard()
    {
        $this->data['title']                        = 'Dashboard';
        $this->data['icon']                         = 'fa fa-dashboard';
		$this->data['content']                      = 'pooling/dashboard';
        $this->load->view('template',$this->data);
		}

    function create_order()
    {
		    $id_customer = $this->session->userdata('log_sess_id_customer');
        $this->data['contract_list']             	= $this->Pooling_model->get_contract_list($id_customer);
        // $this->data['part_number_list']             = $this->Pooling_model->get_part_number_list();
        $this->data['requirement_category_list']    = $this->Pooling_model->get_requirement_category_list();
        // $this->data['aircraft_registration_list']   = $this->Pooling_model->get_aircraft_registration_list();
        // $this->data['delivery_point_list']          = $this->Pooling_model->get_delivery_point_list();

        $this->data['title']                        = 'Create Order';
        $this->data['icon']                         = 'fa fa-edit';
        $this->data['content']                      = 'pooling/create_order';
        $this->load->view('template', $this->data);
    }

    function list_order()
    {
        $this->data['title']                        = 'List Order';
        $this->data['icon']                         = 'fa fa-list';
        $this->data['content']                      = 'pooling/list_order';
        $this->load->view('template', $this->data);
    }

    function update_order($NO_ACCOUNT)
    {
        $this->data['ldl2']                         = 'index.php/Pooling/list_order';
        $this->data['ldt2']                         = 'List Order';
        $this->data['ldi2']                         = 'fa fa-list';

        // $this->data['part_number_list']             = $this->Pooling_model->get_part_number_list();
        $this->data['requirement_category_list']    = $this->Pooling_model->get_requirement_category_list();
        // $this->data['aircraft_registration_list']   = $this->Pooling_model->get_aircraft_registration_list();
        // $this->data['delivery_point_list']          = $this->Pooling_model->get_delivery_point_list();

        $this->data['order']                        = $this->Pooling_model->select_request_where_no_account($NO_ACCOUNT);
        $order = $this->data['order'];
        // print_r($order);
        // die;
        // echo '<pre>';
        // print_r($this->Pooling_model->select_request_where_no_account($NO_ACCOUNT));
        // die;

				$section_role['main'] = [
					'SERVICE_LEVEL' => '',
					'ID_PART_NUMBER' => '',
					'COMPONENT_DESCRIPTION' => '',
					'ID_REQ_CAT' => '',
					'REQUESTED_DELIVERY_DATE' => '',
					'MAINTENANCE' => '',
					'REASON_OF_REQUESTED_SERVICE_LEVEL' => '',
					'ATA_CHAPTER' => '',
					'RELEASE_TYPE' => '',
					'AIRCRAFT_REGISTRATION' => '',
					'3_DIGIT_AIRPORT_CODE' => '',
					'DESTINATION_ADDRESS' => '',
					'CUSTOMER_PO' => '',
					'REMARKS_OF_CUSTOMER' => '',
					'THY_REMARKS' => '',
					'3_DIGIT_AIRPORT_CODE' => '',
				];
				$section_role['serviceable'] = [
                    'SERVICE_SHIPMENT_TYPE' => '',
					'ID_PART_NUMBER' => '',
					'COMPONENT_DESCRIPTION_V' => '',
					'AWB_NO' => '',
					'FLIGHT_NO' => '',
					'EST_DELIVERY_DATE' => '',
					'AIR_DEST' => '',
					'ATTENTION_EMAIL' => '',
				];
				$section_role['unserviceable'] = [
					'SEND_TYPE' => '',
					'SHIPMENT_TYPE' => '',
					'PART_NUMBER' => '',
					'SERIAL_NUMBER' => '',
					'REMOVAL_DATE' => '',
					'AC_REGISTRATION' => '',
					'REMOVAL_REASON' => '',
                    'ATTENTION_EMAIL1' => '',
                    'AWN_NO' => '',
                    'FLIGHT_NO1' => '',
                    'EST_DELIVERY_DATE1' => '',
                    'REMARKS' => '',
                    'REMOVAL_REASON_TYPE' => '',
                    // 'REMOVAL_REASON2' => '',
                    // 'REMOVAL_REASON3' => '',
				];

				$this->data['main'] = 'disabled';
				$this->data['serviceable'] = 'disabled';
				$this->data['unserviceable'] = 'disabled';

				$this->data['main_btn'] = 'disabled';
				$this->data['serviceable_btn'] = 'disabled';
				$this->data['unserviceable_btn'] = 'disabled';
        $this->data['serviceable_btn_update'] = 'disabled';
        $this->data['unserviceable_btn_update'] = 'disabled';

          if ($order->STATUS_ORDER != 1 and $order->STATUS_ORDER != 4) {
    				if ($this->session->userdata('log_sess_id_user_role')==1) {
    					// code...
    					$this->data['main'] = 'disabled';
                        // $this->data['serviceable'] = '';
                        $this->data['unserviceable'] = 'disabled';
                         if ( $order->REVISI_SERVICE == 0){
                           $this->data['serviceable'] = '';
                           $this->data['serviceable_btn'] = '';
                        } else {
                           $this->data['serviceable'] = 'disabled'; 
                           $this->data['serviceable_btn'] = 'disabled';
                        }

                        $this->data['main_btn'] = 'disabled';
                        // $this->data['serviceable_btn'] = '';
                        $this->data['serviceable_btn_update'] = '';
                        $this->data['unserviceable_btn'] = 'disabled';
                        $this->data['unserviceable_btn_update'] = 'disabled';

    				}else if ($this->session->userdata('log_sess_id_user_role')==2) {
    					// code...
    					$this->data['main'] = 'disabled';
    					// $this->data['serviceable'] = '';
    					$this->data['unserviceable'] = 'disabled';
                         if ( $order->REVISI_SERVICE == 0){
                           $this->data['serviceable'] = '';
                           $this->data['serviceable_btn'] = '';
                        } else {
                           $this->data['serviceable'] = 'disabled'; 
                           $this->data['serviceable_btn'] = 'disabled';
                        }

    					$this->data['main_btn'] = 'disabled';
    					// $this->data['serviceable_btn'] = '';
                        $this->data['serviceable_btn_update'] = '';
    					$this->data['unserviceable_btn'] = 'disabled';
                        $this->data['unserviceable_btn_update'] = 'disabled';

    				}else if ($this->session->userdata('log_sess_id_user_role')==3) {
    					// code...
    					$this->data['main'] = 'disabled';
    					$this->data['serviceable'] = 'disabled';
                        if ( $order->REVISI_UNSERVICE == 0){
    					   $this->data['unserviceable'] = '';
                           $this->data['unserviceable_btn'] = '';
                        } else {
                           $this->data['unserviceable'] = 'disabled'; 
                           $this->data['unserviceable_btn'] = 'disabled';
                        }

    					$this->data['main_btn'] = 'disabled';
    					$this->data['serviceable_btn'] = 'disabled';
    					
                        $this->data['serviceable_btn_update'] = 'disabled';
                        $this->data['unserviceable_btn_update'] = '';
    				}
                }

        $this->data['title']                        = 'Update Order';
        $this->data['icon']                         = 'fa fa-edit';
        $this->data['content']                      = 'pooling/update_order';
        $this->load->view('template', $this->data);
    }

      // Report

      function report_serviceable_level()
      {
          $this->data['title']                        = 'Report Serviceable Level';
          $this->data['icon']                         = 'fa fa-bar-chart';
          $this->data['content']                      = 'pooling/report_serviceable_level';
          $this->load->view('template', $this->data);
      }

      function report_unserviceable_level()
      {
          $this->data['title']                        = 'Report Unserviceable Level';
          $this->data['icon']                         = 'fa fa-bar-chart';
          $this->data['content']                      = 'pooling/report_unserviceable_level';
          $this->load->view('template', $this->data);
      }

        // ./Report

        // Configuration

        function part_number()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/part_number';
            $this->load->view('template',$this->data);
        }

        function contract()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/contract';
            $this->load->view('template',$this->data);
        }

        function requirement_category()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/requirement_category';
            $this->load->view('template',$this->data);
        }

        function user()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/user';
            $this->load->view('template',$this->data);
        }

        function customer()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/customer';
            $this->load->view('template',$this->data);
        }

        // ./Configuration

    // ./ View



    // Get Data

    public function get_part_number_description()
    {
        $ID_PN                          = $this->input->post('ID_PN');                                   //mengambil post data yang dijabarkan pada javascript yaitu type: "POST"
        $part_number_description        = $this->Pooling_model->get_part_number_description($ID_PN);                      //mengambil data dari database melalui model mcombox
        echo json_encode($part_number_description); // Encode to json data
    }
    public function get_part_number_contract($id)
    {                             //mengambil post data yang dijabarkan pada javascript yaitu type: "POST"
        $ID_PN       = $this->input->post('ID_PN');  
        $data        = $this->Pooling_model->get_part_number_contract_description($ID_PN);
                            //mengambil data dari database melalui model mcombox
        echo json_encode($data); // Encode to json data
    }
    public function get_fleet_list($id_c=null)
    {
        $data        = $this->Pooling_model->get_fleet_list_by_contract($id_c);                      //mengambil data dari database melalui model mcombox
        echo json_encode($data); // Encode to json data
    }
    public function get_part_number_list($id_c=null)
    {
        $data        = $this->Pooling_model->get_pn_list_by_contract($id_c);   
        // print_r($data)         ;          //mengambil data dari database melalui model mcombox
        echo json_encode($data); // Encode to json data
    }

    public function get_delivery_point_address()
    {
        $ID_DP                          = $this->input->post('ID_DP');                                   //mengambil post data yang dijabarkan pada javascript yaitu type: "POST"
        $delivery_point_address         = $this->Pooling_model->get_delivery_point_address($ID_DP);                      //mengambil data dari database melalui model mcombox
        echo json_encode($delivery_point_address); // Encode to json data
    }

     public function get_delivery_point($id_c)
    {
        $data        = $this->Pooling_model->get_delivery_point($id_c);  
        // echo $this->db->last_query(); 
        // print_r($data)         ;          //mengambil data dari database melalui model mcombox
        echo json_encode($data);
    }

    // ./ Get Data



    // Actions

    public function insert_create_order()
    {
        // if(isset($_POST['insert']))
        // {
            $getcounter                                 = $this->Pooling_model->get_counter();
            $referencenumber                            = date("Y").'-'.sprintf("%03d",$getcounter);
            $insert = array(
              'REFERENCE'               => $referencenumber,
              // 'ID_USER_POOL'       => $this->session->userdata(''),
              'ID_CUSTOMER'             => $this->session->userdata('log_sess_id_customer'),
              'SERVICE_LEVEL'           => $this->input->post('SERVICE_LEVEL'),
              'ID_PART_NUMBER'          => $this->input->post('ID_PART_NUMBER'),
              'DESCRIPTION'             => $this->input->post('COMPONENT_DESCRIPTION'),
              'ID_REQUIREMENT_CATEGORY' => $this->input->post('ID_REQUIREMENT_CATEGORY'),
              'REQUESTED_DELIVERY_DATE' => $this->input->post('REQUESTED_DELIVERY_DATE'),
              'MAINTENANCE'             => $this->input->post('MAINTENANCE'),
              'ATA_CHAPTER'             => $this->input->post('ATA_CHAPTER_V'),
              'ID_AIRCRAFT_REGISTRATION'=> $this->input->post('ID_AIRCRAFT_REGISTRATION'),
              'RELEASE_TYPE'            => $this->input->post('RELEASE_TYPE'),
              'ID_DELIVERY_POINT'       => $this->input->post('ID_DELIVERY_POINT'),
              'DESTINATION_ADDRESS'     => $this->input->post('DESTINATION_ADDRESS_V'),
              'CUSTOMER_PO'             => $this->input->post('CUSTOMER_PO'),
              // 'CUSTOMER_RO'        => $this->input->post('CUSTOMER_RO'),
              'REMARKS_OF_CUSTOMER'     => $this->input->post('REMARKS_OF_CUSTOMER'),
              'EXISTING_EMAIL'          => $this->input->post('EXISTING_EMAIL'),
              'EMAIL1'                  => $this->input->post('EMAIL1'),
              'EMAIL2'                  => $this->input->post('EMAIL2'),
              'EMAIL3'                  => $this->input->post('EMAIL3'),
              'REQUEST_ORDER_DATE'      => date('Y-m-d H:i:s'),
              'ORDER_REQUESTED_BY'      => $this->session->userdata('log_sess_name'),
              'STATUS_ORDER'            => 1,
              'SERVICE_STATUS'          => "Waiting Delivery",
              'UNSERVICE_STATUS'        => "Waiting Delivery",
              'REVISI_SERVICE'          => 0,
              'REVISI_UNSERVICE'        => 0,
            );
           
            $query_insert_pooling_order       = $this->Pooling_model->insert_pooling_order($insert);
           if ($query_insert_pooling_order) {
               $data = array(
                  "msg" => 'Thanks! Your Order has been created!',
                  "id" => $query_insert_pooling_order  
                  );    
            }
            else {
                $data = array(
                        "msg" => 'Operation Failed'
                    ); 
              }
            echo json_encode($data);
    }

    public function update_status_order()
    {   $id = $this->input->post('ID');
        $status = $this->input->post('status');
        if ($id != '') 
        {   if ($status == 4) {
            $data = array(
                'STATUS_ORDER' => $status,
                'CANCELLED_BY' => $this->session->userdata('log_sess_name'),
                'CANCELLED_ON' => date('Y-m-d H:i:s'),
                'CHANGED_ON' => date('Y-m-d H:i:s'),
                'CHANGED_BY' => $this->session->userdata('log_sess_name')
            );
          } else if ($status == 2) {
            $data = array(
                'STATUS_ORDER' => $status,
                'ORDER_CONFIRMED_BY' => $this->session->userdata('log_sess_name'),
                'ORDER_CONFIRMED_DATE' => date('Y-m-d H:i:s'),
                'CHANGED_ON' => date('Y-m-d H:i:s'),
                'CHANGED_BY' => $this->session->userdata('log_sess_name')
            );
          } else if ($status == 3) {
            $data = array(
                'STATUS_ORDER' => $status,
                'ORDER_COMPLETE_BY' => $this->session->userdata('log_sess_name'),
                'ORDER_COMPLETE_DATE' => date('Y-m-d H:i:s'),
                'CHANGED_ON' => date('Y-m-d H:i:s'),
                'CHANGED_BY' => $this->session->userdata('log_sess_name')
            );

          }
            $query_update_order             = $this->Pooling_model->update_status($id, $data);
            if ($query_update_order) {
                if ($status == 2){
                     $data = array(
                        "msg" => 'Good Job! This Order has been confirmed!'
                    );  
                }
                 else if ($status == 3){
                     $data = array(
                        "msg" => 'Good Job! This Order has been completed!'
                    );  
                }
                else if ($status == 4){
                     $data = array(
                        "msg" => ' This Order has been cancelled!'
                    );  
                }

            }
            else {
                $data = array(
                        "msg" => 'Operation Failed'
                    ); 
            }
        echo json_encode($data);
        }
    }

    public function submit_unserviceable()
    {   $id = $this->input->post('id');
        if ($id != '') 
        {
            $data = array(
                'UNSERVICE_SEND_TYPE' => $this->input->post('SEND_TYPE'),
                'UNSERVICE_SHIPMENT_TYPE' => $this->input->post('SHIPMENT_TYPE'),
                'UNSERVICE_PART_NUMBER' => $this->input->post('PART_NUMBER'),
                'UNSERVICE_SERNUM' => $this->input->post('SERIAL_NUMBER'),
                'UNSERVICE_REMOVE_DATE' => $this->input->post('REMOVAL_DATE'),
                'UNSERVICE_AC_REG' => $this->input->post('AC_REGISTRATION'),
                'UNSERVICE_REMOVAL_REASON' => $this->input->post('REMOVAL_REASON'),
                'UNSERVICE_EMAIL' => $this->input->post('ATTENTION_EMAIL1'),
                'UNSERVICE_AWB_NO' => $this->input->post('AWN_NO'),
                'UNSERVICE_FLIGHT_NO' => $this->input->post('FLIGHT_NO1'),
                'UNSERVICE_EDT' => $this->input->post('EST_DELIVERY_DATE1'),
                'UNSERVICE_REMARKS' => $this->input->post('REMARKS'),
                'UNSERVICE_REMOVAL_REASON_TYPE' => $this->input->post('REMOVAL_REASON_TYPE'),
                'REVISI_UNSERVICE' => (int)$this->input->post('revisi_unserv') + 1,
                'UNSERVICE_SUBMITTER' => $this->session->userdata('log_sess_name'),
                'CHANGED_UNSERVICE_ON' => date('Y-m-d H:i:s'),
                'CHANGED_UNSERVICE_BY' => $this->session->userdata('log_sess_name'),
                'UNSERVICE_STATUS' => 'Delivered'
            );
            // if ((int)$this->input->post('revisi_unserv') == 0 ){
            //   array_push($data,"blue","yellow");
            // }
            
            $query_update_unserviceable = $this->Pooling_model->update_unserviceable($id, $data);
        }
        // else
        // {
        //     redirect('Pooling');
        // }
    }

    public function submit_serviceable()
    {   $id = $this->input->post('id');
        if ($id != '') 
        {   
            $data = array(
                'SERVICE_SHIPMENT_TYPE' => $this->input->post('SERVICE_SHIPMENT_TYPE'),
                'SERVICE_PART_NUMBER' => $this->input->post('ID_PART_NUMBER'),
                'SERVICE_MFR_SERNUM' => $this->input->post('SERVICE_MFR_SERNUM'),
                'SERVICE_AWB_NO' => $this->input->post('AWB_NO'),
                'SERVICE_FLIGHT_NO' => $this->input->post('FLIGHT_NO'),
                'SERVICE_EDT' => $this->input->post('EST_DELIVERY_DATE'),
                'SERVICE_DESTINATION' => $this->input->post('AIR_DEST'),
                'SERVICE_EMAIL' => $this->input->post('ATTENTION_EMAIL'),
                'REVISI_SERVICE' => (int)$this->input->post('revisi_serv') + 1,
                'SERVICE_SUBMITTER' => $this->session->userdata('log_sess_name'),
                'CHANGED_SERVICE_ON' => date('Y-m-d H:i:s'),
                'CHANGED_SERVICE_BY' => $this->session->userdata('log_sess_name'),
                'SERVICE_STATUS' => 'Delivered'
            );
            $query_update_serviceable = $this->Pooling_model->update_serviceable($id, $data);
            // redirect('Pooling/update_order/'+$id);
            // redirect('Pooling');
            // header("Location: $current_url");
        }
        // else
        // {
        //     redirect('Pooling');
        // }
    }
    // ./Actions
}