<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Retail extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->model('Retail_model', '', TRUE);
        $this->load->library('pagination');
    }

    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'Retail',
        'ldl1'              => 'index.php/Retail/dashboard',
        'ldi1'              => 'fa fa-tags',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'nav_tabs'          => 'retail/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data

    function index()
    {
        redirect('Retail/dashboard');
    }

    function dashboard() {
        $this->data["session"]    = $this->session->userdata('logged_in');

        $listStatus = $this->Retail_model->getStatusOrder();

        // $total = 0;
        // $str_labels = '[';
        // $str_data = '[';

        // $provider = '[';

        $tmp1 = [];
        //$tmp2 = [];
        $tmp11 = [];
        $tmp22 = [];
        foreach ($listStatus as $key => $value) {
            $tmp1[] = $value;
          }

        $chart = [0,0,0, 0];
        $result = $this->Retail_model->select_retail();
        $txt = 50;
        $tmp = [];
        $n_result = []; 
          if (isset($result)) {
            foreach ($result as $key => $value) {
                $tmp = $value;
                // if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
                //     $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                //     $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));


                //     $start = date_create($date1);
                //     $end = date_create($date2);

                // }else{
                //     $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                //     $date2 = date('Ymd');


                //     $start = date_create($date1);
                //     $end = date_create($date2);

                // }
                if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                    $teco_date = $tmp['TECO_DATE'];
                    $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
                    $e = date_create(date('Y'));
                 
                    // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
                    $diff = (int) date_diff($s, $e)->format("%a");
                    // echo $diff;echo " diff<br>";
                    if ( $diff > 5) {
                        $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
                        // print_r($newdate);echo " newdate <br>";
                        $date2 = date('Ymd', strtotime($newdate));
                    }else{
                      
                      $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
                    }

                    // print_r($date1);echo "<br>";
                    // print_r($date2);

                    $start = date_create($date1);
                    $end = date_create($date2);

                }else{
                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                    $date2 = date('Ymd');

                    $start = date_create($date1);
                    $end = date_create($date2);

                }

                $tmp['RECEIVED_DATE'] = date('d-m-Y', strtotime($tmp['RECEIVED_DATE']));
                
                // $tmp['STATUS'] = '';
                $tmp['DELIVERY_DATE'] = '';
                $tmp['REMARKS'] = '';
                $tmp['ID_RETAIL'] = '';
                $tmp['QUOTATION_DATE'] = '';
                $tmp['APPROVAL_DATE'] = '';
                $day_waiting = 0;

                for ($i=1; $i <= $txt ; $i++) { 
                    if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
                      break;

                    } 
                    // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
                    if (strpos($value["TXT".$i], 'SROA') !== false) {

                        $tmp['QUOTATION_DATE'] = date('d-m-Y', strtotime($value["UDATE".$i]));
                        $nexti = $i+1;
                        for ($ii=$i+1; $ii <= $txt ; $ii++)
                            if ((strpos($value["TXT".$ii], 'WR') !== false) or (strpos($value["TXT".$ii], 'SROA') !== false)){
                               
                                $tmp['APPROVAL_DATE'] = date('d-m-Y', strtotime($value["UDATE".$ii]));
                                $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
                            break 1;  
                            }
                        // $tmp[][$i] = $value["UDATE".$i];
                 
                    }
                }
                if ($tmp['LFGSA'] != 'C') {
                  if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
                    $tmp22[] = $value;
                  }else if ((strpos($tmp['STAT'], 'WR') !== false)){
                    //or (strpos($tmp['STAT'], 'UR') !== false)){
                    $tmp11[] = $value;
                  }
                }
                $tat = (date_diff($start, $end)->format("%a")) - $day_waiting;
                $chart[3]++;
                if ($tat<10) {
                    $chart[2]++;
                }else if ($tat>9 && $tat<21) {
                    $chart[1]++;
                }else if ($tat>20) {
                    $chart[0]++;
                }
            }
            // print_r($chart);
            // die;
            $sum_wr1 = count($tmp11);
            $sum_wr2 = count($tmp22);
            array_push($tmp1, ((object) array('STATUS'=> 'Approval Received / Repair Started', 'JUMLAH' => $sum_wr2)),((object) array('STATUS'=> 'Repair Started', 'JUMLAH' => $sum_wr1)) );
        
        }
        $this->data['listStatus'] = $tmp1;
        $total = 0 ;
        $provider = json_encode($tmp1);
        foreach ($tmp1 as $key => $value) {
          $total += $value->JUMLAH;
        }
        
        //listStatus =========================================

        // $this->data['chart_label']     = $str_labels;
        // $this->data['chart_data']     = $str_data;

        $this->data['provider']     = $provider;

        // $this->data['grand_total']     = $total;
        $this->data['sum_grand_total'] = $total;
        $this->data['chart'] = $chart;
        $this->data['title']            = 'Dashboard';
        $this->data['icon']             = 'fa fa-dashboard';
        $this->data['content']          = 'retail/dashboard'; //view dashboard
        $this->load->view('template', $this->data);
    }

    function finance()
    {
        $this->data['session']          = $this->session->userdata('logged_in');
        $this->data['listOutstanding']   = $this->Retail_model->getlistOutstanding();
        $category = [0,0,0];
        foreach ($this->data['listOutstanding'] as $key => $value) {
            if ($value->CATEGORY == 1)
                { $category[($value->CATEGORY-1)] += ($value->NETWR); }
            else if ($value->CATEGORY == 2)
                { $category[($value->CATEGORY-1)] += ($value->NETWR); }
            else if ($value->CATEGORY == 3)
                { $category[($value->CATEGORY-1)] += ($value->NETWR); }
            }
        $ii = 0; 
        $provider = "[
                    {'aging': '0-30 Days',
                     'total': $category[0]
                    },
                    {'aging': '>30 Days',
                     'total': $category[1]
                    },
                    {'aging': '>60 Days',
                     'total': $category[2]
                    }]"; 
          
        $this->data['category'] = $provider;

        $this->data['title']            = 'Finance';
        $this->data['icon']             = 'fa fa-money';
        $this->data['content']          = 'retail/finance';
        $this->load->view('template', $this->data);
    }

    function finance_historical($start1, $end1){

      $filename = "HISTORICAL TRANSACTION [{$start1} - {$end1}]";

      $start = date('Ymd', strtotime($start1));
      $end = date('Ymd', strtotime($end1));

      $this->load->library('Libexcel');

      $excel = new PHPExcel();

      // Settingan awal fil excel
      $excel->getProperties()->setCreator('CRM-TC')
                   ->setLastModifiedBy('CRM-TC')
                   ->setTitle("RETAIL - HISTORICAL TRANSACTION")
                   ->setSubject("HISTORICAL TRANSACTION");
                   // ->setDescription("Laporan Semua Data Siswa")
                   // ->setKeywords("Data Siswa");
      // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
      $style_col = array(
        'font' => array('bold' => true), // Set font nya jadi bold
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
      $style_row = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      ); 
      $style_row_right = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      $excel->setActiveSheetIndex(0)->setCellValue('A1', "HISTORICAL TRANSACTION [{$start1} - {$end1}]"); // Set kolom A1 dengan tulisan "DATA SISWA"
      $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

      // Buat header tabel nya pada baris ke 3
      $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
      $excel->setActiveSheetIndex(0)->setCellValue('B3', "Billing Document"); // Set kolom B3 dengan tulisan "NIS"
      $excel->setActiveSheetIndex(0)->setCellValue('C3', "Document Date"); // Set kolom C3 dengan tulisan "NAMA"
      $excel->setActiveSheetIndex(0)->setCellValue('D3', "Amount"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
      $excel->setActiveSheetIndex(0)->setCellValue('E3', "Currency"); // Set kolom E3 dengan tulisan "ALAMAT"

      // Apply style header yang telah kita buat tadi ke masing-masing kolom header
      $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
      // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya

      // Set width kolom
      $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
      $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
      $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
      $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
      $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15); // Set width kolom E


      $result = $this->Retail_model->get_list_historical($start, $end);
      $index = 1;
      $start_row = 4;
      $row = $start_row ;
      foreach ($result as $key => $value) {

          $excel->setActiveSheetIndex(0)->setCellValue('A'.$row, $index); 
          $excel->setActiveSheetIndex(0)->setCellValue('B'.$row, $value['BILLING']); 
          $excel->setActiveSheetIndex(0)->setCellValue('C'.$row, $value['DOC_DATE']); 
          $excel->setActiveSheetIndex(0)->setCellValue('D'.$row, preg_replace('/\s+/', '', $value['AMOUNT'])); 
          $excel->setActiveSheetIndex(0)->setCellValue('E'.$row, $value['CURRENCY']); 

          $excel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($style_row_right);
          $excel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($style_row);

          $row++;
          $index++;

      }

      // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
      $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
      // Set orientasi kertas jadi LANDSCAPE
      $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
      // Set judul file excel nya
      // $excel->getActiveSheet(0)->setTitle("");
      $excel->setActiveSheetIndex(0);
      // Proses file excel

      // We'll be outputting an excel file
      header('Content-type: application/vnd.ms-excel');
    

      // It will be called file.xls
      header('Content-Disposition: attachment; filename="'.$filename.'.xls"');

      $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
      $write->save('php://output');
    }  

    function finance_invoice_outstanding($par){

      if ($par=='1') $range = "< 30";
      else if ($par=='2') $range = ">= 30";
      else if ($par=='3') $range = "> 60";
        

      $this->load->library('Libexcel');

      $excel = new PHPExcel();

      // Settingan awal fil excel
      $excel->getProperties()->setCreator('CRM-TC')
                   ->setLastModifiedBy('CRM-TC')
                   ->setTitle("RETAIL - INVOICE OUTSTANDING")
                   ->setSubject("INVOICE OUTSTANDING");
                   // ->setDescription("Laporan Semua Data Siswa")
                   // ->setKeywords("Data Siswa");
      // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
      $style_col = array(
        'font' => array('bold' => true), // Set font nya jadi bold
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
      $style_row = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      ); 
      $style_row_right = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      $excel->setActiveSheetIndex(0)->setCellValue('A1', "INVOICE OUTSTANDIG"); // Set kolom A1 dengan tulisan "DATA SISWA"
      $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

      // Buat header tabel nya pada baris ke 3
      $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
      $excel->setActiveSheetIndex(0)->setCellValue('B3', "Billing Document"); // Set kolom B3 dengan tulisan "NIS"
      $excel->setActiveSheetIndex(0)->setCellValue('C3', "Document Date"); // Set kolom C3 dengan tulisan "NAMA"
      $excel->setActiveSheetIndex(0)->setCellValue('D3', "Amount"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
      $excel->setActiveSheetIndex(0)->setCellValue('E3', "Currency"); // Set kolom E3 dengan tulisan "ALAMAT"

      // Apply style header yang telah kita buat tadi ke masing-masing kolom header
      $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
      // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya

      // Set width kolom
      $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
      $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
      $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
      $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
      $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15); // Set width kolom E


      $result = $this->Retail_model->get_list_invoice_outstanding($par);
      $index = 1;
      $start_row = 4;
      $row = $start_row ;
      foreach ($result as $key => $value) {

          $excel->setActiveSheetIndex(0)->setCellValue('A'.$row, $index); 
          $excel->setActiveSheetIndex(0)->setCellValue('B'.$row, $value['BILLING']); 
          $excel->setActiveSheetIndex(0)->setCellValue('C'.$row, $value['DOC_DATE']); 
          $excel->setActiveSheetIndex(0)->setCellValue('D'.$row, preg_replace('/\s+/', '', $value['AMOUNT'])); 
          $excel->setActiveSheetIndex(0)->setCellValue('E'.$row, $value['CURRENCY']); 

          $excel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($style_row_right);
          $excel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($style_row);

          $row++;
          $index++;

      }

      // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
      $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
      // Set orientasi kertas jadi LANDSCAPE
      $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
      // Set judul file excel nya
      // $excel->getActiveSheet(0)->setTitle("");
      $excel->setActiveSheetIndex(0);
      // Proses file excel

      // We'll be outputting an excel file
      header('Content-type: application/vnd.ms-excel');
      $filename = 'INVOICE OUTSTANDING '.$range;

      // It will be called file.xls
      header('Content-Disposition: attachment; filename="'.$filename.'.xls"');

      $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
      $write->save('php://output');
    }

    function list_order()
    {
        $page=$this->input->get('per_page');
        $batas=10;
        if(!$page):
           $offset = 0;
        else:
           $offset = $page;
        endif;

        $tmp1 = [];
        $tmp11 = [];
        $tmp22 = [];
  
        $listStatus = $this->Retail_model->getStatusOrder();
        foreach ($listStatus as $key => $value) {
          if ($value->JUMLAH != 0){
            $tmp1[] = $value;
          }
        } 
        
        // foreach ($listStatus as $key => $value) {

        //   //listStatus ======================================

        //   if ($value->STATUS == 'Unit Under Inspection') {
        //     $tmp2[] = $value;
        //   }else{
        //     $tmp1[] = $value;
        //   }


        // }
 
        //listStatus =========================================
        // $sum = array_sum(array_map(function($item) { return $item->JUMLAH; }, $tmp2));
        // array_push($tmp1, ((object) array('STATUS'=> 'Unit Under Inspection', 'JUMLAH' => $sum)) );
        
        // $this->data['listStatus'] = $tmp1;


        $this->data['ldt2']             = 'Dashboard';
        $this->data['ldl2']             = 'index.php/Retail/dashboard';
        $this->data['ldi2']             = 'fa fa-dashboard';

        $result = $this->Retail_model->select_retail();
        $txt = 50;
        $tmp = [];
        $n_result = []; 
          if (isset($result)) {
            foreach ($result as $key => $value) {
                $tmp = $value;
                // $status_invoice = $this->Retail_model->getStatusInvoice($tmp['SALES_ORDER']);
                // if (empty($status_invoice))
                // {
                //   $tmp['BILLING_STATUS'] = 'UNPAID';
                // }
                // else {
                //   foreach ($status_invoice as $key => $value2) {
                //     if ((strpos($value2['FKSAA'], 'A') !== false) or (strpos($value2['FKSAA'], 'B') !== false))
                //     {
                //       $tmp['BILLING_STATUS'] = 'UNPAID';
                //     }
                //     else {
                //       $tmp['BILLING_STATUS'] = 'PAID';
                //     }
                //     break 1;
                //   }
                // }
                // print_r($status_invoice);
                // die;

                if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                    $teco_date = $tmp['TECO_DATE'];
                    $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
                    $e = date_create(date('Y'));
                 
                    // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
                    $diff = (int) date_diff($s, $e)->format("%a");
                    // echo $diff;echo " diff<br>";
                    if ( $diff > 5) {
                        $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
                        // print_r($newdate);echo " newdate <br>";
                        $date2 = date('Ymd', strtotime($newdate));
                    }else{
                      
                      $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
                    }

                    // print_r($date1);echo "<br>";
                    // print_r($date2);

                    $start = date_create($date1);
                    $end = date_create($date2);

                }else{
                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                    $date2 = date('Ymd');

                    $start = date_create($date1);
                    $end = date_create($date2);

                }
                // if ($tmp['DELIVERY_DATE'] != '00000000') { 
                //   $tmp['TXT_STAT'] = 'Delivered';
                // }
                // else if (strpos($tmp['STAT'], 'FRA') !== false) {
                //   $tmp['TXT_STAT'] = 'Ready To Deliver Return As is Conditiion';
                // }
                // else if (strpos($tmp['STAT'], 'FSB') !== false) {
                //   $tmp['TXT_STAT'] = 'Ready To Deliver Serviceable Condition';
                // }
                // else if (strpos($tmp['STAT'], 'F') !== false) {
                //   $tmp['TXT_STAT'] = 'Ready To Deliver BER' ;
                // }
                // else if (strpos($tmp['STAT'], 'WR') !== false) { 
                //   $tmp['TXT_STAT'] = 'Approval Received / Repair Started';
                // }
                // else if (strpos($tmp['STAT'], 'UR') !== false){
                //   $tmp['TXT_STAT'] = 'Repair Started';
                // }
                // else if (strpos($tmp['STAT'], 'SRMC') !== false) {
                //   $tmp['TXT_STAT'] = 'Waiting Supply Materials From Customer';
                // }
                // else if (strpos($tmp['STAT'], 'SRAM') !== false) {
                //   $tmp['TXT_STAT'] = 'Waiting Materials';
                // }
                // else if (strpos($tmp['STAT'], 'SROA') !== false) {
                //  $tmp['TXT_STAT'] = 'Quotation Provided / Awaiting Approval';
                // }
                // else if (strpos($tmp['STAT'], 'SR') !== false){
                //   $tmp['TXT_STAT'] = 'Repair Started' ;
                // }
                // else if ((strpos($tmp['STAT'], 'CRTD') !== false) OR (strpos($tmp['STAT'], 'RELD'))) {
                //   $tmp['TXT_STAT'] = 'Unit Under Inspection';
                // }
                // else if (strpos($tmp['STAT'], 'HOLD') !== false) {
                //   $tmp['TXT_STAT'] = 'Release Hold';
                // }
                // else if (strpos($tmp['STAT'], 'HSHP') !== false) {
                //   $tmp['TXT_STAT'] = 'Hold Shipment';
                // }
                // else { $tmp['TXT_STAT'] =  'Unit Under Inspection' ;
                // }       
              

                $tmp['RECEIVED_DATE'] = date('d-M-Y', strtotime($tmp['RECEIVED_DATE']));
                
                // $tmp['STATUS'] = '';
                if (($tmp['LFDAT']!='00000000') and (($tmp['LFDAT'])!='')) {
                  $tmp['DELIVERY_DATE'] = date('d-M-Y', strtotime($tmp['LFDAT']));
                } else {
                  $tmp['DELIVERY_DATE'] = '';
                }
                $tmp['REMARKS'] = '';
                $tmp['ID_RETAIL'] = '';
                $tmp['QUOTATION_DATE'] = '';
                $tmp['APPROVAL_DATE'] = '';
                $day_waiting = 0;

                for ($i=1; $i <= $txt ; $i++) { 
                  if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
                      break;
                    } 
                  // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
                  if (strpos($value["TXT".$i], 'SROA') !== false) {

                      $tmp['QUOTATION_DATE'] = date('d-M-Y', strtotime($value["UDATE".$i]));
                      $nexti = $i+1;
                      for ($ii=$i+1; $ii <= $txt ; $ii++)
                          if ((strpos($value["TXT".$ii], 'WR') !== false) OR (strpos($value["TXT".$ii], 'SROA') !== false)){
                             
                              $tmp['APPROVAL_DATE'] = date('d-M-Y', strtotime($value["UDATE".$ii]));
                              $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
                          break 1;  
                          }
                      // $tmp[][$i] = $value["UDATE".$i];
               
                  }
                }
              if ($tmp['LFGSA'] != 'C') {
                if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
                  $tmp22[]= $value;
                  $tmp['TXT_STAT'] = 'Approval Received / Repair Started';
                }else if ((strpos($tmp['STAT'], 'WR') !== false)){
                // or (strpos($tmp['STAT'], 'UR') !== false)){
                  $tmp11[] = $value;
                  $tmp['TXT_STAT'] = 'Repair Started';
                }
              }
              $tmp['TAT'] = (date_diff($start, $end)->format("%a"))-$day_waiting ;
              // print_r($tmp['TAT']);
              // echo "===================================================================<br>";
               // echo $tmp['QUOTATION_DATE'];
               //  echo $tmp['APPROVAL_DATE'];
               //  echo "QUOTATION <br>";
              $n_result[] = $tmp;

            }
            // die;
        }
        $sum_wr1 = count($tmp11);
        if ($sum_wr1 != 0){
          array_push($tmp1, ((object) array('STATUS'=> 'Repair Started', 'JUMLAH' => $sum_wr1)) );
        }
        $sum_wr2 = count($tmp22);
        if ($sum_wr2 != 0)
        array_push($tmp1, ((object) array('STATUS'=> 'Approval Received / Repair Started', 'JUMLAH' => $sum_wr2)) );

        $this->data['listStatus']       = $tmp1;        
        $this->data['data_table']       = $n_result;
        $this->data['title']            = 'List Order';
        $this->data['icon']             = 'fa fa-list';
        $this->data['content']          = 'retail/list_order';
        $this->load->view('template', $this->data);
    }

    function order_status()
    {
        $status                     = $_GET['status'];
        $this->data['ldt2']         = 'Dashboard';
        $this->data['ldl2']         = 'index.php/Retail/dashboard';
        $this->data['ldi2']         = 'fa fa-dashboard';

        $this->data['status']       = $status;
        $this->data['title']        = 'Order Status';
        $this->data['icon']         = 'fa fa-flag';
        $this->data['content']      = 'retail/order_status';
        $this->load->view('template', $this->data);
    }

    function order_detail($ID_RETAIL)
    {
        $this->data['ldt2']         = 'Dashboard';
        $this->data['ldl2']         = 'index.php/Retail/dashboard';
        $this->data['ldi2']         = 'fa fa-dashboard';
        $this->data['ldt3']         = 'List Order';
        $this->data['ldl3']         = 'index.php/Retail/list_order';
        $this->data['ldi3']         = 'fa fa-list';

        $this->data['data_order_details']    = $this->Retail_model->get_retail_where_id($ID_RETAIL);

        $this->data['title']                = 'Order Detail';
        $this->data['icon']                 = 'fa fa-file-text';
        $this->data['content']              = 'retail/order_detail';
        $this->load->view('template', $this->data);
        // echo '<pre>';
        // print_r($this->Retail_model->get_retail_where_id($ID_RETAIL));
        // die;
    }

    function order_detail2()
    {   
        $order = $this->input->get('order');
        $status = $this->input->get('status');
        $this->data['ldt2']         = 'Dashboard';
        $this->data['ldl2']         = 'index.php/Retail/dashboard';
        $this->data['ldi2']         = 'fa fa-dashboard';
        $this->data['ldt3']         = 'Order';
        $this->data['ldl3']         = 'index.php/Retail/order_status?status='.$status;
        $this->data['ldi3']         = 'fa fa-list';

        $this->data['status']       = $status; 
        $this->data['data_order_details']    = $this->Retail_model->get_retail_where_id($order);

        $this->data['title']                = 'Order Detail';
        $this->data['icon']                 = 'fa fa-file-text';
        $this->data['content']              = 'retail/order_detail';
        $this->load->view('template', $this->data);
        // echo '<pre>';
        // print_r($this->Retail_model->get_retail_where_id($ID_RETAIL));
        // die;
    }

    
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
