<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_exchange extends CI_Controller {

    // Construct

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user'))) 
        {
            redirect('Login');
        }

        $this->load->database();
        $this->load->model('Loan_exchange_model', '', TRUE);
        // $this->load->model('Pooling_model', '', TRUE);
        $this->load->helper('url');
        $this->load->library('pagination');
    }    

    // ./Construct



    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'Loan Exchange',
        'ldl1'              => 'index.php/Loan_exchange',
        'ldi1'              => 'fa fa-usd',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'nav_tabs'          => 'loan-exchange/nav_tabs',
        'nav_tabs2'         => 'loan-exchange/nav_tabs2',
        'link_directory'    => 'layout/link-directory'
    );

    // fa fa-refresh fa-spin fa-fw
    // ./Parsing Public Data

    function se_mysession(){
        echo "string";
        echo $this->session->userdata('log_sess_id_customer');
        
    }

    // View

    function index() {
        redirect('Loan_exchange/exchange_order');
    }

    function loan_order() {
        // $this->data['listOrder']        = $this->Loan_exchange_model->getListDataOrderDT($s_result);

        // $this->data['listOrder']        = $temp;
        $this->data['title']            = 'Loan Order List';
        $this->data['icon']             = 'fa fa-money';
        $this->data['content']          = 'loan-exchange/loan_order';
        $this->load->view('template',$this->data);
    }

    function set_curl($number){

        $curl = curl_init();
        // $curl_obj = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"personnel_number\"\r\n\r\n".$number."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--" ;
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://172.16.232.60/hcis-dev/public/api/v1/external/employee",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "personnel_number=".$number,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer 9dd3990778dc4ced0d668efbe8292a67",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 061d19d0-8b01-41a8-af88-96c9787fa9ee",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        // print_r($response);
        // print_r($err);
        // die;

        if ($err) {
          return array();
        } else {
          $result = json_decode($response, 1);
          return $result;
        }
    }



    function loan_request() {
        $this->data['title']            = 'Loan Request List';
        $this->data['icon']             = 'fa fa-inbox';
        $this->data['content']          = 'loan-exchange/loan_request';
        $this->load->view('template',$this->data);
    }

    function form_request_loan() {
        $this->data['title']            = 'Form Request Loan';
        $this->data['icon']             = 'fa fa-edit';
        $this->data['content']          = 'loan-exchange/form_request_loan';
        $this->load->view('template',$this->data);
    }

    function exchange_order() {
        $this->data['title']            = 'Exchange Order List';
        $this->data['icon']             = 'fa fa-exchange';
        $this->data['content']          = 'loan-exchange/exchange_order';
        $this->load->view('template',$this->data);
    }

    function exchange_request() {
        $this->data['title']            = 'Exchange Request List';
        $this->data['icon']             = 'fa fa-inbox';
        $this->data['content']          = 'loan-exchange/exchange_request';
        $this->load->view('template',$this->data);
    }

    function form_request_exchange() {
        $this->data['title']            = 'Form Request Exchange';
        $this->data['icon']             = 'fa fa-edit';
        $this->data['content']          = 'loan-exchange/form_request_exchange';
        $this->load->view('template',$this->data);
    }

    // ./ View



    // Actions

    public function insert_request_loan() 
    {
        if(isset($_POST['insert'])) 
        {
            $query_insert_loan_request                       = $this->Loan_exchange_model->insert_loan_request();
            if($query_insert_loan_request)
            {
                $this->session->set_flashdata('alert_success','Add Data Request Loan Success');
                redirect('Loan_exchange/loan_request');
            }
        }
        else
        {
            $this->session->set_flashdata('alert_failed','Add Data Request Loan Failed');
            redirect('Loan_exchange/form_request_loan');
        }
    }

    public function insert_request_exchange() 
    {
        if(isset($_POST['insert'])) 
        {
            $query_insert_exchange_request                       = $this->Loan_exchange_model->insert_exchange_request();
            if($query_insert_exchange_request)
            {
                $this->session->set_flashdata('alert_success','Add Data Request Exchange Success');
                redirect('Loan_exchange/exchange_request');
            }
        }
        else
        {
            $this->session->set_flashdata('alert_failed','Add Data Request Exchange Failed');
            redirect('Loan_exchange/form_request_exchange');
        }
    }

    // ./Actions
}

?>