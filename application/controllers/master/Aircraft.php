<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Aircraft extends CI_Controller{

    // Construct

	function __construct() {
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('/master/Aircraft_m', '', TRUE);
	}

    // ./Construct

    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'AIRCRAFT REGISTRATION',
        'ldl1'              => 'index.php/Master/Aircraft',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'MASTER',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'Pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data
    // View

    function index()
    {
        redirect('master/aircraft/datalist');
    }

		function dashboard()
    {
        $this->data['title']                        = 'Dashboard';
        $this->data['icon']                         = 'fa fa-dashboard';
				$this->data['content']                      = 'master/dashboard';
        $this->load->view('template',$this->data);
		}

    function create()
    {
      	$param = $this->input->post();
				// print_r($param);
				// die;

				if($this->Aircraft_m->insert_Aircraft($param)){
					redirect('master/aircraft/datalist');
				}else{
					redirect('master/aircraft/datalist');
				}
    }

    function datalist()
    {
        $this->data['title']                        = 'List Aircraft Registration';
        $this->data['icon']                         = 'fa fa-list';
        $this->data['content']                      = 'master/Aircraft';
        $this->load->view('template', $this->data);
    }

    function update($id)
    {
			$param = $this->input->post();
			$param['ID'] = $id;
			// print_r($param);
			// die;

			if($this->Aircraft_m->update_Aircraft($param)){
				redirect('master/aircraft/datalist');
			}else{
				redirect('master/aircraft/datalist');
			}
		}
    function delete($id)
    {
			$param = $this->input->post();
			$param['ID'] = $id;
			// print_r($param);
			// die;

			if($this->Aircraft_m->delete_Aircraft($param)){
				redirect('master/aircraft/datalist');
			}else{
				redirect('master/aircraft/datalist');
			}
    }



}
