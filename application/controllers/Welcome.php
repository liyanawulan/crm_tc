<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->model('Retail_model', '', TRUE);
        // $this->data["open_menu"] = TRUE;
    }

    function see_my_session(){

        echo('log_sess_id_user => '.        $this->session->userdata('log_sess_id_user')      ."<br>");
        echo('log_sess_name => '.           $this->session->userdata('log_sess_name')         ."<br>");
        echo('log_sess_username => '.       $this->session->userdata('log_sess_username')     ."<br>");
        echo('log_sess_password => '.       $this->session->userdata('log_sess_password')     ."<br>");
        echo('log_sess_id_user_role => '.   $this->session->userdata('log_sess_id_user_role') ."<br>");
        echo('log_sess_id_user_group => '.  $this->session->userdata('log_sess_id_user_group')."<br>");
        echo('log_sess_id_customer => '.    $this->session->userdata('log_sess_id_customer')  ."<br>");
        echo('log_sess_last_login => '.     $this->session->userdata('log_sess_last_login')   ."<br>");
        echo('log_sess_status => '.         $this->session->userdata('log_sess_status')       ."<br>");
        echo('log_sess_user_role => '.      $this->session->userdata('log_sess_user_role')    ."<br>");
        echo('log_sess_user_group => '.     $this->session->userdata('log_sess_user_group')   ."<br>");
        echo('log_sess_id_user_group => '.  $this->session->userdata('log_sess_id_user_group')."<br>");
        echo('log_sess_customer_name => '.  $this->session->userdata('log_sess_customer_name')."<br>");
    }
    function index() {
        $data['content']    = 'welcome/welcome'; //view home
        $this->load->view('template', $data);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
