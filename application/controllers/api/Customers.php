<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Customers extends CI_Controller{

    // Construct

		function __construct() {
			parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
	    $this->load->model('Customers_m', '', TRUE);
		}

	
		function index(){

			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;
			$param['draw'] = $this->input->post('draw');
			// print_r($param);
			// die;

			$res = $this->Customers_m->get_data_list($param);
			


			// $param['search']['value'] = '';
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');

			}
			// $extra_param = $this->find_search_column($this->input->post('column'));



			$result = $this->Customers_m->get_data_list($param);
			$totalRow = count($res);
			// echo $this->db->last_query();

			// $totalRow = $this->Contract_m->get_counter_tr();
			$totalFIltered = count($res);

			$n = (int)$param['start'];

			foreach ($result as $i => $v) $result[$i]['NO'] = ++$n;


			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $result;
			// $data['extra']						= $extra_param;


			echo json_encode($data);

		}

			public function datalist($search = ''){

			$param['start'] = '';
			$param['end']   = '';
			$param['draw']  = '';
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');

			}
			$res = $this->Customers_m->get_data_list($param);

			echo json_encode($res);
		}
	
		private function find_search_column($arr){

				$val = [];

				foreach ($arr as $key => $value) {
					if ($value['search']['value']) {
							array_push($val, ['colname' => $column_show[$key], 'val' => $value['search']['value']]);
					}
				}

				return $val;
		}



}
