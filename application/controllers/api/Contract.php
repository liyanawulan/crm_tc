<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Contract extends CI_Controller{

	private $column_show = ['REFERENCE', 'REQUIREMENT_CATEGORY_NAME', 'PN_NAME', 'PN_DESC', 'DELIVERY_POINT_CODE', 'AIRCRAFT_REGISTRATION', 'CUSTOMER_PO', 'CUSTOMER_RO', 'CUSTOMER_PO', 'CUSTOMER_RO', 'REQUEST_ORDER_DATE', 'REQUESTED_DELIVERY_DATE', 'STATUS_ORDER_NAME', 'STATUS_ORDER'];

	function __construct() 
	{
		parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Contract_m', '', TRUE);
	}

	function list_table()
	{
		$param['start'] = $this->input->post('start');
		$param['end'] 	= ($param['start']) + ($this->input->post('length')) + 1;
		$param['draw'] 	= $this->input->post('draw');

		$res = $this->Contract_m->get_data_list_table($param);
		//echo $this->db->last_query();
		//$param['search']['value'] = '';
		if($this->input->post('search')){
			$param['search'] = $this->input->post('search');
		}
		//$extra_param = $this->find_search_column($this->input->post('column'));
		$result 	= $this->Contract_m->get_data_list_table($param);
		$totalRow 	= count($res);
		//echo $this->db->last_query();
		//$totalRow = $this->Contract_m->get_counter_tr();
		$totalFIltered = count($res);

		$n = (int)$param['start'];
		foreach ($result as $i => $v) $result[$i]['NO'] = ++$n;

			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $result;
			//$data['extra']		  = $extra_param;

			echo json_encode($data);
	}
		
	function detail_fleet($no_contract)
	{
		$param['start'] = $this->input->post('start');
		$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;
		$param['draw'] = $this->input->post('draw');
		$param['ID_CONTRACT'] = $no_contract;

		$res = $this->Contract_m->get_detail_fleet($param);
		//echo $this->db->last_query();
		//$param['search']['value'] = '';
		if($this->input->post('search')){
			$param['search'] = $this->input->post('search');
		}
		//$extra_param = $this->find_search_column($this->input->post('column'));
		$result = $this->Contract_m->get_detail_fleet($param);
		$totalRow = count($res);
		//echo $this->db->last_query();
		//$totalRow = $this->Contract_m->get_counter_tr();
		$totalFIltered = count($res);

		$n = (int)$param['start'];
		foreach ($result as $i => $v) $result[$i]['NO'] = ++$n;

			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $result;
			//$data['extra']		  = $extra_param;

			echo json_encode($data);
	}

	function detail_part_number($no_contract)
	{
		$param['start'] 		= $this->input->post('start');
		$param['end'] 			= ($param['start']) + ($this->input->post('length')) + 1;
		$param['draw'] 			= $this->input->post('draw');
		$param['ID_CONTRACT'] 	= $no_contract;

		$res = $this->Contract_m->get_detail_part_number($param);
		//echo $this->db->last_query();
		//$param['search']['value'] = '';
		if($this->input->post('search')){
			$param['search'] = $this->input->post('search');
		}
		//$extra_param = $this->find_search_column($this->input->post('column'));
		$result = $this->Contract_m->get_detail_part_number($param);
		$totalRow = count($res);
		//echo $this->db->last_query();
		//$totalRow = $this->Contract_m->get_counter_tr();
		$totalFIltered = count($res);

		$n = (int)$param['start'];
		foreach ($result as $i => $v) $result[$i]['NO'] = ++$n;

			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $result;
			//$data['extra']		  = $extra_param;

			echo json_encode($data);
	}

	private function find_search_column($arr)
	{
		$val = [];
		foreach($arr as $key => $value){
			if ($value['search']['value']){
				array_push($val, ['colname' => $column_show[$key], 'val' => $value['search']['value']]);
			}
		}
		return $val;
	}

/*===================================================================================================*/

	function detail_pn($id_ct)
	{
		$query = $this->Contract_m->get_datatables($id_ct,'pn');
		$query = explode("FROM", $query);

		$table = explode("WHERE", $query[1]);
		$table_fix = $table[0];

		$order = explode("ORDER", $table[1]);
		$order_fix = $order[1];

		$where = explode("AND", $order[0]);
		if (count($where) == 2) {
			$where_fix = $where[0]." AND ".$where[1];
		}else{
			$where_fix = $where[0]." AND ".$where[1]." AND (".$where[2].")";
		}


		$start = $_REQUEST['start'];
		$end   = $_REQUEST['length']+$_REQUEST['start'];

		$sql = "WITH TblDatabases AS
		(
		SELECT *, ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
		WHERE $where_fix
		)
		SELECT * FROM TblDatabases WHERE Row>$start and Row<=$end";


		$list = $this->Contract_m->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $detail) {        	
            $no++;
            $row = array();
            $row['no']		= "<center>".$no."</center>";
            $row['ata']		= "<center>".$detail->ATA."</center>";
            $row['pn']		= $detail->PART_NUMBER;
            $row['desc']	= $detail->DESCRIPTION;
            $row['ess']		= "<center>".$detail->ESS."</center>";
            $row['mel']		= "<center>".$detail->MEL."</center>";
            $row['sla']		= "<center>".$detail->TARGET_SLA."</center>";
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Contract_m->count_all($id_ct,'pn'),
            "recordsFiltered" => $this->Contract_m->count_filtered($id_ct,'pn'),
            "data" => $data,
            /*"query" => $sql,
            "where" => $where_fix,*/
        ];
        //output to json format
        echo json_encode($output);
	}

	function detail_flt($id_ct)
	{
		$query = $this->Contract_m->get_datatables($id_ct,'fleet');
		$query = explode("FROM", $query);

		$table = explode("WHERE", $query[1]);
		$table_fix = $table[0];

		$order = explode("ORDER", $table[1]);
		$order_fix = $order[1];

		$where = explode("AND", $order[0]);
		if (count($where) == 2) {
			$where_fix = $where[0]." AND ".$where[1];
		}else{
			$where_fix = $where[0]." AND ".$where[1]." AND (".$where[2].")";
		}


		$start = $_REQUEST['start'];
		$end   = $_REQUEST['length']+$_REQUEST['start'];

		$sql = "WITH TblDatabases AS
		(
		SELECT *, ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
		WHERE $where_fix
		)
		SELECT * FROM TblDatabases WHERE Row>$start and Row<=$end";


		$list = $this->Contract_m->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $fleet) {        	
            $no++;
            $row = array();
            $row['no']		= "<center>".$no."</center>";
            $row['regis']	= $fleet->REGISTRATION;
            $row['type']	= $fleet->TYPE;
            $row['msn']		= $fleet->MSN;
            $row['mfg']		= $fleet->MFG_DATE;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Contract_m->count_all($id_ct,'fleet'),
            "recordsFiltered" => $this->Contract_m->count_filtered($id_ct,'fleet'),
            "data" => $data,
            /*"query" => $sql,
            "where" => $where_fix,*/
        ];
        //output to json format
        echo json_encode($output);
	}

	function detail_ct()
	{
		$query = $this->Contract_m->get_datatables(null,'ct');
		//print("<pre>".print_r($query,true)."</pre>");
		$query = explode("FROM", $query);

		$table = explode("JOIN", $query[1]);
		$table_fix = $table[0];

		$order = explode("ORDER", $table[1]);
		$order_fix = $order[1];

		$where = explode("WHERE", $order[0]);
		$where = explode("AND", $where[1]);
		if (count($where) == 2) {
			$where_fix = $where[0]." AND (".$where[1].")";
		}else{
			$where_fix = "TC_CONTRACT.IS_DELETE =  '0'";
		}


		$start = $_REQUEST['start'];
		$end   = $_REQUEST['length']+$_REQUEST['start'];

		$sql = "WITH TblDatabases AS
				(
				SELECT 
				CUSTOMER.COMPANY_NAME,
				TC_CONTRACT.CONTRACT_NUMBER,
				TC_CONTRACT.ID_CUSTOMER,
				TC_CONTRACT.DELIVERY_POINT,
				TC_CONTRACT.DELIVERY_ADDRESS,
				TC_CONTRACT.DESCRIPTION,
				TC_CONTRACT.CREATE_AT,
				TC_CONTRACT.UPDATE_AT,
				TC_CONTRACT.ID,
				TC_CONTRACT.IS_DELETE,
				ROW_NUMBER() OVER (ORDER $order_fix) as Row 
				FROM TC_CONTRACT
				JOIN CUSTOMER ON TC_CONTRACT.ID_CUSTOMER = CUSTOMER.ID_CUSTOMER
				WHERE $where_fix
				)
				SELECT * FROM TblDatabases WHERE Row > $start and Row <= $end";
		//print("<pre>".print_r($sql,true)."</pre>");


		$list = $this->Contract_m->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $ct) {        	
            $no++;
            $row = array();
            $row['no'] 					= "<center>".$no."</center>";
            $row['contract_number']		= "<center><a href='".site_url('Contract/list_contract/'.$ct->CONTRACT_NUMBER.'/'.$ct->ID)."'>".$ct->CONTRACT_NUMBER."</a></center>";
            $row['customer']			= $ct->COMPANY_NAME;
            $row['delivery_point']		= $ct->DELIVERY_POINT;
            $row['delivery_address']	= $ct->DELIVERY_ADDRESS;
            $row['description']			= $ct->DESCRIPTION;
            $row['act']					= "<center>
            <a href='#'><button class='btn btn-success btn-xs'><i class='fa fa-edit'></i></button></a>
            &nbsp&nbsp&nbsp
            <button class='btn btn-danger btn-xs btn_del' data-x='".$ct->ID."' data-y='".$ct->CONTRACT_NUMBER."'><i class='fa fa-trash'></i></button>
			</center>";
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Contract_m->count_all(null,'ct'),
            "recordsFiltered" => $this->Contract_m->count_filtered(null,'ct'),
            "data" => $data,
            "query" => $sql,
            //"where" => $where_fix,
        ];
        //output to json format
        echo json_encode($output);
	}

}
