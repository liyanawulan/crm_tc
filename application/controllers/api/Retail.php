<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Retail extends CI_Controller{

	private $column_show = ['VBELN', 'MATNR', 'ARKTX', 'AUART'];

    // Construct

	function __construct() {
		parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Retail_model', '', TRUE);
	}



		function list_order2(){

			$param['draw'] = $this->input->post('draw');			
			$param['order'] = $this->input->post('order');
			

			if ($this->input->post('columns')) {
				$param['column'] = $this->input->post('columns');
			}
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');
			}
			$res = $this->Retail_model->select_retail2_count($param);
		    $totalRow = $res[0]['JUMLAH'];
		    $totalFIltered = $res[0]['JUMLAH'];
			// $res2 = $this->Retail_model->select_retail2_count($param);
		    // $totalRow = count($res);
			

			// print_r($param['column']);
			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;

			$n = (int)$param['start'];
			$tmp1 = [];
	        $tmp11 = [];
	        $tmp22 = [];
	  
	        $listStatus = $this->Retail_model->getStatusOrder();
	        foreach ($listStatus as $key => $value) {
				if ($value->JUMLAH != 0){
					$tmp1[] = $value;
				}
	        } 
	        
		    $result = $this->Retail_model->select_retail2($param);
		    // echo $this->db->last_query();
	        $txt = 50;
	        $tmp = [];
	        $n_result = []; 
	          if (isset($result)) {
	            foreach ($result as $key => $value) {
	                $tmp = $value;
	                if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
	                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
	                    $teco_date = $tmp['TECO_DATE'];
	                    $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
	                    $e = date_create(date('Y'));
	                 
	                    // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
	                    $diff = (int) date_diff($s, $e)->format("%a");
	                    // echo $diff;echo " diff<br>";
	                    if ( $diff > 5) {
	                        $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
	                        // print_r($newdate);echo " newdate <br>";
	                        $date2 = date('Ymd', strtotime($newdate));
	                    }else{
	                      
	                      $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
	                    }

	                    $start = date_create($date1);
	                    $end = date_create($date2);

	                }else{
	                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
	                    $date2 = date('Ymd');

	                    $start = date_create($date1);
	                    $end = date_create($date2);

	                }
	               
	                $tmp['RECEIVED_DATE'] = date('d-M-Y', strtotime($tmp['RECEIVED_DATE']));
	                
	                // $tmp['STATUS'] = '';
	                if (($tmp['LFDAT']!='00000000') and (($tmp['LFDAT'])!='')) {
	                  $tmp['DELIVERY_DATE'] = date('d-M-Y', strtotime($tmp['LFDAT']));
	                } else {
	                  $tmp['DELIVERY_DATE'] = '';
	                }
	                $tmp['REMARKS'] = '';
	                $tmp['ID_RETAIL'] = '';
	                $tmp['QUOTATION_DATE'] = '';
	                $tmp['APPROVAL_DATE'] = '';
	                $day_waiting = 0;

	                for ($i=1; $i <= $txt ; $i++) { 
	                  if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
	                      break;
	                    } 
	                  // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
	                  if (strpos($value["TXT".$i], 'SROA') !== false) {

	                      $tmp['QUOTATION_DATE'] = date('d-M-Y', strtotime($value["UDATE".$i]));
	                      $nexti = $i+1;
	                      for ($ii=$i+1; $ii <= $txt ; $ii++)
	                          if ((strpos($value["TXT".$ii], 'WR') !== false) OR (strpos($value["TXT".$ii], 'SROA') !== false)){
	                             
	                              $tmp['APPROVAL_DATE'] = date('d-M-Y', strtotime($value["UDATE".$ii]));
	                              $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
	                          break 1;  
	                          }
	                      // $tmp[][$i] = $value["UDATE".$i];
	               
	                  }
	                }
	              if ($tmp['LFGSA'] != 'C') {
	                if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
	                  $tmp22[]= $value;
	                  $tmp['TXT_STAT'] = 'Approval Received / Repair Started';
	                }else if ((strpos($tmp['STAT'], 'WR') !== false)){
	                // or (strpos($tmp['STAT'], 'UR') !== false)){
	                  $tmp11[] = $value;
	                  $tmp['TXT_STAT'] = 'Repair Started';
	                }
	              }
	              $tmp['TAT'] = (date_diff($start, $end)->format("%a"))-$day_waiting ;
	              $n_result[] = $tmp;

	            }
	            // die;
	        }
	        $sum_wr1 = count($tmp11);
	        if ($sum_wr1 != 0){
	          array_push($tmp1, ((object) array('STATUS'=> 'Repair Started', 'JUMLAH' => $sum_wr1)) );
	        }
	        $sum_wr2 = count($tmp22);
	        if ($sum_wr2 != 0)
	        array_push($tmp1, ((object) array('STATUS'=> 'Approval Received/Repair Started', 'JUMLAH' => $sum_wr2)) );

        	

		$data['draw']             = $param['draw'];
		$data['recordsTotal']     = $totalRow;
		$data['recordsFiltered']  = $totalFIltered;
		$data['data'] 			  = $n_result;
			// $data['extra']						= $extra_param;


			echo json_encode($data);
		}

		function order_status2(){
			$param['status'] = $this->input->post('status');
			$param['draw'] = $this->input->post('draw');			
			$param['order'] = $this->input->post('order');
			
			
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');
			}
			$res = $this->Retail_model->select_retail_where_status2_count($param);
		    $totalRow = $res[0]['JUMLAH'];
			$totalFIltered = $res[0]['JUMLAH'];

			// $res2 = $this->Retail_model->select_retail_where_status2_count($param);
			// $totalFIltered = $res2[0]['JUMLAH'];
			// print_r($param['order']);
			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;

			$n = (int)$param['start'];
			$result   = $this->Retail_model->select_retail_where_status2($param);
        // echo $this->db->last_query();
	        $txt = 50;
	        $tmp = [];
	        $tmp1 = [];
	        $n_result = []; 
	        if (isset($result)) {
	            foreach ($result as $key => $value) {
	                $tmp = $value;
	                if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
	                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
	                    $teco_date = $tmp['TECO_DATE'];
	                    $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
	                    $e = date_create(date('Y'));
	                 
	                    // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
	                    $diff = (int) date_diff($s, $e)->format("%a");
	                    // echo $diff;echo " diff<br>";
	                    if ( $diff > 5) {
	                        $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
	                        // print_r($newdate);echo " newdate <br>";
	                        $date2 = date('Ymd', strtotime($newdate));
	                    }else{
	                      
	                      $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
	                    }

	                    // print_r($date1);echo "<br>";
	                    // print_r($date2);

	                    $start = date_create($date1);
	                    $end = date_create($date2);

	                }else{
	                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
	                    $date2 = date('Ymd');

	                    $start = date_create($date1);
	                    $end = date_create($date2);

	                }

	                $tmp['RECEIVED_DATE'] = date('d-M-Y', strtotime($tmp['RECEIVED_DATE']));
	                
	                // $tmp['STATUS'] = '';
	                if (($tmp['LFDAT']!='00000000') and (($tmp['LFDAT'])!='')){
	                  $tmp['DELIVERY_DATE'] = date('d-M-Y', strtotime($tmp['LFDAT']));
	                } else {
	                  $tmp['DELIVERY_DATE'] = '';
	                }
	                $tmp['REMARKS'] = '';
	                $tmp['ID_RETAIL'] = '';
	                $tmp['QUOTATION_DATE'] = '';
	                $tmp['APPROVAL_DATE'] = '';
	                $day_waiting = 0;

	                for ($i=1; $i <= $txt ; $i++) { 

	                  // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
	                  if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
	                    break;
	                  }
	                  if (strpos($value["TXT".$i], 'SROA') !== false) {

	                      $tmp['QUOTATION_DATE'] = date('d-M-Y', strtotime($value["UDATE".$i]));
	                      $nexti = $i+1;
	                      for ($ii=$i+1; $ii <= $txt ; $ii++)
	                          if (strpos($value["TXT".$ii], 'WR') !== false) {
	                             
	                              $tmp['APPROVAL_DATE'] = date('d-M-Y', strtotime($value["UDATE".$ii]));
	                              $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
	                          break 1;  
	                          }
	                      // $tmp[][$i] = $value["UDATE".$i];
	                  }
	                }
	              if ($tmp['LFGSA'] != 'C') {
	                if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
	                  // $tmp22[]= $value;
	                  $tmp['TXT_STAT'] = 'Approval Received / Repair Started';
	                }else if ((strpos($tmp['STAT'], 'WR') !== false)){
	                // or (strpos($tmp['STAT'], 'UR') !== false)){
	                  // $tmp11[] = $value;
	                  $tmp['TXT_STAT'] = 'Repair Started';
	                }
	              }
	              $tmp['TAT'] = (date_diff($start, $end)->format("%a")) - $day_waiting;
	              // if ($tmp['TXT_STAT'] == $status ) {
	              //     $n_result[] = $tmp;
	              // }
	              $n_result[] = $tmp;
	            }
	          }

			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $n_result;
			// $data['extra']						= $extra_param;


			echo json_encode($data);
		}

		private function find_search_column($arr){

				$val = [];

				foreach ($arr as $key => $value) {
					if ($value['search']['value']) {
							array_push($val, ['colname' => $column_show[$key], 'val' => $value['search']['value']]);
					}
				}

				return $val;
		}

	function dashboard()
    {
        $this->data["session"]    = $this->session->userdata('logged_in');

        $listStatus = $this->Retail_model->getStatusOrder();

        $tmp1 = [];
        //$tmp2 = [];
        $tmp11 = [];
        $tmp22 = [];
        foreach ($listStatus as $key => $value) {
            $tmp1[] = $value;
         }

        $chart = [0,0,0, 0];
        $result = $this->Retail_model->select_retail();
        $txt = 50;
        $tmp = [];
        $n_result = []; 
          if (isset($result)) {
            foreach ($result as $key => $value) {
                $tmp = $value;
               
                if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                    $teco_date = $tmp['TECO_DATE'];
                    $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
                    $e = date_create(date('Y'));
                 
                    // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
                    $diff = (int) date_diff($s, $e)->format("%a");
                    // echo $diff;echo " diff<br>";
                    if ( $diff > 5) {
                        $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
                        // print_r($newdate);echo " newdate <br>";
                        $date2 = date('Ymd', strtotime($newdate));
                    }else{
                      
                      $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
                    }

                    // print_r($date1);echo "<br>";
                    // print_r($date2);

                    $start = date_create($date1);
                    $end = date_create($date2);

                }else{
                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                    $date2 = date('Ymd');

                    $start = date_create($date1);
                    $end = date_create($date2);

                }

                $tmp['RECEIVED_DATE'] = date('d-m-Y', strtotime($tmp['RECEIVED_DATE']));
                
                // $tmp['STATUS'] = '';
                $tmp['DELIVERY_DATE'] = '';
                $tmp['REMARKS'] = '';
                $tmp['ID_RETAIL'] = '';
                $tmp['QUOTATION_DATE'] = '';
                $tmp['APPROVAL_DATE'] = '';
                $day_waiting = 0;

                for ($i=1; $i <= $txt ; $i++) { 
                    if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
                      break;

                    } 
                    // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
                    if (strpos($value["TXT".$i], 'SROA') !== false) {

                        $tmp['QUOTATION_DATE'] = date('d-m-Y', strtotime($value["UDATE".$i]));
                        $nexti = $i+1;
                        for ($ii=$i+1; $ii <= $txt ; $ii++)
                            if ((strpos($value["TXT".$ii], 'WR') !== false) or (strpos($value["TXT".$ii], 'SROA') !== false)){
                               
                                $tmp['APPROVAL_DATE'] = date('d-m-Y', strtotime($value["UDATE".$ii]));
                                $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
                            break 1;  
                            }
                        // $tmp[][$i] = $value["UDATE".$i];
                 
                    }
                }
                if ($tmp['LFGSA'] != 'C') {
                  if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
                    $tmp22[] = $value;
                  }else if ((strpos($tmp['STAT'], 'WR') !== false)){
                    //or (strpos($tmp['STAT'], 'UR') !== false)){
                    $tmp11[] = $value;
                  }
                }
                $tat = (date_diff($start, $end)->format("%a")) - $day_waiting;
                $chart[3]++;
                if ($tat<10) {
                    $chart[2]++;
                }else if ($tat>9 && $tat<21) {
                    $chart[1]++;
                }else if ($tat>20) {
                    $chart[0]++;
                }
            }
            // print_r($chart);
            // die;
            $sum_wr1 = count($tmp11);
            $sum_wr2 = count($tmp22);
            array_push($tmp1, ((object) array('STATUS'=> 'Approval Received / Repair Started', 'JUMLAH' => $sum_wr2)),((object) array('STATUS'=> 'Repair Started', 'JUMLAH' => $sum_wr1)) );
        
        }
        $this->data['listStatus'] = $tmp1;
        $total = 0 ;
        $provider = json_encode($tmp1);
        foreach ($tmp1 as $key => $value) {
          $total += $value->JUMLAH;
        }

        $data                       = array();
        $data['provider']           = $provider;
        $data['sum_grand_total']    = $total;
        $data['chart']              = $chart;

        $result = array('provider' => $provider, 'sum_grand_total' => $total, 'chart' => $chart);
        // print_r($result);
        echo json_encode($result);
      }
	
}