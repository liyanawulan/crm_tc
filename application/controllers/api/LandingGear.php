<?php defined('BASEPATH') OR exit('No direct script access allowed');
class LandingGear extends CI_Controller{

	private $column_show = ['REFERENCE', 'REQUIREMENT_CATEGORY_NAME', 'PN_NAME', 'PN_DESC', 'DELIVERY_POINT_CODE', 'AIRCRAFT_REGISTRATION', 'CUSTOMER_PO', 'CUSTOMER_RO', 'CUSTOMER_PO', 'CUSTOMER_RO', 'REQUEST_ORDER_DATE', 'REQUESTED_DELIVERY_DATE', 'STATUS_ORDER_NAME', 'STATUS_ORDER'];

	function __construct() 
	{
		parent::__construct();

        $this->load->database();
        // $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Landinggear_model', '', TRUE);
        $this->load->helper('form');
	}

/*===================================================================================================*/

	function list_aircraft()
	{
		$query = $this->Landinggear_model->get_datatables(Null,Null, 'ac');
		$query = explode("FROM", $query);
		// print_r($query);

		$table = explode("WHERE", $query[1]);
		$table_fix = $table[0];
		// print_r($table);
		$order = explode("ORDER", $table[1]);
		$order_fix = $order[1];
		// print_r($order);

		$where = explode("AND", $order[0]);
		// print_r($where);
		if (count($where) == 1){
			$where_fix = $where[0];
		}
		else if (count($where) == 2) {
			$where_fix = $where[0]." AND ".$where[1];
		}else if (count($where) == 3) {
			$where_fix = $where[0]." AND ".$where[1]." AND (".$where[2].")";
		}else if (count($where) == 4) {
			$where_fix = $where[0]." AND ".$where[1]." AND (".$where[2].") AND (".$where[3].")";
		}else if (count($where) == 5) {
			$where_fix = $where[0]." AND ".$where[1]." AND (".$where[2].") AND (".$where[3].") AND (".$where[4].")";
		}
	
		$start = $_REQUEST['start'];
		$end   = $_REQUEST['length']+$_REQUEST['start'];

		$sql = "WITH TblDatabases AS
		(
		SELECT ACREG_REVTX, EARTX, SERGE, ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
		WHERE $where_fix
		)
		SELECT * FROM TblDatabases WHERE Row>$start and Row<=$end";

		$sql2 = "WITH TblDatabases AS
		(
		SELECT ACREG_REVTX, EARTX, SERGE, ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
		WHERE $where_fix
		)
		SELECT * FROM TblDatabases";


		$list = $this->Landinggear_model->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $detail) {        	
            $no++;
            $row = array();
            $row['no']			= "<center>".$no."</center>";
            $row['ACREG_REVTX']	= "<center><a href='".site_url('Landing_gear/project_list/'.$detail->ACREG_REVTX)."'>".$detail->ACREG_REVTX."</a></center>";
            $row['EARTX']		= $detail->EARTX;
            $row['SERGE']		= $detail->SERGE;
 
            $data[] = $row;
        }

        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Landinggear_model->count_all(Null,Null,'ac'),
            "recordsFiltered" => count($this->Landinggear_model->count_filtered($sql2)),
            "data" => $data,
            /*"query" => $sql,
            "where" => $where_fix,*/
        ];
        
        // echo $this->db->last_query();
        //output to json format
        echo json_encode($output);
	}


	function list_project($ac, $type=1)
	{
		$query = $this->Landinggear_model->get_datatables($ac,$type,'project');
		$query = explode("FROM", $query);
		// print_r($query);

		$table = explode("WHERE", $query[1]);
		$table_fix = $table[0];
		// print_r($table);
		$order = explode("ORDER", $table[1]);
		$order_fix = $order[1];
		// print_r($order);

		$where = explode("AND", $order[0]);
		// print_r($where);
		if (count($where) == 1){
			$where_fix = $where[0];
		}
		else if (count($where) == 2) {
			$where_fix = $where[0]." AND ".$where[1];
		}else if (count($where) == 3) {
			$where_fix = $where[0]." AND ".$where[1]." AND (".$where[2].")";
		}else if (count($where) == 4) {
			$where_fix = $where[0]." AND ".$where[1]." AND (".$where[2].") AND (".$where[3].")";
		}
		// print_r($where_fix);
		// echo "--------";


		$start = $_REQUEST['start'];
		$end   = $_REQUEST['length']+$_REQUEST['start'];

		$sql = "WITH TblDatabases AS
		(
		SELECT M_REVISION.REVNR, M_REVISION.ACREG_REVTX, M_REVISION.MATNR, M_REVISION.SERNR, M_REVISION.PARNR, M_REVISIONDETAIL.WORKSCOPE, 
				CUSTOMER.COMPANY_NAME,
			CASE
				WHEN ISDATE(M_REVISION.REVBD)= 1 THEN (CAST (M_REVISION.REVBD AS DATE))
				ELSE '-'
				END AS REVBD,
			CASE
				WHEN ((M_REVISION.REVBD != '00000000') AND (M_REVISION.REVBD != '00000000')) THEN	DATEDIFF( DAY, CONVERT ( DATE, M_REVISION.REVBD, 104 ), CONVERT ( DATE, M_REVISION.REVED, 104 ) )
				ELSE ' '		
			END AS TAT,
			ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
		WHERE $where_fix
		)
		SELECT * FROM TblDatabases WHERE Row>$start and Row<=$end";

		$sql2 = "WITH TblDatabases AS
		(
		SELECT M_REVISION.REVNR, M_REVISION.ACREG_REVTX, M_REVISION.MATNR, M_REVISION.SERNR, M_REVISION.PARNR, M_REVISIONDETAIL.WORKSCOPE, CUSTOMER.COMPANY_NAME,
			CASE
				WHEN ISDATE(M_REVISION.REVBD)= 1 THEN (CAST (M_REVISION.REVBD AS DATE))
				ELSE '-'
				END AS REVBD, 
			CASE
				WHEN ((M_REVISION.REVBD != '00000000') AND (M_REVISION.REVBD != '00000000')) THEN	DATEDIFF( DAY, CONVERT ( DATE, M_REVISION.REVBD, 104 ), CONVERT ( DATE, M_REVISION.REVED, 104 ) )
				ELSE ' '		
			END AS TAT,
			ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
		WHERE $where_fix
		)
		SELECT * FROM TblDatabases";



		$list = $this->Landinggear_model->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $detail) {        	
            $no++;
            $row = array();
            $row['no']			= "<center>".$no."</center>";
            $row['REVNR']		= "<a href='".site_url('Landing_gear/overview/'.$detail->REVNR)."'>".$detail->REVNR."</a>";
            $row['ACREG_REVTX'] = $detail->ACREG_REVTX;
            $row['MATNR']		= $detail->MATNR;
            $row['SERNR']		= $detail->SERNR;
            $row['PARNR']		= $detail->COMPANY_NAME;
            $row['WORKSCOPE']	= $detail->WORKSCOPE;
            $date = new DateTime($detail->REVBD);
            $row['REVBD']		= $date->format('d-M-Y');
            $row['TAT']		    = $detail->TAT;

 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Landinggear_model->count_all($ac, $type,'project'),
            "recordsFiltered" => count($this->Landinggear_model->count_filtered($sql2)),
            "data" => $data,
            /*"query" => $sql,
            "where" => $where_fix,*/
        ];
        // echo $this->db->last_query();
        //output to json format
        echo json_encode($output);
	}

	function task_list($revnr)
	{
		$param['docno'] = $revnr;
		$param['draw'] = $_REQUEST['draw'];			
		$param['order'] = $_REQUEST['order'];

		$res = $this->Landinggear_model->get_datatables_order_all($param);
		
		if ($_REQUEST['search']) {
			$param['search'] = $_REQUEST['search'];
		}
		// print_r($param['search']);
		$res2 = $this->Landinggear_model->get_datatables_order_all($param);
	    $totalRow = $res[0]['JUMLAH'];
	    $totalFIltered = $res2[0]['JUMLAH'];
	    // echo $this->db->last_query();
	    $param['start'] = $_REQUEST['start'];
		$param['end']   = $_REQUEST['length']+$_REQUEST['start'];

		$list = $this->Landinggear_model->get_datatables_order($param);
        $data = array();
        
        $no = $_REQUEST['start'];
        foreach ($list as $detail) {        	
            $no++;
            $row = array();
            $row['no']			= "<center>".$no."</center>";
            $row['AUFNR']		= $detail->AUFNR;
            $row['ILART']		= $detail->ILART;
            $row['KTEXT']		= $detail->KTEXT;
            if ($detail->TXT_STAT == 'close') {
            	$row['TXT_STAT']	= "<span class='label label-success'>Complete</span>";
            }
            else {
            	$row['TXT_STAT']	= "<span class='label label-warning'>Progress</span>";
 			}
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $totalRow,
            "recordsFiltered" => $totalFIltered,
            "data" => $data,
            /*"query" => $sql,
            "where" => $where_fix,*/
        ];
        // echo $this->db->last_query();
        //output to json format
        echo json_encode($output);
	}


	// function detail_flt($id_ct)
	// {
	// 	$query = $this->Contract_m->get_datatables($id_ct,'fleet');
	// 	$query = explode("FROM", $query);

	// 	$table = explode("WHERE", $query[1]);
	// 	$table_fix = $table[0];

	// 	$order = explode("ORDER", $table[1]);
	// 	$order_fix = $order[1];

	// 	$where = explode("AND", $order[0]);
	// 	if (count($where) == 2) {
	// 		$where_fix = $where[0]." AND ".$where[1];
	// 	}else{
	// 		$where_fix = $where[0]." AND ".$where[1]." AND (".$where[2].")";
	// 	}


	// 	$start = $_REQUEST['start'];
	// 	$end   = $_REQUEST['length']+$_REQUEST['start'];

	// 	$sql = "WITH TblDatabases AS
	// 	(
	// 	SELECT *, ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
	// 	WHERE $where_fix
	// 	)
	// 	SELECT * FROM TblDatabases WHERE Row>$start and Row<=$end";


	// 	$list = $this->Contract_m->get_datatables_query($sql);
 //        $data = array();
 //        $no = $_REQUEST['start'];
 //        foreach ($list as $fleet) {        	
 //            $no++;
 //            $row = array();
 //            $row['no']		= "<center>".$no."</center>";
 //            $row['regis']	= $fleet->REGISTRATION;
 //            $row['type']	= $fleet->TYPE;
 //            $row['msn']		= $fleet->MSN;
 //            $row['mfg']		= $fleet->MFG_DATE;
 
 //            $data[] = $row;
 //        }
 
 //        $output = [
 //            "draw" => $_REQUEST['draw'],
 //            "recordsTotal" => $this->Contract_m->count_all($id_ct,'fleet'),
 //            "recordsFiltered" => $this->Contract_m->count_filtered($id_ct,'fleet'),
 //            "data" => $data,
 //            /*"query" => $sql,
 //            "where" => $where_fix,*/
 //        ];
 //        //output to json format
 //        echo json_encode($output);
	// }

	// function detail_ct()
	// {
	// 	$query = $this->Contract_m->get_datatables(null,'ct');
	// 	//print("<pre>".print_r($query,true)."</pre>");
	// 	$query = explode("FROM", $query);

	// 	$table = explode("JOIN", $query[1]);
	// 	$table_fix = $table[0];

	// 	$order = explode("ORDER", $table[1]);
	// 	$order_fix = $order[1];

	// 	$where = explode("WHERE", $order[0]);
	// 	$where = explode("AND", $where[1]);
	// 	if (count($where) == 2) {
	// 		$where_fix = $where[0]." AND (".$where[1].")";
	// 	}else{
	// 		$where_fix = "TC_CONTRACT.IS_DELETE =  '0'";
	// 	}


	// 	$start = $_REQUEST['start'];
	// 	$end   = $_REQUEST['length']+$_REQUEST['start'];

	// 	$sql = "WITH TblDatabases AS
	// 			(
	// 			SELECT 
	// 			CUSTOMER.COMPANY_NAME,
	// 			TC_CONTRACT.CONTRACT_NUMBER,
	// 			TC_CONTRACT.ID_CUSTOMER,
	// 			TC_CONTRACT.DELIVERY_POINT,
	// 			TC_CONTRACT.DELIVERY_ADDRESS,
	// 			TC_CONTRACT.DESCRIPTION,
	// 			TC_CONTRACT.CREATE_AT,
	// 			TC_CONTRACT.UPDATE_AT,
	// 			TC_CONTRACT.ID,
	// 			TC_CONTRACT.IS_DELETE,
	// 			ROW_NUMBER() OVER (ORDER $order_fix) as Row 
	// 			FROM TC_CONTRACT
	// 			JOIN CUSTOMER ON TC_CONTRACT.ID_CUSTOMER = CUSTOMER.ID_CUSTOMER
	// 			WHERE $where_fix
	// 			)
	// 			SELECT * FROM TblDatabases WHERE Row > $start and Row <= $end";
	// 	//print("<pre>".print_r($sql,true)."</pre>");


	// 	$list = $this->Contract_m->get_datatables_query($sql);
 //        $data = array();
 //        $no = $_REQUEST['start'];
 //        foreach ($list as $ct) {        	
 //            $no++;
 //            $row = array();
 //            $row['no'] 					= "<center>".$no."</center>";
 //            $row['contract_number']		= "<center><a href='".site_url('Contract/list_contract/'.$ct->CONTRACT_NUMBER.'/'.$ct->ID)."'>".$ct->CONTRACT_NUMBER."</a></center>";
 //            $row['customer']			= $ct->COMPANY_NAME;
 //            $row['delivery_point']		= $ct->DELIVERY_POINT;
 //            $row['delivery_address']	= $ct->DELIVERY_ADDRESS;
 //            $row['description']			= $ct->DESCRIPTION;
 //            $row['act']					= "<center>
 //            <a href='#'><button class='btn btn-success btn-xs'><i class='fa fa-edit'></i></button></a>
 //            &nbsp&nbsp&nbsp
 //            <button class='btn btn-danger btn-xs btn_del' data-x='".$ct->ID."' data-y='".$ct->CONTRACT_NUMBER."'><i class='fa fa-trash'></i></button>
	// 		</center>";
 
 //            $data[] = $row;
 //        }
 
 //        $output = [
 //            "draw" => $_REQUEST['draw'],
 //            "recordsTotal" => $this->Contract_m->count_all(null,'ct'),
 //            "recordsFiltered" => $this->Contract_m->count_filtered(null,'ct'),
 //            "data" => $data,
 //            "query" => $sql,
 //            //"where" => $where_fix,
 //        ];
 //        //output to json format
 //        echo json_encode($output);
	// }

}
