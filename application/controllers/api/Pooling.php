<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pooling extends CI_Controller{


	private $column_show = ['REFERENCE', 'REQUIREMENT_CATEGORY_NAME', 'PN_NAME', 'PN_DESC', 'DELIVERY_POINT_CODE', 'AIRCRAFT_REGISTRATION', 'CUSTOMER_PO', 'CUSTOMER_RO', 'CUSTOMER_PO', 'CUSTOMER_RO', 'REQUEST_ORDER_DATE', 'REQUESTED_DELIVERY_DATE', 'STATUS_ORDER_NAME', 'STATUS_ORDER'];

    // Construct

	function __construct() {
		parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Pooling_model', '', TRUE);
	}



		function list_table(){
			$param['draw'] = $this->input->post('draw');
			$param['order'] = $this->input->post('order');
			if ($this->input->post('columns')) {
				$param['column'] = $this->input->post('columns');
			}
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');
			}
			// print_r($param['column']);

			$res = $this->Pooling_model->get_data_pooling($param);
			// echo $this->db->last_query();
			// $param['search']['value'] = '';
			
			// $extra_param = $this->find_search_column($this->input->post('column'));

			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;

			$result = $this->Pooling_model->get_data_pooling($param);
			
			$totalRow = count($res);
			// echo $this->db->last_query();

			// $totalRow = $this->Pooling_model->get_counter_tr();
			$totalFIltered = count($res);

			$n = (int)$param['start'];
			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $result;
			// // $data['extra']						= $extra_param;

			echo json_encode($data);
		}

		private function find_search_column($arr){

				$val = [];

				foreach ($arr as $key => $value) {
					if ($value['search']['value']) {
							array_push($val, ['colname' => $column_show[$key], 'val' => $value['search']['value']]);
					}
				}

				return $val;
		}



}
