<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once $_SERVER['DOCUMENT_ROOT'].'\app_crm_tc\application\libraries\spout\src\Spout\Autoloader\autoload.php';
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Contract extends CI_Controller{

	function __construct() 
	{
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('contract_m', '', TRUE);
        $this->load->model('Customers_m', '', TRUE);
        $this->load->model('Pooling_model', '', TRUE);
	}
    // Parsing Public Data
    public $data = array(
        'ldt1'              => 'CONTRACT',
        'ldl1'              => 'index.php/Contract',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'Contract',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'Pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );
    // View
    function index()
    {
		$this->data['part_number_list']             = $this->Pooling_model->get_part_number_list();
		$this->data['aircraft_registration_list']   = $this->Pooling_model->get_aircraft_registration_list();
		$this->data['customer_list']   				= $this->Customers_m->get_customer_list();

		$this->data['title']                        = 'Create Contract';
		$this->data['icon']                         = 'fa fa-list';
		$this->data['content']                      = 'Contract/Contract';
		$this->load->view('template', $this->data);
    }

    function list_contract($no_contract=null, $id_contract=null )
    {
		//$this->data['customer_list']   = $this->contract_m->get_data_list();
		if (!$no_contract) {
			// code...
			$this->data['title']	= 'List Contract';
			$this->data['icon']		= 'fa fa-list';
			$this->data['content']	= 'Contract/list_contract';
		}else{
			$this->data['title']			= "Detail Nomor Contract [ {$no_contract} ]";
			$this->data['detailcontract']	= $this->contract_m->getDetailContract($id_contract);
			$this->data['icon']             = 'fa fa-list';
			$this->data['content']          = 'Contract/list_contract_detail';
			$this->data['no_contract']		= $no_contract;
			$this->data['id_contract']		= $id_contract;
		}
		$this->load->view('template', $this->data);
    }

	function create()
	{
		error_reporting(0);
		$param = $this->input->post();

		$param['ID_CUSTOMER']	= '';
		$param['DESCRIPTION']	= '';
		$param['ID_CUSTOMER']	= $param['CUSTOMER'];
		$param['DESCRIPTION']	= $param['DESC_CONTRACT'];

		$subparam['fleet']		= ($param['fleet'])?$param['fleet']:[];
		$subparam['pn']			= ($param['pn'])?$param['pn']:[];

		unset($param['CUSTOMER']);
		unset($param['DESC_CONTRACT']);
		unset($param['fleet']);
		unset($param['pn']);

		$id = $this->contract_m->insert_contract($param);
		//$id=1;
		//print("<pre>".print_r($param,true)."</pre>");
		if($id){
			$count_fleet = count($subparam['fleet']['TYPE']);
			$count_pn = count($subparam['pn']['ATA']);

			for($i=0; $i < $count_fleet ; $i++){
				$paramFleet['REGISTRATION'] = $subparam['fleet']['AIRCRAFT_REGISTRATION'][$i];
				$paramFleet['TYPE'] = $subparam['fleet']['TYPE'][$i];
				$paramFleet['MSN'] = $subparam['fleet']['MSN'][$i];
				$paramFleet['MFG_DATE'] = $subparam['fleet']['MFN_GATE'][$i];
				$paramFleet['ID_CONTRACT'] = $id;
				$paramFleet['NAMA_FILE'] = "by form";

				$this->contract_m->insert_contract_fleet($paramFleet);
				//print("<pre>".print_r($paramFleet,true)."</pre>");
			}

			for($i=0; $i < $count_pn ; $i++){
				$paramPn['ATA'] = $subparam['pn']['ATA'][$i];
				$paramPn['ID_PN'] = $subparam['pn']['ID_PN'][$i];
				$paramPn['ESS'] = $subparam['pn']['ESS'][$i];
				$paramPn['MEL'] = $subparam['pn']['MEL'][$i];
				$paramPn['TARGET_SLA'] = $subparam['pn']['TARGET_SLA'][$i];
				$paramPn['ID_CONTRACT'] = $id;

				$pn = "select PN_NAME,PN_DESC from MS_PART_NUMBER where ID_PN='".$subparam['pn']['ID_PN'][$i]."'";
				$pn = $this->contract_m->query($pn);
				$pn_name = $pn[0]->PN_NAME;
				$pn_desc = $pn[0]->PN_DESC;
				$paramPn['PN_NAME'] = $pn_name;
				$paramPn['PN_DESC'] = $pn_desc;

				$sla = explode(" ", $subparam['pn']['TARGET_SLA'][$i]);
				$waktu_sla = $sla[0];
				$satuan_sla = $sla[1];
				$paramPn['WAKTU_SLA'] = $waktu_sla;
				$paramPn['SATUAN_SLA'] = $satuan_sla;
				$paramPn['NAMA_FILE'] = 'by form';
					
				$this->contract_m->insert_contract_pn($paramPn);
				//print("<pre>".print_r($paramPn,true)."</pre>");
			}
			//die();

			$con_num = $param['CONTRACT_NUMBER'];
			$url_redir = base_url("index.php/contract/list_contract/$con_num/$id");
			echo "<script LANGUAGE='JavaScript'>
			    window.alert('Succesfully Create, Please Upload Data');
			    window.location.href='".$url_redir."';
			    </script>";
				//redirect('contract');
		}else{
				redirect('contract');
		}
	}

	function cek_upload()
	{
		$ct_id = $_POST['ct_id'];

		$sql_pn = "SELECT COUNT(*) as PN FROM TC_CONTRACT_PN WHERE ID_CONTRACT = '$ct_id'";
		$cek_pn = $this->contract_m->get_datatables_query($sql_pn);

		$sql_fleet = "SELECT COUNT(*) as PN FROM TC_CONTRACT_FLEET WHERE ID_CONTRACT = '$ct_id'";
		$cek_fleet = $this->contract_m->get_datatables_query($sql_fleet);

		if (($cek_pn[0]->PN >= 1) || ($cek_fleet[0]->PN >= 1)) {
			echo TRUE;
		}else{
			echo FALSE;
		}
	}

	function upload()
	{
		$url_redir = $_POST['url'];
		if(empty($url_redir)) $url_redir = base_url();
		$ct_num = $_POST['ct_num'];
		$ct_id = $_POST['ct_id'];
		$file_type = explode(".", $_FILES['files']['name']);
		$file_type = strtolower($file_type[1]);
		if($file_type != "xlsx"){
			echo "<script language='JavaScript'>
					swal({
						title: 'Erorr',
						text: 'Type file is not .xlsx',
						icon: 'error',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
		}else{
			$file_path = $_FILES['files']['tmp_name'];
			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($file_path);

			foreach ($reader->getSheetIterator() as $sheet) {
				if (strtolower($sheet->getName()) == "pn list") {
					foreach ($sheet->getRowIterator() as $row) {
				        $list[] = $row;
				    }
				}else{
					foreach ($sheet->getRowIterator() as $row) {
				        $fleet[] = $row;
				    }
				}
			}

			$insert_list 	= $this->contract_m->insert_list($list,$ct_id,$_FILES['files']['name']);
			$insert_fleet 	= $this->contract_m->insert_fleet($fleet,$ct_id,$_FILES['files']['name']);
			//print("<pre>".print_r($insert,true)."</pre>");

			$reader->close();
			//die();
			if($insert_list && $insert_fleet) {
				echo "<script language='JavaScript'>
					swal({
						title: 'Upload Succesfully',
						text: 'Upload File is Succesfully',
						icon: 'success',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
			}else{
				echo "<script language='JavaScript'>
					swal({
						title: 'Upload Failed',
						text: 'Upload File is Failed',
						icon: 'error',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
			}
		}
	}

	function updel()
	{
		$url_redir = $_POST['url'];
		if(empty($url_redir)) $url_redir = base_url();
		$ct_num = $_POST['ct_num'];
		$ct_id = $_POST['ct_id'];
		$file_type = explode(".", $_FILES['files']['name']);
		$file_type = strtolower($file_type[1]);
		if($file_type != "xlsx"){
			echo "<script language='JavaScript'>
					swal({
						title: 'Erorr',
						text: 'Type file is not .xlsx',
						icon: 'error',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
		}else{
			$file_path = $_FILES['files']['tmp_name'];
			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($file_path);

			foreach ($reader->getSheetIterator() as $sheet) {
				if (strtolower($sheet->getName()) == "pn list") {
					foreach ($sheet->getRowIterator() as $row) {
				        $list[] = $row;
				    }
				}else{
					foreach ($sheet->getRowIterator() as $row) {
				        $fleet[] = $row;
				    }
				}
			}

			$delete_list 	= $this->contract_m->delete_list($ct_id);
			$delete_fleet 	= $this->contract_m->delete_fleet($ct_id);

			$reader->close();
			//die();
			if($delete_list && $delete_fleet) {
				$insert_list 	= $this->contract_m->insert_list($list,$ct_id,$_FILES['files']['name']);
				$insert_fleet 	= $this->contract_m->insert_fleet($fleet,$ct_id,$_FILES['files']['name']);
				if($insert_list && $insert_fleet){
					echo "<script language='JavaScript'>
						swal({
							title: 'Upload Succesfully',
							text: 'Upload File is Succesfully',
							icon: 'success',
						}).then((value) => {
							window.location.href='".$url_redir."'
						});				
					</script>";
				}else{
					echo "<script language='JavaScript'>
						swal({
							title: 'Upload Failed',
							text: 'Upload File is Failed',
							icon: 'error',
						}).then((value) => {
							window.location.href='".$url_redir."'
						});				
					</script>";
				}
			}else{
				echo "<script language='JavaScript'>
					swal({
						title: 'Upload Failed',
						text: 'Delete previous data failed!',
						icon: 'error',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
			}
		}
	}

}
