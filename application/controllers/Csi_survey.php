<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Csi_survey extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (empty($this->session->userdata('log_sess_id_user'))) 
        {
            redirect('Login');
        }
        $this->load->model('Survey_model', '', TRUE);
        $this->load->model('Retail_model', '', TRUE);
        $this->load->model('Landinggear_model', '', TRUE);
        $this->load->model('pooling_model', '', TRUE);
    }

    function index() {
        $id_customer = $this->session->userdata('log_sess_id_customer');
        if (isset($id_customer)){
            $data['customer_list']                = $this->Survey_model->get_customer_list($id_customer);
        }else {
            $data['customer_list']                = $this->Survey_model->get_customer_list(False);
        }

        $data['content']        = 'csi_survey/index';
        $this->load->view('template', $data);
    }

    public function submit_survey()
    {
        if(isset($_POST['insert']))
        {
            $query_insert_survey           = $this->Survey_model->insert_survey();
            if($query_insert_survey)
            {
                $this->session->set_flashdata('alert_success','Submit Survey success');
                redirect('csi_survey');
            } 
        }
        else
        {   $this->session->set_flashdata('alert_failed','Submit Survey Failed');
            redirect('csi_survey');
        }
    }

    public function data($type){
        $data = $this->Survey_model->getDocumentNumber($type);
        echo json_encode($data); 
    }
    

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */

