<?php

class Dashboard2 extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (empty($this->session->userdata('log_sess_id_user'))) 
        {
            redirect('Login');
        }
        $this->load->model('services/landinggear/dashboard_model', '', TRUE);
    }

    function index() {
        /*$data['content']        = 'administrator/services/landinggear/dashboard/index';
        $data['listProject']    = $this->dashboard_model->getlistProject();
        $this->load->view('template', $data);*/
        //{
        redirect('Dashboard2/csi_survey');
        //}
    }   
    
    function overview($docno) {

        $data["docno"]          = $docno;
        $data['header_menu']    = 'administrator/services/landinggear/header_menu';
        $data['title_menu']     = 'administrator/services/landinggear/title_menu';
        $data['title']          = "OVERVIEW";
        $data['content']        = 'administrator/services/landinggear/dashboard/overview';
        $data['listProject']    = $this->dashboard_model->getOverviewProject($docno);
        /*$data1 = $this->dashboard_model->getOverviewProject($docno); 
        echo '<pre>';
        print_r($data1);
        die;*/
        $this->load->view('template', $data);
    }
    
    function profit_analisyst($docno) {
        $data["docno"]          = $docno;
        $data['header_menu']    = 'administrator/services/landinggear/header_menu';
        $data['title_menu']     = 'administrator/services/landinggear/title_menu';
        $data['title']          = "PROFIT ANALYSIST";
        $data['content']        = 'administrator/services/landinggear/dashboard/profit_analisyst';
        $data['listProject']    = $this->dashboard_model->getOverviewProject($docno);
        $this->load->view('template', $data);
    }
    
    function lo_report($docno) {
        $data["docno"]          = $docno;
        $data['header_menu']    = 'administrator/services/landinggear/header_menu';
        $data['title_menu']     = 'administrator/services/landinggear/title_menu';
        $data['title']          = "LO-REPORT";
        $data['content']        = 'administrator/services/landinggear/dashboard/lo_report';
        $data['listProject']    = $this->dashboard_model->getOverviewProject($docno);
        $this->load->view('template', $data);
    }

    function csi_survey() {
        $data['content']        = 'administrator/csi_survey/index';
        $this->load->view('template', $data);
    }   

     function header_menu($docno) {
        $data["docno"]          = $docno;
        $data['listProject']    = $this->dashboard_model->getOverviewProject($docno);
        $this->load->view('administrator/services/landinggear/header_menu', $data);
    }
    
    

}