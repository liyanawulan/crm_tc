<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->model('Retail_model', '', TRUE);
        // $this->data["open_menu"] = TRUE;
    }

    function see_my_session(){

        echo('log_sess_id_user => '.        $this->session->userdata('log_sess_id_user')      ."<br>");
        echo('log_sess_name => '.           $this->session->userdata('log_sess_name')         ."<br>");
        echo('log_sess_username => '.       $this->session->userdata('log_sess_username')     ."<br>");
        echo('log_sess_password => '.       $this->session->userdata('log_sess_password')     ."<br>");
        echo('log_sess_id_user_role => '.   $this->session->userdata('log_sess_id_user_role') ."<br>");
        echo('log_sess_id_user_group => '.  $this->session->userdata('log_sess_id_user_group')."<br>");
        echo('log_sess_id_customer => '.    $this->session->userdata('log_sess_id_customer')  ."<br>");
        echo('log_sess_last_login => '.     $this->session->userdata('log_sess_last_login')   ."<br>");
        echo('log_sess_status => '.         $this->session->userdata('log_sess_status')       ."<br>");
        echo('log_sess_user_role => '.      $this->session->userdata('log_sess_user_role')    ."<br>");
        echo('log_sess_user_group => '.     $this->session->userdata('log_sess_user_group')   ."<br>");
        echo('log_sess_id_user_group => '.  $this->session->userdata('log_sess_id_user_group')."<br>");
        echo('log_sess_customer_name => '.  $this->session->userdata('log_sess_customer_name')."<br>");
    }
    function index() {
        $data['content']    = 'dashboard/index'; //view home

        $chart = [0,0,0];
        $result = $this->Retail_model->select_retail();
        $txt = 10;
        $tmp = [];
        $n_result = []; 
          if (isset($result)) {
            foreach ($result as $key => $value) {
                $tmp = $value;
                if (isset($tmp['IDAT2']) && $tmp['IDAT2']!='00000000') {
                    $date1 = date('Ymd', strtotime($tmp['ERDAT']));
                    $date2 = date('Ymd', strtotime($tmp['IDAT2']));


                    $start = date_create($date1);
                    $end = date_create($date2);

                }else{
                    $date1 = date('Ymd', strtotime($tmp['ERDAT']));
                    $date2 = date('Ymd');


                    $start = date_create($date1);
                    $end = date_create($date2);

                }

                $tmp['RECEIVED_DATE'] = date('d-m-Y', strtotime($tmp['ERDAT']));
                
                // $tmp['STATUS'] = '';
                $tmp['DELIVERY_DATE'] = '';
                $tmp['REMARKS'] = '';
                $tmp['ID_RETAIL'] = '';
                $tmp['QUOTATION_DATE'] = '';
                $tmp['APPROVAL_DATE'] = '';
                $day_waiting = 0;

                for ($i=1; $i <= $txt ; $i++) { 

                    // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
                    if (strpos($value["TXT".$i], 'SROA') !== false) {

                        $tmp['QUOTATION_DATE'] = date('d-m-Y', strtotime($value["UDATE".$i]));
                        $nexti = $i+1;
                        for ($ii=$i+1; $ii <= $txt ; $ii++)
                            if (strpos($value["TXT".$ii], 'WR') !== false) {
                               
                                $tmp['APPROVAL_DATE'] = date('d-m-Y', strtotime($value["UDATE".$ii]));
                                $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
                            break 1;  
                            }
                        // $tmp[][$i] = $value["UDATE".$i];
                 
                    }
                }
                $tat = (date_diff($start, $end)->format("%a")) - $day_waiting;

                if ($tat<10) {
                    $chart[2]++;
                }else if ($tat>9 && $tat<21) {
                    $chart[1]++;
                }else if ($tat>20) {
                    $chart[0]++;
                }
            }
        }

        $data['chart'] = $chart;
        $data['home_control']  = TRUE;
        $this->load->view('template', $data);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
