<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	// Act

	public function index()
	{
		// Parameter
		// $id 					= $this->session->userdata('log_sess_iduser');
		// $param['lastlogin']		= date('Y-m-d H:i:s');

		// $this->load->model('MLogout');
		// $this->MLogout->last_login($id,$param);
		// helper_log("Logout");
		
		$this->session->sess_destroy();
		$this->session->set_flashdata('alert_logout', 'You are Logout from CRM - TC, Please login for Next :)');
		redirect('Login');

	}

	// ./Act

}




/* End of file Logout.php */
/* Location: ./application/controllers/Logout.php */
/* Copyright © 2017-2018 ASC Tech - Herdy Jr */