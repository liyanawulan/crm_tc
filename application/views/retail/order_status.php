<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
                <!-- <div class="col-md-12">
                    <h3 class="box-title">Data Retail Order</h3>
                </div> -->
              <div class="col-md-12">
                  <h3 class="box-title">Status : <span class="badge bg-orange"> <?php echo $_GET['status']; ?> </span>
                  </h3>
                  <input type="hidden" id="status_order" value="<?php echo $_GET['status']; ?>"  />
              </div>

               <!--  <div class="col-md-2 pull-right">
                    <div class="input-group">
                        <select class="form-control " id="STATUS_PAID" name="STATUS_PAID">
                            <option value="" >-- Select one --</option>
                            <option value="PAID" >Paid</option>
                            <option value="UNPAID" >Unpaid</option>
                        </select>
                    </div>
                </div> -->
            </div>
            <br>
            <div class="box-body no-padding">
                <!-- <div class=" table-responsive "> -->
                  <table id="tbList2" class="table  table-bordered table-hover table-striped">
                      <thead style="background-color: #3c8dbc; color:#ffffff;">
                          <tr>
                              <th><center>Act</center></th>
                              <th style="width: 40px">NO</th>
                              <th>SALES ORDER</th>
                              <th>MO</th>
                              <th>PURCHASE ORDER</th>
                              <th>PART NUMBER</th>
                              <th>PART NAME</th>
                              <th>SERIAL NUMBER</th>
                              <th>RECEIVED DATE</th>
                              <th>QUOTATION DATE</th>
                              <th>APPROVAL DATE</th>
                              <th>TAT</th>
                              <th>STATUS</th>
                              <th>DELIVERY DATE</th>
                              <th>REMARKS</th>
                              <!-- <th>SALES BILLING</th> -->
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
                <!-- </div> -->
            </div>
           </section>
         </div>
       </div>
     </div>
   </div>
 </section>


<script>
  $(function () {
    var unsearchable = [0,10,11,12,13,14];
    $('#tbList2 thead tr#searchtr th').each( function () {
        var title = $(this).text();
        var index = $(this)[0].cellIndex;
    } );
    var status = $('#status_order').val() ;
    //var parameters = "{'status':'" +  + "'}";
    var tablo = $("#tbList2").DataTable({
      "dom": 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "language": {
          "processing": "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
        // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
        },
      "serverSide": true,
      "scrollX": true,
      "ordering": true,
      "ajax": {
        "url" : "<?= site_url('api/Retail/order_status2') ?>",
        "type": 'post',
        "data": {'status':status},
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
              return_data.push({
                1: json.data[i].RowNum,
                2: 1*(json.data[i].SALES_ORDER),
                3: json.data[i].MAINTENANCE_ORDER,
                4: json.data[i].PURCHASE_ORDER,
                5: json.data[i].PART_NUMBER,
                6: json.data[i].PART_NAME,
                7: json.data[i].SERIAL_NUMBER,
                8: json.data[i].RECEIVED_DATE,
                9: json.data[i].QUOTATION_DATE,
                10: json.data[i].APPROVAL_DATE,
                11: json.data[i].TAT,
                12:json.data[i].TXT_STAT,
                13:json.data[i].DELIVERY_DATE,
                14:json.data[i].REMARKS,
                //15:json.data[i].BILLING_STATUS,
              })
            }
            /* set new token after request completed */
            // localStorage.setItem("api_token", json.token);

            /* Set User Permission */
            return return_data;
        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": false
        },{
          "targets": 0,
          "className": "dt-center",
          "data": null,
          "defaultContent":
          '<button title="update" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button>'
        }
      ]
    });

    $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
    });

    $('#tbList2').on( 'click', 'tbody tr .btUpdate', function (e) {
        var data = tablo.row( $(this).parents('tr') ).data();
        var no_ = data[3];
        var st = data[12];

          e.preventDefault();
         $(this).animate({
             opacity: 0 //Put some CSS animation here
         }, 500);
         setTimeout(function(){
           // OK, finished jQuery staff, let's go redirect
           window.location.href = "order_detail2?order="+no_+"&status="+st;
         },500);

    });

   
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();

  });

</script>