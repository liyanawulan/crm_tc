<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <!-- <small>#<?php echo $data_order_detail['SALES_ORDER']; ?></small> -->
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>
<?php
    if($data_order_details) {
        $jum = count($data_order_details);
        $no = 0;
        foreach ($data_order_details as $data_order_detail) {
        $no++;
    if((($data_order_detail['VBELN_BILL']) != '') or ($jum < 2)) {
?>
<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <img src="<?php echo base_url(); ?>assets/dist/img/GMF_AeroAsia_logo.png" width="200"/>
    </div><!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <h4><b>ORDER</b>
        <span class="badge bg-blue" style="font-size: 18px">#<?php echo $data_order_detail['AUFNR']; ?></span>
      </h4>
      <hr>
      <?php
        if(strpos($data_order_detail['PHAS0'], 'X') !== false) {
      ?>
      <span class="badge bg-yellow" style="font-size: 18px">MAINTENANCE - OPEN</span> 
      <?php } 
        else if(strpos($data_order_detail['PHAS1'], 'X') !== false) {
      ?>
      <span class="badge bg-yellow" style="font-size: 18px">MAINTENANCE - IN PROGRESS</span> 
      <?php } 
        else if((strpos($data_order_detail['PHAS2'], 'X') !== false) or (strpos($data_order_detail['PHAS3'], 'X'))) {
      ?>
      <span class="badge bg-yellow" style="font-size: 18px">MAINTENANCE - FINISHED</span> 
      <?php } ?>
    </div>
    <div class="col-sm-4 invoice-col">
      <h4><b>INVOICE</b>
        <span class="badge bg-green" style="font-size: 18px">#<?php echo $data_order_detail['VBELN_BILL']; ?></span>
      </h4>
      <hr>
    </div>
  </div>
  <hr> 
  <!-- info row -->
  <br>
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <b>SHIP TO</b>
      <br>
      <?php echo $data_order_detail['SHIPTOPARTY']; ?>
      <br>
      <br>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>OUTBOUND SHIPPING INFO</b>
      <br>
      -
    </div>
    <div class="col-sm-4 invoice-col">
      <b>END USER :</b>
      <br>
      <!-- GARUDA MAINTENANCE FACILITY SOERKARNO HATTA INTL AIRPORT JAKARTA -->
      <br>
      <br>
    </div>
  </div>
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <b>ORDER CREATION</b>
      <br>
       Date : <?php $source = strtotime($data_order_detail['ERDAT']);
                        $date = new DateTime($source);
                        echo $date->format('d-M-Y'); // 31-07-2012?>
       <br>
       By : <?php echo $data_order_detail['ERNAM']; ?> 
       <br>
       <!-- Repairs Order : 
       <br> -->
       <br>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>ORDER TYPE</b>
      <br>
      <?php echo $data_order_detail['AUART']; ?>
      <br>
      <br>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>CUSTOMER</b>
      <br>
      <?php echo $data_order_detail['NAME1']; ?>
      <br>
      <br>
    </div>
  </div>
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <b>BILL INFORMATION</b>
      <br>
      <?php echo $data_order_detail['BILLING_STATUS']; ?>
      <br>
      <br>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>OUTBOUND AIRWAYS BILL NUMBER</b>
      <br>
      <?php echo $data_order_detail['BOLNR']; ?>
      <br>
    </div>
  </div>


  <!-- <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <b>SHIP TO :</b><br>
      <?php //echo $data_invoice['SHIP_TO']; ?>
      <b>ORDER CREATION :</b><br>
      <?php //echo $data_invoice['ORDER_CREATION']; ?>
      <b>BILL INFORMATION :</b><br>
      <?php //echo $data_invoice['STATUS_PAID']; ?>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>OUTBOUND SHIPPING INFO :</b><br>
      <?php //echo $data_invoice['SHIP_INFO']; ?>
      <b>ORDER TYPE :</b><br>
      <?php //echo $data_invoice['ORDER_TYPE']; ?>
      <b>OUTBOUND AIRWAYS BILL NUMBER :</b><br>
      <?php //echo $data_invoice['BILL_NUMBER']; ?>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>END USER :</b><br>
      <?php //echo $data_invoice['END_USER']; ?>
      <b>CUSTOMER :</b><br>
      <?php //echo $data_invoice['CUSTOMER']; ?>
    </div>
  </div> -->

  <!-- Table row -->
  <!-- <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Qty</th>
            <th>Product</th>
            <th>Serial #</th>
            <th>Description</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Call of Duty</td>
            <td>455-981-221</td>
            <td>El snort testosterone trophy driving gloves handsome</td>
            <td>$64.50</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Need for Speed IV</td>
            <td>247-925-726</td>
            <td>Wes Anderson umami biodiesel</td>
            <td>$50.00</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Monsters DVD</td>
            <td>735-845-642</td>
            <td>Terry Richardson helvetica tousled street art master</td>
            <td>$10.70</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Grown Ups Blue Ray</td>
            <td>422-568-642</td>
            <td>Tousled lomo letterpress</td>
            <td>$25.99</td>
          </tr>
        </tbody>
      </table> -->
    <!--</div>
  </div>--><!-- /.row -->
  <br>
  <div class="row">
    <!-- accepted payments column -->
    <div class="col-lg-6">
      <!-- <p class="lead">Payment Methods:</p>
      <img src="../../dist/img/credit/visa.png" alt="Visa">
      <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
      <img src="../../dist/img/credit/american-express.png" alt="American Express">
      <img src="../../dist/img/credit/paypal2.png" alt="Paypal">
      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
      </p> -->
    </div><!-- /.col -->
    <div class="col-lg-6 pull-right">
      <p class="lead">TOTAL PAYMENT</p>
      <div class="table-responsive">
        <table class="table">
          <tr>
            <th>Subtotal</th>
            <td style="text-align: right;"><?php echo $data_order_detail['NETWR'];?> &nbsp; <?php echo $data_order_detail['WAERK']; ?></td>
          </tr>
          <tr>
            <th>Tax</th>
            <td style="text-align: right;"><?php echo $data_order_detail['MWSBK'];?> &nbsp; <?php echo $data_order_detail['WAERK']; ?> </td>
          </tr>
          <tr>
            <th>Total</th>
            <td style="text-align: right;"><?php echo $data_order_detail['TOTAL'];?> &nbsp; <?php echo $data_order_detail['WAERK']; ?></td>
          </tr>
        </table>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->

  <div class="row no-print">
    <div class="col-xs-12">
      <!-- <a href="invoice-print.html" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Print</a> -->
      <!-- <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button> -->
      <button class="btn btn-success pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
    </div>
  </div>

  <!-- this row will not appear when printing -->
</section>
<?php } } }?>
<!-- /.content -->
<div class="clearfix"></div>