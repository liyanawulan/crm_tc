<style>
.loading-mro {
    position: absolute;
    left: 50%;
    top: 100%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: 80px 0 0 -70px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
}
.loading-mro-detail {
    position: absolute;
    left: 50%;
    top: 10%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: 80px 0 0 -70px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
}
</style>
<section class="content-header">
    <h1>
      <!--<?php //echo strtoupper($title) ?>-->
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<section class="content-header">
  <div class="row">
        <div class="col-md-12">
            <div class="box no-border">
                <div class="box-header ">
                  <h4 style="text-align: center;"><b>STATUS ORDER DASHBOARD</b></h3>
                  <!-- <a href="<?php //echo base_url('index.php/Retail/dashboard'); ?>" taget=_blank><button class="btn btn-block btn-primary" ><i class="fa fa-dashboard"></i>  See Full Dashboard</button></a> -->
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">

        <?php $this->load->view($nav_tabs); ?>

        <div class="tab-content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-default">
                <!-- <div class="box box-success"> -->
                <!-- <div class="box-header with-border">
                  <h3 class="box-title" style="text-align: center">Work Progress</h3>
                </div> -->
                <div class="box-body">
                  <div class="row">

                    <div class="col-lg-12 col-xs-12">
                       <div class="box no-border">
                        <div class="box-header">
                          <!-- <h3 class="box-title">STATUS ORDER</h3> -->

                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                          </div>
                        </div>
                        <div class="box-body">
                           <div id="chartdiv" style="width: 100%; height: 450px; background-color: #FFFFFF;" ></div>
                        </div>
                        <!-- <div style="display: none;" class="loading-mro" id="loader1" style=""></div> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-lg-5 col-xs-12">
                      <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered table-hover table-striped">
                          <thead style="background-color: #3c8dbc; color:#ffffff;">
                            <tr>
                              <th style="width: 150px"><center>Component Status</center></th>
                              <th style="width: 150px"><center>Count Of Status</center></th>
                              <th><center>Act</center></th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                              if (isset($listStatus)) {
                                foreach ($listStatus as $key => $value) {
                                  if ($value->JUMLAH != 0){
                                  ?>
                                  <tr>
                                   <td><?= $value->STATUS ?></td>
                                   <td><?= $value->JUMLAH ?></td>
                                   <td class="align-center"><a href="<?php echo base_url() ?>index.php/Retail/order_status?status=<?= $value->STATUS ?>" style="" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-right"></i></a></td>
                                 </tr>
                                  <?php
                                  }
                                }
                              }
                             ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td style="background: #ccfff5 "><b>All List Order</b></td>
                              <td style="background: #ccfff5"><b><?php echo $sum_grand_total; ?></b></td>
                              <td style="background: #ccfff5"> <a href="<?php echo base_url() ?>index.php/Retail/list_order" style="" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-right"></i></a></td>
                            </tr>
                          </tfoot>
                        </table>
                       </div>
                      </div>
                    <div class="col-lg-7 col-xs-12">
                      <div class="box">
                        <div class="box-header with-border">
                          <h3 class="box-title">TAT ACHIEVEMENT</h3>

                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="chart">
                               <div id="chartdiv2" style="width: 100%; height: 350px; background-color: #FFFFFF;" ></div>
                          </div>
                        </div>
                      </div>
                    </div>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>

<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/pie.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>
<!-- <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> -->


<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script type="text/javascript">

  var dataProvider = <?= $provider ?>;
  var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
  "dataProvider": dataProvider,
  "valueField": "JUMLAH",
  "titleField": "STATUS",
  // "labelsEnabled": false,
  "autoMargins": false,
  "marginTop": 0,
  "marginBottom": 0,
  "marginLeft": 0,
  "marginRight": 0,
  // "pullOutRadius": 0,
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "colors": [
            "#2E7D32",
            "#29B6F6",
            "#f44336",
            "#FFA726"
          ],
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 20,
  "export": {
    "enabled": true
  }
} );
</script>

<script type="text/javascript">
      AmCharts.makeChart("chartdiv2",
        {
          "type": "serial",
          "categoryField": "category",
          "angle": 30,
          "depth3D": 30,
          "colors": [
            "#1a75ff",
            "#80b3ff",
          ],
          "startDuration": 1,
          "categoryAxis": {
            "gridPosition": "start"
          },
          "trendLines": [],
          "graphs": [
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-1",
              "precision": 0,
              "title": "Process",
              "type": "column",
              "valueField": "column-1"
            },
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-2",
              "title": "Max.Limit 100%",
              "type": "column",
              "valueField": "column-2"
            }
          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "stackType": "100%",
              "title": ""
            }
          ],
          "allLabels": [],
          "balloon": {},
          "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
          "titles": [],
          "dataProvider": [
            {
              "category": "> 20 days",
              "column-1": "<?= $chart[0] ?>",
              "column-2": "<?= $chart[3] ?>"
            },
            {
              "category": "> 9 days < 21 days",
              "column-1": "<?= $chart[1] ?>",
              "column-2": "<?= $chart[3] ?>"
            },
            {
              "category": "< 10 days",
              "column-1": "<?= $chart[2] ?>",
              "column-2": "<?= $chart[3] ?>"
            }
          ]
        }
      );
    </script>
<!-- <script>
    // Load pertama kali
    $(document).ready(function() {
        // load_chart_year();
        load_chart_region();
        // load_chart_business_segments();
        // back_grafik_year();
    });

    function load_chart_region() {
        $.ajax({
                url: "<?= site_url('api/Retail/dashboard') ?>",
                // data: {
                //     'year_from': year_from,
                //     'year_to': year_to,
                //     'aircraft_type': aircraft_type,
                //     'engine_type_id': engine_type,
                //     'operator_id': operator,
                //     'tipe': 0
                // },
                type: 'POST',
                dataType: "JSON",
                // beforeSend: function() {
                //     $("#loader1").fadeIn(1000);
                // },
                error: function (jqXHR, textStatus, errorThrown){
                    // notif('error', "Sorry cannot load data, please check your connection");
                },
                success: function(data) {
                    var prov = (data.provider);
                    window.alert(prov);
                    // notif('success', 'Data region successfully displayed');
                    AmCharts.makeChart( "chartdiv", {
                              "type": "pie",
                              "theme": "light",
                              "dataProvider": data.provider,
                              "valueField": "JUMLAH",
                              "titleField": "STATUS",
                              // "labelsEnabled": false,
                              "autoMargins": false,
                              "marginTop": 0,
                              "marginBottom": 0,
                              "marginLeft": 0,
                              "marginRight": 0,
                              // "pullOutRadius": 0,
                              "outlineAlpha": 0.4,
                              "depth3D": 15,
                              "colors": [
                                        "#2E7D32",
                                        "#29B6F6",
                                        "#f44336",
                                        "#FFA726"
                                      ],
                              "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                              "angle": 20,
                              "export": {
                                "enabled": true
                              }
                    });
                }
            })
            .done(function() {
                // $("#loader1").fadeOut(1000, function() {
                    $("#chartdiv").fadeIn(1000);
                // });
            })
    }
</script> -->