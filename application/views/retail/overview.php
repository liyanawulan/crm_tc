<section class="content-header">
    <h1>
        <?php echo $title; ?>
        <small>#<?php echo $ID_LSN; ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-google-wallet"></i> Retail</a></li>
        <li><a href="<?php echo base_url() ?>index.php/Retail/project_list"><i class="fa fa-folder-o"></i> Project List</a></li>
        <li class="active"><i class="fa fa-tv"></i> Overview #<?php echo $ID_LSN; ?></li>
    </ol>
</section>

<!-- Main content -->
<section class="content" style="background:" img src="<?php echo base_url(); ?>assets/dist/img/Garuda-Indonesia_GMF_Aero.jpg" width="50">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
      <div class="col-lg-12 col-xs-12">
        <div class="box box-solid">

          <div class="box-header with-border" style="background: white">
            <h3 class="box-title">Order Status <?php //echo $ID_LSN; ?></h3>              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
          </div>
          
          <br>
          <br>
          
          <div class="row" style="width:100%; margin:0 auto;">
            
            <div class="col-lg-1 col-xs-6">
            </div>

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <!-- <div class="small-box bg-maroon"> -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $total_waiting_approved; ?></h3>

                  <p>Waiting Approved</p>
                </div>
                <div class="icon">
                  <!-- <i class="ion ion-gear-b fa-spin"></i> -->
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <a href="<?php echo base_url() ?>index.php/Retail/order_status/<?php echo $ID_LSN; ?>?status_order=Waiting Approved" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <!-- <div class="small-box bg-green"> -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $total_waiting_material; ?><sup style="font-size: 20px"> </sup></h3>

                  <p>Waiting Material</p>
                </div>
                <div class="icon">
                  <!-- <i class="ion ion-shuffle"></i> -->
                  <i class="fa fa-cubes"></i>
                </div>
                <a href="<?php echo base_url() ?>index.php/Retail/order_status/<?php echo $ID_LSN; ?>?status_order=Waiting Material" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $total_under_maintenance; ?></h3>

                  <p>Under Maintenance</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-settings fa-spin"></i>
                </div>
                <a href="<?php echo base_url() ?>index.php/Retail/order_status/<?php echo $ID_LSN; ?>?status_order=Under Maintenance" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
            </div>            
          </div><!-- row Box Atas -->


          <div class="row" style="width:100%; margin:0 auto;">
            <div class="col-lg-3 col-xd-6"></div>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <!-- <div class="small-box bg-red"> -->
              <div class="small-box bg-blue">
                <div class="inner">
                  <h3><?php echo $total_waiting_repair; ?></h3>

                  <p>Waiting Repair</p>
                </div>
                <div class="icon">
                  <i class="ion ion-wrench"></i>
                </div>
                <a href="<?php echo base_url() ?>index.php/Retail/order_status/<?php echo $ID_LSN; ?>?status_order=Waiting Repair" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <!-- <div class="small-box bg-blue"> -->
                <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $total_finished; ?></h3>

                  <p>Finished</p>
                </div>
                <div class="icon">
                  <!-- <i class="ion ion-stats-bars"></i> -->
                  <i class="fa fa-check"></i>
                </div>
                <a href="<?php echo base_url() ?>index.php/Retail/order_status/<?php echo $ID_LSN; ?>?status_order=Finished" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-md-6">  
            </div><!-- ./col -->
          </div>

          <div class="box-footer">
            <a href="<?php echo base_url('index.php/Retail/list_order').'/'.$ID_LSN; ?>" taget=_blank><button class="btn btn-block btn-primary" ><i class="fa fa-tv"></i>  See Full List Order Of #<?php echo $ID_LSN; ?></button></a>
          </div><!-- /.box-footer -->

          </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
            
    <div class="row">
        <div class="col-md-6">
          <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">STATUS ORDER</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>    <!-- /.box-tools -->
            </div>      <!-- /.box-header -->
            <div class="box-body chart-responsive">
              <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
            </div>      <!-- /.box-body -->
          </div>        <!-- /.box -->
        </div>          <!-- /.col -->

        <div class="col-md-6">
            <!-- /.Bar Chart-->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">TAT ACHIEVEMENT</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>  <!-- /.box-tools -->
              </div>    <!-- /.box-header -->

              <div class="box-body">
                <div class="chart">
                   <canvas id="chart" width="800" height="450" style="height: 300px;"></canvas>
                </div>  <!-- /.chart -->
              </div>    <!-- /.box-body -->
            </div>      <!-- /.box-info -->
        </div>          <!-- /.col -->  
         
    </div>
    
    <!-- BAR CHART -->
  </div>
</div>



</section>

<script src="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>
<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<script>
  $(function () {
    "use strict";    

    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#AD1457", "#2E7D32", "#FF8F00", "#D84315", "#1565C0"],
      data: [
        {label: "Waiting Approved", value: 12},
        {label: "Waiting Material", value: 30},
        {label: "Under Maintenance", value: 20},
        {label: "Waiting Repair", value: 34},
        {label: "Finished", value: 22}
      ],
      hideHover: 'auto'
    });

    //BAR CHART
    // var bar = new Morris.Bar({
    //   element: 'bar-chart1',
    //   resize: true,
    //   data: [
    //     {y: '0-30 Days', a: 380920.50/*, b: 72*/},
    //     {y: '>30 Days', a: 928684.22},
    //     {y: '>60 Days', a: 1794228.72},
    //   ],
    //   barColors: ['#03A9F4'/*, '#FFC107'*/],
    //   xkey: 'y',
    //   ykeys: ['a'/*, 'b'*/],
    //   labels: ['Total',/* 'Series2'*/],
    //   hideHover: 'auto'
    // });
    
  });
</script>


<script type="text/javascript">
 var ctx = document.getElementById('chart');

var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['>21 Days','>10 Days <20 Days', '<9 Days'],
    datasets: [
      {
        label: 'Process',
        data: [12, 78, 49.8],
        backgroundColor: '#C2185B',
      },
      {
        label: 'Max Limit 100%',
        data: [100-12, 100-78, 100-49.8],
        backgroundColor: '#FFF9C4',
      }
    ],
  },
  options: {
    scales: {
      xAxes: [{ stacked: true }],
      yAxes: [{ stacked: true }]
    }
  }
});
</script>
