<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<section class="content-header">
  <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                  <h3 style="text-align: center;"><b>STATUS ORDER DASHBOARD</b></h3>
                  <!-- <a href="<?php //echo base_url('index.php/Retail/dashboard'); ?>" taget=_blank><button class="btn btn-block btn-primary" ><i class="fa fa-dashboard"></i>  See Full Dashboard</button></a> -->
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <?php $this->load->view($nav_tabs); ?>

        <div class="tab-content">

          <div class="row" style="width:100%; margin:0 auto;">
            
            <div class="col-lg-5 col-xs-6">

                <div class="info-box bg-green">
                  <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Delivered</span>
                    <span class="info-box-number"><?php echo $total_Delivered; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: <?php echo $percentage_Delivered; ?>%"></div>
                    </div>
                    <span class="progress-description">                          
                          <a href="<?php echo base_url() ?>index.php/Retail/order_status?status=Delivered" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                         <!--  <?php echo $percentage_Delivered; ?>% -->
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

                <div class="info-box bg-aqua">
                  <span class="info-box-icon"><i class="fa fa-file-text"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Quotation Provide Awaiting Approval</span>
                    <span class="info-box-number"><?php echo $total_Quotation_provide_awaiting_approval; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: <?php echo $percentage_Quotation_provide_awaiting_approval; ?>%"></div>
                    </div>
                    <span class="progress-description">                          
                          <a href="<?php echo base_url() ?>index.php/Retail/order_status?status=Quotation Provided Awaiting Approval" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          <!-- <?php echo $percentage_Quotation_provide_awaiting_approval; ?>% -->
                        </span> 
                  </div>
                  <!-- /.info-box-content -->
                </div>

                <!-- /.info-box -->
                <div class="info-box bg-red">
                  <span class="info-box-icon"><i class="fa fa-pencil-square"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Approval Received Repair Started</span>
                    <span class="info-box-number"><?php echo $total_Approval_received_repair_started; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: <?php echo $percentage_Approval_received_repair_started; ?>%"></div>
                    </div>
                    <span class="progress-description">                         
                         <a href="<?php echo base_url() ?>index.php/Retail/order_status?status=Approval Received Repair Started" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                         <!-- <?php echo $percentage_Approval_received_repair_started; ?>% -->
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

                <!-- /.info-box -->
                <div class="info-box bg-yellow">
                  <span class="info-box-icon"><i class="fa fa-cubes"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Ready to Deliver</span>
                    <span class="info-box-number"><?php echo $total_Ready_to_deliver; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: <?php echo $percentage_Ready_to_deliver; ?>%"></div>
                    </div>
                    <span class="progress-description">
                          <a href="<?php echo base_url() ?>index.php/Retail/order_status?status=Ready to Deliver" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          <!-- <?php echo $percentage_Ready_to_deliver; ?>% -->
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

                <!-- /.info-box -->
                <div class="info-box bg-black">
                  <span class="info-box-icon"><i class="fa fa-list"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">All List Order</span>
                    <span class="info-box-number"><?php echo $sum_grand_total; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: 100%"></div>
                    </div>
                    <span class="progress-description">
                          <a href="<?php echo base_url() ?>index.php/Retail/list_order" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

              </div>  <!-- /.col -->

              <div class="col-lg-7 col-xs-6">
                <div id="chartdiv" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>

              </div>
                        
          </div>

          <!-- Open Main 3 row -->
          <div class="row">
            <div class="col-md-2">
            </div>

              <div class="col-md-8">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title">TAT ACHIEVEMENT</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="chart">
                         <div id="chartdiv2" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
                    </div>     
                  </div>
                </div>
              </div>  

              <div class="col-md-2">
              </div>
          </div>        <!-- /.row -->
          <!-- Closed Main 3 row -->

        </div>

      </div>
    </div>
  </div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/pie.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>
<!-- <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> -->


<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script type="text/javascript">
  var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "country": "Delivered",
    "value": <?php echo $total_Delivered; ?>
  }, {
    "country": "Quotation",
    "value": <?php echo $total_Quotation_provide_awaiting_approval; ?>
  }, {
    "country": "Approval",
    "colors": "#f44336",
    "value": <?php echo $total_Approval_received_repair_started; ?>
  }, {
    "country": "Ready",
    "value": <?php echo $total_Ready_to_deliver; ?>
    
  } ],
  "valueField": "value",
  "titleField": "country",
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "colors": [
            "#2E7D32",
            "#29B6F6",
            "#f44336",
            "#FFA726"
          ],
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 30,
  "export": {
    "enabled": true
  }
} );
</script>

<script type="text/javascript">
      AmCharts.makeChart("chartdiv2",
        {
          "type": "serial",
          "categoryField": "category",
          "angle": 30,
          "depth3D": 30,
          "colors": [
            "#0000CC",
            "#0D8ECF"
          ],
          "startDuration": 1,
          "categoryAxis": {
            "gridPosition": "start"
          },
          "trendLines": [],
          "graphs": [
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-1",
              "precision": 0,
              "title": "Process",
              "type": "column",
              "valueField": "column-1"
            },
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-2",
              "title": "Max.Limit 100%",
              "type": "column",
              "valueField": "column-2"
            }
          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "stackType": "100%",
              "title": ""
            }
          ],
          "allLabels": [],
          "balloon": {},
          "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
          "titles": [],
          "dataProvider": [
            {
              "category": "> 20 days",
              "column-1": "12",
              "column-2": "88"
            },
            {
              "category": "> 9 days < 21 days",
              "column-1": "78",
              "column-2": "22"
            },
            {
              "category": "< 10 days",
              "column-1": "54",
              "column-2": "46"
            }
          ]
        }
      );
    </script>