<style>
/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 0px;
    cursor: pointer;
    font-size: 13px;
    font-family: Roboto,sans-serif;
    font-weight: 300;
    line-height: 1.571429;
    color: #37474f;
    text-align: left;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background-color: #eee;
    border-radius: 4px;

}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #009933;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}

 hr.style2 {
    height: 10px;
    border: 0;
    box-shadow: 0 10px 10px -10px #080808 inset;

}
</style>
<!-- <div class="se-pre-con"></div> -->
<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
              <!-- <div class="col-md-12">
                  <h3 class="box-title">Data Retail Order</h3>
              </div> -->
              <div id="ch-group" class="col-md-12">

              </div>

              <div class="form-inline" align="justify-content-md-center">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="input-group margin">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger">Payment Status</button>
                    </div>
                    <!-- /btn-group -->
                    <select class="form-control" data-style="btn-outline btn-primary" id="STATUS_PAID" name="STATUS_PAID">
                            <option value="" >--Select--</option>
                            <option value="Paid " >PAID</option>
                            <option value="Unpaid" >UNPAID</option>
                    </select>
                  </div>
                </div>  
              </div>
            </div>
		        <br>
            <div class="box-body no-padding">
                <!-- <div class=" table-responsive "> -->
                  <table id="tbList" class="table  table-bordered table-hover table-striped">
                      <thead style="background-color: #3c8dbc; color:#ffffff;">
                          <tr>
                              <th><center>Act</center></th>
                              <th style="width: 40px">NO</th>
                              <th>SALES ORDER</th>
                              <th>MO</th>
                              <th>PURCHASE ORDER</th>
                              <th>PART NUMBER</th>
                              <th>PART NAME</th>
                              <th>SERIAL NUMBER</th>
                              <th>RECEIVED DATE</th>
                              <th>QUOTATION DATE</th>
                              <th>APPROVAL DATE</th>
                              <th>TAT</th>
                              <th>STATUS</th>
                              <th>DELIVERY DATE</th>
                              <th>REMARKS</th>
                              <th>SALES BILLING</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <script>
  //paste this code under head tag or in a seperate js file.
  // Wait for window load
  $(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut();
  });
</script>  -->  
<script>
  $(function () {
    // var unsearchable = [0,10,11,12,13,14];
    $('#tbList thead tr#searchtr th').each( function () {
        var title = $(this).text();
        var index = $(this)[0].cellIndex;
    } );

    var tablo = $("#tbList").DataTable({
      "dom": 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "language": {
          "processing": "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
        // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
        },
      "serverSide": true,
      "scrollX": true,
      "ordering": true,
      "ajax": {
        "url" : "<?= site_url('api/Retail/list_order2') ?>",
        "type": 'post',
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
              return_data.push({
                1: json.data[i].RowNum,
                2: 1*(json.data[i].SALES_ORDER),
                3: json.data[i].MAINTENANCE_ORDER,
                4: json.data[i].PURCHASE_ORDER,
                5: json.data[i].PART_NUMBER,
                6: json.data[i].PART_NAME,
                7: json.data[i].SERIAL_NUMBER,
                8: json.data[i].RECEIVED_DATE,
                9: json.data[i].QUOTATION_DATE,
                10: json.data[i].APPROVAL_DATE,
                11: json.data[i].TAT,
                12:json.data[i].TXT_STAT,
                13:json.data[i].DELIVERY_DATE,
                14:json.data[i].REMARKS,
                15:json.data[i].BILLING_STATUS,
              })
            }
            /* set new token after request completed */
            // localStorage.setItem("api_token", json.token);

            /* Set User Permission */
            return return_data;
        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": false
        },{
          "targets": 0,
          "className": "dt-center",
          "data": null,
          "defaultContent":
          '<button title="update" class="btUpdate btn btn-success btn-xs" type="button"><i class="fa fa-edit"></i></button>'
        }
      ]
    });
    $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
    });

    // $('#tbList_filter input').unbind();
    // $('#tbList_filter input').bind('keyup', function(e) {
    //     if (e.keyCode == 13) {
    //         tablo.fnFilter($(this).val());
    //     }
    // });

    $('#tbList').on( 'click', 'tbody tr .btUpdate', function (e) {
        var data = tablo.row( $(this).parents('tr') ).data();
        var no_ = data[3];

          e.preventDefault();
         $(this).animate({
             opacity: 0 //Put some CSS animation here
         }, 500);
         setTimeout(function(){
           // OK, finished jQuery staff, let's go redirect
           window.location.href = "order_detail/"+no_;
         },500);

    });

   
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();

    var listStatus = <?php echo json_encode($listStatus); ?>;
      // console.log(listStatus);

    str = "";
    $.each(listStatus, function(index, el){
        // str += "<div class='form-group'><label>"+
        //           "<input type='checkbox' class='cbstatus flat-red' id='"+el.STATUS+"' name='status' value='"+el.STATUS+"'>"+  
        //           el.STATUS+
        //         "</label></div>";

        str += "<div class='col-md-4'>"+
               "<label class='container'> "+
                el.STATUS+
                "<input type='checkbox' class='cbstatus' id='"+el.STATUS+"' name='status' value='"+el.STATUS+"'>"+
                "<span class='checkmark'></span></label><br></div>";

          // if (index%2==1) {
          //   str +="</div>";
          //   str +="<div class='col-md-4' >";
          // }
    });

      $('#ch-group').append(str);
      $(".cbstatus").click(function() {
        var types = $('input:checkbox[name="status"]:checked').map(function() {
          return  "'"+ this.value + "'";
          }).get().join(',');

        var val = $.fn.dataTable.util.escapeRegex(types);
        tablo.columns(12).search( val ? val: '', true, true ).draw();
          // tablo.fnFilter(val, 12, false, false, false, false);

        // if ($('.cbstatus:checkbox:checked').length>0) {
          // var favorite = [];
          // $.each($('#ch-group :checked'), function(index, el){            
          //       favorite.push($('#ch-group :checked').val());
          //   });

          // var selected;
          // selected = favorite.join(',') ;
          
          //use radio values
          // var v = $('.cbstatus:checked')[0].value;
          // console.log(v);
          //now filter in column 2, with no regex, no smart filtering, no inputbox,not case sensitive
          // tablo.fnFilter(v, 12, false, false, false, false);
          
        // }else{
        //   // tablo.fnFilter('', 12, false, false, false, false);
        //   tablo.columns( 12 ).search( '' ).draw();
        // }
        
      });

      $("#STATUS_PAID").change(function() {
      
        if ($('#STATUS_PAID').val()) {
      
          //use radio values
          // var v = $('#STATUS_PAID').val();
          // console.log(v);
          //now filter in column 2, with no regex, no smart filtering, no inputbox,not case sensitive
          var val = $.fn.dataTable.util.escapeRegex($('#STATUS_PAID').val());
          // alert(val);
          tablo.columns(15).search( val ? '^'+val+'$' : '', true, true ).draw();
          // tablo.fnFilter('^'+val+'$', 15, false, true, true, false);
        }else{
          // tablo.fnFilter('', 15, false, false, true, true);
          tablo.columns(15).search( val ? '' : '', true, true ).draw();
        }
      });


  
                        

  });

</script>