<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                  <a href="<?php echo base_url('index.php/Retail/dashboard'); ?>" taget=_blank><button class="btn btn-block btn-primary" ><i class="fa fa-dashboard"></i>  See Full Dashboard</button></a>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>

    <div class="row">  

        <div class="col-md-6">

          <div class="col-md-12">
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">TAT AGING</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div id="bar-chart1" style="height:450px"></div>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>

        </div>

        <div class="col-md-6">

          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">INVOICE OUTSTANDING</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->

              <div class="box-body" style="height:200px">
                
                <div class="row">
                  <div class="col-lg-8 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <!-- <label class="col-sm-5 control-label">What Is This</label> -->
                        <div class="col-sm-7">
                          <select class="form-control select2" id="ID_PART_NUMBER" name="ID_PART_NUMBER" style="width: 100%;" required>
                            <option value="">Search For .....</option>
                            <option value="">DATA DATA 1 .....</option>
                            <option value="">DATA DATA 2 .....</option>
                            <option value="">DATA DATA 3 .....</option>
                            <option value="">DATA DATA 4 .....</option>
                            <!-- <?php 
                              //foreach ($part_number_list as $row_pn) { 
                            ?>
                            <option value="<?php //echo $row_pn->ID_PN ?>"><?php //echo $row_pn->PN_NAME; ?></option>
                            <?php //} ?> -->
                          </select>                                                                
                        </div>
                      </div>
                    </div>
                  </div>                
                                         
                  <div class="col-lg-4 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <button type="submit" id="search" name="search" class="btn btn-success"> <i class="fa fa-download"> Download</i></button>
                        
                      </div>                            
                    </div>
                  </div>
                </div>

              </div><!-- /.box-body -->

            </div><!-- /.box-info -->
          </div>

          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">HISTORICAL TRANSACTION</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div><!-- /.box-tools -->
              </div><!-- /.box-header -->

              <div class="box-body" style="height:200px">
                
                <div class="row">
                  <div class="col-lg-5 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-2 control-label"> From</label>
                        <div class="col-sm-10">
                          <input type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">                            
                        </div>
                      </div>
                    </div>
                  </div>                
                                         
                  <div class="col-lg-5 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-2 control-label"> To</label>
                        <div class="col-sm-10">
                          <input type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">                            
                        </div>
                      </div>                            
                    </div>
                  </div>

                  <div class="col-lg-2 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <button type="submit" id="search" name="search" class="btn btn-success"><i class="fa fa-download"> </i></button>
                        
                      </div>                            
                    </div>
                  </div>
                </div>

              </div><!-- /.box-body -->

            </div><!-- /.box-info -->
          </div>

        </div><!-- /.col -->
         
    </div><!-- /.row -->

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {


  //td color
  for (var i=1; i<=<?php echo $no; ?>; i++){
    var start_ =  document.getElementById('start_'+i).value;
    var end_ =  document.getElementById('end_'+i).value;    
    if(end_ - start_ > 0){      
      document.getElementById('color_curtat_'+i).style.backgroundColor='#f1c1c0';
    }else{
      document.getElementById('color_curtat_'+i).style.backgroundColor='#dbecc6';
    }
  } 


  // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>

<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<script>
  $(function () {
    "use strict";    

    //DONUT CHART
    // var donut = new Morris.Donut({
    //   element: 'sales-chart',
    //   resize: true,
    //   colors: ["#AD1457", "#2E7D32", "#FF8F00", "#D84315", "#1565C0"],
    //   data: [
    //     {label: "Waiting Approved", value: 12},
    //     {label: "Waiting Material", value: 30},
    //     {label: "Under Maintenance", value: 20},
    //     {label: "Waiting Repair", value: 34},
    //     {label: "Finished", value: 22}
    //   ],
    //   hideHover: 'auto'
    // });

    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart1',
      resize: true,
      data: [
        {y: '0-30 Days', a: 380920.50/*, b: 72*/},
        {y: '>30 Days', a: 928684.22},
        {y: '>60 Days', a: 1794228.72},
      ],
      barColors: ['#03A9F4'/*, '#FFC107'*/],
      xkey: 'y',
      ykeys: ['a'/*, 'b'*/],
      labels: ['Total',/* 'Series2'*/],
      hideHover: 'auto'
    });
    
  });
</script>
