<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <?php $this->load->view($nav_tabs); ?>
        <div class="tab-content">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="col-md-12">
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title">TAT AGING</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div class="box-body">
                    <div id="bar-chart1" style="height:450px"></div>
                  </div>
                  <!-- <div class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div> --><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="col-lg-12 col-md-12">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">INVOICE OUTSTANDING</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                  </div><!-- /.box-header -->

                  <div class="box-body" style="height:200px">

                    <div class="row">
                      <div class="col-lg-8 col-xs-12">
                        <div class="form-horizontal">
                          <div class="form-group">
                            <label class="col-sm-5 control-label">Aging</label>
                            <div class="col-sm-7">
                              <select class="form-control select2" id="RANGE" name="RANGE" style="width: 100%;" required>
                                <option value="">Choose</option>
                                <option value="1"> 0 - 30 Days</option>
                                <option value="2"> > 30 Days</option>
                                <option value="3"> > 60 Days</option>
                                <!-- <?php
                                  //foreach ($part_number_list as $row_pn) {
                                ?>
                                <option value="<?php //echo $row_pn->ID_PN ?>"><?php //echo $row_pn->PN_NAME; ?></option>
                                <?php //} ?> -->
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-4 col-xs-12">
                        <div class="form-horizontal">
                          <div class="form-group">
                            <a href="#" onclick="alertify.warning('Choose Aging First');"  id="searchRange" name="searchRange" class="btn btn-success"> <i class="fa fa-download"> Download</i></a>

                          </div>
                        </div>
                      </div>
                    </div>

                  </div><!-- /.box-body -->

                </div><!-- /.box-info -->
              </div>

              <div class="col-lg-12 col-md-12">
                <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">HISTORICAL TRANSACTION</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div><!-- /.box-tools -->
                  </div><!-- /.box-header -->

                  <div class="box-body" style="height:200px">

                    <div class="row">
                      <div class="col-lg-12 col-xs-12">
                        <div class="form-horizontal" style="margin:10px;">
                          <div class="form-group">
                            <label class="col-sm-2 control-label"> From</label>
                            <div class="col-sm-10 input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="date" id="range_start" class="form-control" name="REQUESTED_DELIVERY_DATE">
                            </div>
                          </div>
                        <!-- </div>
                        <div class="form-horizontal" style="margin:20px;"> -->
                          <div class="form-group">
                            <label class="col-sm-2 control-label"> To</label>
                            <div class="col-sm-10 input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="date" id="range_end" class="form-control" name="REQUESTED_DELIVERY_DATE">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                      <div class="col-lg-6 col-xs-12">
                        <div class="form-horizontal">
                          <div class="form-group">
                            <a href="#" id="download_historical" ><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download </button></a>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-6 col-xs-12">
                        <p class="text-red"><b> * Max 3 Month Earlier</b></p>
                      </div>
                    </div>

                  </div><!-- /.box-body -->

                </div><!-- /.box-info -->
              </div>

            </div><!-- /.col -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#RANGE').on('change', function(){
        $data =  $(this).val();
        if ($data) {
          $('#searchRange').attr('onclick', '');
          $('#searchRange').attr('href', '<?= site_url('retail/finance_invoice_outstanding') ?>/'+$data);

        }else{
          $('#searchRange').attr('onclick', 'alertify.warning("Choose Aging First");');
          $('#searchRange').attr('href', '#');
        }

    });
    $('#download_historical').on('click', function(){
      $data_start = $('#range_start').val();
      $data_end = $('#range_end').val();

      if (!$data_end) {
        $('#range_end').focus();
      }

      if (!$data_start) {
        $('#range_start').focus();
      }

      if ($data_end && $data_start) {
        var date1 = new Date($data_start);
        var date2 = new Date($data_end);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        if (diffDays<=90) 
        $('#download_historical').attr('href', '<?= site_url('retail/finance_historical') ?>/'+$data_start+'/'+$data_end);
        else{
          alertify.warning('Max range 3 Month.');
        }
      }else{
        $('#download_historical').attr('href', '#');

      }



    });


  //td color
  // for (var i=1; i<=<?php// echo $no; ?>; i++){
  //   var start_ =  document.getElementById('start_'+i).value;
  //   var end_ =  document.getElementById('end_'+i).value;
  //   if(end_ - start_ > 0){
  //     document.getElementById('color_curtat_'+i).style.backgroundColor='#f1c1c0';
  //   }else{
  //     document.getElementById('color_curtat_'+i).style.backgroundColor='#dbecc6';
  //   }
  // }


  // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>

<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<script type="text/javascript">
  var dataProvider = <?= $category ?>;
  var chart = AmCharts.makeChart("bar-chart1", {
  "theme": "light",
  "type": "serial",
  "startDuration": 2,
  "dataProvider":dataProvider,
  "graphs": [{
      "balloonText": "[[category]]: <b>[[value]]</b>",
      "fillColorsField": "color",
      "fillAlphas": 1,
      "lineAlpha": 0.1,
      "type": "column",
      "valueField": "total"
  }],
  "colors": [
            "#66c2ff",
          ],
  "depth3D": 20,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
  "categoryField": "aging",
  "categoryAxis": {
      "gridPosition": "start",
  },
  "export": {
    "enabled": true
   }
});
</script>
