<section class="content-header">
    <h1>
        CRM TC
        <small>Status</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Order</a></li>
        <li class="active">Status</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <table id="tabel-data" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ORDER</th>
                                <th>STATUS</th>
                                <th>CUSTOMER</th>
                                <th>MAINTENANCE</th>
                                <th>DUE DATE</th>
                                <th colspan="2">OPSI</th>
                            </tr>
                        </thead>
                
                        <tbody>
                            <!-- /.Waiting Approved -->
                            <tr>
                                <td>99022191</td>
                                <td>Waiting Approved</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022192</td>
                                <td>Waiting Approved</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022193</td>
                                <td>Waiting Approved</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022194</td>
                                <td>Waiting Approved</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>

                            <!-- /.Waiting Material -->
                            <tr>
                                <td>99022191</td>
                                <td>Waiting Material</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022192</td>
                                <td>Waiting Material</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022193</td>
                                <td>Waiting Material</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022194</td>
                                <td>Waiting Material</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>

                            <!-- /.Under Maintenance -->
                            <tr>
                                <td>99022191</td>
                                <td>Under Maintenance</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022192</td>
                                <td>Under Maintenance</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022193</td>
                                <td>Under Maintenance</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022194</td>
                                <td>Under Maintenance</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>

                            <!-- /.Waiting Repair -->
                            <tr>
                                <td>99022191</td>
                                <td>Waiting Repair</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022192</td>
                                <td>Waiting Repair</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022193</td>
                                <td>Waiting Repair</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                            <tr>
                                <td>99022194</td>
                                <td>Waiting Repair</td>
                                <td>Garuda Maintenanace Facility</td>
                                <td>C-Check</td>
                                <td>10/07/2017</td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /.description-block -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</section>