<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
		                    <!-- <div class="col-md-12">
		                        <h3 class="box-title">Data Retail Order</h3>
		                    </div> -->
		                    <div class="col-md-8">
		                        <div class="input-group">
		                            <div class="checkbox">
		                              <label>
		                                <input type="checkbox" class="chk" id="Waiting_Approved" name="status" value="Delivered" <?php if($status1=='Delivered' || $status2=='Delivered' || $status3=='Delivered' || $status4=='Delivered' || $status5=='Delivered') { echo 'checked'; } ?>>
		                                Delivered
		                              </label>
		                              <label>
		                                <input type="checkbox" class="chk" id="Waiting_Material" name="status" value="Quotation Provided Awaiting Approval" <?php if($status1=='Quotation Provided Awaiting Approval' || $status2=='Quotation Provided Awaiting Approval' || $status3=='Quotation Provided Awaiting Approval' || $status4=='Quotation Provided Awaiting Approval' || $status5=='Quotation Provided Awaiting Approval') { echo 'checked'; } ?>> 
		                                Quotation Provided Awaiting Approval
		                              </label>
		                              <label>
		                                <input type="checkbox" class="chk" id="Under_Maintenance" name="status" value="Approval Received Repair Started" <?php if($status1=='Approval Received Repair Started' || $status2=='Approval Received Repair Started' || $status3=='Approval Received Repair Started' || $status4=='Approval Received Repair Started' || $status5=='Approval Received Repair Started') { echo 'checked'; } ?>>
		                                Approval Received Repair Started
		                              </label>
		                              <label>
		                                <input type="checkbox" class="chk" id="Waiting_Repair" name="status" value="Ready to Deliver" <?php if($status1=='Ready to Deliver' || $status2=='Ready to Deliver' || $status3=='Ready to Deliver' || $status4=='Ready to Deliver' || $status5=='Ready to Deliver') { echo 'checked'; } ?>>
		                                Ready to Deliver
		                              </label>
		                              <!-- <label>
		                                <input type="checkbox" class="chk" id="Finished" name="status" value="Finished" <?php //if($status1=='Finished' || $status2=='Finished' || $status3=='Finished' || $status4=='Finished' || $status5=='Finished') { echo 'checked'; } ?>>
		                                Finished
		                              </label> -->
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-2 pull-right">
		                        <div class="input-group" style="width: 150px;">
		                            <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
		                            <div class="input-group-btn">
		                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-2 pull-right">
		                        <div class="input-group">
		                           <select class="form-control" id="status_paid" name="status_paid">
		                                <option value="" <?php if($_GET['status_paid']=='') { echo 'selected'; } ?>> - Choose - </option>
		                                <option value="Paid" <?php if($_GET['status_paid']=='Paid') { echo 'selected'; } ?>>Paid</option>
		                                <option value="Unpaid" <?php if($_GET['status_paid']=='Unpaid') { echo 'selected'; } ?>>Unpaid</option>
		                                
		                            </select>
		                        </div>
		                    </div>
		                    
		                    

		                    <!-- <div class="box-tools">
		                        <div class="input-group" style="width: 150px;">
		                            <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
		                            <div class="input-group-btn">
		                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		                            </div>
		                        </div>
		                    </div> -->
		                </div>
		                <br>
		                <div class="box-body table-responsive no-padding">
		                    <table class="table table-bordered table-hover table-striped">
		                        <thead>
		                            <tr>
		                                <th style="width: 40px">NO</th>
		                                <th>SALES ORDER</th>
		                                <th>MO</th>
		                                <th>PURCHASE ORDER</th>
		                                <th>PART NUMBER</th>
		                                <th>PART NAME</th>
		                                <th>SERIAL NUMBER</th>
		                                <th>RECEIVED DATE</th>
		                                <th>QUOTATION DATE</th>
		                                <th>APPROVAL DATE</th>
		                                <th>TAT</th>
		                                <th>STATUS</th>
		                                <th>DELIVERY DATE</th>
		                                <th>REMARKS</th>
		                                <th><center>Act</center></th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php
		                                if($data_table) { 
		                                    $no = $jlhpage;
		                                    foreach ($data_table as $rows) {
		                                    $no++;
		                            ?>
		                            <tr>
		                                <td><?php echo $no; ?></td>
		                                <td><?php echo $rows->SALES_ORDER; ?></td>
		                                <td><?php echo $rows->MAINTENANCE_ORDER; ?></td>
		                                <td><?php echo $rows->PURCHASE_ORDER; ?></td>
		                                <td><?php echo $rows->PART_NUMBER; ?></td>
		                                <td><?php echo $rows->PART_NAME; ?></td>
		                                <td><?php echo $rows->SERIAL_NUMBER?></td>       
		                                <td>
		                                	<?php 
			                                	$source = $rows->RECEIVED_DATE;
			                                    $date = new DateTime($source);
			                                    echo $date->format('d-m-Y'); 
			                               	?>
		                               	</td>
		                                <td>
		                                	<?php 
		                                		$source = $rows->QUOTATION_DATE;
		                                    	$date = new DateTime($source);
		                                    	echo $date->format('d-m-Y'); 
		                                  	?>
		                              	</td>
		                                <td>
		                                	<?php 
			                                	$source = $rows->APPROVAL_DATE;
			                                    $date = new DateTime($source);
			                                    echo $date->format('d-m-Y'); 
			                             	?>
		                                </td>
		                                <td id="color_tat_<?php echo $no; ?>"><input type="hidden" id="end_<?php echo $no; ?>" 
		                                    value="<?php echo $rows->TAT; ?>"/><?php echo $rows->TAT; ?> Days</td>
		                                <td><?php echo $rows->STATUS?></td>
		                                <td>
		                                	<?php 
			                                	$source = $rows->DELIVERY_DATE;
			                                    $date = new DateTime($source);
			                                    echo $date->format('d-m-Y'); 
			                             	?>
		                                </td>
		                                <td><?php echo $rows->REMARKS?></td>
		                                <td>
		                                  <div class="btn-group">
		                                    <a href="<?php echo base_url('index.php/administrator/Retail_control/order_detail').'/'.$rows->ID_RETAIL ?>" onclick="return confirm('Are You Sure Want To Show This Order Detail ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-file-text"></i></button></a>
		                                    <!-- <a href="#ModalView" data-toggle="modal"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></button></a>
		                                    <a href="#ModalViewLog" data-toggle="modal"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-tasks"></i></button></a>
		                                    <a href="<?php //echo base_url('Working_order/edit_in_progress_wo'); //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Edit This New Working Order ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
		                                    <a href="<?php //echo base_url('Working_order/cancel_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Cancel This New Working Order ???')"><button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button></a> -->
		                                  </div><!-- /.btn-group -->
		                                </td>
		                            </tr>
		                            <?php } }?>
		                        </tbody>
		                    </table>
		                    <br>
		                    <div class="col-md-3">
		                        <b><?php if($total_rows) { echo '<p class="text-yellow">'.$total_rows.' Total Data Records </p>'; } else { echo '<p class="text-yellow"> No Data Records </p>'; } ?></b>
		                    </div>
		                    <div class="col-md-6">
		                        <?php echo $paging; ?>
		                    </div>
		                    <div class="col-md-3 pull-right">
		                        <a href="<?php //echo base_url('Working_order/download_new_wo').'/'.$type.'/'.$key.'/'.$start.'/'.$end; ?>" taget=_blank><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download Data <?php echo $title; ?></button></a>
		                    </div>

		                </div>
	                  <br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- SEARCH -->
<script type="text/javascript">
    $(document).ready(function () {
	  $(".chk").click(function() {
	      getValueUsingClass();
	  });

	  function getValueUsingClass(){
		  var chkArray = [];
		  
		  $(".chk:checked").each(function() {
		    chkArray.push($(this).val());
		  });
		  
		  var selected;
		  selected = chkArray.join('-') ;
		  
		  if(selected.length > 0){
		    var get_status    		= "<?php echo $_GET['status']; ?>";
            var get_status_paid     = "<?php echo $_GET['status_paid']; ?>";
            window.location         = "<?=base_url()?>index.php/administrator/Retail_control/order_filter?status="+selected+"&status_paid="+get_status_paid;
		  }else{
		    window.location         = "<?=base_url()?>index.php/administrator/Retail_control/list_order"; 
		  }
		}

	});
</script>

<script type="text/javascript">
$(document).ready(function () { 
    $('#status_paid').change(function () { 
        // var ID_LSN    = "<?php //echo $ID_LSN; ?>";
        var selSTATUS     = $('#status_paid').val();
        var get_status    = "<?php echo $_GET['status']; ?>";
        window.location         = "<?=base_url()?>index.php/administrator/Retail_control/order_filter?status="+get_status+"&status_paid="+selSTATUS;
    });
});
</script>
<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
