<section class="content-header">
    <h1>
        <?php echo $title; ?>
        <small>Retail</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>index.php/administrator/Retail_control"><i class="fa fa-google-wallet"></i> Retail</a></li>
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content" style="background:" img src="<?php echo base_url(); ?>assets/dist/img/Garuda-Indonesia_GMF_Aero.jpg" width="50">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
      <div class="col-lg-12 col-xs-12">
        <div class="box box-danger">
          <div class="box-header with-border" style="background: white">
            <h3 class="box-title">Component Status Summary</h3>              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <div class="row" style="width:100%; margin:0 auto;">
              <div class="col-lg-5 col-xs-6">

                <a href="<?php echo base_url() ?>index.php/administrator/Retail_control/order_status?status=Delivered" style="color:#FFF" class="small-box-footer">
                <div class="info-box bg-green">
                  <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Delivered</span>
                    <span class="info-box-number"><?php echo $total_Delivered; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: <?php echo $percentage_Delivered; ?>%"></div>
                    </div>
                    <span class="progress-description">                          
                          <a href="<?php //echo base_url() ?>index.php/administrator/Retail_control/order_status?status=Delivered" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          <?php echo $percentage_Delivered; ?>%
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                </a>

                <div class="info-box bg-aqua">
                  <span class="info-box-icon"><i class="fa fa-file-text"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Quotation Provide Awaiting Approval</span>
                    <span class="info-box-number"><?php echo $total_Quotation_provide_awaiting_approval; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: <?php echo $percentage_Quotation_provide_awaiting_approval; ?>%"></div>
                    </div>
                    <span class="progress-description">                          
                          <a href="<?php echo base_url() ?>index.php/administrator/Retail_control/order_status?status=Quotation Provided Awaiting Approval" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          <?php echo $percentage_Quotation_provide_awaiting_approval; ?>%
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

                <!-- /.info-box -->
                <div class="info-box bg-red">
                  <span class="info-box-icon"><i class="fa fa-cogs"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Approval Received Repair Started</span>
                    <span class="info-box-number"><?php echo $total_Approval_received_repair_started; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: <?php echo $percentage_Approval_received_repair_started; ?>%"></div>
                    </div>
                    <span class="progress-description">                         
                         <a href="<?php echo base_url() ?>index.php/administrator/Retail_control/order_status?status=Approval Received Repair Started" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                         <?php echo $percentage_Approval_received_repair_started; ?>%
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

                <!-- /.info-box -->
                <div class="info-box bg-yellow">
                  <span class="info-box-icon"><i class="fa fa-cubes"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Ready to Deliver</span>
                    <span class="info-box-number"><?php echo $total_Ready_to_deliver; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: <?php echo $percentage_Ready_to_deliver; ?>%"></div>
                    </div>
                    <span class="progress-description">
                          <a href="<?php echo base_url() ?>index.php/administrator/Retail_control/order_status?status=Ready to Deliver" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          <?php echo $percentage_Ready_to_deliver; ?>%
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

                <!-- /.info-box -->
                <div class="info-box bg-black">
                  <span class="info-box-icon"><i class="fa fa-spinner fa-spin"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Grand Total</span>
                    <span class="info-box-number"><?php echo $sum_grand_total; ?></span>

                    <div class="progress">
                      <div class="progress-bar" style="width: 100%"></div>
                    </div>
                    <span class="progress-description">
                          <a href="<?php echo base_url() ?>index.php/administrator/Retail_control/list_order" style="color:#FFF" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>

              </div>  <!-- /.col -->

              <div class="col-lg-7 col-xs-6">
                <!-- Sales Chart Canvas -->
                <!-- /.chart-responsive 1 -->
                <div class="box-body piechart col-md-4" style="background: #9FC5E8">
                  <div class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                    <?php echo $total_Delivered; ?>
                    <?php echo $total_Quotation_provide_awaiting_approval; ?>
                    <?php echo $total_Approval_received_repair_started; ?>
                    <?php echo $total_Ready_to_deliver; ?>
                  </div>
                </div>

                <div class="box-body piechart col-md-4" style="background: #9FC5E8">
                  <ul class="chart-legend clearfix">
                    <li style="color:#ADFF2F"><i class="fa fa-square"> </i> Delivered   </li> 
                    <li style="color:#00FFFF"><i class="fa fa-square"> </i> Quotation_provide_awaiting_approval   </li> 
                    <li style="color:#FFFF00"><i class="fa fa-square"> </i> Approval_received_repair_started  </li> 
                    <li style="color:#ff0000"><i class="fa fa-square"> </i> Ready_to_deliver     </li> 
                </div>

                <div class="box-body piechart col-md-4" style="background: #9FC5E8">
                  <ul class="chart-legend clearfix">
                    <li style="color:#ADFF2F"> <b><?php echo $percentage_Delivered;  ?>%</b></li> 
                    <li style="color:#00FFFF"> <b><?php echo $percentage_Quotation_provide_awaiting_approval;  ?>%</b></li> 
                    <li style="color:#FFFF00"> <b><?php echo $percentage_Approval_received_repair_started; ?>%</b></li> 
                    <li style="color:#ff0000"> <b><?php echo $percentage_Ready_to_deliver;    ?>%</b></li> 
                  </ul>
                 </div>

              </div>

          </div>
          </div>
        </div>
      </div>
    </div>


            
            
    <!-- Open Main 3 row -->
    <div class="row">
      <div class="col-md-2">
      </div>

        <div class="col-md-8">
          <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">TAT ACHIEVEMENT</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                   <div id="chartdiv2" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
              </div>     
            </div>
          </div>
        </div>  

        <div class="col-md-2">
        </div>
    </div>        <!-- /.row -->
    <!-- Closed Main 3 row -->


  </div>
</div>



</section>


<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<!-- STACKEDBAR -->
<!-- <script type="text/javascript">
 var ctx = document.getElementById('chart');

var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['>21 Days','>10 Days <20 Days', '<9 Days'],
    datasets: [
      {
        label: 'Process',
        data: [12, 78, 49.8],
        backgroundColor: '#C2185B',
      },
      {
        label: 'Max Limit 100%',
        data: [100-12, 100-78, 100-49.8],
        backgroundColor: '#FFF9C4',
      }
    ],
  },
  options: {
    scales: {
      xAxes: [{ stacked: true }],
      yAxes: [{ stacked: true }]
    }
  }
});
</script> -->

<script>
        $('.sparkline').sparkline('html',
                {
                    type: 'pie',
                    height: '11.0em',
                    sliceColors: ['#ADFF2F', '#00FFFF', '#FFFF00', '#ff0000', '#EE82EE'],
                    highlightLighten: 1.1,
                    tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                });   
</script>
