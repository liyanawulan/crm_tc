<section class="content-header">
<div class="panel panel-default">
   <h1 align="center">
        <u>Project List</u><br>
        Landing Gear Maintenance
    </h1>
</div>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                <table id="example" class="table table-bordered table-striped" style="width: 100%">
                         <thead>
                                <th>NO</th>
                                <th>SALES ORDER</th>
                                <th>MO</th>
                                <th>PURCHASE ORDER</th>
                                <th>PART NUMBER</th>
                                <th>PART NAME</th>
                                <th>SERIAL NUMBER</th>
                                <th>RECEIVED DATE</th>
                                <th>QUOTATION DATE</th>
                                <th>APPROVAL DATE</th>
                                <th>TAT</th>
                                <th>STATUS</th>
                                <th>REMARKS</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0;
                          if (is_array($listData )) {
                           foreach ($listData as $row) {
                            $no++;
                             ?>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $row->SALES_ORDER; ?></td>
                              <td><?php echo $row->MAINTENANCE_ORDER; ?></td>
                              <td><?php echo $row->PURCHASE_ORDER; ?></td>
                              <td><?php echo $row->PART_NUMBER; ?></td>
                              <td><?php echo $row->PART_NAME; ?></td>
                              <td><?php echo $row->SERIAL_NUMBER?></td>       
                              <td><?php $source = $row->RECEIVED_DATE;
                                  $date = new DateTime($source);
                                  echo $date->format('d-m-Y'); // 31-07-2012?></td>
                              <td><?php $source = $row->QUOTATION_DATE;
                                  $date = new DateTime($source);
                                  echo $date->format('d-m-Y'); // 31-07-2012?></td>
                              <td><?php $source = $row->APPROVAL_DATE;
                                  $date = new DateTime($source);
                                  echo $date->format('d-m-Y'); // 31-07-2012?></td>
                              <td id="color_tat_<?php echo $no; ?>"><input type="hidden" id="end_<?php echo $no; ?>" 
                                  value="<?php echo $row->TAT; ?>"/><?php echo $row->TAT; ?> Days</td>
                              <td><?php echo $row->STATUS?></td>
                              <td><?php echo $row->REMARKS?></td>

                            </tr>
                          <?php }} ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {


  //td color
  for (var i=1; i<=<?php echo $no; ?>; i++){
    var start_ =  document.getElementById('start_'+i).value;
    var end_ =  document.getElementById('end_'+i).value;    
    if(end_ - start_ > 0){      
      document.getElementById('color_tat_'+i).style.backgroundColor='#f1c1c0';
    }else{
      document.getElementById('color_tat_'+i).style.backgroundColor='#dbecc6';
    }
  } 


  // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>
