<section class="content-header">
<div class="panel panel-default">
   <h1 align="center">
        <u>Project List</u><br>
        Landing Gear Maintenance
    </h1>
</div>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                <table id="example" class="table table-bordered table-striped" style="width: 100%">
                         <thead>
                                <th>NO</th>
                                <th>PART NUMBER</th>
                                <th>SERIAL NUMBER</th>
                                <th>AIRCRAFT REGISTRATION</th>
                                <th>MSN</th>
                                <th>WORKSCOPE</th>
                                <th>PROJECT START DATE</th>
                                <th>CONTRACTUAL TAT</th>
                                <th>CURRENT PROGRESS TAT</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0;
                          if (is_array($listProject )) {
                           foreach ($listProject as $row) {
                            $no++;
                             ?>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $row->PART_NUMBER; ?></td>
                              <td>
                                <a href= "<?php echo base_url(); ?>index.php/administrator/dashboard/overview/<?php echo $row->ID_LSN; ?>" href='javascript:void(0);'><?php echo $row->ID_LSN; ?></a>
                              </td>
                              <td><?php echo $row->AIRCRAFT_REGISTRATION; ?></td>
                              <td><?php echo $row->MSN; ?></td>
                              <td><?php echo $row->WORKSCOPE; ?></td>       
                              <td><?php $source = $row->PROJECT_START_DATE;
                                  $date = new DateTime($source);
                                  echo $date->format('d-m-Y'); // 31-07-2012?></td>
                              <td ><input type="hidden" id="start_<?php echo $no; ?>" value="<?php echo $row->CONTRACTUAL_TAT; ?>"/><?php echo $row->CONTRACTUAL_TAT; ?> Days</td>
                              <td id="color_curtat_<?php echo $no; ?>"><input type="hidden" id="end_<?php echo $no; ?>" value="<?php echo $row->CURRENT_PROGRESS_TAT; ?>"/><?php echo $row->CURRENT_PROGRESS_TAT; ?> Days</td>

                            </tr>
                          <?php }} ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {


  //td color
  for (var i=1; i<=<?php echo $no; ?>; i++){
    var start_ =  document.getElementById('start_'+i).value;
    var end_ =  document.getElementById('end_'+i).value;    
    if(end_ - start_ > 0){      
      document.getElementById('color_curtat_'+i).style.backgroundColor='#f1c1c0';
    }else{
      document.getElementById('color_curtat_'+i).style.backgroundColor='#dbecc6';
    }
  } 


  // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>
