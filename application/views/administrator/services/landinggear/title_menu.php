

<?php //list($ID_LSN, $LO, $AIRCRAFT_REGISTRATION, $WORKSCOPE, $CONTRACTUAL_TAT, $PROJECT_START_DATE, $CURRENT_PROGRESS_TAT, $TYPE, $COMPANY_NAME) = $listProject; ?>
<h3 align="center"><u><?php echo $title; ?></u></br><?php echo $docno; ?></h3>

<div class="row">
	<div class="col-lg-12 col-xs-12">   
	  <div class="box box-solid">
		  <div class="box">
	
			  <div class="container-fluid" style="margin-left: 140px">
			  <div class="col-lg-12">
				  <table  class="stripe row-border order-column" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td><b>A/C Registration</b></td>
							<td>:</td>
							<td><?php echo $listProject['AIRCRAFT_REGISTRATION']; ?></td>
							<td><b>Customer</b></td>
							<td>:</td>
							<td><?php echo $listProject['COMPANY_NAME'] ?></td>
						</tr>

						<tr>
							<td><b>Workscope</b></td>
							<td>:</td>
							<td><?php echo $listProject['WORKSCOPE'] ?></td>
							<td><b>A/C Type</b></td>
							<td>:</td>
							<td><?php //echo $TYPE ?></td>
						</tr>

						<tr>
							<td><b>Project Start Date</b></td>
							<td>:</td>
								<td>
									<?php 
									$source = $listProject['PROJECT_START_DATE'];
									$date = new DateTime($source);
									echo $date->format('d-m-Y'); // 31-07-2012
									?>
								</td>
							<td><b>Contractual TAT</b></td>
							<td>:</td>
							<td><?php echo $listProject['CONTRACTUAL_TAT'] ?></td>
						</tr>
					</tbody>
				</table>
			  </div>
			  </div>
			</div>
		  </div>
	  </div>
	</div>
