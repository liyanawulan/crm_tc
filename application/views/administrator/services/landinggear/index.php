<section class="content-header">
    <h1>
        Services and Support
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Services</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<!-- /.row -->

  <div class="row">
    <div class="col-md-12">
        <div class="box">
          <div class="box-header">
              <h3 class="box-title">Landing Gear</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <!-- <?php /*echo*/ $output; ?> -->
          </div>
          <!-- /.box-body -->
        </div>          
        <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
                
<div class="modal fade mymodal1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"></div>
  </div>
</div>                  

<script>
$(document).ready(function() {
    $('#example11').DataTable( {
        
        "scrollX": true
    } );
} );
</script>
