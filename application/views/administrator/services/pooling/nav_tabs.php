        <ul class="nav nav-tabs">
          <li <?php if($this->uri->segment(3)=='dashboard') { echo 'class="active"'; } ?>><a class="fa fa-dashboard" href="<?php echo base_url(); ?>index.php/administrator/pooling_control/dashboard"> Dashboard</a></li>
          <li <?php if($this->uri->segment(3)=='create_order') { echo 'class="active"'; } ?>><a class="fa fa-edit" href="<?php echo base_url(); ?>index.php/administrator/pooling_control/create_order"> Create Order</a></li>
          <li <?php if($this->uri->segment(3)=='list_order') { echo 'class="active"'; } ?>><a class="fa fa-list" href="<?php echo base_url(); ?>index.php/administrator/pooling_control/list_order"> List Order</a></li>
          <?php if($this->uri->segment(3)=='update_order') { ?><li class="active"><a class="fa fa-edit" href="<?php echo base_url(); ?>index.php/administrator/pooling_control/all_request_order"> Update Order</a></li><?php } ?>
          <!-- <li <?php //if($this->uri->segment(3)=='report') { echo 'class="active"'; } ?>><a class="fa fa-bar-chart" href="<?php //echo base_url(); ?>index.php/administrator/pooling_control/report/"> Report</a></li> -->
          <!-- <li class="dropdown">
            <a class="dropdown-toggle fa fa-bar-chart" data-toggle="dropdown" href="#tab_report"> Report <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li role="presentation"><a class="fa fa-bar-chart" href="<?php //echo base_url(); ?>index.php/administrator/pooling_control/report_serviceable_level/">Serviceable Level</a></li>
              <li role="presentation"><a class="fa fa-bar-chart" href="<?php //echo base_url(); ?>index.php/administrator/pooling_control/report_unserviceable_level/">Unserviceable Level</a></li>
            </ul>
          </li> -->
          <li class="dropdown <?php if($this->uri->segment(3)=='report_serviceable_level' || $this->uri->segment(3)=='report_unserviceable_level') { echo 'active'; } ?>">
            <a class="dropdown-toggle fa fa-bar-chart" data-toggle="dropdown" href="#tab_configuration"> Report <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li role="presentation"><a class="fa fa-bar-chart" href="<?php echo base_url(); ?>index.php/administrator/pooling_control/report_serviceable_level/"> Serviceable Level</a></li>
              <li role="presentation"><a class="fa fa-bar-chart" href="<?php echo base_url(); ?>index.php/administrator/pooling_control/report_unserviceable_level/"> Unserviceable Level</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a class="dropdown-toggle fa fa-wrench" data-toggle="dropdown" href="#tab_configuration"> Configuration <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li role="presentation"><a class="fa fa-cubes" href="<?php echo base_url(); ?>index.php/administrator/"> Part Number</a></li>
              <li role="presentation"><a class="fa fa-pencil" href="<?php echo base_url(); ?>index.php/administrator/"> Contract</a></li>
              <li role="presentation"><a class="fa fa-sitemap" href="<?php echo base_url(); ?>index.php/administrator/"> Requirement Category</a></li>
              <li role="presentation"><a class="fa fa-user" href="<?php echo base_url(); ?>index.php/administrator/"> User</a></li>
              <li role="presentation"><a class="fa fa-users" href="<?php echo base_url(); ?>index.php/administrator/"> Customers</a></li>
            </ul>
          </li>
        </ul>