<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <?php $this->load->view($nav_tabs); ?>

        <div class="tab-content">
          <!-- /.tab-pane New Order-->    
          <!-- <div class="tab-pane" id="tab_new_order"> -->
            
            <legend>Request Info</legend>
            <form action="<?php echo base_url() ?>index.php/administrator/Pooling_control/update_order" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">

                      <div class="form-group">
                          <label class="col-sm-5 control-label">Customer :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-green">CITILINK INDONESIA</span>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Status :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-red">CANCELED BY CUSTOMER</span>       
                          </div>
                      </div>
                      <br>

                      <div class="form-group">
                        <label class="col-sm-5 control-label">Reference No</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="REFERENCE" disabled> 
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Service Level</label>
                          <div class="col-sm-7">
                              <label class="radio-inline">
                                  <input type="radio" name="SERVICE_LEVEL" id="optionsRadios1" value="Pre-Defined">Pre-Defined
                              </label>
                              <label class="radio-inline">
                                  <input type="radio" name="SERVICE_LEVEL" id="optionsRadios2" value="Customer Request">Customer Request
                              </label>    
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Request Part Number</label>
                        <div class="col-sm-7">
                          <select class="form-control select2" id="ID_PART_NUMBER" name="ID_PART_NUMBER" style="width: 100%;" disabled>
                            <?php 
                              foreach ($part_number_list as $row_pn) { 
                            ?>
                            <option value="<?php echo $row_pn->ID_PN ?>"><?php echo $row_pn->PN_NAME; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Component Description</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V" disabled>
                          <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requirement Category</label>
                        <div class="col-sm-7">
                          <select class="form-control select2" id="ID_REQ_CAT" name="ID_REQ_CAT" style="width: 100%;" disabled>
                            <!-- <option value="">Search Requirement Category</option> -->
                            <?php 
                              foreach ($request_category_list as $row_rc) { 
                            ?>
                            <option value="<?php echo $row_rc->ID_REQ_CAT ?>"><?php echo $row_rc->REQ_CAT_NAME.' - '.$row_rc->REQ_CAT_DESC; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Delivery Date</label>
                        <div class="col-sm-7">
                          <input type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">                            
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Maintenance</label>
                        <div class="col-sm-7">
                          <label class="radio-inline">
                            <input type="radio" name="MAINTENANCE" value="No">No
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="MAINTENANCE" value="Yes">Yes
                          </label>    
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="col-sm-5 control-label">Pool</label>
                        <div class="col-sm-7">
                          <label class="radio-inline">
                            <input type="radio" name="POOL" value="No">No
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="POOL" value="Yes">Yes
                          </label>    
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Off-Wing</label>
                        <div class="col-sm-7">
                          <label class="radio-inline">
                            <input type="radio" name="OFF_WING" value="No">No
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="OFF_WING" value="Yes">Yes
                          </label>    
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Reaseon Of Requested Service Level</label>
                        <div class="col-sm-7">
                          <textarea name="REASON_OF_REQUESTED_SERVICE_LEVEL" type="textarea" class="form-control" rows="5" cols="22" disabled></textarea> 
                        </div>
                      </div>
                    </div>
                  </div>                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">

                      <div class="form-group">
                          <label class="col-sm-5 control-label">Resquested By / Date :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-blue">AOG Desk GMF</span> <span class="pull-left badge bg-yellow">14-02-2018</span> 
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-5 control-label"> Confirmed By / Date :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-blue">AOG Desk GMF</span>  
                          </div>
                      </div>
                      <br>

                      <div class="form-group">
                        <label class="col-sm-5 control-label">Ata Chapter</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="ATA_CHAPTER">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Release Type</label>
                        <div class="col-sm-7">
                          <label class="checkbox-inline">
                            <input type="checkbox" name="RELEASE_TYPE" value="EASA">EASA
                          </label>   
                          <label class="checkbox-inline">
                            <input type="checkbox" name="RELEASE_TYPE" value="FAA">FAA
                          </label>                             
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Aircraft Registration</label>
                        <div class="col-sm-7">
                          <select class="form-control select2" id="AIRCRAFT_REGISTRATION" name="AIRCRAFT_REGISTRATION" style="width: 100%;" disabled>
                            <option value="">Choose Aircraft Reg.</option>
                            <?php 
                              foreach ($aircraft_registration_list as $row_ar) { 
                            ?>
                            <option value="<?php echo $row_ar->ID_AR ?>"><?php echo $row_ar->AIRCRAFT_REGISTRATION; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">3 Digit Airport Code</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="3_DIGIT_AIRPORT_CODE" disabled>                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Destination Address</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="DESTINATION_ADDRESS" disabled> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Customer PO/RO</label>
                        <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-7">
                              <input type="text" class="form-control" name="CUSTOMER_PO" placeholder="" disabled>
                            </div>
                            <div class="col-sm-5">
                              <input type="text" class="form-control" name="CUSTOMER_RO" placeholder="" disabled>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Remarks Of Customer</label>
                        <div class="col-sm-7">
                          <textarea name="REMARKS_OF_CUSTOMER" type="textarea" class="form-control" rows="5" cols="22" disabled></textarea> 
                        </div>
                      </div>                            
                      <div class="form-group">
                        <label class="col-sm-5 control-label">THYT Remarks</label>
                        <div class="col-sm-7">
                          <textarea name="THYT REMARKS" type="textarea" class="form-control" rows="5" cols="22" disabled></textarea> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <!-- <br>
                <legend>Delivery To</legend>
                
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Attention Name</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="ATTENTION_NAME">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Attention Phone</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="ATTENTION_PHONE" placeholder="">                            
                        </div>
                      </div>  
                    </div>
                  </div>                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Company Consigne Name</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="COMPANY_CONSIGNE_NAME">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Company Consigne Address</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" name="COMPANY_CONSIGNE_ADDRESS" placeholder="">                            
                        </div>
                      </div>               
                    </div>
                  </div>

                </div> -->

              </div>
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" id="insert" name="insert" class="btn btn-success pull-right">Submit Order</button>
              </div><!-- /.box-footer -->           
            </form>
          
        </div>
            
        <div class="modal fade mymodal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content"></div>
            </div>
        </div>
        <div class="modal fade mymodal1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default box-solid">
        <!-- <div class="box box-success"> -->
        <div class="box-header with-border">
          <h3 class="box-title">Serviceable Component</h3>
          <!-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div> -->
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">

                <div class="form-group">
                    <label class="col-sm-5 control-label">Status :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-red">NOT SHIPPED YET</span>       
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Submitter :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-blue">EMRE HERDY PRABOWO EMIROGLU</span>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Shipment Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios1" value="Pre-Defined">Hand-Delivered
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios2" value="Customer Request">Overseas Shipment
                        </label>    
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Part Number</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V" disabled>
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">MFR Serial Number</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V" disabled>
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
        
              </div>
            </div>                
                                   
            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">

                <div class="form-group">
                  <label class="col-sm-5 control-label">AWB No.</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V" disabled>
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Flight No.</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V" disabled>
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Estimated Delivery Date</label>
                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">                            
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Airport/Destination</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V" disabled>
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Attention Email</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V" disabled>
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
          
              </div>
            </div>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default box-solid">
        <!-- <div class="box box-success"> -->
        <div class="box-header with-border">
          <h3 class="box-title">Unserviceable Component</h3>
          <!-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div> -->
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">

                <div class="form-group">
                    <label class="col-sm-5 control-label">Status :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-red">NOT SHIPPED YET</span>       
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Submitter :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-blue">EMRE HERDY PRABOWO EMIROGLU</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Confirmor :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-blue">EMRE HERDY PRABOWO EMIROGLU</span>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Send Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios1" value="Pre-Defined">Serviceable
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios2" value="Customer Request">Unserviceable
                        </label>    
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Shipment Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios1" value="Pre-Defined">Hand-Delivered
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios2" value="Customer Request">Overseas Shipment
                        </label>    
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Part Number</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Serial Number</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Removal Date From AC</label>
                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">                            
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">AC Registration</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Removal Reason</label>
                  <div class="col-sm-7">
                    <textarea name="REMARKS_OF_CUSTOMER" type="textarea" class="form-control" rows="5" cols="22"></textarea> 
                  </div>
                </div>
        
              </div>
            </div>                
                                   
            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">

                <div class="form-group">
                  <label class="col-sm-5 control-label">AWB No.</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Flight No.</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Estimated Delivery Date</label>
                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">                            
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Attention Email</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                    <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Remarks</label>
                  <div class="col-sm-7">
                    <textarea name="REMARKS_OF_CUSTOMER" type="textarea" class="form-control" rows="5" cols="22"></textarea> 
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Removal Reason Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios1" value="Pre-Defined">Scheduled Rep.
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios2" value="Customer Request">On Condition (Unchecked) Rep.
                        </label>  
                        <label class="radio-inline">
                            <input type="radio" name="SERVICE_LEVEL" id="optionsRadios1" value="Pre-Defined">Return Unused
                        </label>  
                    </div>
                </div>

          
              </div>
            </div>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>

<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<script type="text/javascript">
    $(document).ready(function () { 
        $('#ID_PART_NUMBER').change(function () { 
            var selID_PN = $('#ID_PART_NUMBER').val();
            $.ajax({
                url: "<?=base_url()?>index.php/administrator/Pooling_control/get_part_number_description/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_PN="+selID_PN, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#COMPONENT_DESCRIPTION").val(data.PN_DESC); //after you explained the JSON response
                    $("#COMPONENT_DESCRIPTION_V").val(data.PN_DESC); //after you explained the JSON response
                }
            });

        });
        var selID_PN = $('#ID_PART_NUMBER').val();
            $.ajax({
                url: "<?=base_url()?>index.php/administrator/Pooling_control/get_part_number_description/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_PN="+selID_PN, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#COMPONENT_DESCRIPTION").val(data.PN_DESC); //after you explained the JSON response
                    $("#COMPONENT_DESCRIPTION_V").val(data.PN_DESC); //after you explained the JSON response
                }
            });
    });
</script>