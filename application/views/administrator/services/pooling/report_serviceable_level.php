<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <?php $this->load->view($nav_tabs); ?>

        <div class="tab-content">
          <!-- /.tab-pane New Order-->    
          <!-- <div class="tab-pane" id="tab_new_order"> -->
            
            <legend>Filters</legend>
            <form action="<?php echo base_url() ?>index.php/administrator/Pooling_control/report_serviceable_level" class="form-horizontal" method="get" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Delivery Date From</label>
                        <div class="col-sm-7">
                          <input type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">                            
                        </div>
                      </div>
                    </div>
                  </div>                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Delivery Date To</label>
                        <div class="col-sm-7">
                          <input type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">                            
                        </div>
                      </div>                            
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="box-footer">
                <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                <button type="submit" id="insert" name="insert" class="btn btn-success pull-right"><i class="fa fa-search"></i> Show Report</button>
              </div><!-- /.box-footer -->           
            </form>
          
          </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">CHART</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>

        <div class="box-body">
          <div class="chart">
             <canvas id="chart" width="800" height="450" style="height: 300px;"></canvas>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">LIST</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-sm-12">
          <!-- style="width:670px; margin:0 auto;"          -->
           <table id="example" class="table table-bordered table-striped" style="width: 100%;">
              <thead>
                <tr>
                  <th>Mel Category</th>
                  <th>Ontime</th>
                  <th>Delayed</th>
                  <th>Act</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>AOG</td>
                  <td>22</td>
                  <td>0</td>
                  <td>
                    <div class="btn-group">
                      <a href="<?php //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Show This Activity ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-tasks"></i></button></a>
                    </div><!-- /.btn-group -->
                  </td>
                </tr>
                <tr>
                  <td>CRITICAL</td>
                  <td>20</td>
                  <td>0</td>
                  <td>
                    <div class="btn-group">
                      <a href="<?php //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Show This Activity ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-tasks"></i></button></a>
                    </div><!-- /.btn-group -->
                  </td>
                </tr>
                <tr>
                  <td>RUTIN</td>
                  <td>7</td>
                  <td>0</td>
                  <td>
                    <div class="btn-group">
                      <a href="<?php //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Show This Activity ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-tasks"></i></button></a>
                    </div><!-- /.btn-group -->
                  </td>
                </tr>
                <tr>
                  <td>SCHEDULED</td>
                  <td>11</td>
                  <td>0</td>
                  <td>
                    <div class="btn-group">
                      <a href="<?php //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Show This Activity ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-tasks"></i></button></a>
                    </div><!-- /.btn-group -->
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th>SUM : 98</th>
                  <th>SUM : 2</th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          </div>

        </div>
      </div>
    </div>
         
  </div>

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>

<script type="text/javascript">
 var ctx = document.getElementById('chart');

var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['AOG','CATEGORY A', 'CATEGORY B', 'CATEGORY C', 'CATEGORY D', 'REP AOG', 'SCHEDULED'],
    datasets: [
      {
        label: 'Level Result',
        data: [100, 100, 100, 100, 100, 90, 100],
        backgroundColor: '#03A9F4',
      }
      // ,
      // {
      //   label: 'Max Limit 100%',
      //   data: [100-12, 100-78, 100-49.8],
      //   backgroundColor: '#FFF9C4',
      // }
    ],
  },
  options: {
    scales: {
      xAxes: [{ stacked: true }],
      yAxes: [{ stacked: true }]
    }
  }
});
</script>