<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <?php $this->load->view($nav_tabs); ?>

        <div class="tab-content">
            
            <legend>Order Info</legend>
            <form action="<?php echo base_url() ?>index.php/administrator/Pooling_control/insert_create_order" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Service Level</label>
                          <div class="col-sm-7">
                              <label class="radio-inline">
                                  <input type="radio" name="SERVICE_LEVEL" id="optionsRadios1" value="Pre-Defined">Pre-Defined
                              </label>
                              <label class="radio-inline">
                                  <input type="radio" name="SERVICE_LEVEL" id="optionsRadios2" value="Customer Request">Customer Request
                              </label>    
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Request Part Number</label>
                        <div class="col-sm-5">
                          <select class="form-control input-sm select2" id="ID_PART_NUMBER" name="ID_PART_NUMBER" style="width: 100%;" required>
                            <option value="">Search For Part Number</option>
                            <?php 
                              foreach ($part_number_list as $row_pn) { 
                            ?>
                            <option value="<?php echo $row_pn->ID_PN ?>"><?php echo $row_pn->PN_NAME; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Component Description</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                          <input type="hidden" class="form-control input-sm" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requirement Category</label>
                        <div class="col-sm-5">
                          <select class="form-control input-sm select2" id="ID_REQUIREMENT_CATEGORY" name="ID_REQUIREMENT_CATEGORY" style="width: 100%;" required>
                            <option value="">Choose Requirement Category</option>
                            <?php 
                              foreach ($requirement_category_list as $row_rc) { 
                            ?>
                            <option value="<?php echo $row_rc->ID_REQUIREMENT_CATEGORY ?>"><?php echo $row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Delivery Date</label>
                        <div class="col-sm-7">
                          <input type="date" class="form-control input-sm" name="REQUESTED_DELIVERY_DATE">                            
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Maintenance</label>
                        <div class="col-sm-7">
                          <label class="radio-inline">
                            <input type="radio" name="MAINTENANCE" value="No">No
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="MAINTENANCE" value="Yes">Yes
                          </label>    
                        </div>
                      </div>

                    </div>
                  </div>                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Ata Chapter</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control input-sm" name="ATA_CHAPTER">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Aircraft Registration</label>
                        <div class="col-sm-5">
                          <select class="form-control input-sm select2" id="ID_AIRCRAFT_REGISTRATION" name="ID_AIRCRAFT_REGISTRATION" style="width: 100%;" required>
                            <option value="">Choose Aircraft Reg.</option>
                            <?php 
                              foreach ($aircraft_registration_list as $row_ar) { 
                            ?>
                            <option value="<?php echo $row_ar->ID_AIRCRAFT_REGISTRATION ?>"><?php echo $row_ar->AIRCRAFT_REGISTRATION; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Release Type</label>
                        <div class="col-sm-7">
                          <label class="checkbox-inline">
                            <input type="checkbox" name="RELEASE_TYPE" value="EASA">EASA
                          </label>   
                          <label class="checkbox-inline">
                            <input type="checkbox" name="RELEASE_TYPE" value="FAA">FAA
                          </label>                             
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Delivery Point</label>
                        <div class="col-sm-5">
                          <select class="form-control input-sm select2" id="ID_DELIVERY_POINT" name="ID_DELIVERY_POINT" style="width: 100%;" required>
                            <option value="">Choose</option> -->
                            <!-- <option value="">CGK</option>
                            <option value="">OTHERS</option> -->
                            <?php 
                              foreach ($delivery_point_list as $row_dp) { 
                            ?>
                            <option value="<?php echo $row_dp->ID_DELIVERY_POINT ?>"><?php echo $row_dp->DELIVERY_POINT_CODE; ?></option>
                            <?php } ?>
                          </select>                                    
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Destination Address</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" id="DESTINATION_ADDRESS" name="DESTINATION_ADDRESS" disabled> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Customer PO/RO</label>
                        <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-7">
                              <input type="text" class="form-control input-sm" name="CUSTOMER_PO" placeholder="">
                            </div>
                            <div class="col-sm-5">
                              <input type="text" class="form-control input-sm" name="CUSTOMER_RO" placeholder="">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Remarks Of Customer</label>
                        <div class="col-sm-7">
                          <textarea name="REMARKS_OF_CUSTOMER" type="textarea" class="form-control input-sm" rows="5" cols="22"></textarea> 
                        </div>
                      </div>                            
                    </div>
                  </div>
                </div>
                
                <br>
                <hr class="title-hr">
                <legend>Attention To </legend>
                
                <div class="row">                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Existing Email</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" multiple="multiple" id="EXISTING_EMAIL" name="EXISTING_EMAIL" style="width: 100%;" required>
                            <option value="">Choose Existing Email</option>
                            <option value="herdy.prabowo@gmail.com" selected>herdy.prabowo@gmail.com</option>
                            <option value="riyan.sasmitha@gmail.com">riyan.sasmitha@gmail.com</option>
                            <option value="liyana@gmail.com">liyana@gmail.com</option>
                            <option value="viar@gmail.com">viar@gmail.com</option>
                            <option value="nando@gmail.com">nando@gmail.com</option>
                            <option value="michael@gmail.com">michael@gmail.com</option>
                            <!-- <?php 
                              //foreach ($request_category_list as $row_rc) { 
                            ?>
                            <option value="<?php //echo $row_rc->ID_REQ_CAT ?>"><?php //echo $row_rc->REQ_CAT_NAME.' - '.$row_rc->REQ_CAT_DESC; ?></option>
                            <?php //} ?> -->
                          </select>
                        </div>
                      </div>             
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 1 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL1">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 2 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL2">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 3 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL3">  
                        </div>
                      </div>

                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" id="insert" name="insert" class="btn btn-success pull-right">Submit Order</button>
              </div><!-- /.box-footer -->           
            </form>
          
        </div>
            
      </div>
    </div>
  </div>
</section>


<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<script type="text/javascript">
    $(document).ready(function () { 
        $('#ID_PART_NUMBER').change(function () { 
            var selID_PN = $('#ID_PART_NUMBER').val();
            $.ajax({
                url: "<?=base_url()?>index.php/administrator/Pooling_control/get_part_number_description/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_PN="+selID_PN, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#COMPONENT_DESCRIPTION").val(data.PN_DESC); //after you explained the JSON response
                    $("#COMPONENT_DESCRIPTION_V").val(data.PN_DESC); //after you explained the JSON response
                }
            });

        });
        var selID_PN = $('#ID_PART_NUMBER').val();
            $.ajax({
                url: "<?=base_url()?>index.php/administrator/Pooling_control/get_part_number_description/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_PN="+selID_PN, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#COMPONENT_DESCRIPTION").val(data.PN_DESC); //after you explained the JSON response
                    $("#COMPONENT_DESCRIPTION_V").val(data.PN_DESC); //after you explained the JSON response
                }
            });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () { 
        $('#ID_DELIVERY_POINT').change(function () { 
            var selID_DP = $('#ID_DELIVERY_POINT').val();
            if (selID_DP!=1)
            {
              $.ajax({
                url: "<?=base_url()?>index.php/administrator/Pooling_control/get_delivery_point_address/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_DP="+selID_DP, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#DESTINATION_ADDRESS").val(data.DELIVERY_POINT_ADDRESS); //after you explained the JSON response
                    $("#DESTINATION_ADDRESS").attr('disabled', 'disabled');
                }
              });
            }
            else
            {
              $("#DESTINATION_ADDRESS").removeAttr('disabled', 'disabled');
              $("#DESTINATION_ADDRESS").val('');
            }

        });
        var selID_DP = $('#ID_DELIVERY_POINT').val();
            $.ajax({
                url: "<?=base_url()?>index.php/administrator/Pooling_control/get_delivery_point_address/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_DP="+selID_DP, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#DESTINATION_ADDRESS").val(data.DELIVERY_POINT_ADDRESS); //after you explained the JSON response
                }
            });
    });
</script>