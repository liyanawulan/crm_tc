 <div class="col-md-6">
    <!-- /.Stacked Bar -->
    <div class="box-box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Stacked Chart</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>  <!-- /.box-tools -->
      </div>    <!-- /.box-header -->  

      <div class="box-body">
         <!-- <label class="label label-success">Stacked Bar</label> -->
         <div id="bar-chart_stacked" ></div>
      </div>    
    </div>
</div>                <!-- /.col -->        

<script type="text/javascript"> /*Stacked Bar*/
new Chart(document.getElementById("bar-chart1"), {
    type: 'bar',
    data: {
      labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Predicted world population (millions) in 2050'
      }
    }
});
</script>