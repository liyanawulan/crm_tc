<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>
<!-- <section class="content-header">
<div class="panel panel-default">
   <h1 align="center">
        <u>Project List</u><br>
        Landing Gear Maintenance
    </h1>
</div>
</section> -->

<!-- Main content -->
<section class="content">
    <div class="row">
     
      <div class="col-lg-12 col-md-12 col-xs-12">
         <div class="box">
        <div class="box-body table-responsive">
           <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <!-- <table id="example" class="table table-bordered table-hover table-striped" style="width:100%;"> -->
             <thead style="background-color: #3c8dbc; color:#ffffff;">
                <tr>
                   <th>NO</th>
                   <th>AIRCRAFT REGISTRATION</th>
                   <th>A/C TYPE</th>
                   <th>Manufacturing Serial <br> Number (MSN)</th>
                 
                </tr>
              </thead>
              <tbody>
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    </section>
<!-- <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> -->   
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#example').DataTable({
        serverSide: true,
        processing: true,
        language: {
          processing: "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
        // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
        },
        ajax: '<?=base_url('index.php/api/LandingGear/list_aircraft')?>',
        columns: [       
            {data: 'no', name: 'no'},
            {data: 'ACREG_REVTX', name: 'ACREG_REVTX'},            
            {data: 'EARTX', name: 'EARTX', orderable: false},
            {data: 'SERGE', name: 'SERGE', orderable: false}
        ],
    });

} );
</script>