<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <?php $this->load->view($header_menu); ?>
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <?php
            // $this->load->view($header_menu);
            $this->load->view($title_menu);
            ?>
            <div class="row">
              <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="box box-primary">
                  <div class="box-body">
                      <div class="col lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="info-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <span class="info-box-icon" style="background-color: #ffaa00;"><i class="fa fa-calendar"></i></span>
                                 <div class="info-box-content">
                                    <span class="info-box-text">Actual TAT</span>
                                    <span class="info-box-number" style="font-size: 20px"><?= $actual ?> Days</span>
                                 </div>
                              </div>          <!-- /.info-box -->

                              <div class="info-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="info-box-icon" style="background-color: #65b328;"><i class="fa fa-list-alt"></i></span>
                                  <div class="info-box-content">
                                      <span class="info-box-text">Job Card</span>
                                      <span class="info-box-number" style="font-size: 20px"><?= $Complete ?>%</span>
                                      <span class="info-box-text"><?= $jobcard_complete ?> of <?= $jobcard ?></span>
                                  </div>
                              </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              Removal:
                              <div class="progress">
                                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Removal?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $Removal ?>%; background-color:#08aebe;">
                                  <?php echo $Removal ?>%
                                  </div>
                              </div>

                            Disassambly:
                              <div class="progress">
                                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Disassambly ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $Disassambly ?>%; background-color:#08aebe;">
                                  <?php echo $Disassambly ?>%
                                </div>
                              </div>
                            Inspection:
                              <div class="progress">
                                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Inspection ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?= $Inspection ?>%; background-color:#08aebe;">
                                  <?= $Inspection ?>%
                                </div>
                              </div>
                            Repair:
                               <div class="progress">
                                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Repair ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $Repair ?>%; background-color:#08aebe;">
                                <?php echo $Repair ?>%
                                  </div>
                              </div>
                            Assembly:
                              <div class="progress">
                                <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Assembly ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $Assembly ?>%; background-color:#08aebe;">
                                <?php echo $Assembly ?>%
                              </div>
                              </div>
                           Install:
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Install ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $Install ?>%; background-color:#08aebe;">
                                <?php echo $Install ?>%
                                </div>
                              </div>
                           Test:
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Test ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $Test ?>%; background-color:#08aebe;">
                              <?php echo $Test ?>%
                              </div>
                            </div>
                            QEC:
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $QEC ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $QEC ?>%; background-color:#08aebe;">
                              <?php echo $QEC ?>%
                              </div>
                            </div>
                        </div>

                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Progress ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $Progress?>%; background-color:#00cc99;">
                                Progress <?php echo $Progress ?>%
                                </div>
                              </div>
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $Complete ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $Complete?>%; background-color:#3973ac;">
                                Complete <?php echo $Complete ?>%
                                </div>
                              </div>
                              <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div id="gauge" class="200x160px"></div>
                              </div>                                      
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div id="gauge1" class="200x160px"></div>
                              </div> -->
                            </div>
                              <div class="col-lg-12 col-xs-12">
                                <div class="box">
                                  <!-- <div class="box-header with-border">
                                      <h3 class="box-title" style="text-align: center"></h3>
                                  </div> -->
                                  <div class="box-body">
                                    <table id="tbList" class="table table-bordered table-striped" style="width: 100%;">
                                      <thead style="background-color: #009973; color:#ffffff;">
                                        <tr>
                                          <th>NO</th>
                                          <th>NO ORDER</th>
                                          <th>MAT</th>
                                          <th>DESCRIPTION</th>
                                          <TH>STATUS</TH>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      <!--   <?php
                                        $no = 0;
                                        if (is_array($listProgress )) {
                                         foreach ($listProgress as $row) {
                                          $no++;
                                           ?>
                                           <tr>
                                             <td> <?= $no ?> </td>
                                             <td><?= $row['AUFNR'] ?></td>
                                             <td><?= $row['ILART'] ?></td>
                                             <td><?= $row['KTEXT'] ?></td>
                                             <td>
                                              <?php if($row['TXT_STAT']== 'close') { ?>
                                              <span class="label label-success">Complete</span> 
                                              <?php } else {?>
                                              <span class="label label-warning">Progress</span>
                                              <?php } ?>
                                              </td>
                                           </tr>
                                        <?php }} ?>
 -->
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
   // DataTable
        var table = $('#tbList').DataTable({
            scrollY:        "500px",
            dom: 'Blfrtip',
            lengthMenu: [
                [ 15, 25, 50, -1 ],
                [ '15 rows', '25 rows', '50 rows', '100 rows' ]
            ],
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 15,
            ordering: true,
            serverSide: true,
            processing: true,
            language: {
              processing: "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
            // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
            },
            ajax: '<?=base_url('index.php/api/LandingGear/task_list/'.$this->uri->segment(3))?>',

            columns: [       
                {data: 'no', name: 'no'},
                {data: 'AUFNR', name: 'AUFNR'},            
                {data: 'ILART', name: 'ILART'},
                {data: 'KTEXT', name: 'KTEXT'},
                {data: 'TXT_STAT', name: 'TXT_STAT'}
            ],
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });

      $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
    });
} );
</script>
<!-- <script type="text/javascript">
  var g = new JustGage({
    id: "gauge",
    value:<?php echo $Progress; ?>,
    min: 0,
    max: 100,
    symbol: '%',
    levelColors: ["#FF5722"],
    label: "In Progress Job Cards"
  });

  var g = new JustGage({
    id: "gauge1",
    value: <?php echo $Complete; ?>,
    min: 0,
    max: 100,
    symbol: '%',
    levelColors: ["#38e144"],
    label: "Closed Job Cards"
  });
</script> -->
