<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>
<section class="content-header">
<div class="panel panel-default">
  <div class="row">
    <div class="col-md-12">
      <div class="panel-body">
        <?php foreach ($aircraft_info as $rows) { ?>
          <div class="col-lg-3 col-xs-6 col-sm-6 col-md-3">
             <h5 ><p style="font-weight:600;"> Aircraft registration : </p> <?= $docno ?></h5> 
           </div>
           <div class="col-lg-3 col-xs-6 col-sm-6 col-md-3">
             <h5 ><p style="font-weight:600;"> Aircraft type : </p> <?php echo $rows->EARTX; ?></h5> 
           </div>
           <div class="col-lg-3 col-xs-6 col-sm-6 col-md-3">
             <h5 ><p style="font-weight:600;"> MSN : </p> <?php echo $rows->SERGE; ?></h5> 
           </div> 
           <?php echo form_open(false, 'id="thePeriodForm" class="form-horizontal"'); ?>
           <div class="col-lg-3 col-xs-6 col-sm-6 col-md-3">
             <h5 ><p style="font-weight:600;"> Status : </p></h5> 
             <select class="form-control" name="status">
               <option value="closed" <?php echo ($this->session->userdata('status_lg')=='closed') ? 'selected' : ''; ?> >Closed</option>
               <option value="progress" <?php echo ($this->session->userdata('status_lg')=='progress') ? 'selected' : ''; ?> >Progress</option>
             </select>
           </div>
           <?php echo form_close(); } ?>
      </div>
    </div>
  </div>
</div>
<div class="row clearfix">
  <!-- <div class="col-lg-12 col-xs-12 col-md-12" id="component_detail"></div> -->
  <div class="col-lg-12 col-xs-12 col-md-12">
  <div class="box">

    <div class="box-body">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="<?= (isset($type[1]))?$type[1]:'' ?>"><a href="<?= base_url('index.php/landing_gear/project_list').'/'.$docno.'/'.'1' ?>">Nose</a></li>
        <li role="presentation" class="<?= (isset($type[2]))?$type[2]:'' ?>"><a href="<?= base_url('index.php/landing_gear/project_list').'/'.$docno.'/'.'2' ?>" >Main Landing Gear</a></li>
        <li role="presentation" class="<?= (isset($type[3]))?$type[3]:'' ?>"><a href="<?= base_url('index.php/landing_gear/project_list').'/'.$docno.'/'.'3' ?>" >Left Landing Gear</a></li>
        <li role="presentation" class="<?= (isset($type[4]))?$type[4]:'' ?>"><a href="<?= base_url('index.php/landing_gear/project_list').'/'.$docno.'/'.'4' ?>" >Right Landing Gear</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="Nose">
          <!-- Main content -->
         
              <div class="row">
                  <div class="box-body box-primary">
                      <table id="tb1" class="table table-bordered table-hover table-striped" style="width:100%;">
                       <thead style="background-color: #009973; color:#ffffff;">
                          <tr>
                            <th>No</th>
                            <th>No Revision</th> 
                            <th>Aircraft Registration</th>
                            <th>Part Number</th>
                            <th>Serial Number</th>
                            <th>Customer Name</th>
                            <th>Workskope</th>
                            <th>Start Project</th>
                            <th>TAT</th>
                            <!-- <th>Current Tat</th> -->
                          </tr>
                        </thead>
                        <tbody>
                         
                        </tbody>
                      </table>
                    </div>
               <!--    </div>
                </div> -->
              </div>
          

        </div>
  
      </div>
    </div>
  </div>
</div>
</div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
        var table = $('#tb1').DataTable({
            // scrollY: "200px",
            // dom: 'Bfrtip',
            dom: 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            serverSide: true,
            processing: true,
            language: {
              processing: "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
            // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
            },
            ajax:
              {
              "url" : '<?=base_url('index.php/api/LandingGear/list_project/'.$this->uri->segment(3))?>',
              "type": 'post',
              "dataSrc" : function(json){
                var return_data = [];

                  json.draw = json.draw;
                  json.recordsFiltered = json.recordsFiltered;
                  json.recordsTotal = json.recordsTotal;

                  /* ReOrdering json result */

                  for(var i=0;i< json.data.length; i++){
                    return_data.push({
                      0: json.data[i].no,
                      1: json.data[i].REVNR,
                      2: json.data[i].ACREG_REVTX,
                      3: json.data[i].MATNR,
                      4: json.data[i].SERNR,
                      5: json.data[i].PARNR,
                      6: json.data[i].WORKSCOPE,
                      7: json.data[i].REVBD,
                      8: json.data[i].TAT,
                    })
                  }
                  /* set new token after request completed */
                  // localStorage.setItem("api_token", json.token);

                  /* Set User Permission */
                  return return_data;

              }
            },
            columnDefs: [
            {
              "targets": 8,
              "orderable": false
            }
          ]
            // columns: [       
            //     {data: 'no', name: 'no'},
            //     {data: 'REVNR', name: 'REVNR'}, 
            //     {data: 'ACREG_REVTX', name: 'ACREG_REVTX'},            
            //     {data: 'MATNR', name: 'MATNR', orderable: false},
            //     {data: 'SERNR', name: 'SERNR', orderable: false},
            //     {data: 'PARNR', name: 'PARNR', orderable: false},
            //     {data: 'WORKSCOPE', name: 'WORKSCOPE', orderable: false},
            //     {data: 'REVBD', name: 'REVBD', orderable: false},
            //     {data: 'TAT', name: 'TAT', orderable: false}
            // ],
    });

    $('select[name="status"]').on('change', function(){
      $('#thePeriodForm').submit();
    });
   // var status = $('select[name="status"]').val();

   //  $('#component_detail').html('<h5><i class="font-trans">loading project list ...</i></h5>');
   //  setTimeout(function () {
   //   $.get('<?php //echo base_url('/index.php/landing_gear/component_detail').'/80000050/1'; ?>', {status: status}, function(htmlcontent){
   //     $('#component_detail').html(htmlcontent);
   //   });
   //  }, 10); 
} );
</script>
