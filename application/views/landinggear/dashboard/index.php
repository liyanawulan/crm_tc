<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>
<!-- <section class="content-header">
<div class="panel panel-default">
   <h1 align="center">
        <u>Project List</u><br>
        Landing Gear Maintenance
    </h1>
</div>
</section> -->

<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-lg-12 col-xs-12 col-md-12">
        <div class="box box-primary box-solid">
          <!-- <div class="box-header with-border">
              <h3 class="box-title" style="text-align: center"></h3>
          </div> -->
          <div class="box-body box-primary">
            <table id="example" class="table-bordered table-hover table-striped" style="width:100%;">
             <thead style="background-color: #3c8dbc; color:#ffffff;">
                <tr>
                   <th>NO</th>
                   <th>NO REVISION</th>
                   <!-- <th>AIRCRAFT REGISTRATION</th>
                   <th>A/C TYPE</th> -->
                   <th>CUSTOMER NAME</th>
                   <th>PART NUMBER</th>
                   <th>SERIAL NUMBER</th>
                   <th>WORKSCOPE</th>
                   <th>START PROJECT</th>
                   <th>TAT</th>
                   <th>CURRENT TAT</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 0;
                if (is_array($listProject )) {
                 foreach ($listProject as $row) {
                  $no++;
                   ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><a href= "<?php echo base_url(); ?>index.php/Landing_gear/overview/<?php echo $row->REVNR.'/'. $row->REVNR; ?>" href='javascript:void(0);'><?php echo $row->REVNR; ?></a></td>
                    <!-- <td></td>
                    <td></td> -->
                    <td><?= "-" ?></td>
                    <td></td>
                    <td></td>
                    <td><?php echo $row->WORKSCOPE; ?></td>
                    <td><?php $source = $row->REVBD;
                        $date = new DateTime($source);
                        echo $date->format('d-m-Y'); // 31-07-2012?></td>
                    <td> - </td>
                    <td> - </td>
                    <!-- <td ><input type="hidden" id="start_<?php echo $no; ?>" value="<?php echo $row->CONTRACTUAL_TAT; ?>"/><?php echo $row->CONTRACTUAL_TAT; ?> Days</td> -->
                    <!-- <td id="color_curtat_<?php echo $no; ?>"><input type="hidden" id="end_<?php echo $no; ?>" value="<?php echo $row->CURRENT_PROGRESS_TAT; ?>"/><?php echo $row->CURRENT_PROGRESS_TAT; ?> Days</td> -->

                  </tr>
                <?php }} ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  //td color
  for (var i=1; i<=<?php echo $no; ?>; i++){
    var start_ =  document.getElementById('start_'+i).value;
    var end_ =  document.getElementById('end_'+i).value;
    if(end_ - start_ > 0){
      document.getElementById('color_curtat_'+i).style.backgroundColor='#f1c1c0';
    }else{
      document.getElementById('color_curtat_'+i).style.backgroundColor='#dbecc6';
    }
  }
  // DataTable
        var table = $('#example').DataTable({
            scrollY: "200px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>
