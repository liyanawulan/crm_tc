<div class="col-lg-12 col-xs-12 col-md-12">
  <div class="box box-primary box-solid">

    <div class="box-body box-primary">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="<?= (isset($type[1]))?$type[1]:'' ?>"><a href="<?= base_url('index.php/landing_gear/component_detail').'/'.$docno.'/'.'1' ?>" >Nose</a></li>
        <li role="presentation" class="<?= (isset($type[2]))?$type[2]:'' ?>"><a href="<?= base_url('index.php/landing_gear/component_detail').'/'.$docno.'/'.'2' ?>" >Main Landing Gear</a></li>
        <li role="presentation" class="<?= (isset($type[3]))?$type[3]:'' ?>"><a href="<?= base_url('index.php/landing_gear/component_detail').'/'.$docno.'/'.'3' ?>" >Left Landing Gear</a></li>
        <li role="presentation" class="<?= (isset($type[4]))?$type[4]:'' ?>"><a href="<?= base_url('index.php/landing_gear/component_detail').'/'.$docno.'/'.'4' ?>" >Right Landing Gear</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="Nose">
          <!-- Main content -->
         
              <div class="row">
                  <div class="box-body box-primary">
                      <table id="tb1" class="table table-bordered table-hover table-striped" style="width:100%;">
                       <thead style="background-color: #3c8dbc; color:#ffffff;">
                          <tr>
                            <th>No</th>
                            <th>No Revision</th> 
                            <th>Aircraft Registration</th>
                            <th>Part Number</th>
                            <th>Serial Number</th>
                            <th>Customer Name</th>
                            <th>Workskope</th>
                            <th>Start Project</th>
                            <th>TAT</th>
                            <!-- <th>Current Tat</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0;
                          if (is_array($listProject )) {
                           foreach ($listProject as $row) {
                            $no++;
                             ?>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><a href= "<?php echo base_url(); ?>index.php/Landing_gear/overview/<?php echo $row->REVNR.'/'. $row->REVNR; ?>" href='javascript:void(0);'><?php echo $row->REVNR; ?></a></td>
                              <!-- <td></td>
                              <td></td> -->
                              <td><?= "-" ?></td>
                              <td><?php echo $row->MATNR; ?></td>
                              <td><?php echo $row->SERNR; ?></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <!-- <td></td> -->

                            </tr>
                          <?php }} ?>
                        </tbody>
                      </table>
                    </div>
               <!--    </div>
                </div> -->
              </div>
          

        </div>
  
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

  // DataTable
        var table = $('#tb1').DataTable({
            scrollY: "200px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        }); 
} );
</script>
