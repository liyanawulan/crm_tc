<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>GMF - Component Services</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <!-- Logo Icon Web -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/LogoIcon.png">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

        <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/themes/bootstrap.min.css"/>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
      <!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
      
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Select2 -->
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
        <!-- DataTables -->
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css"> -->
        
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css"> -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/alt/AdminLTE-bootstrap-social.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
        <!-- Bootstrap 3.3.6 -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.css"/>
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css"> -->

        
        
        <!-- JQwidget -->
        <!-- <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/bower_components/jqwidgets/styles/jqx.base.css">
        <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/bower_components/jqwidgets/styles/jqx.darkblue.css"> -->
       
        
        <style>
            /*.example-modal .modal {
                position: relative;
                top: auto;
                bottom: auto;
                right: auto;
                left: auto;
                display: block;
                z-index: 1;
            }

            .example-modal .modal {
                background: transparent !important;
            }*/
            .title-hr {
                margin-bottom: 0px;
            }

            body {
                margin: 0;
                font-family: Roboto,sans-serif;
                font-size: 12px;
                font-weight: 300;
                line-height: 1.571429;
                color: #37474f;
                text-align: left;
                background-color: #fff;
            }

            .treeview-menu>li>a {
                padding: 5px 5px 5px 15px;
                display: block;
                font-size: 13px;
            }

            .nav-tabs > li {
                float: left;
                margin-bottom: -1px;
                font-size: 15px;
                font-family: Roboto,sans-serif;
                font-weight: 300;
            }


        .skin-black .sidebar-menu>li.active>a {
            border-left-color: #fff;
            /*border-top-color: #fff;
            border-bottom-color: #fff;*/
        }
        .skin-black .sidebar-menu>li:hover>a, .skin-black .sidebar-menu>li.active>a, .skin-black .sidebar-menu>li.menu-open>a {
            color: #fff;
            background: #1e282c;
        }
        .skin-black .sidebar-menu>li>a {
            border-left: 5px solid transparent;
           /* border-top: 1px solid transparent;
            border-bottom: 1px solid transparent;*/
        }

        #overlay{
          position:fixed;
          z-index:99999;
          top:0;
          left:0;
          bottom:0;
          right:0;
          background:rgba(0,0,0,0.9);
          transition: 1s 0.4s;
        }
        #progress{
          height:1px;
          background:#fff;
          position:absolute;
          width:0;
          top:50%;
        }
        #progstat{
          font-size:0.7em;
          letter-spacing: 3px;
          position:absolute;
          top:50%;
          margin-top:-40px;
          width:100%;
          text-align:center;
          color:#fff;
        }

        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
         /* position: fixed;
          left: 0px;
          top: 0px;
          width: 100%;
          height: 100%;
          z-index: 9999;*/
          background: url('<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double_Ring1.svg') center no-repeat rgba(0,0,0,0.9);
          position:fixed;
          z-index:99999;
          top:0;
          left:0;
          bottom:0;
          right:0;
        }

        /*div.dataTables_wrapper div.dataTables_processing {
           top: 50%;
        }*/
        div.dataTables_processing { z-index: 1; }

        </style>
        <!-- Jquery -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.js"></script>

        <!-- JavaScript -->
    <script src="<?= base_url() ?>assets/plugins/alertifyjs/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/alertify.min.css"/>
   
   
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
     -->
     <script type="text/javascript">
        //  $(window).load(function() {
        //     // Animate loader off screen
        //     $(".se-pre-con").fadeOut();
        // });
      alertify.set('notifier','position', 'top-center');

    </script>
    <script>
        // (function(){
        //   // $(window).load(function() {
        //   //   // Animate loader off screen
        //   //   $(".se-pre-con").fadeOut("slow");
        //   // });
        $(function() { // waiting for the page to bo be fully loaded
          $('.se-pre-con').fadeOut('slow', function() { // fading out preloader
            $(this).remove(); // removing it from the DOM
          });
        });
        
    </script>

    </head>


    <!-- <body class="hold-transition skin-black-light sidebar-mini fixed"> -->
    <body class="hold-transition skin-black sidebar-mini fixed">
        <!-- <div id="overlay">
            <div id="progstat"></div>
            <div id="progress"></div>
        </div> -->
        <div class="se-pre-con"></div>
        <!-- <div class="loader-wrapper" id="loader-1">
          <div id="loader"></div>
        </div> -->
        <div class="wrapper">
            <!-- <div class="se-pre-con"></div> -->
            <header class="main-header">

                <!-- Logo -->
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></b>
                    <b>CRM</b> Component</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                  
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                         
                             <?php if($this->session->userdata('log_sess_id_user_role')==1) { ?>
                                <li style="padding-top: 7px; background-color: #444444;">
                                    <select id="select2" class="form-control" style="width: 100%;>
                                        <option value="">All Customer</option>
                                    </select>
                                </li>
                            <?php }?>
                            <!-- User Account Menu -->
                            <li class="dropdown user user-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                  <!-- The user image in the navbar-->
                                  <img src="<?php echo base_url() ?>assets/dist/img/user/user1-128x128.jpg<?php //if($this->session->userdata('log_sess_user_image')) { echo $this->session->userdata('log_sess_userimage'); } else { echo 'user-man.png'; } ?>" class="user-image" alt="User Image">
                                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                  <span class="hidden-xs"><?php echo $this->session->userdata('log_sess_username'); ?></span>
                                </a>
                                <ul class="dropdown-menu" style="width:300px;">
                                  <!-- The user image in the menu -->
                                  <li class="user-header" style="height:200px;">
                                    <img src="<?php echo base_url() ?>assets/dist/img/user/user1-128x128.jpg<?php //if($this->session->userdata('log_sess_userimage')) { echo $this->session->userdata('log_sess_userimage'); } else { echo 'user-man.png'; } ?>" class="img-circle" alt="User Image">
                                    <p>
                                      <?php echo $this->session->userdata('log_sess_name').' - '.$this->session->userdata('log_sess_user_role'); ?>
                                    </p>
                                    <!-- <p class="text-left">
                                      <?php if($this->session->userdata('log_sess_id_user_role')==1) { ?>
                                        <small>Last Login : <?php echo $this->session->userdata('log_sess_last_login'); ?></small>
                                      <?php } elseif($this->session->userdata('log_sess_id_user_role')==2) { ?>
                                        <small>User Group : <?php echo $this->session->userdata('log_sess_user_group'); ?></small>
                                        <small>Last Login : <?php echo $this->session->userdata('log_sess_last_login'); ?></small>
                                      <?php } elseif($this->session->userdata('log_sess_id_user_role')==3) { ?>
                                        <!-- <small>User Group : <?php //echo $this->session->userdata('log_sess_user_group'); ?></small> -->
                                        <!-- <small>Cust. Name : <?php echo $this->session->userdata('log_sess_customer_name'); ?></small>
                                        <small>Last Login : <?php echo $this->session->userdata('log_sess_last_login'); ?></small>
                                      <?php } ?>
                                    </p> -->
                                  </li>
                                  <!-- Menu Footer-->
                                  <li class="user-footer">
                                    <div class="pull-left">
                                      <a href="<?php echo base_url('Profile'); ?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                      <a href="<?php echo base_url('index.php/Logout'); ?>" class="btn btn-default btn-flat">Logout</a>
                                    </div>
                                  </li>
                                </ul>
                            </li>
                            <!-- / User Account Menu -->
                        </ul>
                    </div>

                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url() ?>assets/dist/img/user/user1-128x128.jpg<?php //if($this->session->userdata('log_sess_user_image')) { echo $this->session->userdata('log_sess_userimage'); } else { echo 'user-man.png'; } ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $this->session->userdata('log_sess_name') ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                            <!-- <a href="#"><i class="fa fa-user"></i> Administrator</a> -->
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <?php if($this->session->userdata('log_sess_id_user_role')==1) { ?>
                     <!--    <li <?php if($this->uri->segment(1)=='Home_control') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Dashboard">
                                <i class="fa fa-home"></i> <span>Dashboard</span>
                            </a>
                        </li> -->

                        <li <?php if($this->uri->segment(1)=='Users') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Users">
                                <i class="fa fa-users"></i> <span> Users</span>
                            </a>
                        </li>
                        <li <?php if($this->uri->segment(1)=='customers') { echo 'class="active"'; } ?>>
                        	<a href="<?php echo base_url() ?>index.php/Customers"> 
                        		<i class="fa fa-users"></i><span> Customers</span>
                        	</a>
                        </li>
                        <?php } ?>

                        <?php if(($this->session->userdata('log_sess_id_user_role')==1 || $this->session->userdata('log_sess_id_user_role')==3) || ($this->session->userdata('log_sess_id_user_role')==2 && $this->session->userdata('log_sess_id_user_group')==1)) { ?>
                        <li class="treeview <?php if(($this->uri->segment(1)=='Pooling') or ($this->uri->segment(1)=='Contract'))  { echo 'active"'; } ?>">
                            <!-- <a href="<?php echo base_url() ?>index.php/Pooling">
                                <i class="fa fa-object-ungroup"></i> <span>Pooling PBTH</span> -->
                            <!-- </a> -->
                              <a href="#">
                                <i class="fa fa-object-ungroup"></i> <span>Pooling</span>
                                <span class="pull-right-container">
					              <i class="fa fa-angle-left pull-right"></i>
					            </span>		
                            </a>
                            <ul class="treeview-menu">
					            <li>
					            	<a href="<?php echo base_url(); ?>index.php/Pooling/list_order"> 
					            	<i class="fa fa-list"></i>
                                    <span>List Order</span></a>
					            </li>
						          <?php if ($this->session->userdata('log_sess_id_user_role')==3) {
						          ?>
						          <li>
                                    <a href="<?php echo base_url(); ?>index.php/Pooling/create_order">
                                    <i class="fa fa-edit"></i>
                                    <span>Create Order</span></a>
                                  </li>
						        <?php } else { ?>
						          <li><a href="<?php echo base_url(); ?>index.php/Contract"> 
                                    <i class="fa fa-pencil"></i>
                                    <span>Create Contract</span></a></li>
						        
						        <li><a href="<?php echo base_url(); ?>index.php/Contract/list_contract">
                                    <i class="fa fa-list"></i>
                                    <span>List Contract</span></a></li>
						        <li class="treeview">
						            <a href="#"> 
                                        <i class="fa fa-circle-o"></i>Configuration
								        <span class="pull-right-container">
							              <i class="fa fa-angle-left pull-right"></i>
							            </span>	
							        </a>
						            <ul class="treeview-menu">
						              <li><a href="<?php echo base_url(); ?>index.php/master/Part">
                                        <i class="fa fa-cubes"></i>
                                        <span> Part Number </span></a></li>
						              <li><a href="<?php echo base_url(); ?>index.php/master/Aircraft">
                                        <i class="fa fa-plane"></i>
                                        <span> Aircraft Registration </span></a></li>
						              <li><a href="<?php echo base_url(); ?>index.php/master/ReqCategory">
                                        <i class="fa fa-sitemap"> </i>
                                        <span>Requirement Category</span></a></li>
						              <!-- <li role="presentation"><a class="fa fa-user" href="<?php echo base_url(); ?>index.php/Users/list_user"> User</a></li> -->
						            </ul>
						          </li>
                                  <?php } ?>
                                   <li> <a href="<?php echo base_url(); ?>index.php/Pooling/report_serviceable_level/"> <i class="fa fa-bar-chart"></i>
                                    <span>Serviceable Level</span></a></li>
                                  <li> <a href="<?php echo base_url(); ?>index.php/Pooling/report_unserviceable_level/"> <i class="fa fa-bar-chart"></i>
                                    <span>Unserviceable Level</span></a></li>
					        </ul>
                        </li>
                        <?php } ?>

                        <?php if(($this->session->userdata('log_sess_id_user_role')==1 || $this->session->userdata('log_sess_id_user_role')==3) || ($this->session->userdata('log_sess_id_user_role')==2 && $this->session->userdata('log_sess_id_user_group')==2)) { ?>
                        <li class="treeview <?php if($this->uri->segment(1)=='Loan_exchange') { echo 'active"'; } ?>">
                            <a href="#">
                                <i class="fa fa-usd"></i> <span>Loan Exchange</span>
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>     
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/Loan_exchange">
                                        <i class="fa fa-list"></i> <span> All Order </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>index.php/Loan_exchange/loan_request">
                                        <i class="fa fa-list"></i> <span> All Request </span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"> 
                                        <i class="fa fa fa-edit"></i> Form Request
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span> 
                                    </a>
                                    <ul class="treeview-menu">
                                      <li><a href="<?php echo base_url(); ?>index.php/Loan_exchange/form_request_loan/"><i class="fa fa-circle-o"></i>
                                      <span> Form Request Loan </span></a></li>
                                      <li><a href="<?php echo base_url(); ?>index.php/Loan_exchange/form_request_exchange/"><i class="fa fa-circle-o"></i>
                                      <span> Form Request Exchange </span></a></li>
                                      <!-- <li role="presentation"><a class="fa fa-user" href="<?php echo base_url(); ?>index.php/Users/list_user"> User</a></li> -->
                                    </ul>
                                </li>
                            </ul>   
                        </li>
                        <?php } ?>

                        <?php if(($this->session->userdata('log_sess_id_user_role')==1 || $this->session->userdata('log_sess_id_user_role')==3) || ($this->session->userdata('log_sess_id_user_role')==2 && $this->session->userdata('log_sess_id_user_group')==3)) { ?>
                        <li <?php if($this->uri->segment(1)=='Retail') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Retail">
                                <i class="fa fa-tags"></i> <span> Retail </span>
                            </a>
                        </li>
                        <?php } ?>

                        <?php if(($this->session->userdata('log_sess_id_user_role')==1 || $this->session->userdata('log_sess_id_user_role')==3) || ($this->session->userdata('log_sess_id_user_role')==2 && $this->session->userdata('log_sess_id_user_group')==4)) { ?>
                        <li <?php if($this->uri->segment(1)=='Landing_gear') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Landing_gear"><i class="fa fa-gears"></i> <span> Landing Gear</span>
                                <!--<i class="fa fa-angle-left pull-right"></i>-->
                            </a>
                            <!--<ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/Dashboard2">
                                        <i class="fa fa-folder-o"></i> <span> Project List</span>
                                    </a>
                                </li>
                            </ul>-->
                        </li>
                        <?php } ?>

                        <li <?php if($this->uri->segment(1)=='Csi_survey') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Csi_survey">
                                <i class="fa fa-table"></i><span> CSI Survey </span>
                            </a>

                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view($content); ?>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2018.</strong> All rights
                reserved.
            </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

        </div>
        <!-- ./wrapper -->


        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Select2 -->
    	<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
        <!-- FastClick -->
        <!-- <script src="<?php //echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script> -->
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
        <!-- SlimScroll -->

        <!-- <script src="<?php //echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script> -->
        <!-- ChartJS -->
        <script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.js"></script>
        <!-- <script src="<?php //echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    	<script src="<?php //echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script> -->

        <script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
        <!-- <script src="<?php //echo base_url(); ?>assets/bower_components/morris.js/morris.min.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>

    
    

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>
        
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        

       <!--  <script>
		$(function () {
		$("#example").DataTable();
		});
		</script> -->

        <script>
            $(document).ready(function(){
              $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
              });

                $('#select2').on('change', function(){
                  var ID_CUSTOMER = $(this).val();
                

                  console.log(ID_CUSTOMER);
                });

                setCustomerSearch();

            });

            function setCustomerSearch($search = ''){
                var sSelect = $('#select2').select2({
                     placeholder: "All Customers"
                });
                $.ajax({
                    type: 'POST',
                    url: '<?= site_url("api/Customers/datalist") ?>/'+$search,
                    dataType: 'json'
                }).then(function (data) {
                    // create the option and append to Select2

                    $.each(data, function(index, el){

                        var option = new Option(el.COMPANY_NAME, el.ID_CUSTOMER, true, true);
                        sSelect.append(option);
                    });

                    // manually trigger the `select2:select` event
                    // studentSelect.trigger({
                    //     type: 'select2:select',
                    //     params: {
                    //         id: data.ID_CUSTOMER,
                    //         text: data.COMPANY_NAME,
                    //     }
                    // });
                });
            }
             // function notif(type, msg) {
             //        toastr.options = {
             //            "closeButton": true,
             //            "debug": false,
             //            "newestOnTop": true,
             //            "progressBar": true,
             //            "positionClass": "toast-top-right",
             //            "preventDuplicates": false,
             //            "onclick": null,
             //            "showDuration": "300",
             //            "hideDuration": "1000",
             //            "timeOut": "5000",
             //            "extendedTimeOut": "1000",
             //            "showEasing": "swing",
             //            "hideEasing": "linear",
             //            "showMethod": "fadeIn",
             //            "hideMethod": "fadeOut"
             //        };
             //        switch (type) {
             //            case "success":
             //                toastr.success(msg);
             //                break;
             //            case "error":
             //                toastr.error(msg);
             //                break;
             //            case "warning":
             //                toastr.warning(msg);
             //                break;
             //            default:
             //        }
             //    }
        </script>


    </body>
    
</html>
