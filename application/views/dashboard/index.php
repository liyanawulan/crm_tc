<section class="content-header">
    <h1>
        Component and Material
        <!--<small>Slider</small>-->
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content" style="background:" img src="<?php echo base_url(); ?>assets/dist/img/Garuda-Indonesia_GMF_Aero.jpg" width="50">
    <!-- Info boxes -->
    <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning box-solid">
        <!-- <div class="box box-success"> -->
        <div class="box-header with-border">
          <h3 class="box-title" style="text-align: center">Retail Order</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-lg-6 col-xs-12">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">TAT ACHIEVEMENT</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                       <div id="chartdiv2" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
                  </div>     
                </div>
              </div>
            </div>  
            <div class="col-lg-6 col-xs-12">
               <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">STATUS ORDER</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                   <div id="chartdiv" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
                </div>   
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- </section>

<section class="content" style="background:" img src="<?php echo base_url(); ?>assets/dist/img/Garuda-Indonesia_GMF_Aero.jpg" width="50"> -->
   <div class="row">
    <div class="col-md-12">
      <div class="box box-warning box-solid">
        <!-- <div class="box box-success"> -->
        <div class="box-header with-border">
          <h3 class="box-title" style="text-align: center">Pooling Order</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-lg-6 col-xs-12">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Serviceable SL</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="chart" width="800" height="450" style="height: 300px;"></canvas>
                  </div>    
                </div>
              </div>
            </div>  
            <div class="col-lg-6 col-xs-12">
               <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Unserviceable SL</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="chart">
                   <canvas id="chart2" width="800" height="450" style="height: 300px;"></canvas>
                </div>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/pie.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>
<!-- <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> -->


<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>

<script type="text/javascript">
  var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "country": "Delivered",
    "value": 40
  }, {
    "country": "Quotation",
    "value": 20
  }, {
    "country": "Approval",
    "colors": "#f44336",
    "value": 10
  }, {
    "country": "Ready",
    "value": 30
    
  } ],
  "valueField": "value",
  "titleField": "country",
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "colors": [
            "#2E7D32",
            "#29B6F6",
            "#f44336",
            "#FFA726"
          ],
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 30,
  "export": {
    "enabled": true
  }
} );
</script>

<script type="text/javascript">
      AmCharts.makeChart("chartdiv2",
        {
          "type": "serial",
          "categoryField": "category",
          "angle": 30,
          "depth3D": 30,
          "colors": [
            "#1a75ff",
            "#80b3ff",
          ],
          "startDuration": 1,
          "categoryAxis": {
            "gridPosition": "start"
          },
          "trendLines": [],
          "graphs": [
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-1",
              "precision": 0,
              "title": "Process",
              "type": "column",
              "valueField": "column-1"
            },
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-2",
              "title": "Max.Limit 100%",
              "type": "column",
              "valueField": "column-2"
            }
          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "stackType": "100%",
              "title": ""
            }
          ],
          "allLabels": [],
          "balloon": {},
          "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
          "titles": [],
          "dataProvider": [
            {
              "category": ">= 21 days",
              "column-1": "12",
              "column-2": "88"
            },
            {
              "category": ">= 10 days <= 20 days",
              "column-1": "78",
              "column-2": "22"
            },
            {
              "category": "<= 9 days",
              "column-1": "54",
              "column-2": "46"
            }
          ]
        }
      );
    </script>
<script type="text/javascript">
 var ctx = document.getElementById('chart');

var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['AOG','CATEGORY A', 'CATEGORY B', 'CATEGORY C', 'CATEGORY D', 'REP AOG', 'SCHEDULED'],
    datasets: [
      {
        label: 'Level Result',
        data: [100, 100, 100, 100, 100, 90, 100],
        backgroundColor:  '#ccebff',
        borderColor:'#66c2ff',
        borderWidth : 2,
        hoverBackgroundColor:'#66c2ff',
        hoverBorderColor :'#ccebff'
      }
      // ,
      // {
      //   label: 'Max Limit 100%',
      //   data: [100-12, 100-78, 100-49.8],
      //   backgroundColor: '#FFF9C4',
      // }
    ],

  },
  options: {
    scales: {
      xAxes: [{ stacked: true }],
      yAxes: [{ stacked: true }]
    }
  }
});
</script>
<script type="text/javascript">
 var ctx = document.getElementById('chart2');

var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['AOG','CATEGORY A', 'CATEGORY B', 'CATEGORY C', 'CATEGORY D', 'REP AOG', 'SCHEDULED'],
    datasets: [
      {
        label: 'Level Result',
        data: [100, 100, 100, 100, 100, 90, 100],
        backgroundColor:  '#ffcce6',
        borderColor:'#ff99cc',
        borderWidth : 2,
        hoverBackgroundColor:'#ff99cc',
        hoverBorderColor :'#ffcce6',
      }
      // ,
      // {
      //   label: 'Max Limit 100%',
      //   data: [100-12, 100-78, 100-49.8],
      //   backgroundColor: '#FFF9C4',
      // }
    ],
  },
  options: {
    scales: {
      xAxes: [{ stacked: true }],
      yAxes: [{ stacked: true }]
    }
  }
});
</script>
