<section class="content-header">
    <h1>
        Component and Material
        <!--<small>Slider</small>-->
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content" style="background:" img src="<?php echo base_url(); ?>assets/dist/img/Garuda-Indonesia_GMF_Aero.jpg" width="50">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row" style="width:100%; margin:0 auto;">
      <div class="form-horizontal">
        <div class="col-md-6">
          <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">TAT ACHIEVEMENT</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                   <div id="chartdiv2" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
              </div>     
            </div>
          </div>
        </div>  
        <div class="col-md-6">
           <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">STATUS ORDER</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                  <div id="chartdiv2" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
              </div>     
            </div>
          </div>
        </div>
      </div>
                        
    </div>       <!-- /.row -->
           

  <!-- ./box-body -->


   <div class="box-footer"> <!-- /.box-header -->
    <div class="col-md-12">
     <div class="row">

        <div class="box-header with-border" style="background: white">
         <h3 class="box-title">Summary Table</h3>              
          <div class="box-tools pull-right">
           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>        <!-- /.box-tools -->
        </div>          <!-- /.box-header -->

        
        <div class="col-sm-2 col-xs-6" style="background: #2c3e50">
         <div class="description-block border-right">
          <h5 class="description-header" style="color:#FFF">MAINTENANCE</h5>
          <span class="description-text" style="color:#FFF"> MARKET</span>
         </div>         <!-- /.description-block -->
        </div>          <!-- /.col -->

        <div class="col-sm-2 col-xs-6" style="background: #2c3e50">
         <div class="description-block border-right">
          <h5 class="description-header" style="color:#FFF"">Waiting Approved</h5> <!-- greenyellow -->
          <span class="description-percentage" style="color:#FFF""><!-- <i class="fa fa-caret-up"></i> 24%-->
            <b><?php echo $total_waiting_approved; ?></b>
          </span>
         </div>         <!-- /.description-block -->
        </div>          <!-- /.col -->

        <div class="col-sm-2 col-xs-6" style="background: #2c3e50">
         <div class="description-block border-right">
          <h5 class="description-header" style="color:#FFF">Waiting Material</h5> <!-- aqua -->
          <span class="description-percentage" style="color:#FFF"><!-- <i class="fa fa-caret-left"></i> 22%-->
            <b><?php echo $total_waiting_material; ?></b>
          </span>                   
         </div>         <!-- /.description-block -->
        </div>          <!-- /.col -->

        <div class="col-sm-2 col-xs-6" style="background: #2c3e50">
         <div class="description-block border-right">
          <h5 class="description-header" style="color:#FFF">Under Maintenance</h5> <!-- yellow -->
          <span class="description-percentage" style="color:#FFF"><!-- <i class="fa fa-caret-up"></i> 20% -->
            <b><?php echo $total_under_maintenance; ?></b>
           </span>                   
         </div>         <!-- /.description-block -->
        </div>          <!-- /.col -->

        <div class="col-sm-2 col-xs-6" style="background: #2c3e50">
         <div class="description-block border-right">
           <h5 class="description-header" style="color:#FFF">Waiting Repair</h5> <!-- red -->
           <span class="description-percentage" style="color:#FFF"><!-- <i class="fa fa-caret-up"></i> 20% -->
            <b><?php echo $total_waiting_repair; ?></b>
           </span>                   
         </div>         <!-- /.description-block -->
        </div>          <!-- /.col -->

        <div class="col-sm-2 col-xs-6" style="background: #2c3e50">
         <div class="description-block border-right">
           <h5 class="description-header" style="color: #FFF">Finished</h5> <!-- //violet -->
           <span class="description-percentage" style="color:#FFF"><!-- <i class="fa fa-caret-down"></i> 22% -->
            <b><?php echo $total_finished; ?></b>
           </span>                   
         </div>         <!-- /.description-block -->
        </div>          <!-- /.col -->
        
        </div>          <!-- /.row -->
    </div>              <!-- /.col -->    
   </div>               <!-- /.box-footer -->
            
            
    <!-- Open Main 3 row -->
    <div class="row">
        <div class="col-md-6">
          <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">STATUS ORDER</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>    <!-- /.box-tools -->
            </div>      <!-- /.box-header -->
            <div class="box-body chart-responsive">
              <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
            </div>      <!-- /.box-body -->
          </div>        <!-- /.box -->
        </div>          <!-- /.col -->

        <div class="col-md-6">
            <!-- /.Bar Chart-->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">TAT ACHIEVEMENT</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>  <!-- /.box-tools -->
              </div>    <!-- /.box-header -->

              <div class="box-body">
                <div class="chart">
                   <canvas id="chart" width="800" height="450" style="height: 300px;"></canvas>
                </div>  <!-- /.chart -->
              </div>    <!-- /.box-body -->
            </div>      <!-- /.box-info -->
        </div>          <!-- /.col -->  
         
    </div>              <!-- /.row -->
    <!-- Closed Main 3 row -->

    <!-- BAR CHART -->
  <!-- <div class="row">
    <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">TAT AGING</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="bar-chart1" style="height:250px"></div>
            </div>      
          </div>          
    </div> -->

    <!-- <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              <div class="chart-responsive"> -->
                 <!-- <canvas id="chart2" style="height: 500px;"></canvas> -->
              <!--</div> -->  <!-- /.chart -->
            <!-- </div>  
          </div> -->
    </div>
  </div>
</div>



</section>


<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>


<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script> /*Bar Chart*/
new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: [">41 days <60 days", ">31 days <40 days", ">20 days <30 days", ">10 days <20 days", "<9 days"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'TAT ACHIEVEMENT'
      }
    }
});
</script>


<!-- <script> alert("tes") </script> -->
<!-- DONUT HART -->
<script>
  $(function () {
    "use strict";    

    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#AD1457", "#2E7D32", "#FF8F00", "#D84315", "#1565C0"],
      data: [
        {label: "Waiting Approved", value: 12},
        {label: "Waiting Material", value: 30},
        {label: "Under Maintenance", value: 20},
        {label: "Waiting Repair", value: 34},
        {label: "Finished", value: 22}
      ],
      hideHover: 'auto'
    });

    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart1',
      resize: true,
      data: [
        {y: '0-30 Days', a: 380920.50/*, b: 72*/},
        {y: '>30 Days', a: 928684.22},
        {y: '>60 Days', a: 1794228.72},
      ],
      barColors: ['#03A9F4'/*, '#FFC107'*/],
      xkey: 'y',
      ykeys: ['a'/*, 'b'*/],
      labels: ['Total',/* 'Series2'*/],
      hideHover: 'auto'
    });
    
  });
</script>


<!-- <script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script> -->

<!-- STACKEDBAR -->
<script type="text/javascript">
 var ctx = document.getElementById('chart');

var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['>21 Days','>10 Days <20 Days', '<9 Days'],
    datasets: [
      {
        label: 'Process',
        data: [12, 78, 49.8],
        backgroundColor: '#C2185B',
      },
      {
        label: 'Max Limit 100%',
        data: [100-12, 100-78, 100-49.8],
        backgroundColor: '#FFF9C4',
      }
    ],
  },
  options: {
    scales: {
      xAxes: [{ stacked: true }],
      yAxes: [{ stacked: true }]
    }
  }
});
</script>

<script type="text/javascript">
  var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "country": "Delivered",
    "value": <?php echo $total_Delivered; ?>
  }, {
    "country": "Quotation",
    "value": <?php echo $total_Quotation_provide_awaiting_approval; ?>
  }, {
    "country": "Approval",
    "colors": "#f44336",
    "value": <?php echo $total_Approval_received_repair_started; ?>
  }, {
    "country": "Ready",
    "value": <?php echo $total_Ready_to_deliver; ?>
    
  } ],
  "valueField": "value",
  "titleField": "country",
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "colors": [
            "#2E7D32",
            "#29B6F6",
            "#f44336",
            "#FFA726"
          ],
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 30,
  "export": {
    "enabled": true
  }
} );
</script>

<script type="text/javascript">
      AmCharts.makeChart("chartdiv2",
        {
          "type": "serial",
          "categoryField": "category",
          "angle": 30,
          "depth3D": 30,
          "colors": [
            "#0000CC",
            "#0D8ECF"
          ],
          "startDuration": 1,
          "categoryAxis": {
            "gridPosition": "start"
          },
          "trendLines": [],
          "graphs": [
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-1",
              "precision": 0,
              "title": "Process",
              "type": "column",
              "valueField": "column-1"
            },
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-2",
              "title": "Max.Limit 100%",
              "type": "column",
              "valueField": "column-2"
            }
          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "stackType": "100%",
              "title": ""
            }
          ],
          "allLabels": [],
          "balloon": {},
          "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
          "titles": [],
          "dataProvider": [
            {
              "category": "> 20 days",
              "column-1": "12",
              "column-2": "88"
            },
            {
              "category": "> 9 days < 21 days",
              "column-1": "78",
              "column-2": "22"
            },
            {
              "category": "< 10 days",
              "column-1": "54",
              "column-2": "46"
            }
          ]
        }
      );
    </script>