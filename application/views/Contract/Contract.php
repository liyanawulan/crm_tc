<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<!-- <div class="nav-tabs-custom"> -->
				<!-- <?php //$this->load->view($nav_tabs); ?> -->
				<!-- /.Grocery CRUD -->
				<!-- <div class="tab-content"> -->
					<section class="content">
						<div class="box-header">
              <!-- <h3 class="box-title"><?php echo $title; ?></h3> -->
              <div class="box-tools">
              </div>
            </div>
            <div class="box-body no-padding">
			         <div class="section">
                 <form id="form-contract" enctype="multipart/form-data" method="post" action="<?= site_url('contract/create') ?>" class="form-horizontal">
                   <div class="col-md-12">
                     <div class="col-md-6">
                         <div class="form-group">
                           <label class="col-sm-5 control-label">Contract Number</label>
                           <div class="col-sm-7">
                             <input type="text" class="form-control input-sm" id="CONTRACT_NUMBER" name="CONTRACT_NUMBER">
                           </div>
                         </div>
                         <div class="form-group">
                           <label class="col-sm-5 control-label">Delivery Address</label>
                           <div class="col-sm-7">
                             <textarea style="min-height: 130px;" class="form-control input-sm" id="DELIVERY_ADDRESS" name="DELIVERY_ADDRESS"></textarea>
                           </div>
                         </div>
                         <div class="form-group">
                           <label class="col-sm-5 control-label">Description Contract</label>
                           <div class="col-sm-7">
                             <textarea style="min-height: 130px;" class="form-control input-sm" id="DESC_CONTRACT" name="DESC_CONTRACT"></textarea>
                           </div>
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group">
                           <label class="col-sm-5 control-label">Customer</label>
                           <div class="col-sm-7">
                             <!-- <input type="text" class="form-control input-sm" id="CUSTOMER" name="CUSTOMER"> -->
                             <select class="form-control select2" id="CUSTOMER" name="CUSTOMER" style="width: 100%;" required>
                               <option value="">Choose Customer</option>
                               <?php
                                 foreach ($customer_list as $row) {
                               ?>
                               <option value="<?php echo $row->ID_CUSTOMER ?>"><?php echo $row->COMPANY_NAME; ?></option>
                               <?php } ?>
                             </select>
                           </div>
                         </div>
                         <div class="form-group">
                           <label class="col-sm-5 control-label">Delivery Point</label>
                           <div class="col-sm-7">
                             <textarea style="min-height: 130px;" class="form-control input-sm" id="DELIVERY_POINT" name="DELIVERY_POINT"></textarea>
                           </div>
                         </div>
                     </div>
                   </div>
                   <div class="col-md-12">
                     <div class="pull-right">
                       <button class="btn btn-primary" type="button" id="btnSubmitContract" > <i class="fa fa-save"> </i> Create</button>
                     </div>
                   </div>
                 </form>
               </div>
               <div class="section">
                 <div class="col-md-12">
                   <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Fleet</a></li>
                      <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Part Number List</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="home">
                          <div class="row" style="padding-bottom: 30px; padding-top: 10px;">
                            <div class="box">
                              <div class="box-body">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="col-md-3">
                                      <div class="col-md-12">
                                        <label class="">Aircraft Registration</label>
                                      </div>
                                      <!-- <div class="col-md-12">
                                        <input class="form-control" id="Aircraft_Registration" name="Aircraft_Registration" placeholder="airraft registration">
                                      </div> -->
                                    </div>
                                    <div class="col-md-3">
                                      <div class="col-md-12">
                                        <label class="">Type</label>
                                      </div>
                                      <!-- <div class="col-md-12">
                                        <input class="form-control" id="type" name="type" placeholder="type">
                                      </div> -->
                                    </div>
                                    <div class="col-md-2">
                                      <div class="col-md-12">
                                        <label class="">MSN</label>
                                      </div>
                                      <!-- <div class="col-md-12">
                                        <input class="form-control" id="msn" name="msn" placeholder="MSN">
                                      </div> -->
                                    </div>
                                    <div class="col-md-2">
                                      <div class="col-md-12">
                                        <label class="">MFG DATE</label>
                                      </div>
                                      <!-- <div class="col-md-12">
                                        <input class="form-control" id="mfggate" name="mfggate" placeholder="MFG GATE">
                                      </div> -->
                                    </div>

                                  </div>
                                </div>
                                <div class="row">
                                  <div id="form-fleet" class="col-md-12">
                                    <div class="col-md-3">
                                      <!-- <div class="col-md-12">
                                        <label class="label">Aircraft Registration</label>
                                      </div> -->
                                      <div class="col-md-12">
                                        <!-- <input class="form-control" id="Aircraft_Registration" name="Aircraft_Registration" placeholder="airraft registration"> -->
                                        <select class="form-control select2" id="aircraft_registration" name="aircraft_registration" style="width: 100%;" required>
                                          <option value="">Choose Aircraft Reg.</option>
                                          <?php
                                            foreach ($aircraft_registration_list as $row_ar) {
                                          ?>
                                          <option value="<?php echo $row_ar->ID_AIRCRAFT_REGISTRATION ?>"><?php echo $row_ar->AIRCRAFT_REGISTRATION; ?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <!-- <div class="col-md-12">
                                        <label class="label">Type</label>
                                      </div> -->
                                      <div class="col-md-12">
                                        <input class="form-control" id="type" name="type" placeholder="type">
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <!-- <div class="col-md-12">
                                        <label class="label">MSN</label>
                                      </div> -->
                                      <div class="col-md-12">
                                        <input class="form-control" id="msn" name="msn" placeholder="MSN">
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                      <!-- <div class="col-md-12">
                                        <label class="label">MFG GATE</label>
                                      </div> -->
                                      <div class="col-md-12">
                                        <input class="form-control" id="mfggate" name="mfggate" placeholder="YYYY-MM-DD">
                                      </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" id="btnFleet" type="button" ><i class="fa fa-plus"></i></button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="box">
                              <div class="box-body">
                              <table id="tbFleet" class="table table-bordered table-hover table-striped">
                                <thead>
                                  <tr>
                                    <!-- <th> No </th> -->
                                    <th> Aircraft Registration </th>
                                    <th> Type </th>
                                    <th> MSN </th>
                                    <th> MFG Date </th>
                                    <th> Act </th>
                                  </tr>
                                </thead>
                                <tbody>

                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="profile">
                        <div class="box">

                          <div class="box-body">
                            <div class="row">
                            <div class="row">
                              <form id="form-pn" class=" form-horizontal">
                                <div  class="col-md-12">

                                <div class="col-md-5">
                                  <div class="form-group">
                                    <label class="col-sm-5 control-label">ATA</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" name="ata" id="ata">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-5 control-label">Part Number</label>
                                    <div class="col-sm-6">
                                      <select class="form-control input-sm select2" id="part_number" name="part_number" style="width: 100%;" required>
                                        <option value="">Search For Part Number</option>
                                        <?php
                                          foreach ($part_number_list as $row_pn) {
                                        ?>
                                        <option value="<?php echo $row_pn->ID_PN ?>"><?php echo $row_pn->PN_NAME; ?></option>
                                        <?php } ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-5 control-label">Description</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" name="description" id="description">
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-5">
                                  <div class="form-group">
                                    <label class="col-sm-5 control-label">ESS</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" name="ess" id="ess">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-5 control-label">MEL</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" name="mel" id="mel">
                                    </div>
                                  </div>
                                  <div class="form-group form-inline">
                                    <label class="col-sm-5 control-label">TARGET SLA</label>
                                    <div class="col-sm-7">
                                      <input type="text" class="form-control input-sm" name="sla" id="sla">
                                      <select class="form-control input-sm select2" id="day_sla" name="day_sla" required>
                                        <option value="hour">Hours</option>
                                        <option value="day">Days</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-2">
                                    <button class="btn btn-primary" id="btnPN" type="button" ><i class="fa fa-plus"></i></button>
                                </div>
                              </div>

                            </form>
                            </div>
                          </div>
                        </div>

                        <div class="box">
                          <div class="box-body">
                          <table id="tbPartNumber" class="table table-bordered table-hover table-striped">
                            <thead>
                              <tr>
                                <th> ATA </th>
                                <th> Part Number </th>
                                <th> Description </th>
                                <th> ESS </th>
                                <th> MEL </th>
                                <th> Target SLA </th>
                                <th> Act </th>
                              </tr>
                            </thead>
                            <tbody>

                            </tbody>
                          </table>
                        </div>
                      </div>
                      </div>
                    </div>

                  </div>
                 </div>
               </div>
            </div>
					</section>
				<!-- </div> -->
				<!-- /.tab-pane -->
			<!-- </div> -->
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>

var table;
var tableFleet;
var tablePartNumber;

  $(function () {
    //datatables
    tableFleet = $('#tbFleet').DataTable({
      "paging":   false,
      "ordering": false,
      "info":     false,

    });
    tablePartNumber = $('#tbPartNumber').DataTable({
      "paging":   false,
      "ordering": false,
      "info":     false,

    });

    tableFleet.clear().draw();
    tablePartNumber.clear().draw();

    table = $('#tbCategory').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('api/customers')?>",
            "type": "POST",
            "dataSrc" : function(json){
              var return_data = [];

              json.draw = json.draw;
              json.recordsFiltered = json.recordsFiltered;
              json.recordsTotal = json.recordsTotal;

                for(var i=0;i< json.data.length; i++){
                  return_data.push({
                    0: json.data[i].NO,
                    9: json.data[i].RowNum,
                    1: json.data[i].COMPANY_NAME,
                    2: json.data[i].COMPANY_EMAIL,
                    3: json.data[i].COMPANY_ADDRESS
                  })
                }
                return return_data;
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          {
            "targets": [],
            "visible": false,
            "searchable": false
          },{
            "targets": -1,
            "className": "dt-center",
            "data": null,
            "defaultContent":
            '<button title="update" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button>'+
            '<button title="delete" class="btUpdate btn btn-danger btn-xs" type="button"><i class="fa fa-times"></i></button>'
          }]

    });

    $('#btnSubmitContract').on('click', function(e){

      var dataFleet = tableFleet.rows().data();
      var dataPartNumber = tablePartNumber.rows().data();

      $.each(dataFleet, function(index, val){
        AIRCRAFT_REGISTRATION = $("<input>").attr('hidden', 'true').attr("name", "fleet[AIRCRAFT_REGISTRATION][]").val(val[10]);
        $('#form-contract').append(AIRCRAFT_REGISTRATION);
        TYPE = $("<input>").attr('hidden', 'true').attr("name", "fleet[TYPE][]").val(val[1]);
        $('#form-contract').append(TYPE);
        MSN = $("<input>").attr('hidden', 'true').attr("name", "fleet[MSN][]").val(val[2]);
        $('#form-contract').append(MSN);
        MFN_GATE = $("<input>").attr('hidden', 'true').attr("name", "fleet[MFN_GATE][]").val(val[3]);
        $('#form-contract').append(MFN_GATE);
      });

      $.each(dataPartNumber, function(index, val){
        ATA = $("<input>").attr('hidden', 'true').attr("name", "pn[ATA][]").val(val[10]);
        $('#form-contract').append(ATA);
        ID_PN = $("<input>").attr('hidden', 'true').attr("name", "pn[ID_PN][]").val(val[10]);
        $('#form-contract').append(ID_PN);
        ESS = $("<input>").attr('hidden', 'true').attr("name", "pn[ESS][]").val(val[1]);
        $('#form-contract').append(ESS);
        MEL = $("<input>").attr('hidden', 'true').attr("name", "pn[MEL][]").val(val[2]);
        $('#form-contract').append(MEL);
        TARGET_SLA = $("<input>").attr('hidden', 'true').attr("name", "pn[TARGET_SLA][]").val(val[3]);
        $('#form-contract').append(TARGET_SLA);
      });


      $( "#form-contract" ).submit();

    });

    ////=============================
    // INISTIALISASI TABLE FLEET
    $('#btnFleet').on('click', function(e){

      $type = $('#form-fleet #type').val();
      $msn = $('#form-fleet #msn').val();
      $mfggate = $('#form-fleet #mfggate').val();
      $aircraft_registration = $('#form-fleet #aircraft_registration').val();

      if ($type && $msn && $mfggate && $aircraft_registration) {

          $data = {
            '0' : $('#form-fleet #aircraft_registration option:selected').text(),
            '1' : $type,
            '2' : $msn,
            '3' : $mfggate,
            '4' : '<button class="btn btn-danger btn-sm btnRemove" type="button"><i class="fa fa-remove"></i></button> ',
            '10' : $aircraft_registration,
          };

          addRow(tableFleet, $data);

      }else if (!$type) {
        $('#type').focus();
      }else if (!$msn) {
        $('#msn').focus();
      }else if (!$mfggate) {
        $('#mfggate').focus();
      }
    });

    $('#tbFleet ').on( 'click','tbody tr button.btnRemove' ,function () {

      tableFleet
          .row( $(this).parents('tr') )
          .remove()
          .draw();
    } );

    ////=============================
    // INISTIALISASI TABLE PART NUMBER
    $('#btnPN').on('click', function(e){

       $ata = $('#form-pn #ata').val();
       $description = $('#form-pn #description').val();
       $ess = $('#form-pn #ess').val();
       $mel = $('#form-pn #mel').val();
       $sla = $('#form-pn #sla').val();
       $part_number = $('#form-pn #part_number').val();

       if ($ata && $description && $ess && $mel && $sla && $part_number) {
         $data = {
             '0' : $ata,
             '1' : $('#form-pn #part_number option:selected').text(),
             '2' : $description,
             '3' : $ess,
             '4' : $mel,
             '5' : $sla,
             '6' : '<button class="btn btn-danger btn-sm btnRemove" type="button"><i class="fa fa-remove"></i></button> ',
             '10' : $part_number,
         };
         addRow(tablePartNumber, $data);
       }else if (!$ata) {
         $('#ata').focus();
       }else if (!$description) {
         $('#description').focus();
       }else if (!$ess) {
         $('#ess').focus();
       }else if (!$mel) {
         $('#mel').focus();
       }else if (!$sla) {
         $('#sla').focus();
       }


    });

    $('#tbPartNumber ').on( 'click','tbody tr button.btnRemove' ,function () {

      tablePartNumber
          .row( $(this).parents('tr') )
          .remove()
          .draw();
    } );

  });

  function removeRow(a){

  }
  function addRow($table, data){
    ($table).row.add(data ).draw();
  }
</script>
