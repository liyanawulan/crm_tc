<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<!-- <?php //$this->load->view($nav_tabs); ?> -->
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
		                  <!-- <h3 class="box-title"><?php //echo $title; ?></h3> -->
		                  <div class="box-tools">

		                  </div>
		                </div>
		                <div class="box-body no-padding">
                    <table id="tbcontract" class="table table-bordered table-hover table-striped" width="100%">
                      <thead style="background-color: #3c8dbc; color:#ffffff;">
                        <tr>
                          <th width="3%"><center> No </center></th>
                          <th width="10%"><center> Contract Number </center></th>
                          <th width="15%"><center> Customer </center></th>
                          <th width="20%"><center> Delivery Point </center></th>
                          <th width="20%"><center> Delivery Address </center></th>
                          <th width="20%"><center> Description </center></th>
                          <th width="20%"><center> Action </center></th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
		              </div>
	                  <br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>
  $(function () {
    $('#tbcontract').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/api/Contract/detail_ct/')?>',
        columns: [       
            {data: 'no', name: 'no'},
            {data: 'contract_number', name: 'contract_number'},            
            {data: 'customer', name: 'customer', orderable: false},
            {data: 'delivery_point', name: 'delivery_point', orderable: false},
            {data: 'delivery_address', name: 'delivery_address', orderable: false},
            {data: 'description', name: 'description', orderable: false},
            {data: 'act', name: 'act', orderable: false}
        ],
    });

    $(".select2").select2();

    $("#tbcontract").on("click",".btn_del", function(){
        var id = $(this).data('x');
        var number = $(this).data('y');

        var span = document.createElement("span");
        span.innerHTML = "Apakah data CONTRACT dengan <b>CONTRACT NUMBER : "+number+"</b> akan dihapus ?";
        swal({
          title: "WARNING",
          content: span,
          //text: "Apakah data CONTRACT dengan CONTRACT NUMBER : "+number+" akan dihapus ?",
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              swal("Data berhasil dihapus!", {
                icon: "success",
              });
            }else{
              swal("Data tidak dihapus!");
            }
        });
    });
  });

</script>
