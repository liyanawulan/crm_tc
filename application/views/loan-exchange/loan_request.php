<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<?php if ($this->session->flashdata('alert_success')) { echo '<div id="alert_success" class="callout callout-success">'; echo '<p>'; echo $this->session->flashdata('alert_success'); echo '</p>'; echo '</div>'; } ?>
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs2); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<!--<div class="box-header">-->
		                    <!--<div class="col-md-12">
		                        <h3 class="box-title">Data Retail Order</h3>
		                    </div>-->
		                    <!--<div class="col-md-10">
		                        <div class="input-group">
		                            <div class="checkbox">
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Approved" name="status" value="Delivered">
		                                Delivered
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Material" name="status" value="Quotation Provided Awaiting Approval">
		                                Quotation Provided Awaiting Approval
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Under_Maintenance" name="status" value="Approval Received Repair Started">
		                                Approval Received Repair Started
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Repair" name="status" value="Ready to Deliver">
		                                Ready to Deliver
		                              </label>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-2 pull-right">
		                        <div class="input-group" style="width: 150px;">
		                            <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
		                            <div class="input-group-btn">
		                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		                            </div>
		                        </div>
		                    </div>-->
		                    <!--<div class="col-md-2 pull-right">
		                        <div class="input-group">
		                            <select class="form-control select2" id="STATUS_PAID" name="STATUS_PAID">
		                                <option value="Paid" <?php //if($this->uri->segment(4)=='Waiting_Approved') { echo 'selected'; } ?>>Paid</option>
		                                <option value="Unpaid" <?php //if($this->uri->segment(4)=='Waiting_Material') { echo 'selected'; } ?>>Unpaid</option>    
		                            </select>
		                        </div>
		                    </div>-->
		                <!-- </div> -->
		                <br>
		                <div class="box-body no-padding">
		                    <table id="requestloan" class="table table-bordered table-hover table-striped" width="100%">
					            <thead style="background-color: #3c8dbc; color:#ffffff;">
					            <tr>
					              <th>No</th>
					              <th>Part Number</th>
					              <th>Start Date</th>
					              <th>End Date</th>
					              <th>Condition</th>
					              <th>Contact Info</th>
					            </tr>
					            </thead>
					            <tbody>
					            </tbody>
					        </table>
		                </div>
	                  <br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function () {

    setTimeout(function() {
      $('#alert_failed').fadeOut('slow');
      $('#alert_success').fadeOut('slow');
    }, 5000); // <-- time in milliseconds
  });
</script>
<script>
  $(function () {
    var unsearchable = [0,10,11,12,13,14];
    $('#requestloan thead tr#searchtr th').each( function () {
        var title = $(this).text();
        var index = $(this)[0].cellIndex;
    } );
    var table = $("#requestloan").DataTable({
      "dom": 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "scrollX": true,
      "ordering": true,
      "ajax": {
        "url" : "<?= site_url('api/LoanExchange/list_request_loan') ?>",
        "type": 'post',
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
              return_data.push({

                0: json.data[i].RowNum,
                1: json.data[i].PART_NUMBER,
                2: json.data[i].PERIOD_FROM,
                3: json.data[i].PERIOD_TO,
                4: json.data[i].CONDITION,
                5: json.data[i].CONTACT_INFO
              })
            }
            /* set new token after request completed */
            // localStorage.setItem("api_token", json.token);

            /* Set User Permission */
            return return_data;

        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": false
        }
      ]
    });

    $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
   	});
   
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>