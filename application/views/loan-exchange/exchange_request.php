<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<?php if ($this->session->flashdata('alert_success')) { echo '<div id="alert_success" class="callout callout-success">'; echo '<p>'; echo $this->session->flashdata('alert_success'); echo '</p>'; echo '</div>'; } ?>
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs2); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
            <br>
            <div class="box-body no-padding">
              <table id="requestexchange" class="table table-bordered table-hover table-striped" width="100%">
  	            <thead style="background-color: #3c8dbc; color:#ffffff;">
  	            <tr>
  	              <th>No</th>
  	              <th>Part Number</th>
  	              <th>Exchange Type</th>
  	              <th>Exchange Rate</th>
  	              <th>Condition</th>
  	              <th>Contact Info</th>
  	            </tr>
  	            </thead>
  	            <tbody>
  	            </tbody>
	           </table>
            </div>
            <br> 
					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function () {

    setTimeout(function() {
      $('#alert_failed').fadeOut('slow');
      $('#alert_success').fadeOut('slow');
    }, 5000); // <-- time in milliseconds
  });
</script>
<script>
  $(function () {
    var unsearchable = [0,10,11,12,13,14];
    $('#requestexchange thead tr#searchtr th').each( function () {
        var title = $(this).text();
        var index = $(this)[0].cellIndex;
    } );
    var table = $("#requestexchange").DataTable({
       "dom": 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "language": {
          "processing": "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
        // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
        },
      "serverSide": true,
      "scrollX": true,
      "ordering": true,
      "ajax": {
        "url" : "<?= site_url('api/LoanExchange/list_request_exchange') ?>",
        "type": 'post',
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
              return_data.push({

                0: json.data[i].RowNum,
                1: json.data[i].PART_NUMBER,
                2: json.data[i].EXCHANGE_TYPE,
                3: json.data[i].EXCHANGE_RATE,
                4: json.data[i].CONDITION,
                5: json.data[i].CONTACT_INFO
              })
            }
            /* set new token after request completed */
            // localStorage.setItem("api_token", json.token);

            /* Set User Permission */
            return return_data;

        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": false
        }
      ]
    });

    $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
   	});
   
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>