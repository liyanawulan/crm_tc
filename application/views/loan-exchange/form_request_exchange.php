<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php if ($this->session->flashdata('alert_failed')) { echo '<div id="alert_failed" class="callout callout-danger">'; echo '<p>'; echo $this->session->flashdata('alert_failed'); echo '</p>'; echo '</div>'; } ?>
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <!-- <?php //$this->load->view($nav_tabs); ?> -->

        <div class="tab-content">
            
            <!-- <legend><?php echo $title; ?></legend> -->
            <form action="<?php echo base_url() ?>index.php/Loan_exchange/insert_request_exchange" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-12 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Part Number</label>
                        <div class="col-sm-3">
                          <input class="form-control input-sm " id="PART_NUMBER" name="PART_NUMBER" style="width: 100%;" required>
                        <!--   <select class="form-control input-sm select2" id="PART_NUMBER" name="PART_NUMBER" style="width: 100%;" required>
                            <option value="">Search For Part Number</option>
                            <?php 
                              foreach ($part_number_list as $row_pn) { 
                            ?>
                            <option value="<?php echo $row_pn->PN_NAME ?>"><?php echo $row_pn->PN_NAME; ?></option>
                            <?php } ?>
                          </select>       -->                                                          
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="col-sm-5 control-label">Component Description</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                          <input type="hidden" class="form-control input-sm" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Exchange Type</label>
                        <div class="col-sm-3">
                          <select class="form-control input-sm select2" id="EXCHANGE_TYPE" name="EXCHANGE_TYPE" style="width: 100%;" required>
                            <option value="">Choose Exchange Type</option>
                            <option value="SVC">SVC</option>
                            <option value="OVC">OVC</option>
                            <option value="NE">NE</option>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Exchange Rate</label>
                        <div class="col-sm-3">
                          <select class="form-control input-sm select2" id="EXCHANGE_RATE" name="EXCHANGE_RATE" style="width: 100%;" required>
                            <option value="">Choose Exchange Rate</option>
                            <option value="Flat Rate">Flat Rate</option>
                            <option value="Exchange Fee + Repair">Exchange Fee + Repair</option>
                            <option value="Outright">Outright</option>
                          </select>                                                                
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Period From</label>
                        <div class="col-sm-2">
                          <input type="date" class="form-control input-sm" name="REQUESTED_DELIVERY_DATE">                            
                        </div>
                        <label class="col-sm-1 control-label">To</label>
                        <div class="col-sm-2">
                          <input type="date" class="form-control input-sm" name="REQUESTED_DELIVERY_DATE">                            
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Condition</label>
                        <div class="col-sm-5">
                          <textarea name="CONDITION" type="textarea" class="form-control input-sm" rows="5" cols="22"></textarea> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Contact Info</label>
                        <div class="col-sm-5">
                          <input type="text" class="form-control input-sm" id="CONTACT_INFO" name="CONTACT_INFO"> 
                        </div>
                      </div>

                    </div>
                  </div>                
                                         
                </div>
                
                <!-- <br>
                <hr class="title-hr">
                <legend>Attention To </legend>
                
                <div class="row">                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Existing Email</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" multiple="multiple" id="EXISTING_EMAIL" name="EXISTING_EMAIL" style="width: 100%;" required>
                            <option value="">Choose Existing Email</option>
                            <option value="herdy.prabowo@gmail.com" selected>herdy.prabowo@gmail.com</option>
                            <option value="riyan.sasmitha@gmail.com">riyan.sasmitha@gmail.com</option>
                            <option value="liyana@gmail.com">liyana@gmail.com</option>
                            <option value="viar@gmail.com">viar@gmail.com</option>
                            <option value="nando@gmail.com">nando@gmail.com</option>
                            <option value="michael@gmail.com">michael@gmail.com</option>
                          </select>
                        </div>
                      </div>             
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 1 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL1">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 2 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL2">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 3 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL3">  
                        </div>
                      </div>

                    </div>
                  </div>

                </div> -->

              </div>
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" id="insert" name="insert" class="btn btn-success pull-right">Request</button>
              </div><!-- /.box-footer -->           
            </form>
          
        </div>
            
      </div>
    </div>
  </div>
</section>

<script>
  $(document).ready(function () {

    setTimeout(function() {
      $('#alert_failed').fadeOut('slow');
      $('#alert_success').fadeOut('slow');
    }, 5000); // <-- time in milliseconds
  });
</script>
<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<!-- <script type="text/javascript">
    $(document).ready(function () { 
        $('#ID_PART_NUMBER').change(function () { 
            var selID_PN = $('#ID_PART_NUMBER').val();
            $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_part_number_description/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_PN="+selID_PN, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#COMPONENT_DESCRIPTION").val(data.PN_DESC); //after you explained the JSON response
                    $("#COMPONENT_DESCRIPTION_V").val(data.PN_DESC); //after you explained the JSON response
                }
            });

        });
        var selID_PN = $('#ID_PART_NUMBER').val();
            $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_part_number_description/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_PN="+selID_PN, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#COMPONENT_DESCRIPTION").val(data.PN_DESC); //after you explained the JSON response
                    $("#COMPONENT_DESCRIPTION_V").val(data.PN_DESC); //after you explained the JSON response
                }
            });
    });
</script> -->

<!-- <script type="text/javascript">
    $(document).ready(function () { 
        $('#ID_DELIVERY_POINT').change(function () { 
            var selID_DP = $('#ID_DELIVERY_POINT').val();
            if (selID_DP!=1)
            {
              $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_delivery_point_address/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_DP="+selID_DP, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#DESTINATION_ADDRESS").val(data.DELIVERY_POINT_ADDRESS); //after you explained the JSON response
                    $("#DESTINATION_ADDRESS").attr('disabled', 'disabled');
                }
              });
            }
            else
            {
              $("#DESTINATION_ADDRESS").removeAttr('disabled', 'disabled');
              $("#DESTINATION_ADDRESS").val('');
            }

        });
        var selID_DP = $('#ID_DELIVERY_POINT').val();
            $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_delivery_point_address/", //memanggil function controller dari url 
                type: "POST", //jenis method yang dibawa ke function 
                data: "ID_DP="+selID_DP, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("#DESTINATION_ADDRESS").val(data.DELIVERY_POINT_ADDRESS); //after you explained the JSON response
                }
            });
    });
</script> -->