<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
		                <div class="box-body table-responsive no-padding">
		                    <table id="tcloanorder" class="table table-bordered table-hover table-striped" width="100%">
					            <thead style="background-color: #3c8dbc; color:#ffffff;">
					            <tr>
					              <th>No</th>
					              <th>Sales Order</th>
					              <th>Part Number</th>
					              <th>Description</th>
					              <th>Type of Service</th>
					              <th>Serial Number</th>
					              <th>Loan Periode</th>
					              <th>Start Date</th>
					              <th>End Date</th>
					              <th>TAT</th>
					              <th>Status</th>
					            </tr>
					            </thead>
					           <!--  <tbody>
					            </tbody> -->
					             <tbody>
		                           <!--  <?php
		                                if($listOrder) { 
		                                    $no = 0;
  		                                    foreach ($listOrder as $rows) {
  		                                    $no++; 
  		                             		{ ?>
		                            <tr>
		                            	<td><?php echo $no; ?></td>
		                            	<td><?php echo $rows['VBELN']; ?></td>
		                            	<td><?php echo $rows['MATNR']; ?></td>
		                            	<td><?php echo $rows['ARKTX']; ?></td>
		                            	<td><?php echo $rows['TYPESERVICE']; ?></td>
		                            	<td><?php echo $rows['SERNR']; ?></td>
		                            	<td><?php if (isset($rows['PERIOD'])) { echo $rows['PERIOD']; ?> days <?php } ?></td>
		                            	<td><?php 
			                                	$source = $rows['START_DATE'];
			                                    $date = new DateTime($source);
			                                    echo $date->format('d-m-Y'); 
			                             	?></td>
		                            	<td><?php 
			                                	$source = $rows['END_DATE'];
			                                    $date = new DateTime($source);
			                                    echo $date->format('d-m-Y'); 
			                             	?></td>
		                            	<td></td>
		                            	<td><?php echo $rows['STATUS']; ?></td>
		                            </tr>
		                           <?php } } }?> -->
		                       </tbody>
					        </table>
		                    <br>
		                   <!--  <div class="col-md-3">
		                        <b><?php //if($total_rows) { echo '<p class="text-yellow">'.$total_rows.' Total Data Records </p>'; } else { echo '<p class="text-yellow"> No Data Records </p>'; } ?></b>
		                    </div> -->
		                   <!--  <div class="col-md-6">
		                        <?php //echo $paging; ?>
		                    </div> -->
		                    <!-- <div class="col-md-3 pull-right">
		                        <a href="<?php //echo base_url('Working_order/download_new_wo').'/'.$type.'/'.$key.'/'.$start.'/'.$end; ?>" taget=_blank><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download Data <?php //echo $title; ?></button></a>
		                    </div>-->
		                </div>
	                  	<br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->

	</div>

</section>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<!-- <script>
	// var tblo;
    $(document).ready(function() {
      $('#tcloanorder').DataTable({
            // scrollY: "500px",
            dom: 'Blfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            // fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                 'copyHtml5',
	            'excelHtml5',
	            'csvHtml5',
	            'pdfHtml5'
            ],
        });
      	// dom: 'Blfrtip',
      	// paging: true,
       //  lengthMenu: [
       //      [ 10, 25, 50, -1 ],
       //      [ '10 rows', '25 rows', '50 rows', 'Show all' ]
       //  ],
       //  buttons: [
       //      'copyHtml5',
       //      'excelHtml5',
       //      'csvHtml5',
       //      'pdfHtml5'
       //  ],
       //  columnDefs: [
       //      {
       //          "targets": [ 11 ],
       //          "visible": true,
       //          "searchable": true
       //      }
       //  ]
      // } );
  });
</script> -->

<script>
  $(function () {
    var unsearchable = [0,10,11,12,13,14];
    $('#tcloanorder thead tr#searchtr th').each( function () {
        var title = $(this).text();
        var index = $(this)[0].cellIndex;
        // if (jQuery.inArray(index, unsearchable)==-1) {
        //
        //   $(this).html( '<input id="col'+index+'_filter" data-column="'+index+'" type="text" class="form-control columns-filter" placeholder="Search '+title+'" />' );
        // }else{
        //     $(this).empty();
        // }
    } );
    var table = $("#tcloanorder").DataTable({
     "dom": 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "language": {
          "processing": "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
        // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
        },
      "serverSide": true,
      "scrollX": true,
      "ordering": true,
      "ajax": {
        "url" : "<?= site_url('api/LoanExchange/list_table') ?>",
        "type": 'post',
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
            	// var START_DATE = json.data[i].START_DATE;
            	// var END_DATE = json.data[i].END_DATE;
            	// if (json.data[i].START_DATE != ' '){
            	// 	START_DATE = new Date(json.data[i].START_DATE);
            	// }
            	// if (json.data[i].END_DATE != ' '){
            	// 	END_DATE = new Date(json.data[i].END_DATE);
            	// }
              return_data.push({

                0: json.data[i].RowNum,
                1: 1*(json.data[i].VBELN),
                2: json.data[i].MATNR,
                3: json.data[i].ARKTX,
                4: json.data[i].TYPESERVICE,
                5: json.data[i].SERNR,
                6: json.data[i].PERIOD,
                7: json.data[i].START_DATE,
                8: json.data[i].END_DATE,
                9: '',
                10:json.data[i].STATUS
              })
            }
            /* set new token after request completed */
            // localStorage.setItem("api_token", json.token);

            /* Set User Permission */
            return return_data;

        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": false
        }
      ]
    });

    $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
   });
   
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });

</script> -->