<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<!-- <?php //$this->load->view($nav_tabs); ?> -->
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
              <!-- <h3 class="box-title"><?php echo $title; ?></h3> -->
              <div class="box-tools">
              </div>
            </div>
            <div class="box-body no-padding">
              <div class="row">
                <form id="form-reqCategory" method="POST" action="<?= site_url('master/reqcategory/create') ?>" class="form-horizontal" enctype="multipart/form-data">
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" id="REQUIREMENT_CATEGORY_NAME" name="REQUIREMENT_CATEGORY_NAME">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-7">
                          <textarea style="min-height: 130px;" class="form-control input-sm" id="REQUIREMENT_CATEGORY_DESC" name="REQUIREMENT_CATEGORY_DESC"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-3">
                      </div>

                      <div id="button-submit" class="col-md-7">
                        <button id="form-submit" class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Add </button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
			          <table id="tbCategory" class="table table-bordered table-hover table-striped">
                  <thead style="background-color: #3c8dbc; color:#ffffff;">
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Name</center></th>
                      <th><center>Description</center></th>
                      <th><center>Act</center></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
            </div>
					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>

var table;

  $(function () {
    //datatables
    table = $('#tbCategory').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('api/ReqCategory')?>",
            "type": "POST",
            "dataSrc" : function(json){
              var return_data = [];

              json.draw = json.draw;
              json.recordsFiltered = json.recordsFiltered;
              json.recordsTotal = json.recordsTotal;

                for(var i=0;i< json.data.length; i++){
                  return_data.push({
                    0: json.data[i].NO,
                    9: json.data[i].RowNum,
                    10: json.data[i].ID_REQUIREMENT_CATEGORY,
                    1: json.data[i].REQUIREMENT_CATEGORY_NAME,
                    2: json.data[i].REQUIREMENT_CATEGORY_DESC
                  })
                }
                return return_data;
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          {
            "targets": [],
            "visible": false,
            "searchable": false
          },{
            "targets": -1,
            "className": "dt-center",
            "data": null,
            "defaultContent":
            '<button title="update" onClick="btUpdate(this)" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button>'+
            '<button title="delete" onClick="btDelete(this)" class="btDelete btn btn-danger btn-xs" type="button"><i class="fa fa-times"></i></button>'
          }]

    });


            $('#button-submit #form-cancel').on('click', function(){
              $('#form-reqCategory').prop('action', "<?= site_url('master/reqcategory/create') ?>/"+data[10]);
              $('#form-reqCategory #AIRCRAFT_REGISTRATION').val(data[1]);
              // $('#form-reqCategory #PN_DESC').val(data[2]);

              $('#form-reqCategory #form-submit').removeClass("btn-warning");
              $('#form-reqCategory #form-submit').addClass("btn-primary");
              $('#form-reqCategory #form-submit').html("");
              $('#form-reqCategory #form-submit').html("<i class='fa fa-save'></i> Add ");
              $('#form-reqCategory #button-submit #form-cancel').remove();

            });
  });
  //
  function btUpdate(a){
    var data = table.row($(a).closest('tr')).data();

    $('#form-reqCategory').prop('action', "<?= site_url('master/reqcategory/update') ?>/"+data[10]);
    $('#form-reqCategory #REQUIREMENT_CATEGORY_NAME').val(data[1]);
    $('#form-reqCategory #REQUIREMENT_CATEGORY_DESC').val(data[2]);
    $('#form-reqCategory #form-submit').removeClass("btn-primary");
    $('#form-reqCategory #form-submit').addClass("btn-warning");
    $('#form-reqCategory #form-submit').html("");
    $('#form-reqCategory #form-submit').html("<i class='fa fa-save'></i> Update ");
    $('#form-reqCategory #button-submit').append('<button id="form-cancel" class="btn "> Cancel </button>');

    $( '#form-reqCategory #REQUIREMENT_CATEGORY_NAME').focus();
  }
  function btDelete(a){
      var data = table.row($(a).closest('tr')).data();

      window.location.href = "<?= site_url('master/reqcategory/delete') ?>/"+data['10'];
  }
</script>
