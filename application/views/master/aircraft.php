<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<!-- <?php //$this->load->view($nav_tabs); ?> -->
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
              <!-- <h3 class="box-title"><?php echo $title; ?></h3> -->
              <div class="box-tools">
              </div>
            </div>
            <div class="box-body no-padding">
              <div class="panel">

                <div class="panel-body">
                  <div class="row">
                    <form id="form-aircraft" method="POST" action="<?= site_url('master/aircraft/create') ?>" class="form-horizontal" enctype="multipart/form-data">
                      <div class="col-md-12">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control input-sm" id="AIRCRAFT_REGISTRATION" name="AIRCRAFT_REGISTRATION">
                            </div>
                          </div>
                          <!-- <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-7">
                              <textarea style="min-height: 130px;" class="form-control input-sm" id="PN_DESC" name="PN_DESC"></textarea>
                            </div>
                          </div> -->
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-3">
                          </div>

                          <div id="button-submit" class="col-md-7">
                            <button id="form-submit" class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Add </button>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
      			          <table id="tbaircraft" class="table table-responsive table-bordered table-hover table-striped">
                        <thead style="background-color: #3c8dbc; color:#ffffff;">
                          <tr>
                            <th><center>No</center></th>
                            <th><center>Aircraft Registration</center></th>
                            <th><center>Act</center></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>

var table;

  $(function () {
    //datatables
    table = $('#tbaircraft').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('api/Aircraft')?>",
            "type": "POST",
            "dataSrc" : function(json){
              var return_data = [];

              json.draw = json.draw;
              json.recordsFiltered = json.recordsFiltered;
              json.recordsTotal = json.recordsTotal;

                for(var i=0;i< json.data.length; i++){
                  return_data.push({
                    0: json.data[i].NO,
                    10: json.data[i].ID_AIRCRAFT_REGISTRATION,
                    9: json.data[i].RowNum,
                    1: json.data[i].AIRCRAFT_REGISTRATION
                  })
                }
                return return_data;
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          {
            "targets": [],
            "visible": false,
            "searchable": false
          },{
            "targets": -1,
            "className": "dt-center",
            "data": null,
            "defaultContent":
            '<button title="update" onClick="btUpdate(this)" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button>'+
            '<button title="delete" onClick="btDelete(this)" class="btDelete btn btn-danger btn-xs" type="button"><i class="fa fa-times"></i></button>'
          }]

    });


    $('#button-submit #form-cancel').on('click', function(){
      $('#form-aircraft').prop('action', "<?= site_url('master/aircraft/create') ?>/"+data[10]);
      $('#form-aircraft #AIRCRAFT_REGISTRATION').val(data[1]);
      // $('#form-aircraft #PN_DESC').val(data[2]);

      $('#form-aircraft #form-submit').removeClass("btn-warning");
      $('#form-aircraft #form-submit').addClass("btn-primary");
      $('#form-aircraft #form-submit').html("");
      $('#form-aircraft #form-submit').html("<i class='fa fa-save'></i> Add ");
      $('#form-aircraft #button-submit #form-cancel').remove();

    });
  });
  //
  function btUpdate(a){
    var data = table.row($(a).closest('tr')).data();

    $('#form-aircraft').prop('action', "<?= site_url('master/aircraft/update') ?>/"+data[10]);
    $('#form-aircraft #AIRCRAFT_REGISTRATION').val(data[1]);
    // $('#form-aircraft #PN_DESC').val(data[2]);

    $('#form-aircraft #form-submit').removeClass("btn-primary");
    $('#form-aircraft #form-submit').addClass("btn-warning");
    $('#form-aircraft #form-submit').html("");
    $('#form-aircraft #form-submit').html("<i class='fa fa-save'></i> Update ");
    $('#form-aircraft #button-submit').append('<button id="form-cancel" class="btn "> Cancel </button>');

    // $('#form-aircraft form-submit').val("<i class='fa fa-save'></i> Update ");
    $( '#form-aircraft #PN_NAME').focus();
  }
  function btDelete(a){
      var data = table.row($(a).closest('tr')).data();

      window.location.href = "<?= site_url('master/aircraft/delete') ?>/"+data['10'];
  }
</script>
