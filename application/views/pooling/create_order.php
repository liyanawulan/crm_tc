<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">

        <!-- <?php //$this->load->view($nav_tabs); ?> -->

        <div class="tab-content">

            <!-- <legend>Order Info</legend> -->
            <!-- <form id="form-pooling" action="<?php echo base_url() ?>index.php/Pooling/insert_create_order" class="form-horizontal" method="post" enctype="multipart/form-data"> -->
            <form id="form-pooling" name="form-pooling" action="" class="form-horizontal" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Contract Request</label>
                          <div class="col-sm-7">
                            <select class="form-control input-sm select2" id="ID_CONTRACT" name="ID_CONTRACT" style="width: 100%;" required>
                              <option value="">Search For Contract</option>
                              <?php
                                foreach ($contract_list as $row) {
                              ?>
                              <option value="<?php echo $row->ID ?>"><?php echo $row->CONTRACT_NUMBER; ?></option>
                              <?php } ?>
                              <option value="X">OTHER</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Request Part Number</label>
                        <div class="col-sm-5">
                          <select class="form-control input-sm select2" id="ID_PART_NUMBER" name="ID_PART_NUMBER" disabled="true" style="width: 100%;" required>
                            <option value="">Search For Part Number</option>
                           
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Component Description</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                          <input type="hidden" class="form-control input-sm" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requirement Category</label>
                        <div class="col-sm-5">
                          <select class="form-control input-sm select2" id="ID_REQUIREMENT_CATEGORY" name="ID_REQUIREMENT_CATEGORY" style="width: 100%;" required>
                            <option value="">Choose Requirement Category</option>
                            <?php
                              foreach ($requirement_category_list as $row_rc) {
                            ?>
                            <option value="<?php echo $row_rc->ID_REQUIREMENT_CATEGORY ?>"><?php echo $row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Delivery Date</label>
                        <div class="col-sm-7">
                          <input type="date" class="form-control input-sm" name="REQUESTED_DELIVERY_DATE">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Maintenance</label>
                        <div class="col-sm-7">
                          <label class="radio-inline">
                            <input type="radio" name="MAINTENANCE" value="No">No
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="MAINTENANCE" value="Yes">Yes
                          </label>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Ata Chapter</label>
                        <div class="col-sm-2">
                          <input type="text" disabled="true"  class="form-control input-sm" name="ATA_CHAPTER" id="ATA_CHAPTER">
                          <input type="hidden" class="form-control input-sm" name="ATA_CHAPTER_V" id="ATA_CHAPTER_V">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Aircraft Registration</label>
                        <div class="col-sm-5">
                          <select disabled="true" class="form-control input-sm select2" id="ID_AIRCRAFT_REGISTRATION" name="ID_AIRCRAFT_REGISTRATION" style="width: 100%;" required>
                            <option value="">Choose Aircraft Reg.</option>
                          
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Release Type</label>
                        <div class="col-sm-7">
                          <label class="checkbox-inline">
                            <input type="checkbox" name="RELEASE_TYPE" value="EASA">EASA
                          </label>
                          <label class="checkbox-inline">
                            <input type="checkbox" name="RELEASE_TYPE" value="FAA">FAA
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Delivery Point</label>
                        <div class="col-sm-5">
                          <select class="form-control input-sm select2" id="ID_DELIVERY_POINT" name="ID_DELIVERY_POINT" style="width: 100%;" required>
                            <option value="">Choose</option>
                            
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Destination Address</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" id="DESTINATION_ADDRESS" name="DESTINATION_ADDRESS" disabled="true">
                          <input type="hidden" class="form-control input-sm" name="DESTINATION_ADDRESS_V" id="DESTINATION_ADDRESS_V">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Customer PO/RO</label>
                        <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-7">
                              <input type="text" class="form-control input-sm" name="CUSTOMER_PO" placeholder="">
                            </div>
                            <!-- <div class="col-sm-5">
                              <input type="text" class="form-control input-sm" name="CUSTOMER_RO" placeholder="">
                            </div> -->
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Remarks Of Customer</label>
                        <div class="col-sm-7">
                          <textarea name="REMARKS_OF_CUSTOMER" type="textarea" class="form-control input-sm" rows="5" cols="22"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <br>
                <hr class="title-hr">
                <legend>Attention To </legend>

                <div class="row">

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Existing Email</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" multiple="multiple" id="EXISTING_EMAIL" name="EXISTING_EMAIL" style="width: 100%;" required>
                            <option value="">Choose Existing Email</option>
                            <option value="herdy.prabowo@gmail.com" selected>herdy.prabowo@gmail.com</option>
                            <option value="riyan.sasmitha@gmail.com">riyan.sasmitha@gmail.com</option>
                            <option value="liyana@gmail.com">liyana@gmail.com</option>
                            <option value="viar@gmail.com">viar@gmail.com</option>
                            <option value="nando@gmail.com">nando@gmail.com</option>
                            <option value="michael@gmail.com">michael@gmail.com</option>
                            <!-- <?php
                              //foreach ($request_category_list as $row_rc) {
                            ?>
                            <option value="<?php //echo $row_rc->ID_REQ_CAT ?>"><?php //echo $row_rc->REQ_CAT_NAME.' - '.$row_rc->REQ_CAT_DESC; ?></option>
                            <?php //} ?> -->
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 1 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL1">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 2 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL2">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 3 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL3">
                        </div>
                      </div>

                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" id="insert" name="insert" class="btn btn-success pull-right">Submit Order</button>
              </div><!-- /.box-footer -->
            </form>

        </div>

      </div>
    </div>
  </div>
</section>


<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#ID_CONTRACT').change(function () {
            var ID_CONTRACT = $('#ID_CONTRACT').val();
            $('#form-pooling #ID_PART_NUMBER').prop("disabled", false);
            $('#form-pooling #ID_AIRCRAFT_REGISTRATION').prop("disabled", false);

            if (ID_CONTRACT=='X') {
              $('#form-pooling #ATA_CHAPTER').prop("disabled", false);
              // console.log('other');
            }else{
              $('#form-pooling #ID_PART_NUMBER').html('').select2();
              $('#form-pooling #ID_AIRCRAFT_REGISTRATION').html('').select2();
              $('#form-pooling #ID_DELIVERY_POINT').html('').select2();
              $.ajax({
                  url: "<?=site_url('Pooling/get_fleet_list')?>/"+ID_CONTRACT, //memanggil function controller dari url
                  type: "POST", //jenis method yang dibawa ke function
                  success: function(data) {
                      data = $.parseJSON(data);
                      // console.log(data);
                      $.each(data, function(index, val){
                        var option = new Option(val['AIRCRAFT_REGISTRATION_NAME'], val['AIRCRAFT_REGISTRATION_NAME'], true, true);
                        $('#ID_AIRCRAFT_REGISTRATION').append(option).trigger('change');
                      });
                      $('#ID_AIRCRAFT_REGISTRATION').append('"<option value="X">OTHER</option>"');

                      $('#ID_AIRCRAFT_REGISTRATION').trigger('change');
                  }
              });

              $.ajax({
                  url: "<?=site_url('Pooling/get_part_number_list')?>/"+ID_CONTRACT, //memanggil function controller dari url
                  type: "POST", //jenis method yang dibawa ke function
                  success: function(data) {
                      data = $.parseJSON(data);
                      $.each(data, function(index, val){
                        var option = new Option(val['PART_NUMBER'], val['PART_NUMBER'], true, true);
                        $('#ID_PART_NUMBER').append(option);
                      });

                      $('#ID_PART_NUMBER').trigger('change');
                  }
              });
              $.ajax({
                  url: "<?=site_url('Pooling/get_delivery_point')?>/"+ID_CONTRACT, //memanggil function controller dari url
                  type: "POST", //jenis method yang dibawa ke function
                  success: function(data) {
                      // $('#ID_DELIVERY_POINT').remove();
                      data = $.parseJSON(data);
                      $.each(data, function(index, val){
                        var option = new Option(val['DELIVERY_POINT_CODE'], val['ID_DELIVERY_POINT'], true, true);
                        $('#ID_DELIVERY_POINT').append(option);
                      });

                      $('#ID_DELIVERY_POINT').trigger('change');
                  }
              });
            }
        });
        $('#ID_PART_NUMBER').change(function () {
            var ID_PART_NUMBER = $('#ID_PART_NUMBER').val();
            $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_part_number_contract/"+ID_PART_NUMBER, //memanggil function controller dari url
                type: "POST", //jenis method yang dibawa ke function
                data: "ID_PN="+ID_PART_NUMBER,
                success: function(data) {
                    data = $.parseJSON(data);

                    
                    // console.log(data);
                    $("#ATA_CHAPTER").val(data.ATA);
                    $("#ATA_CHAPTER_V").val(data.ATA); //after you explained the JSON response
                }
            });
            $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_part_number_description/", //memanggil function controller dari url
                type: "POST", //jenis method yang dibawa ke function
                data: "ID_PN="+ID_PART_NUMBER, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    // alert(data['PN_DESC']);
                    $("#COMPONENT_DESCRIPTION").val(data.DESCRIPTION); //after you explained the JSON response
                    $("#COMPONENT_DESCRIPTION_V").val(data.DESCRIPTION); //after you explained the JSON response
                }
            });
          });

        $(window).keydown(function(event){
          if( (event.keyCode == 13)) {
            event.preventDefault();
            return false;
          }
        });

        $("#form-pooling").submit(function(event) {
      /* Act on the event */
          event.preventDefault();
          // $('#submit').text("Proses Simpan");
          // $('#submit').attr('disabled', true);
          var formData = new FormData($('#form-pooling')[0]);
           $.ajax({
             url:"<?php echo base_url() ?>index.php/Pooling/insert_create_order",
             type:'POST',
             dataType: 'text',
             data: formData,
             processData: false,
             contentType: false,
             success:function(response){
                  var data = JSON.parse(response);
                    swal( data.msg , {
                          icon: "success",
                    })
                    .then(function(willgo){
                        if (willgo) {
                            window.location.href = "update_order/"+data.id;
                        }
                    })   // reloading page
             }
          });
         });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#ID_DELIVERY_POINT').change(function () {
            var selID_DP = $('#ID_DELIVERY_POINT').val();
            if (selID_DP!=1)
            {
              $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_delivery_point_address/", //memanggil function controller dari url
                type: "POST", //jenis method yang dibawa ke function
                data: "ID_DP="+selID_DP, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    // alert(data.DELIVERY_POINT_ADDRESS);
                    // console.log(data);
                    $("#DESTINATION_ADDRESS").val(data.DELIVERY_POINT_ADDRESS);
                    $("#DESTINATION_ADDRESS_V").val(data.DELIVERY_POINT_ADDRESS);  //after you explained the JSON response
                    // $("#DESTINATION_ADDRESS").attr('disabled', 'disabled');
                }
              });
            }
            else
            {
              $("#DESTINATION_ADDRESS").removeAttr('disabled', 'disabled');
              $("#DESTINATION_ADDRESS").val('');
            }

        });
        var selID_DP = $('#ID_DELIVERY_POINT').val();
            $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_delivery_point_address/", //memanggil function controller dari url
                type: "POST", //jenis method yang dibawa ke function
                data: "ID_DP="+selID_DP, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);

                    // console.log(data);
                    $("#DESTINATION_ADDRESS").val(data.DELIVERY_POINT_ADDRESS);
                    $("#DESTINATION_ADDRESS_V").val(data.DELIVERY_POINT_ADDRESS);
                }
            });
    });
</script>
