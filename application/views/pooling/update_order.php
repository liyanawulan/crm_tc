<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section id="main" class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">

        <!-- <?php //$this->load->view($nav_tabs); ?> -->

        <div class="tab-content">
          <!-- /.tab-pane New Order-->
          <!-- <div class="tab-pane" id="tab_new_order"> -->

            <legend>Request Info</legend>
            <form action="<?php echo base_url() ?>index.php/administrator/Pooling_control/update_order" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">

                      <div class="form-group">
                          <label class="col-sm-5 control-label">Customer :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-green"><?= $order->COMPANY_NAME ?></span>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Status :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-red"><?= $order->STATUS_ORDER_NAME ?></span>
                          </div>
                      </div>
                      <br>

                      <div class="form-group">
                        <label class="col-sm-5 control-label">Reference No</label>
                        <div class="col-sm-7">
                          <input <?= $main ?> value="<?= $order->REFERENCE ?>" type="text" class="form-control" name="REFERENCE" >
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Service Level</label>
                          <div class="col-sm-7">
                              <?php
                                $SL[1] = '';
                                $SL[2] = '';
                                if ($order->SERVICE_LEVEL=='Pre-Defined') {
                                  $SL[1] = 'checked';
                                  $SL[2] = '';
                                }else {
                                  $SL[1] = '';
                                  $SL[2] = 'checked';
                                }
                              ?>
                              <label class="radio-inline">
                                  <input <?= $main ?> type="radio" name="SERVICE_LEVEL" id="optionsRadios1" <?= $SL[1] ?>>Pre-Defined
                              </label>
                              <label class="radio-inline">
                                  <input <?= $main ?> type="radio" name="SERVICE_LEVEL" id="optionsRadios2" <?= $SL[2] ?>>Customer Request
                              </label>
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Request Part Number</label>
                        <div class="col-sm-7">
                          <input <?= $main ?> type="text" class="form-control" ="true" id="ID_PART_NUMBER" name="ID_PART_NUMBER" value="<?= $order->ID_PART_NUMBER ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Component Description</label>
                        <div class="col-sm-7">
                          <input <?= $main ?> type="text" class="form-control" ="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V" value="<?= $order->DESCRIPTION ?>">
                          <input <?= $main ?> type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION" value="<?= $order->DESCRIPTION ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requirement Category</label>
                        <div class="col-sm-7">
                          <select <?= $main ?> class="form-control select2" id="ID_REQ_CAT" name="ID_REQ_CAT" style="width: 100%;" >
                            <!-- <option value="">Search Requirement Category</option> -->
                            <?php
                              foreach ($requirement_category_list as $row_rc) {
                                if ($row_rc->ID_REQUIREMENT_CATEGORY==$order->ID_REQUIREMENT_CATEGORY) {
                                  ?>
                                  <option value="<?php echo $row_rc->ID_REQUIREMENT_CATEGORY ?>" selected><?php echo $row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
                              <?php
                                }else{
                            ?>
                            <option value="<?php echo $row_rc->ID_REQUIREMENT_CATEGORY ?>"><?php echo $row_rc->REQUIREMENT_CATEGORY_NAME.' - '.$row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
                          <?php }} ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Delivery Date</label>
                        <div class="col-sm-7">
                          <input <?= $main ?> value="<?= $order->REQUESTED_DELIVERY_DATE ?>"  type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Maintenance</label>
                        <div class="col-sm-7">
                          <?php
                            $mnt[1] = '';
                            $mnt[2] = '';
                            if ($order->MAINTENANCE=='Yes') {
                              $mnt[1] = 'checked';
                              $mnt[2] = '';
                            }else {
                              $mnt[1] = '';
                              $mnt[2] = 'checked';
                            }
                          ?>
                          <label class="radio-inline">
                            <input <?= $main ?> type="radio" name="MAINTENANCE" value="No" <?= $mnt[2] ?> >No
                          </label>
                          <label class="radio-inline">
                            <input <?= $main ?> type="radio" name="MAINTENANCE" value="Yes"  <?= $mnt[1] ?> >Yes
                          </label>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-5 control-label">Reason Of Requested Service Level</label>
                        <div class="col-sm-7">
                          <textarea <?= $main ?> name="REASON_OF_REQUESTED_SERVICE_LEVEL" type="textarea" class="form-control" rows="5" cols="22" ></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">

                      <div class="form-group">
                          <label class="col-sm-5 control-label">Resquested By / Date :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-blue"><?= $order->ORDER_REQUESTED_BY ?></span> <span class="pull-left badge bg-yellow"><?= $order->REQUEST_ORDER_DATE ?></span>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-5 control-label"> Confirmed By / Date :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-blue"><?= $order->ORDER_CONFIRMED_BY ?></span>
                            <span class="pull-left badge bg-yellow"><?= $order->ORDER_CONFIRMED_DATE ?></span>
                          </div>
                      </div>
                      <br>

                      <div class="form-group">
                        <label class="col-sm-5 control-label">Ata Chapter</label>
                        <div class="col-sm-7">

                          <input <?= $main ?> value="<?= $order->ATA_CHAPTER ?>" type="text" class="form-control" name="ATA_CHAPTER">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Release Type</label>
                        <div class="col-sm-7">
                          <?php
                            $rls[1] = '';
                            $rls[2] = '';
                            if ($order->RELEASE_TYPE=='EASA') {
                              $rls[1] = 'checked';
                              $rls[2] = '';
                            }else {
                              $rls[1] = '';
                              $rls[2] = 'checked';
                            }
                          ?>
                          <label class="checkbox-inline">
                            <input <?= $main ?> type="checkbox" name="RELEASE_TYPE" value="EASA" <?= $rls[1] ?>>EASA
                          </label>
                          <label class="checkbox-inline">
                            <input <?= $main ?> type="checkbox" name="RELEASE_TYPE" value="FAA" <?= $rls[2] ?>>FAA
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Aircraft Registration</label>
                        <div class="col-sm-7">
                          <input <?= $main ?> value="<?= $order->ID_AIRCRAFT_REGISTRATION ?>"  type="text" class="form-control" name="AIRCRAFT_REGISTRATION" id="AIRCRAFT_REGISTRATION">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">3 Digit Airport Code</label>
                        <div class="col-sm-7">
                          <input <?= $main ?> type="text" class="form-control" name="3_DIGIT_AIRPORT_CODE" value="<?php echo $order->AIRPORT_CODE ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Destination Address</label>
                        <div class="col-sm-7">
                          <input <?= $main ?> value="<?= $order->DESTINATION_ADDRESS ?>" type="text" class="form-control" name="DESTINATION_ADDRESS" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Customer PO/RO</label>
                        <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-7">
                              <input <?= $main ?> value="<?= $order->CUSTOMER_PO ?>"  type="text" class="form-control" name="CUSTOMER_PO" placeholder="" >
                            </div>
                            <div class="col-sm-5">
                              <!-- <input type="text" class="form-control" name="CUSTOMER_RO" placeholder="" > -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Remarks Of Customer</label>
                        <div class="col-sm-7">
                          <textarea <?= $main ?> name="REMARKS_OF_CUSTOMER" type="textarea" class="form-control" rows="5" cols="22" >
                            <?= $order->REMARKS_OF_CUSTOMER ?>
                          </textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">THYT Remarks</label>
                        <div class="col-sm-7">
                          <textarea <?= $main ?> name="THY_REMARKS" type="textarea" class="form-control" rows="5" cols="22" ></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button <?= $main_btn ?> type="reset" class="btn btn-default">Cancel</button>
                <button <?= $main_btn ?> type="submit" id="insert" name="insert" class="btn btn-success pull-right">Submit Order</button>
              </div><!-- /.box-footer -->
            </form>

        </div>
      </div>
    </div>
  </div>
</section>

<section id="serviceable" class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default box-solid">
        <!-- <div class="box box-success"> -->
        <div class="box-header with-border">
          <h3 class="box-title">Serviceable Component</h3>

        </div>
        <div class="box-body">
          <div class="row">
            <form action="" class="form-horizontal" method="POST" enctype="multipart/form-data" name="form-serviceable" id="form-serviceable">
           <!--  <form action="<?php //echo base_url() ?>index.php/Pooling/submit_serviceable" class="form-horizontal" method="post" enctype="multipart/form-data" id="form-serviceable"> -->
            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">
                <input type="hidden" name="id" value=<?php echo $order->ID_POOLING_ORDER; ?> >
                <input type="hidden" id="revisi_serv" name="revisi_serv" value=<?php echo $order->REVISI_SERVICE; ?>>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Status :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-red"><?= $order->SERVICE_STATUS ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Submitter :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-blue"><?= $order->SERVICE_SUBMITTER ?></span>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Shipment Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input <?= $serviceable ?>  type="radio" name="SERVICE_SHIPMENT_TYPE" id="optionsRadios1" value="Hand-Delivered" <?php echo ($order->SERVICE_SHIPMENT_TYPE=='Hand-Delivered') ? 'checked' : ''; ?>>Hand-Delivered
                        </label>
                        <label class="radio-inline">
                            <input <?= $serviceable ?>  type="radio" name="SERVICE_SHIPMENT_TYPE" id="optionsRadios2" value="Overseas Shipment" <?php echo ($order->SERVICE_SHIPMENT_TYPE=='Overseas Shipment') ? 'checked' : ''; ?>>Overseas Shipment
                        </label>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Part Number</label>
                  <div class="col-sm-7">
                    <input <?= $serviceable ?>  value="<?= $order->SERVICE_PART_NUMBER ?>" type="text" class="form-control" ="true" id="ID_PART_NUMBER_SC" name="ID_PART_NUMBER" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">MFR Serial Number</label>
                  <div class="col-sm-7">
                    <input <?= $serviceable ?> type="text" class="form-control" value="<?= $order->SERVICE_MFR_SERNUM ?>" id="SERVICE_MFR_SERNUM" name="SERVICE_MFR_SERNUM" >
                  </div>
                </div>

              </div>
            </div>

            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">
                <div class="form-group">
                  <div class="col-sm-12">
                  <?php if($order->REVISI_SERVICE != '0') { ?>
                <!-- echo "Submit Unserviceable" ;} else { echo "Submit Change";} ?> -->
                  <button <?= $serviceable_btn_update ?>  type="button" id="update_serv_comp" name="update_serv_comp" class="btn btn-success pull-right">Update</button>
                  <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">AWB No.</label>
                  <div class="col-sm-7">
                    <input <?= $serviceable ?> type="text" class="form-control" value="<?= $order->SERVICE_AWB_NO ?>" id="AWB_NO" name="AWB_NO" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Flight No.</label>
                  <div class="col-sm-7">
                    <input <?= $serviceable ?> type="text" class="form-control" value="<?= $order->SERVICE_FLIGHT_NO ?>" id="FLIGHT_NO" name="FLIGHT_NO" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Estimated Delivery Date</label>
                  <div class="col-sm-7">
                    <input <?= $serviceable ?> required type="date" class="form-control" name="EST_DELIVERY_DATE" id="EST_DELIVERY_DATE" value="<?= $order->SERVICE_EDT ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Airport/Destination</label>
                  <div class="col-sm-7">
                    <input <?= $serviceable ?> type="text" class="form-control" id="AIR_DEST" name="AIR_DEST" value="<?= $order->SERVICE_DESTINATION ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Attention Email</label>
                  <div class="col-sm-7">
                    <input <?= $serviceable ?> type="text" class="form-control" id="ATTENTION_EMAIL" name="ATTENTION_EMAIL" value="<?= $order->SERVICE_EMAIL ?>">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                  <button <?= $serviceable_btn ?>  type="submit" id="update_serv" name="update_serv" class="btn btn-success pull-right"><?php if($order->REVISI_SERVICE == '0') { echo "Submit Serviceable" ;} else { echo "Submit Change";} ?></button>
              </div>
            </div>
          </form>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>

<section id="unserviceable" class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default box-solid">
        <!-- <div class="box box-success"> -->
        <div class="box-header with-border">
          <h3 class="box-title">Unserviceable Component</h3>
          <!-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div> -->
        </div>
        <div class="box-body">
          <div class="row">
            <!-- <form action="<?php echo base_url() ?>index.php/Pooling/submit_unserviceable" class="form-horizontal" method="post" enctype="multipart/form-data"> -->
            <form action="" class="form-horizontal" method="POST" id="form-unserviceable" name="form-unserviceable" enctype="multipart/form-data">
            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">
                <input type="hidden" name="id" value=<?php echo $order->ID_POOLING_ORDER; ?> >
                <input type="hidden" id="revisi_unserv" name="revisi_unserv" value=<?php echo $order->REVISI_UNSERVICE; ?>>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Status :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-red"><?= $order->UNSERVICE_STATUS ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Submitter :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-blue"><?= $order->UNSERVICE_SUBMITTER ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Confirmor :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-blue"><?= $order->UNSERVICE_CONFIRMER ?></span>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Send Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input <?= $unserviceable ?> type="radio" name="SEND_TYPE" id="SEND_TYPE1" value="Serviceable" <?php echo ($order->UNSERVICE_SEND_TYPE=='Serviceable') ? 'checked' : ''; ?>>Serviceable
                        </label>
                        <label class="radio-inline">
                            <input <?= $unserviceable ?> type="radio" name="SEND_TYPE" id="SEND_TYPE2" value="Unserviceable" <?php echo ($order->UNSERVICE_SEND_TYPE=='Unserviceable') ? 'checked' : ''; ?>>Unserviceable
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Shipment Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input <?= $unserviceable ?> type="radio" name="SHIPMENT_TYPE" id="SHIPMENT_TYPE1" value="Hand-Delivered" <?php echo ($order->UNSERVICE_SHIPMENT_TYPE=='Hand-Delivered') ? 'checked' : ''; ?>>Hand-Delivered
                        </label>
                        <label class="radio-inline">
                            <input <?= $unserviceable ?> type="radio" name="SHIPMENT_TYPE" id="SHIPMENT_TYPE2" value="Overseas Shipment" <?php echo ($order->UNSERVICE_SHIPMENT_TYPE=='Overseas Shipment') ? 'checked' : ''; ?>>Overseas Shipment
                        </label>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Part Number</label>
                  <div class="col-sm-7">
                    <input <?= $unserviceable ?> type="text" class="form-control" id="PART_NUMBER2" name="PART_NUMBER2" value="<?= $order->UNSERVICE_PART_NUMBER ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Serial Number</label>
                  <div class="col-sm-7">
                    <input <?= $unserviceable ?> type="text" class="form-control" id="SERIAL_NUMBER" name="SERIAL_NUMBER" value="<?= $order->UNSERVICE_SERNUM ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Removal Date From AC</label>
                  <div class="col-sm-7">
                    <input <?= $unserviceable ?> required type="date" class="form-control" name="REMOVAL_DATE" id="REMOVAL_DATE" value="<?= $order->UNSERVICE_REMOVE_DATE ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">AC Registration</label>
                  <div class="col-sm-7">
                    <input <?= $unserviceable ?> type="text" class="form-control" id="AC_REGISTRATION" name="AC_REGISTRATION" value="<?= $order->UNSERVICE_AC_REG ?>">
                    <!-- <input type="hidden" class="form-control" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                   -->
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Removal Reason</label>
                  <div class="col-sm-7">
                    <textarea <?= $unserviceable ?> id="REMOVAL_REASON" name="REMOVAL_REASON" type="textarea" class="form-control" rows="5" cols="22" value="<?= $order->UNSERVICE_REMOVAL_REASON ?>"></textarea>
                  </div>
                </div>

              </div>
            </div>

            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">
                <div class="form-group">
                  <div class="col-sm-12">
                  <?php if($order->REVISI_UNSERVICE != 0) { ?>
                <!-- echo "Submit Unserviceable" ;} else { echo "Submit Change";} ?> -->
                  <button <?= $unserviceable_btn_update ?> type="button" id="update_unserv_comp" name="update_unserv_comp" class="btn btn-success pull-right">Update</button>
                  <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">AWB No.</label>
                  <div class="col-sm-7">
                    <input <?= $unserviceable ?> type="text" class="form-control" id="AWN_NO" name="AWN_NO" value="<?= $order->UNSERVICE_AWB_NO ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Flight No.</label>
                  <div class="col-sm-7">
                    <input <?= $unserviceable ?> type="text" class="form-control" id="FLIGHT_NO1" name="FLIGHT_NO1" value="<?= $order->UNSERVICE_FLIGHT_NO ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Estimated Delivery Date</label>
                  <div class="col-sm-7">
                    <input <?= $unserviceable ?> required type="date" class="form-control"  name="EST_DELIVERY_DATE1" id="EST_DELIVERY_DATE1" value="<?= $order->UNSERVICE_EDT ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Attention Email</label>
                  <div class="col-sm-7">
                    <input <?= $unserviceable ?> type="text" class="form-control" id="ATTENTION_EMAIL1" name="ATTENTION_EMAIL1" value="<?= $order->UNSERVICE_EMAIL ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Remarks</label>
                  <div class="col-sm-7">
                    <textarea  <?= $unserviceable ?> name="REMARKS" id="REMARKS" type="textarea" class="form-control" rows="5" cols="22" value="<?= $order->UNSERVICE_REMARKS ?>"></textarea>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Removal Reason Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input <?= $unserviceable ?> type="radio" name="REMOVAL_REASON_TYPE" id="REMOVAL_REASON1" value="Scheduled Rep" <?php echo ($order->UNSERVICE_REMOVAL_REASON_TYPE=='Scheduled Rep.') ? 'checked' : ''; ?> >Scheduled Rep.
                        </label>
                        <label class="radio-inline">
                            <input <?= $unserviceable ?> type="radio" name="REMOVAL_REASON_TYPE" id="REMOVAL_REASON2" value="On Condition" <?php echo ($order->UNSERVICE_REMOVAL_REASON_TYPE=='On Condition') ? 'checked' : ''; ?>>On Condition (Unchecked) Rep.
                        </label>
                        <label class="radio-inline">
                            <input <?= $unserviceable ?> type="radio" name="REMOVAL_REASON_TYPE" id="REMOVAL_REASON3" value="Return Unused" <?php echo ($order->UNSERVICE_REMOVAL_REASON_TYPE=='Return Unused') ? 'checked' : ''; ?>>Return Unused
                        </label>
                    </div>
                </div>
                <div class="box-footer">
                  <button <?= $unserviceable_btn ?> type="submit" id="update_unserv" name="update_unserv" class="btn btn-success pull-right"><?php if($order->REVISI_UNSERVICE == 0) { echo "Submit Unserviceable" ;} else { echo "Submit Change";} ?></button>
                </div>


              </div>
            </div>
          </form>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>

<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<script type="text/javascript">
    $(document).ready(function () {
      $(window).keydown(function(event){
        if( (event.keyCode == 13)) {
          event.preventDefault();
          return false;
        }
      });

      $("#form-serviceable").submit(function(event) {
    /* Act on the event */
        event.preventDefault();
        // $('#submit').text("Proses Simpan");
        // $('#submit').attr('disabled', true);
        var formData = new FormData($('#form-serviceable')[0]);
         $.ajax({
           url:"<?php echo base_url() ?>index.php/Pooling/submit_serviceable",
           type:'POST',
           data: formData,
           processData: false,
           contentType: false,
           success:function(response){
              location.reload(); // reloading page
           }
        });
       });

     // $("#update_unserv ").click(function(){
      $("#form-unserviceable").submit(function(event) {
    /* Act on the event */
        event.preventDefault();
        // $('#submit').text("Proses Simpan");
        // $('#submit').attr('disabled', true);
        var formData = new FormData($('#form-unserviceable')[0]);
         $.ajax({
           url:"<?php echo base_url() ?>index.php/Pooling/submit_unserviceable",
           type:'POST',
           data: formData,
           processData: false,
           contentType: false,
           success:function(response){
              location.reload(); // reloading page
           }
        });
       });
        // $('#ID_PART_NUMBER').change(function () {
        //     var selID_PN = $('#ID_PART_NUMBER').val();
        //     $.ajax({
        //         url: "<?=base_url()?>index.php/administrator/Pooling_control/get_part_number_description/", //memanggil function controller dari url
        //         type: "POST", //jenis method yang dibawa ke function
        //         data: "ID_PN="+selID_PN, //data yang akan dibawa di url
        //         success: function(data) {
        //             data = $.parseJSON(data);
        //             console.log(data);
        //             $("#COMPONENT_DESCRIPTION").val(data.PN_DESC); //after you explained the JSON response
        //             $("#COMPONENT_DESCRIPTION_V").val(data.PN_DESC); //after you explained the JSON response
        //             // document.getElementById('#SERIAL_NUMBER').removeattr('disabled');
        //         }
        //     });

        // });
        // var selID_PN = $('#ID_PART_NUMBER').val();
        //     $.ajax({
        //         url: "<?=base_url()?>index.php/administrator/Pooling_control/get_part_number_description/", //memanggil function controller dari url
        //         type: "POST", //jenis method yang dibawa ke function
        //         data: "ID_PN="+selID_PN, //data yang akan dibawa di url
        //         success: function(data) {
        //             data = $.parseJSON(data);
        //             console.log(data);
        //             $("#COMPONENT_DESCRIPTION").val(data.PN_DESC); //after you explained the JSON response
        //             $("#COMPONENT_DESCRIPTION_V").val(data.PN_DESC); //after you explained the JSON response
        //         }
        //     });

      $( "#update_unserv_comp" ).click(function() {
        // console.log('work');
        // $("#div").removeAttr("disable");
        $('#SEND_TYPE1').removeAttr("disabled");
        $('#SEND_TYPE2').removeAttr("disabled");
        $('#SHIPMENT_TYPE1').removeAttr("disabled");
        $('#SHIPMENT_TYPE2').removeAttr("disabled");
        $('#PART_NUMBER2').removeAttr("disabled");
        $('#SERIAL_NUMBER').removeAttr("disabled");
        $('#REMOVAL_DATE').removeAttr("disabled");
        $('#AC_REGISTRATION').removeAttr("disabled");
        $('#ATTENTION_EMAIL1').removeAttr("disabled");
        $('#REMOVAL_REASON').removeAttr("disabled");
        $('#AWN_NO').removeAttr("disabled");
        $('#FLIGHT_NO1').removeAttr("disabled");
        $('#EST_DELIVERY_DATE1').removeAttr("disabled");
        $('#REMARKS').removeAttr("disabled");
        $('#REMOVAL_REASON1').removeAttr("disabled");
        $('#REMOVAL_REASON2').removeAttr("disabled");
        $('#REMOVAL_REASON3').removeAttr("disabled");
        $('#update_unserv').removeAttr("disabled");
      });

      $( "#update_serv_comp" ).click(function() {
        // console.log('work');
        $('#optionsRadios1').attr("disabled", false);
        $('#optionsRadios2').attr("disabled", false);
        $('#ID_PART_NUMBER_SC').attr("disabled", false);
        $('#SERVICE_MFR_SERNUM').attr("disabled", false);
        $('#AWB_NO').attr("disabled", false);
        $('#FLIGHT_NO').attr("disabled", false);
        $('#EST_DELIVERY_DATE').attr("disabled", false);
        $('#AIR_DEST').attr("disabled", false);
        $('#ATTENTION_EMAIL').attr("disabled", false);
        $('#update_serv').attr("disabled", false);
      });

  });
</script>
