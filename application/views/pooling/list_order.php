<style>

  .tfoot .input {
          width: 100%;
          padding: 3px;
          box-sizing: border-box;
      }

</style>
<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<section class="content">
    <div class="row">
      <div class="col-lg-12 col-xs-12 col-md-12">
        <div class="box box-solid">
        <div class="box-body">
          <table id="tbpooling" class="table table-bordered table-hover table-striped">
            <col width="9%">
            <col width="1%">
            <col width="1%">
            <col width="4%">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="4%">
            <col width="7%">
            <col width="7%">
            <col width="7%">
            <col width="7%">
            <col width="7%">
            <col width="7%">
            <col width="1%">
            <col width="7%">
            <col width="7%">
            <thead >
              <tr style="background-color: #37474f; color:#ffffff;">
              	<th style="width: 150px"><center>Action  </center></th>
                <!-- <th style="width: 40px">NO</th> -->
                <th name="REFERENCE"><center>RefNo</center></th>
                <th style="display:none;"></th>
                <th><center>Category</center></th>
                <th><center>Part No</center></th>
                <th><center>PN Desc</center></th>
                <th><center>Dest</center></th>
                <th><center>AC Reg</center></th>
                <th><center>Cust PO</center></th>
                <th><center>SO Number</center></th>
                <th><center>Request By</center></th>
                <th><center>Target Delivery</center></th>
                <th><center>Request Date</center></th>
                <th><center>Status</center></th>
                <th style="display:none;"></th>
                <!-- <th><center>Respond</center></th> -->
                <th><center>Serviceable</center></th>
                <th><center>Unserviceable</center></th>
              </tr>
              <!-- <tr id="searchtr">
                <th></th>
                <th style="width: 150px">RefNo</th>
                <th style="width: 150px">Category</th>
                <th style="width: 100px">Part No</th>
                <th style="width: 200px">PN Desc</th>
                <th>Dest</th>
                <th>AC Reg</th>
                <th style="width: 125px">Cust PO</th>
                <th style="width: 125px">SO Number</th>
                <th style="width: 125px">Request By</th>
                <th>Target</th>
                <th>Request Date</th>
                <th>Status</th>
                <th>Respond</th>
                <th>Serviceable</th>
                <th>Unserviceable</th>
              </tr> -->
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr>
                <th><center></center></th>
                <!-- <th style="width: 40px">NO</th> -->
                <th><center><input type="text" placeholder="Search Reference" /></center></th>
                <th><center></center></th>
                <th><center>
                  <input type="text" placeholder="Search Category" />
                </center></th>
                <th><center><input type="text" placeholder="Search Part Number" /></center></th>
                <th><center><input type="text" placeholder="Search Description" /></center></th>
                <th><center><!-- Dest --></center></th>
                <th><center><input type="text" placeholder="Search AC Reg" /></center></th>
                <th><center><input type="text" placeholder="Search Cust PO" /></center></th>
                <th><center><!-- SO Number --></center></th>
                <th><center><!-- Request By --></center></th>
                <th><center><!-- Target Delivery --></center></th>
                <th><center><!-- Request Date --></center></th>
                <th><center><!-- Status --></center></th>
                <th><center></center></th>
                <th><center><!-- Serviceable --></center></th>
                <th><center><!-- Unserviceable --></center></th>
              </tr>
            </tfoot>
          </table>
      </div>
     </div>
   </div>
 </div>
</section>
<script>
  $(function () {
    /*$('#kaki').on("change",".input" function(){
      var data = $('.input').val();
      console.log(data);
    });*/

  /*  $('#tbpooling tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });*/

    // initComplete: function ()
    // {
    //   var r = $('#tbpooling tfoot tr');
    //   r.find('th').each(function(){
    //     $(this).css('padding', 8);
    //   });
    //   $('#tbpooling thead').append(r);
    //   $('#search_0').css('text-align', 'center');
    // },

    var unsearchable = [0,10,11,12,13,14];
    $('#tbpooling thead tr#searchtr th').each( function () {
        var title = $(this).text();
        var index = $(this)[0].cellIndex;
        // if (jQuery.inArray(index, unsearchable)==-1) {
        //
        //   $(this).html( '<input id="col'+index+'_filter" data-column="'+index+'" type="text" class="form-control columns-filter" placeholder="Search '+title+'" />' );
        // }else{
        //     $(this).empty();
        // }
    } );
    var table = $("#tbpooling").DataTable({
      // "searching": false,
      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "language": {
          "processing": "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
        // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
        },
      "serverSide": true,
      "scrollX": true,
      "ordering": true,

      "ajax": {
        "url" : "<?= site_url('api/Pooling/list_table') ?>",
        "type": 'post',
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
              return_data.push({
                1: json.data[i].REFERENCE,
                2: json.data[i].ID_POOLING_ORDER,
                3: json.data[i].REQUIREMENT_CATEGORY_NAME,
                4: json.data[i].ID_PART_NUMBER,
                5: json.data[i].DESCRIPTION,
                6: json.data[i].DELIVERY_POINT_CODE,
                7: json.data[i].AIRCRAFT_REGISTRATION,
                8: json.data[i].CUSTOMER_PO,
                9: json.data[i].CUSTOMER_PO,
                10: json.data[i].CUSTOMER_PO,
                11: json.data[i].REQUEST_ORDER_DATE,
                12: json.data[i].REQUESTED_DELIVERY_DATE,
                13: json.data[i].STATUS_ORDER_NAME,
                14: json.data[i].STATUS_ORDER,
                15: json.data[i].SERVICE_STATUS,
                16: json.data[i].UNSERVICE_STATUS
              })
            }
            /* set new token after request completed */
            // localStorage.setItem("api_token", json.token);

            /* Set User Permission */
            return return_data;

        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": true,
        },
        {
          "targets": [0],
          "className": "dt-center",
          "render": function ( data, type, full) {
           if(full[14] != '1'){
              if (full[14] == '2') {
                return '<button title="update" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button> <?php if (!($this->session->userdata('log_sess_id_customer'))) { ?> <button title="Complete Order" class="btnComplete btn btn-warning btn-xs" type="button"><i class="fa fa-check"></i></button> <?php }?> '
              }
              else {
                return '<button title="update" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button>'
              }
            }else if (full[14] == '1'){
                return '<button title="View Order" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button> <?php if (!($this->session->userdata('log_sess_id_customer'))) { ?> <button title="Confirm Order" class="btnSwal btn btn-success btn-xs" type="button"><i class="fa fa-check"></i></button> <?php }?> <button title="Cancel Order" class="btnCancel btn btn-danger btn-xs" type="button"><i class="fa fa-close"></i></button>';
            }
         }
         
        },
        {
          "targets": [2],
          "visible": false,
          "searchable": false
        },
        {
          "targets": [14],
          "visible": false,
          "searchable": false
        },
      ]
    });

    $('#tbpooling .btUpdate').on( 'click', function () {
      alert('hehehh');
        var data = $(this).parents('tr');
        console.log(data);
    });
    $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
   });

    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    // table.columns().eq(0).each(function (colIdx) {
    //   var that = this;
    //   $('input', table.column(colIdx).footer()).on('keyup change', function () {
    //       table.column (colIdx)
    //            .search (this.value.replace(/;/g, &quot;|&quot;), true, false)
    //            .draw ();
    //     } );
    // } );

   $('#tbpooling').on( 'click', 'tbody tr .btUpdate', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        var no_ = data[2];

          e.preventDefault();
         $(this).animate({
             opacity: 0 //Put some CSS animation here
         }, 500);
         setTimeout(function(){
           // OK, finished jQuery staff, let's go redirect
           window.location.href = "update_order/"+no_;
         },500);

   } );

    $('#tbpooling').on( 'click', 'tbody tr .btnSwal', function (e) {
      var data = table.row( $(this).parents('tr') ).data();
      var no_ = data[2];
    
    swal({
      title: 'Confirm Order',
      text: 'Are you sure want to confirm this order?',
      icon: 'success',
      buttons: true,
      dangerMode: true

    })
    .then(function(willUpdate){
        if (willUpdate) {
            updateStatus(no_, 2)
        } else {
          swal("You don't do anything for this Order ");
        }
    })    
  });

  $('#tbpooling').on( 'click', 'tbody tr .btnCancel', function (e) {
      var data = table.row( $(this).parents('tr') ).data();
      var no_ = data[2];
    
    swal({
      title: 'Cancel Order',
      text: 'Are you sure want to cancel this order?',
      icon: 'success',
      buttons: true,
      dangerMode: true

    })
    .then(function(willUpdate){
        if (willUpdate) {
            updateStatus(no_, 4)
        } else {
          swal("You don't do anything for this Order ");
        }
    })    
  });

  $('#tbpooling').on( 'click', 'tbody tr .btnComplete', function (e) {
      var data = table.row( $(this).parents('tr') ).data();
      var no_ = data[2];
    
    swal({
      title: 'Cancel Order',
      text: 'Are you sure want to Complete this order?',
      icon: 'success',
      buttons: true,
      dangerMode: true

    })
    .then(function(willUpdate){
        if (willUpdate) {
            updateStatus(no_, 3)
        } else {
          swal("You don't do anything for this Order ");
        }
    })    
  });

    //$("#iduserrole").select2({ width: 'resolve' });
  $(".select2").select2();

  });

  function filterColumn ( i ) {
    // console.log('#col'+i+'_filter =>'+ $('#col'+i+'_filter').val());
    $('#tbpooling').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
    ).draw();
  }

  function update_form(row){
      var data = $(this).parents('tr');
      console.log(data);
  }

  function updateStatus(id, status){
    var url = '<?php echo base_url() ?>index.php/Pooling/update_status_order';

    var data = { ID: id, status: status}

    $.ajax({
        url: url,
        method: 'POST',
        dataType: 'text',
        data: data,
        success: function(data){
                    var data = JSON.parse(data);
                    swal( data.msg , {
                          icon: "success",
                    })
                    .then(function(willgo){
                        if (willgo) {
                              window.location.href = "update_order/"+id;
                        }
                    })  
                }
            })
    }



</script>
