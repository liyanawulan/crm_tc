

<?php list($ID_LSN,$LO,$WORKSCOPE,$CONTRACTUAL_TAT,$INDUCTION_DATE,$CURRENT_TAT,$TYPE,$COMPANY_NAME) = $listProject; ?>
<h3 align="center"><u><?php echo $title; ?></u></br><?php echo $docno; ?></h3>

<div class="row">
	<div class="col-lg-12 col-xs-12">   
	  <div class="box box-solid">
		  <div class="box">
	
			  <div class="container-fluid" style="margin-left: 140px">
			  <div class="col-lg-12">
				  <table  class="stripe row-border order-column" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td><b>Landing Owner</b></td>
							<td>:</td>
							<td><?php echo $LO ?></td>
							<td><b>Customer</b></td>
							<td>:</td>
							<td><?php echo $COMPANY_NAME ?></td>
						</tr>
						<tr>
							<td><b>Workscope</b></td>
							<td>:</td>
							<td><?php echo $WORKSCOPE ?></td>
							<td><b>Type</b></td>
							<td>:</td>
							<td><?php echo $TYPE ?></td>
						</tr>
						<tr>
							<td><b>Induction Date</b></td>
							<td>:</td>
							<td><?php echo $INDUCTION_DATE ?></td>
							<td><b>Contractual TAT</b></td>
							<td>:</td>
							<td><?php echo $CONTRACTUAL_TAT ?></td>
						</tr>
					</tbody>
				</table>
			  </div>
			  </div>
			</div>
		  </div>
	  </div>
	</div>
