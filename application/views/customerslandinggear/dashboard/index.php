<section class="content-header">
<div class="panel panel-default">
   <h1 align="center">
        <u>Project List</u><br>
        Landing Gear Maintenance
    </h1>
</div>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                <table id="example" class="table table-bordered table-striped" style="width: 100%">
					               <thead>
                                <th>No.</th>
                                <th>ID_sLSN</th>
                                <th>LO</th>
                                <th>CUSTOMER</th>
                                <th>WORKSCOPE</th>
                                <th>CONTRACTUAL TAT</th>
                                <th>INDUCTION DATE</th>
                                <th>CURRENT TAT</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no = 0;
                          if (is_array($listProject )) {
                           foreach ($listProject as $row) {
                            $no++;
                             ?>
                            <tr>
                              <td><?php echo $no; ?></td>
							  <td>
								<a href= "<?php echo base_url(); ?>index.php/customerslandinggear/dashboard/overview/<?php echo $row->ID_LSN; ?>" href='javascript:void(0);'><?php echo $row->ID_LSN; ?></a>
							  </td>
                              <td><?php echo $row->LO; ?></td>
                              <td><?php echo $row->CUSTOMER_NAME; ?></td>
                              <td><?php echo $row->WORKSCOPE; ?></td>
                              <td><?php echo $row->CONTRACTUAL_TAT; ?> Days</td>
                              <td><?php echo $row->INDUCTION_DATE; ?></td>
                              <td class="success"><?php echo $row->CURRENT_TAT; ?> Days</td>
                            </tr>
                          <?php }} ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
   // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>
