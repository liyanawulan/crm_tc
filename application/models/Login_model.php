<?php

Class Login_model extends CI_Model {


    private $tb_users = 'TC_USERS';
    private $tb_users_role = 'TC_USER_ROLE';
    private $tb_users_group = 'TC_USER_GROUP';
    private $tb_customer = 'CUSTOMER';

    public function cek_login()
    {
        $username       = $this->input->post('username');
        $password       = sha1($this->input->post('password'));
        $param          = array('username' => $username, 'password' => $password);

        $this->db->select('*');
        $this->db->from($this->tb_users);
        $this->db->join($this->tb_users_role,"{$this->tb_users}.ID_USER_ROLE = {$this->tb_users_role}.ID_USER_ROLE",'INNER');
        $this->db->where($param);
        $cek = $this->db->get();

        if($cek->num_rows()==1)
        {
            $row = $cek->row();
            if($row->ID_USER_ROLE==1)
            {
                $log = array(
                    'log_sess_id_user'                  => $row->ID_USER,
                    'log_sess_name'                     => $row->NAME,
                    'log_sess_username'                 => $row->USERNAME,
                    'log_sess_password'                 => $row->PASSWORD,
                    'log_sess_id_user_role'             => $row->ID_USER_ROLE,
                    'log_sess_last_login'               => $row->LAST_LOGIN,
                    'log_sess_status'                   => $row->STATUS,
                    'log_sess_user_role'                => $row->USER_ROLE
                );

                // echo '<pre>';
                // print_r($log);
                // die;

                $this->session->set_userdata($log);
                $id         = $row->ID_USER;
                date_default_timezone_set('Asia/Jakarta');
                $datetime   = date("Y-m-d h:i:s");
                $sql        = "UPDATE {$this->tb_users} SET LAST_LOGIN = '$datetime' WHERE ID_USER = '$id'";
                $query      =  $this->db->query($sql);
                return TRUE;
            }
            elseif($row->ID_USER_ROLE==2)
            {
                $this->db->select('*');
                $this->db->from($this->tb_users);
                $this->db->join($this->tb_users_role,"{$this->tb_users}.ID_USER_ROLE = {$this->tb_users_role}.ID_USER_ROLE",'INNER');
                $this->db->join($this->tb_users_group,"{$this->tb_users}.ID_USER_GROUP = {$this->tb_users_group}.ID_USER_GROUP",'INNER');
                $this->db->where($param);
                $cek = $this->db->get();

                $row = $cek->row();

                $log = array(
                    'log_sess_id_user'                  => $row->ID_USER,
                    'log_sess_name'                     => $row->NAME,
                    'log_sess_username'                 => $row->USERNAME,
                    'log_sess_password'                 => $row->PASSWORD,
                    'log_sess_id_user_role'             => $row->ID_USER_ROLE,
                    'log_sess_id_user_group'            => $row->ID_USER_GROUP,
                    'log_sess_last_login'               => $row->LAST_LOGIN,
                    'log_sess_status'                   => $row->STATUS,
                    'log_sess_user_role'                => $row->USER_ROLE,
                    'log_sess_user_group'               => $row->USER_GROUP
                );

                // echo '<pre>';
                // print_r($log);
                // die;

                $this->session->set_userdata($log);
                $id         = $row->ID_USER;
                date_default_timezone_set('Asia/Jakarta');
                $datetime   = date("Y-m-d h:i:s");
                $sql        = "UPDATE {$this->tb_users} SET LAST_LOGIN = '$datetime' WHERE ID_USER = '$id'";
                $query      =  $this->db->query($sql);
                return TRUE;
            }
            elseif($row->ID_USER_ROLE==3)
            {
                $this->db->select('*');
                $this->db->from($this->tb_users);
                $this->db->join($this->tb_users_role,"{$this->tb_users}.ID_USER_ROLE = {$this->tb_users_role}.ID_USER_ROLE",'INNER');
                $this->db->join($this->tb_users_group,"{$this->tb_users}.ID_USER_GROUP = {$this->tb_users_group}.ID_USER_GROUP",'INNER');
                $this->db->join($this->tb_customer,"{$this->tb_users}.ID_CUSTOMER = {$this->tb_customer}.ID_CUSTOMER",'INNER');
                $this->db->where($param);
                $cek = $this->db->get();

                $row = $cek->row();
                // print_r($row);
                // die;

                $log = array(
                    'log_sess_id_user'                  => $row->ID_USER,
                    'log_sess_name'                     => $row->NAME,
                    'log_sess_username'                 => $row->USERNAME,
                    'log_sess_password'                 => $row->PASSWORD,
                    'log_sess_id_user_role'             => $row->ID_USER_ROLE,
                    'log_sess_id_user_group'            => $row->ID_USER_GROUP,
                    'log_sess_id_customer'              => $row->ID_CUSTOMER,
                    'log_sess_last_login'               => $row->LAST_LOGIN,
                    'log_sess_status'                   => $row->STATUS,
                    'log_sess_user_role'                => $row->USER_ROLE,
                    'log_sess_user_group'               => $row->USER_GROUP,
                    'log_sess_customer_name'            => $row->COMPANY_NAME
                );

                // echo '<pre>';
                // print_r($log);
                // die;

                $this->session->set_userdata($log);
                $id         = $row->ID_USER;
                date_default_timezone_set('Asia/Jakarta');
                $datetime   = date("Y-m-d h:i:s");
                $sql        = "UPDATE {$this->tb_users} SET LAST_LOGIN = '$datetime' WHERE ID_USER = '$id'";
                $query      =  $this->db->query($sql);
                return TRUE;
            }

            
        }
        else
        {
            return FALSE;
        }
    }

}
?>
