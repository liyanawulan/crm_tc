<?php
class Aircraft_m extends CI_Model{

	private $tb_name = "MS_AIRCRAFT_REGISTRATION";


	public function get_counter()
	{
		$this->db->select('*');
		$this->db->from($this->tb_name);
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return $query->num_rows();
	}

	// Listing Other


	public function get_data_list($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				ID_AIRCRAFT_REGISTRATION LIKE {$keyword} OR
				AIRCRAFT_REGISTRATION LIKE {$keyword}
			)";
		}
		$sql = " SELECT

								*
							FROM (
								SELECT ROW_NUMBER () OVER ( ORDER BY ID_AIRCRAFT_REGISTRATION ) AS RowNum, * FROM
								{$this->tb_name}
							) tb
							WHERE
								$where
								$rownum
						";
		$query = $this->db->query($sql);

		return $query->result_array();
	}
	// ./ Listing Other



	// Get Function JSON



	// Get Function JSON



	// Count Data


	// ./ Count Data



	// CRUD

	public function insert_aircraft($param){


		$data = array(
        'AIRCRAFT_REGISTRATION' => $param['AIRCRAFT_REGISTRATION']
		);

		$result = $this->db->insert($this->tb_name, $data);

		return $result;
	}
	public function update_aircraft($param){


		$data = array(
        'AIRCRAFT_REGISTRATION' => $param['AIRCRAFT_REGISTRATION']
		);
		$this->db->where('ID_AIRCRAFT_REGISTRATION', $param['ID']);
		unset($param['ID']);
		$this->db->set($data);
		$result = $this->db->update($this->tb_name);

		return $result;
	}
	public function delete_aircraft($param){

		$this->db->where('ID_AIRCRAFT_REGISTRATION', $param['ID']);

		$result = $this->db->delete($this->tb_name);

		return $result;
	}

}
