<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_Model extends CI_Model {
  // Fungsi untuk menampilkan semua data siswa
  public function view(){
    return $this->db->get('tb_order')->result();
  }
  
  // Fungsi untuk menampilkan data siswa berdasarkan NIS nya
  public function view_by($order){
    $this->db->where('order', $order);
    return $this->db->get('tb_order')->row();
  }
  
  // Fungsi untuk validasi form tambah dan ubah
  public function validation($mode){
    $this->load->library('form_validation'); // Load library form_validation untuk proses validasinya
    
    // Tambahkan if apakah $mode save atau update
    // Karena ketika update, NIS tidak harus divalidasi
    // Jadi NIS di validasi hanya ketika menambah data siswa saja
    if($mode == "save")
      $this->form_validation->set_rules('input_order', 'Order', 'required|numeric|max_length[11]');
    
    $this->form_validation->set_rules('input_status', 'Status', 'required|max_length[50]');
    $this->form_validation->set_rules('input_customer', 'Customer', 'required');
    $this->form_validation->set_rules('input_maintenance', 'Maintenance', 'required|numeric|max_length[15]');
    $this->form_validation->set_rules('input_duedate', 'Duedate', 'required');
      
    if($this->form_validation->run()) // Jika validasi benar
      return TRUE; // Maka kembalikan hasilnya dengan TRUE
    else // Jika ada data yang tidak sesuai validasi
      return FALSE; // Maka kembalikan hasilnya dengan FALSE
  }
  
  // Fungsi untuk melakukan simpan data ke tabel siswa
  public function save(){
    $data = array(
      "order"       => $this->input->post('input_order'),
      "status"      => $this->input->post('input_status'),
      "customer"    => $this->input->post('input_customer'),
      "maintenance" => $this->input->post('input_maintenance'),
      "duedate"     => $this->input->post('input_duedate')
    );
    
    $this->db->insert('tb_order', $data); // Untuk mengeksekusi perintah insert data
  }
  
  // Fungsi untuk melakukan ubah data siswa berdasarkan NIS siswa
  public function edit($nis){
    $data = array(
      "status" => $this->input->post('input_status'),
      "customer" => $this->input->post('input_customer'),
      "maintenance" => $this->input->post('input_maintenance'),
      "duedate" => $this->input->post('input_duedate')
    );
    
    $this->db->where('order', $order);
    $this->db->update('tb_order', $data); // Untuk mengeksekusi perintah update data
  }
  
  // Fungsi untuk melakukan menghapus data siswa berdasarkan NIS siswa
  public function delete($orderr){
    $this->db->where('order', $order);
    $this->db->delete('tb_order'); // Untuk mengeksekusi perintah delete data
  }
}