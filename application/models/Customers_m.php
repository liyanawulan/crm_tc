<?php
class Customers_m extends CI_Model{

	private $tb_name = "CUSTOMER";


	public function get_counter()
	{
		$this->db->select('*');
		$this->db->from($this->tb_name);
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return $query->num_rows();
	}

	// Listing Other


	public function get_data_list($param, $ext=null)
	{
		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}
		// print_r($param);

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				ID_CUSTOMER LIKE {$keyword} OR
				COMPANY_NAME LIKE {$keyword} OR
				COMPANY_EMAIL LIKE {$keyword} OR
				COMPANY_ADDRESS LIKE {$keyword}
			)";
		}
		$sql = " SELECT
						*
					FROM (
						SELECT ROW_NUMBER () OVER ( ORDER BY ID_CUSTOMER ) AS RowNum, * FROM
						{$this->tb_name}
					) tb
				WHERE
					$where
					$rownum
						";
		$query = $this->db->query($sql);
		// echo $this->db->last_query();

		return $query->result_array();
	}

	public function get_customer_list()
	{
		$data = $this->db->get($this->tb_name);
		return $data->result();
	}
	// ./ Listing Other



	// Get Function JSON



	// Get Function JSON



	// Count Data


	// ./ Count Data



	// CRUD

}
