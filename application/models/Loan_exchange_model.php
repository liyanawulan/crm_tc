
<?php

Class Loan_exchange_model extends CI_Model {

	private $column_order_loan = array('VBELN',
										'VBELN',
										'MATNR',
										'ARKTX',
										'TYPESERVICE',
										'SERNR',
										'PERIOD',
										'VBEGDAT',
										'VENDDAT',
										'PERIOD',
										'STATUS',
										// 'LFDAT'
                            );

	private $column_order_exc = array('VBELN',
										'VBELN',
										'MATNR',
										'ARKTX',
										'SERNR',
										'TYPESERVICE'
                            );
	private $column_req_loan = array(
								'ID_LOAN_REQUEST',
								'PART_NUMBER',
								'PERIOD_FROM',
								'PERIOD_TO',
								'CONDITION',
								'CONTACT_INFO'
                            	);
	private $column_req_exc = array(
							'PART_NUMBER',
							'EXCHANGE_TYPE',
							'EXCHANGE_RATE',
							'CONDITION',
							'CONTACT_INFO '
                            	);
	

	public function select_loan_request($param, $ext=null)
	{
	    $keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum > {$param['start']}
				AND RowNum < {$param['end']}";
		}
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_req_loan[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY ID_LOAN_REQUEST ASC";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				PART_NUMBER LIKE {$keyword} OR
				CONDITION LIKE {$keyword} OR
				PERIOD_FROM LIKE {$keyword} OR
				PERIOD_TO LIKE {$keyword} OR
				CONTACT_INFO LIKE {$keyword}
			)";
		}
		$sql = "SELECT
					*
					FROM
						(
							SELECT 
							ROW_NUMBER () OVER ( $order ) AS RowNum,
							*	
							FROM TC_LOAN_REQUEST
							WHERE  YEAR(CREATED_DATE) = YEAR ( getdate ( ) )  
						) tb
					WHERE
						$where
						$rownum
				";
		$query = $this->db->query($sql);

		return $query->result_array();
		$this->db->select('*');
		$this->db->from('M_LOAN_REQUEST');
		$this->db->order_by('ID_LOAN_REQUEST', 'DESC');
		
		$query = $this->db->get();	
		return $query->result_object();
	}

	public function count_loan_order()
	{
		$this->db->select('*');
		$this->db->from('M_SALESORDER');
		$this->db->where('AUART', 'ZP01');
		$query = $this->db->get();	
		return $query->num_rows();
	}

	public function update_code_user($name, $unit)
	{
		$data = array(
        'UNIT_CODE' => $unit,
        'USERNAME' => $name,
		);
		
		return  $this->db->insert('SAP_USERLIST', $data);
	}

	public function getlistsalesperson() 
	{
		$keyword = "'%%'";
		$rownum = '';
		$where = '';
		if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where = " AND S.KUNNR LIKE '{$kunnr}' ";
			}
		$sql = "SELECT DISTINCT
					S.ERNAM,
					L.UNIT_CODE
				FROM
					M_SALESORDER S
					LEFT JOIN SAP_USERLIST L ON S.ERNAM = L.USERNAME
				WHERE
					S.AUART IN ('ZP01','ZX01') 
					$where ";

		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
	public function getListDataOrder($param, $ext=null) 
	{
		$keyword = "'%%'";
		$rownum = '';
		$where = '';
		$where_kunnr = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum > {$param['start']}
				AND RowNum < {$param['end']}";
		}
		$sp = "";
		// print_r();
		if ($param['salesperson'])
		{
			$sps = $param['salesperson'];
			$swe = "('" . implode("','",$sps) . "')";
			$sp = "AND B.ERNAM IN {$swe} ";
			// echo $sp;
		}
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order_loan[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$kunnr = '';
			if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where_kunnr = " AND KUNNR LIKE '{$kunnr}' ";
			}
			$where .= "AND (
				VBELN LIKE {$keyword} OR
				MATNR LIKE {$keyword} OR
				ARKTX LIKE {$keyword} OR
				TYPESERVICE LIKE {$keyword} OR
				SERNR LIKE {$keyword} OR
				PERIOD LIKE {$keyword} OR
				VBEGDAT LIKE {$keyword} OR
				VENDDAT LIKE {$keyword} OR
				STATUS LIKE {$keyword}
			)";
		}
		$sql = "WITH CC AS ( SELECT DISTINCT VBELN, MATNR, ARKTX FROM M_SALESORDER WHERE MATKL = 'DIEN' AND AUART IN ( 'ZP01' )),
					BB AS (
					SELECT
						H.ARKTX AS 'TYPESERVICE',
						B.*,
						CASE
							WHEN ( ( VBEGDAT != '00000000' ) AND ( VENDDAT != '00000000' ) ) THEN
							DATEDIFF( DAY, CONVERT ( DATE, VBEGDAT, 104 ), CONVERT ( DATE, VENDDAT, 104 ) ) 
						END AS PERIOD,
						CASE
							WHEN ( GBSTA LIKE 'C' ) THEN
							'Completed' 
							WHEN ( GBSTA LIKE 'A' ) THEN
							'Open' ELSE GBSTA 
						END AS STATUS
					FROM
						M_SALESORDER B
						LEFT JOIN CC H ON B.VBELN = H.VBELN 
					WHERE
						B.AUART = 'ZP01' 
						AND B.MATKL != 'DIEN' 
						 $where_kunnr $sp 
						),
						DD AS (
					SELECT
						ROW_NUMBER ( ) OVER ( $order ) AS RowNum,
						VBELN,
						MATNR,
						ARKTX,
						ERNAM,
						VBEGDAT,
						VENDDAT,
						MATKL,
						SERNR,
						LFDAT,
						BSTNK,
						TYPESERVICE,
						STATUS,
						PERIOD,			
					CASE
							
							WHEN ( VBEGDAT != '00000000' ) THEN
							VBEGDAT ELSE ' ' 
						END AS START_DATE,
					CASE
							
							WHEN ( VENDDAT != '00000000' ) THEN
							VENDDAT ELSE ' ' 
						END AS END_DATE,
					US.UNIT_CODE
					FROM
						BB LEFT JOIN SAP_USERLIST US ON BB.ERNAM = US.USERNAME
					WHERE 
						MATKL != 'DIEN' AND (US.UNIT_CODE LIKE 'TC%' OR US.UNIT_CODE IS NULL)
						$where
						 ) SELECT * FROM DD WHERE MATKL != 'DIEN' $rownum ;";
		// $sql = "SELECT
		// 			RowNum, 
		// 			VBELN, 
		// 			MATNR, 
		// 			DATEDIFF(DAY, convert(date,AUDAT, 112),convert(date,ERDAT, 112)) as RANGE_DATE, 
		// 			ARKTX,
		// 			ERNAM,
		// 			VBEGDAT,
		// 			VENDDAT,
		// 			-- MATKL,
		// 			-- MTART,
		// 			-- ERDAT_VBFA,
		// 			SERNR,
		// 			LFDAT,
		// 			-- LFSTA,
		// 			CASE
		// 				WHEN (GBSTA LIKE 'C') THEN 'Completed'
		// 				WHEN (GBSTA LIKE 'A') THEN 'Open'
		// 				ELSE GBSTA  
		// 			END AS STATUS,
		// 			BSTNK,
		// 			TYPESERVICE,
		// 			CASE
		// 				WHEN (VBEGDAT != '00000000') THEN VBEGDAT
		// 				ELSE ' '  
		// 			END AS START_DATE,
		// 			CASE
		// 				WHEN ( VENDDAT != '00000000' ) THEN	VENDDAT 
		// 				ELSE ' ' 
		// 			END AS END_DATE,
		// 			CASE
		// 				WHEN ((VBEGDAT != '00000000') AND (VENDDAT != '00000000')) THEN
		// 				DATEDIFF( DAY, CONVERT ( DATE, VBEGDAT, 104 ), CONVERT ( DATE, VENDDAT, 104 ) )  
		// 			END AS PERIOD
		// 			FROM
		// 				(
		// 				SELECT
		// 					ROW_NUMBER ( ) OVER ( ORDER BY B.VBELN ) AS RowNum, H.ARKTX AS 'TYPESERVICE',
		// 					B.*
		// 				FROM
		// 					M_SALESORDER B LEFT JOIN TC_V_LOANEXCHANGE H ON B.VBELN = H.VBELN 
		// 				WHERE
		// 					B.AUART = 'ZP01' 
		// 					AND B.MATKL != 'DIEN' 
		// 					AND B.KUNNR LIKE '%{$kunnr}%'
		// 				) TB
		// 			WHERE
		// 				$where
		// 				$rownum
		// 		";
		$query = $this->db->query($sql);
		$n_result = [];
		$tmp = [];
		$result = $query->result_array();
		// foreach ($result as $key => $value) {
  //           $tmp = $value;
  //           $ernam = substr($value['ERNAM'], 1);
  //           $response = $this->set_curl($ernam);
  //           if (!empty($response)) {
  //           	foreach ($response as $key => $value2) {
	 //                if (array_key_exists('unit_code', $response)) {
  //                       if (strpos($value2['unit_code'], 'TC') !== FALSE) {
		// 	                $n_result[] = $tmp;
		// 	                echo $value2['unit_code'];
  //                       }
	 //                 }
  //           	}
  //           }
  //       }

        return $result;	
    }

    public function getListDataOrder_count($param, $ext=null) 
	{
		$keyword = "'%%'";
		$rownum = '';
		$where = '';
		$where_kunnr = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			// $rownum = "	 AND RowNum >= {$param['start']}
			// 	AND RowNum < {$param['end']}";
		}
		$sp = "";
		// print_r();
		if ($param['salesperson'])
		{
			$sps = $param['salesperson'];
			$swe = "('" . implode("','",$sps) . "')";
			$sp = "AND B.ERNAM IN {$swe} ";
			// echo $sp;
		}
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order_loan[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$kunnr = '';
			if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where_kunnr = " AND KUNNR LIKE '{$kunnr}' ";
			}
			$where .= "AND (
				VBELN LIKE {$keyword} OR
				MATNR LIKE {$keyword} OR
				ARKTX LIKE {$keyword} OR
				TYPESERVICE LIKE {$keyword} OR
				SERNR LIKE {$keyword} OR
				PERIOD LIKE {$keyword} OR
				VBEGDAT LIKE {$keyword} OR
				VENDDAT LIKE {$keyword} OR
				STATUS LIKE {$keyword}
			)";
		}
		$sql = "WITH CC AS ( SELECT DISTINCT VBELN, MATNR, ARKTX FROM M_SALESORDER WHERE MATKL = 'DIEN' AND AUART IN ( 'ZP01' )),
					BB AS (
					SELECT
						H.ARKTX AS 'TYPESERVICE',
						B.*,
						CASE
							WHEN ( ( VBEGDAT != '00000000' ) AND ( VENDDAT != '00000000' ) ) THEN
							DATEDIFF( DAY, CONVERT ( DATE, VBEGDAT, 104 ), CONVERT ( DATE, VENDDAT, 104 ) ) 
						END AS PERIOD,
						CASE
							WHEN ( GBSTA LIKE 'C' ) THEN
							'Completed' 
							WHEN ( GBSTA LIKE 'A' ) THEN
							'Open' ELSE GBSTA 
						END AS STATUS
					FROM
						M_SALESORDER B
						LEFT JOIN CC H ON B.VBELN = H.VBELN 
					WHERE
						B.AUART = 'ZP01' 
						AND B.MATKL != 'DIEN' 
						 $where_kunnr $sp 
						),
						DD AS (
					SELECT
						ROW_NUMBER ( ) OVER ( $order ) AS RowNum,
						VBELN,
						MATNR,
						ARKTX,
						ERNAM,
						VBEGDAT,
						VENDDAT,
						MATKL,
						SERNR,
						LFDAT,
						BSTNK,
						TYPESERVICE,
						STATUS,
						PERIOD,
					CASE
							
							WHEN ( VBEGDAT != '00000000' ) THEN
							VBEGDAT ELSE ' ' 
						END AS START_DATE,
					CASE
							
							WHEN ( VENDDAT != '00000000' ) THEN
							VENDDAT ELSE ' ' 
						END AS END_DATE,
					US.UNIT_CODE
					FROM
						BB LEFT JOIN SAP_USERLIST US ON BB.ERNAM = US.USERNAME
					WHERE 
						MATKL != 'DIEN' AND (US.UNIT_CODE LIKE 'TC%' OR US.UNIT_CODE IS NULL)
						$where
						 ) SELECT COUNT(*) AS 'JUMLAH' FROM DD WHERE MATKL != 'DIEN' ;";

		$query = $this->db->query($sql);
		$n_result = [];
		$tmp = [];
		$result = $query->result_array();

        return $result;	
    }

	public function getListDataOrderDT($param) 
	{
		$where = "";
		$sp = "";
		// print_r();
		if ($param)
		{
			$sps = $param;
			$swe = "('" . implode("','",$sps) . "')";
			$sp = "AND B.ERNAM IN {$swe} ";
			// echo $sp;
		}
		
		$kunnr = '';
		if ($this->session->userdata('log_sess_id_customer')) {
			$kunnr = $this->session->userdata('log_sess_id_customer');
			$where = " AND KUNNR LIKE '{$kunnr}' ";
		}
			// $where .= " (
			// 	VBELN LIKE {$keyword} OR
			// 	MATNR LIKE {$keyword} OR
			// 	ARKTX LIKE {$keyword} 
			// )";
		$sql = "WITH CC AS ( SELECT DISTINCT VBELN, MATNR, ARKTX FROM M_SALESORDER WHERE MATKL = 'DIEN' AND AUART IN ( 'ZP01' )),
					BB AS (
					SELECT
						ROW_NUMBER ( ) OVER ( ORDER BY B.VBELN ) AS RowNum,
						H.ARKTX AS 'TYPESERVICE',
						B.* 
					FROM
						M_SALESORDER B
						LEFT JOIN CC H ON B.VBELN = H.VBELN 
					WHERE
						B.AUART = 'ZP01' 
						AND B.MATKL != 'DIEN' 
						$where $sp
						),
						DD AS (
					SELECT
						RowNum,
						VBELN,
						MATNR,
						ARKTX,
						ERNAM,
						VBEGDAT,
						VENDDAT,
						MATKL,
						SERNR,
						LFDAT,
					CASE
						
						WHEN ( GBSTA LIKE 'C' ) THEN
						'Completed' 
						WHEN ( GBSTA LIKE 'A' ) THEN
						'Open' ELSE GBSTA 
						END AS STATUS,
						BSTNK,
						TYPESERVICE,
					CASE
							
							WHEN ( VBEGDAT != '00000000' ) THEN
							VBEGDAT ELSE ' ' 
						END AS START_DATE,
					CASE
							
							WHEN ( VENDDAT != '00000000' ) THEN
							VENDDAT ELSE ' ' 
						END AS END_DATE,
					CASE
							
							WHEN ((
									VBEGDAT != '00000000' 
									) 
								AND ( VENDDAT != '00000000' )) THEN
							DATEDIFF( DAY, CONVERT ( DATE, VBEGDAT, 104 ), CONVERT ( DATE, VENDDAT, 104 ) ) 
					END AS PERIOD 
					FROM BB
					WHERE 
						MATKL != 'DIEN'
						$where
						) SELECT * FROM DD ;";
		
		$query = $this->db->query($sql);
		$result = $query->result_array();

        return $result;	
    }
	

	public function select_exchange_request($param, $ext=null)
	{
		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum > {$param['start']}
				AND RowNum < {$param['end']}";
		}
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_req_exc[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY ID_EXCHANGE_REQUEST ASC";
        }

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				PART_NUMBER LIKE {$keyword} OR
				CONDITION LIKE {$keyword} OR
				EXCHANGE_TYPE LIKE {$keyword} OR
				EXCHANGE_RATE LIKE {$keyword} OR
				CONTACT_INFO LIKE {$keyword}
			)";
		}
		$sql = "SELECT
					*
					FROM
						(
							SELECT 
							ROW_NUMBER () OVER ( $order ) AS RowNum,
							*	
							FROM TC_EXCHANGE_REQUEST
							WHERE  YEAR(CREATED_DATE) = YEAR ( getdate ( ) ) 
						) tb
					WHERE
						$where
						$rownum
				";
		$query = $this->db->query($sql);

		return $query->result_array();
	}

	function getListDataOrderExchange($param, $ext=null) {
		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum > {$param['start']}
				AND RowNum < {$param['end']}";
		}
		$where = '';
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order_exc[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$kunnr = '';
			if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where_kunnr = " AND KUNNR LIKE '{$kunnr}' ";
			}
			$where .= "(
				VBELN LIKE {$keyword} OR
				MATNR LIKE {$keyword} OR
				ARKTX LIKE {$keyword} 
			)";
		}
	
		$sql = "
				WITH CC AS ( SELECT DISTINCT VBELN, MATNR, ARKTX FROM M_SALESORDER WHERE MATKL = 'DIEN' AND AUART IN ( 'ZX01' ) ),
					BB AS (
					SELECT
						H.ARKTX AS 'TYPESERVICE',
						B.* 
					FROM
						M_SALESORDER B
						LEFT JOIN cc H ON B.VBELN = H.VBELN 
					WHERE
						B.AUART = 'ZX01' 
						AND B.MATKL != 'DIEN' 
						AND B.KUNNR LIKE '%{$kunnr}%'
						AND (SUBSTRING ( B.AUDAT, 1, 4 ) ) = YEAR ( getdate ( ) )
						),
						DD AS (
					SELECT
						ROW_NUMBER ( ) OVER ( $order ) AS RowNum,
						VBELN,
						MATNR,
						ARKTX,
						TYPESERVICE,
						SERNR,
						AUDAT
					FROM
						BB 
					WHERE
						$where
						) SELECT
						* 
					FROM
						DD
					WHERE
						$where
		 				$rownum;";

		
		$query = $this->db->query($sql);
		$n_result = [];
		$tmp = [];
		$result = $query->result_array();

        return $result;	
    }

    function getListDataOrderExchange_count($param, $ext=null) {
		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			// $rownum = "	 AND RowNum >= {$param['start']}
			// 	AND RowNum < {$param['end']}";
		}
		$where = '';
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order_exc[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$kunnr = '';
			if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where_kunnr = " AND KUNNR LIKE '{$kunnr}' ";
			}
			$where .= "(
				VBELN LIKE {$keyword} OR
				MATNR LIKE {$keyword} OR
				ARKTX LIKE {$keyword} 
			)";
		}
	
		$sql = "
				WITH CC AS ( SELECT DISTINCT VBELN, MATNR, ARKTX FROM M_SALESORDER WHERE MATKL = 'DIEN' AND AUART IN ( 'ZX01' ) ),
					BB AS (
					SELECT
						H.ARKTX AS 'TYPESERVICE',
						B.* 
					FROM
						M_SALESORDER B
						LEFT JOIN cc H ON B.VBELN = H.VBELN 
					WHERE
						B.AUART = 'ZX01' 
						AND B.MATKL != 'DIEN' 
						AND B.KUNNR LIKE '%{$kunnr}%'
						AND (SUBSTRING ( B.AUDAT, 1, 4 ) ) = YEAR ( getdate ( ) )
						),
						DD AS (
					SELECT
						ROW_NUMBER ( ) OVER ( $order ) AS RowNum,
						VBELN,
						MATNR,
						ARKTX,
						TYPESERVICE,
						SERNR,
						AUDAT
					FROM
						BB 
					WHERE
						$where
						) SELECT
						count(*) AS 'JUMLAH'
					FROM
						DD
					WHERE
						$where
					;";

		
		$query = $this->db->query($sql);
		$n_result = [];
		$tmp = [];
		$result = $query->result_array();

        return $result;	
    }


    public function insert_loan_request()
	{
		$insert = array(
			'PART_NUMBER' 			=> $this->input->post('PART_NUMBER'),
			'PERIOD_FROM' 			=> $this->input->post('PERIOD_FROM'),
			'PERIOD_TO' 			=> $this->input->post('PERIOD_TO'),
			'CONDITION' 			=> $this->input->post('CONDITION'),
			'CONTACT_INFO' 			=> $this->input->post('CONTACT_INFO'),
			'CREATED_DATE' 			=> date('Y-m-d H:i:s'),
			'STATUS' 				=> 1,
		);
		return $this->db->insert('TC_LOAN_REQUEST', $insert);
	}

	public function insert_exchange_request()
	{
		$insert = array(
			'PART_NUMBER' 			=> $this->input->post('PART_NUMBER'),
			'EXCHANGE_TYPE' 		=> $this->input->post('EXCHANGE_TYPE'),
			'EXCHANGE_RATE' 		=> $this->input->post('EXCHANGE_RATE'),
			'CONDITION' 			=> $this->input->post('CONDITION'),
			'CONTACT_INFO' 			=> $this->input->post('CONTACT_INFO'),
			'CREATED_DATE' 			=> date('Y-m-d H:i:s'),
			'STATUS' 				=> 1,
		);
		return $this->db->insert('TC_EXCHANGE_REQUEST', $insert);
	}
}

?>