<?php

Class User_model extends CI_Model {

	public function get_user_role()
	{
		$this->db->order_by('ID_USER_ROLE', 'ASC');
		$data = $this->db->get('TC_USER_ROLE');
		return $data->result();
	}

	public function get_user_group()
	{
		$this->db->order_by('ID_USER_GROUP', 'ASC');
		$data = $this->db->get('TC_USER_GROUP');
		return $data->result();
	}

	public function get_customer()
	{
		$this->db->order_by('ID_CUSTOMER', 'ASC');
		$data = $this->db->get('CUSTOMER');
		return $data->result();
	}

	public function select_user()
	{
	    $this->db->select('*');
		$this->db->from('TC_USERS');
		$this->db->join('TC_USER_ROLE','TC_USERS.ID_USER_ROLE = TC_USER_ROLE.ID_USER_ROLE','INNER');
		$this->db->order_by('ID_USER', 'DESC');
		
		$query = $this->db->get();	
		return $query->result_object();
	}

	public function select_user_where_id($ID_USER)
	{
	    $this->db->select('*');
		$this->db->from('TC_USERS');
		$this->db->join('TC_USER_ROLE','TC_USERS.ID_USER_ROLE = TC_USER_ROLE.ID_USER_ROLE','INNER');
		$this->db->where('ID_USER', $ID_USER);
		$this->db->order_by('ID_USER', 'DESC');
		
		$query = $this->db->get();	
		return $query->row_array();
	}

	public function insert_user()
	{
		$insert = array(
			'NAME' 					=> $this->input->post('NAME'),
			'USERNAME' 				=> $this->input->post('USERNAME'),
			'PASSWORD' 				=> sha1($this->input->post('PASSWORD')),
			'EMAIL' 				=> $this->input->post('EMAIL'),
			'ID_USER_ROLE' 			=> $this->input->post('ID_USER_ROLE'),
			'ID_USER_GROUP' 		=> $this->input->post('ID_USER_GROUP'),
			'ID_CUSTOMER' 			=> $this->input->post('ID_CUSTOMER'),
			'STATUS' 				=> 1,
		);
		return $this->db->insert('TC_USERS', $insert);
	}

	public function update_user($ID_USER)
	{
		$update = array(
			'NAME' 					=> $this->input->post('NAME'),
			'USERNAME' 				=> $this->input->post('USERNAME'),
			'EMAIL' 				=> $this->input->post('EMAIL'),
			'ID_USER_ROLE' 			=> $this->input->post('ID_USER_ROLE'),
			'ID_USER_GROUP' 		=> $this->input->post('ID_USER_GROUP'),
			'ID_CUSTOMER' 			=> $this->input->post('ID_CUSTOMER'),
			'STATUS' 				=> 1,
		);
		if($this->input->post('PASSWORD'))
		{
			$update['PASSWORD'] 	= sha1($this->input->post('PASSWORD'));
		}

		// echo '<pre>';
		// print_r($update);
		// die;

		$this->db->where('ID_USER', $ID_USER);
		return $this->db->update('TC_USERS', $update);
	}

	public function delete_user($ID_USER)
	{
		return $this->db->delete('TC_USERS',array('ID_USER' => $ID_USER));
	}

}

?>