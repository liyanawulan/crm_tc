<?php

Class Survey_model extends CI_Model {

  private $cust_name = "CUSTOMER";
  private $survey_table = "TC_CSI_SURVEY";

	// Get City departments
  function getDocumentNumber($type){
  	$where = '';
  	if ($this->session->userdata('log_sess_id_customer')){
  		$kunnr = $this->session->userdata('log_sess_id_customer');
      if ($type=='Landing Gear') {
        $where = "AND PARNR LIKE '{$kunnr}'";
      }
      else {
  		$where = " AND KUNNR LIKE '{$kunnr}'";
      }
  	}
    if ($type=='Pooling') {
        $sql = "SELECT DISTINCT 
    				AUFNR as doc_no
    			FROM M_PMORDERH
    			$where ";
    } else if ($type=='Loan Exchange') {
    	// $sql = "SELECT DISTINCT 
    	// 			VBELN as doc_no
    	// 		FROM M_SALESORDER WHERE LFSTA = 'C' AND AUART IN ('ZP01','ZX01')
    	// 		$where ";
      $sql = "WITH C2 AS (
                SELECT DISTINCT
                    VBELN,
                    BWART,
                    LFGSA,
                    LFDAT,
                    ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, LFSTA DESC ) AS RowNum
                FROM
                    M_SALESORDER S 
                WHERE
                    BWART LIKE '621' AND AUART IN ('ZP01','ZX01') $where
                )
              SELECT (1*VBELN) as doc_no FROM C2 WHERE RowNum = 1 ;";
    } else if ($type=='Retail') {
       //  $sql = "SELECT DISTINCT 
    			// 	VBELN as doc_no
    			// FROM M_SALESORDER WHERE LFSTA = 'C' AND AUART = 'ZC01'
    			// $where ";
        $sql = "WITH AA AS 
                    (SELECT
                        DISTINCT AUFNR, IDAT2, KDAUF
                      FROM M_PMORDER
                      WHERE 
                         IDAT2 != '00000000' AND ( SUBSTRING ( IDAT2, 5, 4 ) ) = YEAR ( getdate ( ) ) ),
                C2 AS (
                        SELECT DISTINCT
                            VBELN,
                            BWART,
                            LFGSA,
                            LFDAT,
                            ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, LFSTA DESC ) AS RowNum
                        FROM
                            AA A
                            LEFT JOIN M_SALESORDER S ON A.KDAUF = (1*S.VBELN)
                        WHERE
                            BWART LIKE '601' $where
                        )
                SELECT (1*VBELN) as doc_no FROM C2 WHERE RowNum = 1 ;";
    } else if ($type=='Landing Gear') {
       $sql = "SELECT DISTINCT 
          				REVNR as doc_no
          			FROM M_REVISION  WHERE TXT04 = 'CLSD' AND IWERK = 'WSWB'
          			$where ";
    } 
    $response = array();
    $query = $this->db->query($sql);
    $response = $query->result_array();
    // print_r($response);
    // die;

    return $response;
  }

  public function get_customer_list($id)
  {
    if ($id){
      $this->db->where('ID_CUSTOMER', $id);
    }
    $data = $this->db->get($this->cust_name);
    return $data->result();
  }

  public function insert_survey()
  { $insert = array(
        // 'ID_USER_POOL'       => $this->session->userdata(''),
        'USER_ID'               => $this->session->userdata('log_sess_id_user'),
        'NAME'                  => $this->input->post('inputName'),
        'COMPANY'               => $this->input->post('companyName'),
        'EMAIL'                 => $this->input->post('exampleInputEmail1'),
        'ESN'                   => $this->input->post('inputProduct'),
        'QUALITY_RATE'          => (int)$this->input->post('quality-rating'),
        'QUALITY_REASON'        => $this->input->post('quality-reason'),
        'TAT_RATE'              => (int)$this->input->post('tat-rating'),
        'TAT_REASON'            => $this->input->post('tat-reason'),
        'PRICE_RATE'            => (int)$this->input->post('price-rating'),
        'PRICE_REASON'          => $this->input->post('price-reason'),
        'MAINTENANCE_SUPPORT_RATE'  => (int)$this->input->post('services-rating'),
        'CORRECT_TIMELINESS_RATE' => (int)$this->input->post('timelines-rating'),
        'REASON'                => $this->input->post('services-reason'),
        'OVERALL_SERVICE_RATE'    => (int)$this->input->post('overall-rating'),
        'CUSTOMER_SERVICES_REASON'  => $this->input->post('overall-reason'),
        'MANAGEMENT_INFORMATION_RATE'=>  (int)$this->input->post('documentation-rating'),
        'DOCUMENTATION_REASON'=> $this->input->post('documentation-reason'),
        'DOCUMENTATION_REASON'    => $this->input->post('project-reason'),
        'TYPE_OF_SERVICE'    => $this->input->post('category_menu'),
        'DOCUMENT_NO'    => $this->input->post('list_menu'),
      );

    return $this->db->insert('TC_CSI_SURVEY', $insert);
  }

}