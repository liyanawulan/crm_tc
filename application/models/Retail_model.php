<?php

Class Retail_model extends CI_Model {

    // ======================================================================================================================================================================================
    //                                                                                  SUM AND COUNT
    // ======================================================================================================================================================================================
    private $order_sort = array('MAINTENANCE_ORDER' => 'asc');
    private $column_order = array('MAINTENANCE_ORDER', 
                            'MAINTENANCE_ORDER',
                            'SALES_ORDER',
                            'MAINTENANCE_ORDER',
                            'PURCHASE_ORDER',
                            'PART_NUMBER',
                            'PART_NAME',
                            'SERIAL_NUMBER',
                            'RECEIVED_DATE',
                            'QUOTATION_DATE',
                            'APPROVAL_DATE',
                            'TAT',
                            'TXT_STAT',
                            'DELIVERY_DATE',
                            'REMARKS',
                            'BILLING_STATUS',
                            );

    function getStatusOrder(){
        
        $kunnr = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $where = " C.KUNNR LIKE '{$kunnr}' ";
        }
        else {
           $where = "D.ORDER_TYPE LIKE 'GA03'"; 
        }

        $sql = "WITH DD AS (
                    SELECT
                        H.AUFNR AS 'MAINTENANCE_ORDER',
                        H.MATNR AS 'PART_NUMBER',
                        H.MAKTX AS 'PART_NAME',
                        H.AUART AS 'ORDER_TYPE',
                        H.KDAUF,
                        H.IDAT2 AS 'TECO_DATE',
                    CASE
                            WHEN ( H.SERNR != '' ) THEN
                            SERNR 
                            WHEN ( H.SERIALNR != '' ) THEN
                            SERIALNR 
                        END AS 'SERIAL_NUMBER'
                    FROM M_STATUSPM B RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                    WHERE
                        H.AUART = 'GA03'
                        AND B.PRCTR LIKE 'GMFTC%'  
                        AND ( H.IDAT2 = '00000000' OR ( SUBSTRING ( H.IDAT2, 5, 4 ) ) = YEAR ( getdate ( ) ) ) 
                        ),
                    C1 AS (
                        SELECT DISTINCT
                            VBELN,
                            BSTNK,
                            AUDAT,
                            KUNNR,
                            NAME1,
                            VBELN_BILL,
                            FKSAA,
                            ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, FKSAA DESC ) AS RowNum 
                        FROM
                            DD D
                            LEFT JOIN M_SALESORDER S ON D.KDAUF = S.VBELN
                        WHERE KUNNR LIKE '%{$kunnr}%' 
                        ),
                        CD AS (
                            SELECT
                                CG.VBELN AS 'SALES_ORDER',
                                CG.BSTNK AS 'PURCHASE_ORDER',
                                CG.AUDAT AS 'RECEIVED_DATE',
                                CG.KUNNR AS 'KUNNR',
                                CG.NAME1 AS 'CUSTOMER',
                                CG.VBELN_BILL,
                                CG.FKSAA,
                                SB.RFBSK
                            FROM
                                C1 CG LEFT JOIN M_SALESBILLING SB ON CG.VBELN_BILL = SB.VBELN
                            WHERE
                                RowNum = 1 
                            ),
                        C2 AS (
                        SELECT DISTINCT
                            VBELN,
                            BWART,
                            LFGSA,
                            LFDAT,
                            ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, LFSTA DESC ) AS RowNum
                        FROM
                            CD C11
                            LEFT JOIN M_SALESORDER S ON C11.SALES_ORDER = S.VBELN 
                        WHERE
                            BWART LIKE '601' 
                        ),
                        CF AS ( SELECT * FROM C2 WHERE RowNum = 1 ),
                        CC AS ( SELECT C4.*, C3.LFGSA, C3.LFDAT, C3.BWART
                                        FROM CD C4
                                        LEFT JOIN CF C3 ON C3.VBELN = C4.SALES_ORDER ),
                    AA AS (
                    SELECT
                    CASE    
                        WHEN
                            TXT50 NOT LIKE '' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' THEN
                                TXT1 
                            END AS STAT,
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            M.*, D.*, C.*
                        FROM
                            DD D
                            INNER JOIN M_STATUSPM M ON D.MAINTENANCE_ORDER = M.AUFNR
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where 
                        ),
                    HG AS (SELECT
                        *,
                    CASE
                            WHEN LFGSA = 'C' THEN 'Delivered'
                            WHEN STAT LIKE 'FRA' THEN
                            'Ready To Deliver Return As is Conditiion' 
                            WHEN STAT LIKE 'FSB' THEN
                            'Ready To Deliver Serviceable Condition' 
                            WHEN STAT LIKE 'F%' THEN
                            'Ready To Deliver BER' 
                            WHEN STAT LIKE 'WR' THEN
                            'Approval Received / Repair Started' 
                            WHEN STAT LIKE 'UR' THEN
                            'Under Repair' 
                            WHEN STAT LIKE 'SRMC' THEN
                            'Waiting Supply Materials From Customer' 
                            WHEN STAT LIKE 'SRAM' THEN
                            'Waiting Materials' 
                            WHEN STAT LIKE 'SROA' THEN
                            'Quotation Provided / Awaiting Approval' 
                            WHEN STAT LIKE 'SR%' THEN
                            'Waiting' 
                            WHEN STAT IS NULL THEN
                            'Undefined' 
                            WHEN STAT LIKE 'CRTD' 
                            OR STAT LIKE 'RELD' THEN
                                'Unit Under Inspection' 
                                WHEN STAT LIKE 'HOLD' THEN
                                'Release Hold' 
                                WHEN STAT LIKE 'HSHP' THEN
                                'Hold Shipment' ELSE 'Unit Under Inspection' 
                            END AS TXT_STAT 
                        FROM
                            AA 
                    WHERE
                ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = YEAR ( getdate ( ) ) 
                ) AND (LFGSA = 'C' OR STAT != 'WR') )
                 SELECT
                        COUNT( * ) AS JUMLAH,
                    TXT_STAT AS STATUS
                FROM HG
                

                GROUP BY TXT_STAT;";

        return $this->db->query($sql)->result();
    }

    public function get_list_invoice_outstanding($range){
        $id_customer = '';
        $where = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $id_customer = $this->session->userdata('log_sess_id_customer');
            $where = " AND KUNAG LIKE '{$id_customer}' ";
        }

        $sql = "WITH AA AS (
                SELECT
                    VBELN AS BILLING,
                    CONVERT (
                    DATE,
                    (
                    SUBSTRING( FKDAT, 7, 2 ) + '-' + SUBSTRING( FKDAT, 5, 2 ) + '-' + SUBSTRING( FKDAT, 1, 4 ) 
                    ),
                    104 
                    ) AS DOC_DATE,
                    NETWR AS AMOUNT,
                    WAERK AS CURRENCY,
                    DATEDIFF( DAY, CONVERT ( DATE, FKDAT, 104 ), CONVERT ( DATE, getdate ( ), 104 ) ) AS RANGE_DATE_NOW 
                FROM
                    M_SALESBILLING A 
                WHERE
                    RFBSK != 'C' $where
                    ),
                    BB AS (
                SELECT
                    *,
                CASE
                    WHEN RANGE_DATE_NOW < 30 THEN 1 WHEN RANGE_DATE_NOW >= 30 
                    AND RANGE_DATE_NOW <= 60 THEN 2 WHEN RANGE_DATE_NOW > 60 THEN
                    3 
                END AS CATEGORY 
                FROM
                    AA 
                    ) SELECT
                    * 
                FROM
                BB WHERE CATEGORY = {$range} ;";

        // $sql = "
        // SELECT * FROM (SELECT
        //     VBELN AS BILLING,
        //     CONVERT(DATE, (SUBSTRING(FKDAT, 7, 2)+'-'+SUBSTRING(FKDAT, 5, 2)+'-'+ SUBSTRING(FKDAT, 1, 4)), 104 ) AS DOC_DATE,
        // --  FKDAT,
        //     NETWR AS AMOUNT,
        //     WAERK AS CURRENCY,
        //     *
        // FROM
        //     TC_V_AGING
        //     WHERE RFBSK NOT LIKE 'C'
        // ) MSB
        // WHERE 
        //     {$where}
        // --DOC_DATE BETWEEN '20171212' AND '20180105'
        //     CATEGORY = {$range}
        // ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    public function get_list_historical($start, $end){

        $id_customer = '';
        $where = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $id_customer = $this->session->userdata('log_sess_id_customer');
            $where = " AND KUNAG LIKE '{$id_customer}' ";
        }

        $sql = "WITH AA AS(
                SELECT
                    *,
                    VBELN AS BILLING,
                        CONVERT (
                            DATE,
                            (
                            SUBSTRING ( FKDAT, 7, 2 ) + '-' + SUBSTRING ( FKDAT, 5, 2 ) + '-' + SUBSTRING ( FKDAT, 1, 4 )),
                            104 
                        ) AS DOC_DATE,
                --  FKDAT,
                        NETWR AS AMOUNT,
                        WAERK AS CURRENCY,
                        C.COMPANY_NAME
                FROM
                    M_SALESBILLING A LEFT JOIN CUSTOMER C
                WHERE
                    RFBSK LIKE 'C' $where
                    )
                    SELECT
                        * 
                    FROM
                        AA
                    WHERE
                        DOC_DATE BETWEEN '{$start}' 
                    AND '{$end}' 
                ORDER BY
                KUNAG;";

        // $sql = "
        // SELECT * FROM (SELECT
        //     VBELN AS BILLING,
        //     CONVERT(DATE, (SUBSTRING(FKDAT, 7, 2)+'-'+SUBSTRING(FKDAT, 5, 2)+'-'+ SUBSTRING(FKDAT, 1, 4)), 104 ) AS DOC_DATE,
        // --  FKDAT,
        //     NETWR AS AMOUNT,
        //     WAERK AS CURRENCY,
        //     *
        // FROM
        //     TC_V_AGING
        //     WHERE RFBSK LIKE 'C'
        // ) MSB
        // WHERE 
        // {$where}
        // DOC_DATE BETWEEN '{$start}' AND '{$end}'
        // ORDER BY KUNAG
        // ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

     public function getStatusInvoice($SALES_ORDER)
    {   
        $sql = "select distinct FKSAA from M_SALESORDER WHERE VBELN LIKE '{$SALES_ORDER}' AND FKSAA not like '' order by FKSAA ASC";
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

     public function select_retail()
    {
        // if($offset != null)
        // {
        //    $row_awal    = $offset+1;
        //    $row_akhir   = $offset+10;
        // }
        // else
        // {
        //     $row_awal   = 1;
        //     $row_akhir  = 10;
        // }
        $kunnr = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $where = " C.KUNNR LIKE '{$kunnr}' ";
        }
        else {
           $where = "D.ORDER_TYPE LIKE 'GA03'"; 
        }

        $sql = " WITH BB AS (
                SELECT DISTINCT
                    AUFNR AS 'MAINTENANCE_ORDER',
                    IDAT2 AS 'TECO_DATE'
                FROM
                    M_PMORDER 
                WHERE
                    AUART = 'GA03' 
                    AND ARBPL LIKE 'W%' 
                    AND WERKS LIKE 'WS%' 
                    AND ( IDAT2 = '00000000' OR ( SUBSTRING ( IDAT2, 5, 4 ) ) = YEAR ( getdate ( ) ) ) 
                    ),
                    DD AS (
                    SELECT
                        MATNR AS 'PART_NUMBER',
                        MAKTX AS 'PART_NAME',
                        AUART AS 'ORDER_TYPE',
                        KDAUF,
                    CASE
                            WHEN ( SERNR != '' ) THEN
                            SERNR 
                            WHEN ( SERIALNR != '' ) THEN
                            SERIALNR 
                        END AS 'SERIAL_NUMBER',
                    B.*
                    FROM BB B RIGHT JOIN M_PMORDERH H ON B.MAINTENANCE_ORDER = H.AUFNR
                        ),
                    C1 AS (
                    SELECT DISTINCT
                        VBELN,
                        BSTNK,
                        AUDAT,
                        KUNNR,
                        NAME1,
                        VBELN_BILL,
                        FKSAA,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, FKSAA DESC ) AS RowNum 
                    FROM
                        DD D
                        LEFT JOIN M_SALESORDER S ON D.KDAUF = S.VBELN 
                    ),
                    CD AS (
                        SELECT
                            CG.VBELN AS 'SALES_ORDER',
                            CG.BSTNK AS 'PURCHASE_ORDER',
                            CG.AUDAT AS 'RECEIVED_DATE',
                            CG.KUNNR AS 'KUNNR',
                            CG.NAME1 AS 'CUSTOMER',
                            CG.VBELN_BILL,
                            CG.FKSAA,
                            SB.RFBSK
                        FROM
                            C1 CG LEFT JOIN M_SALESBILLING SB ON CG.VBELN_BILL = SB.VBELN
                        WHERE
                            RowNum = 1 
                        ),
                    C2 AS (
                    SELECT DISTINCT
                        VBELN,
                        BWART,
                        LFGSA,
                        LFDAT,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, LFSTA DESC ) AS RowNum
                    FROM
                        CD C11
                        LEFT JOIN M_SALESORDER S ON C11.SALES_ORDER = S.VBELN 
                    WHERE
                        BWART LIKE '601' 
                    ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum = 1 ),
                    CC AS ( SELECT C4.*, C3.LFGSA, C3.LFDAT, C3.BWART
                            FROM CD C4
                            LEFT JOIN CF C3 ON C3.VBELN = C4.SALES_ORDER ),
                    AA AS (
                    SELECT
                    CASE    
                        WHEN
                            TXT50 NOT LIKE '' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' THEN
                                TXT1 
                            END AS STAT,
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            M.*, D.*, C.*
                        FROM
                            DD D
                            INNER JOIN M_STATUSPM M ON D.MAINTENANCE_ORDER = M.AUFNR
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where 
                        ) SELECT
                        *,
                    CASE
                            WHEN LFGSA = 'C' THEN 'Delivered'
                            WHEN STAT LIKE 'FRA' THEN
                            'Ready To Deliver Return As is Conditiion' 
                            WHEN STAT LIKE 'FSB' THEN
                            'Ready To Deliver Serviceable Condition' 
                            WHEN STAT LIKE 'F%' THEN
                            'Ready To Deliver BER' 
                            WHEN STAT LIKE 'WR' THEN
                            'Approval Received / Repair Started' 
                            WHEN STAT LIKE 'UR' THEN
                            'Under Repair' 
                            WHEN STAT LIKE 'SRMC' THEN
                            'Waiting Supply Materials From Customer' 
                            WHEN STAT LIKE 'SRAM' THEN
                            'Waiting Materials' 
                            WHEN STAT LIKE 'SROA' THEN
                            'Quotation Provided / Awaiting Approval' 
                            WHEN STAT LIKE 'SR%' THEN
                            'Repair Started' 
                            WHEN STAT IS NULL THEN
                            'Undefined' 
                            WHEN STAT LIKE 'CRTD' 
                            OR STAT LIKE 'RELD' THEN
                                'Unit Under Inspection' 
                                WHEN STAT LIKE 'HOLD' THEN
                                'Release Hold' 
                                WHEN STAT LIKE 'HSHP' THEN
                                'Hold Shipment' ELSE 'Unit Under Inspection' 
                            END AS TXT_STAT 
                        FROM
                            AA 
                    WHERE
                ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = YEAR ( getdate ( ) ) )      
                            ;";
    
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function select_retail2($param, $ext=null)
    {
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';

        if (isset($param['search'])) {
            // code...
            // print_r($param['search']);
            $keyword = "'%".strtolower($param['search']['value'])."%'";

            // $rownum = " AND RowNum > {$param['start']}
            //     AND RowNum < {$param['end']}";
        }

        if (isset($param['start']) and isset($param['end'])) {
            // code...
            // print_r($param['search']);
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }


        if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        $paid = '';
        $where_status = '';
        if (isset($param['column'])) // here order processing
        {
            if (($param['column']['15']['search']['value'])!= ''){
                $pay = substr($param['column']['15']['search']['value'], 1, -1);
                $paid = "AND BILLING_STATUS LIKE '$pay'";
            } 
            if (($param['column']['12']['search']['value'])!= ''){
                $sw = "(" . $param['column']['12']['search']['value'] . ")";
                $swe = str_replace('\\', '', $sw);
                $where_status = "AND TXT_STAT IN $swe ";
            }
        }
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            $kunnr = '';
            if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            }
            else {
               $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            }
            $where = " (
                C.SALES_ORDER LIKE {$keyword} OR
                D.MAINTENANCE_ORDER LIKE {$keyword} OR
                C.PURCHASE_ORDER LIKE {$keyword} 
            )";
        }

        $sql = " WITH DD AS (
                    SELECT
                        H.AUFNR AS 'MAINTENANCE_ORDER',
                        H.MATNR AS 'PART_NUMBER',
                        H.MAKTX AS 'PART_NAME',
                        H.AUART AS 'ORDER_TYPE',
                        H.KDAUF,
                        H.IDAT2 AS 'TECO_DATE',
                    CASE
                            WHEN ( H.SERNR != '' ) THEN
                            SERNR 
                            WHEN ( H.SERIALNR != '' ) THEN
                            SERIALNR 
                        END AS 'SERIAL_NUMBER'
                    FROM M_STATUSPM B RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                    WHERE
                        H.AUART = 'GA03'
                        AND B.PRCTR LIKE 'GMFTC%'  
                        AND ( H.IDAT2 = '00000000' OR ( SUBSTRING ( H.IDAT2, 5, 4 ) ) = YEAR ( getdate ( ) ) ) 
                        ),
                    C1 AS (
                    SELECT DISTINCT
                        VBELN,
                        BSTNK,
                        AUDAT,
                        KUNNR,
                        NAME1,
                        VBELN_BILL,
                        FKSAA,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, FKSAA DESC ) AS RowNum 
                    FROM
                        DD D
                        LEFT JOIN M_SALESORDER S ON D.KDAUF = S.VBELN
                    WHERE KUNNR LIKE '%{$kunnr}%'
                    ),
                    CD AS (
                        SELECT
                            CG.VBELN AS 'SALES_ORDER',
                            CG.BSTNK AS 'PURCHASE_ORDER',
                            CG.AUDAT AS 'RECEIVED_DATE',
                            CG.KUNNR AS 'KUNNR',
                            CG.NAME1 AS 'CUSTOMER',
                            CG.VBELN_BILL,
                            CG.FKSAA,
                            SB.RFBSK
                        FROM
                            C1 CG LEFT JOIN M_SALESBILLING SB ON CG.VBELN_BILL = SB.VBELN
                        WHERE
                            RowNum = 1 
                        ),
                    C2 AS (
                    SELECT DISTINCT
                        VBELN,
                        BWART,
                        LFGSA,
                        LFDAT,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, LFSTA DESC ) AS RowNum
                    FROM
                        CD C11
                        LEFT JOIN M_SALESORDER S ON C11.SALES_ORDER = S.VBELN 
                    WHERE
                        BWART LIKE '601' 
                    ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum = 1 ),
                    CC AS ( SELECT C4.*, C3.LFGSA, C3.LFDAT, C3.BWART
                            FROM CD C4
                            LEFT JOIN CF C3 ON C3.VBELN = C4.SALES_ORDER ),
                    AA AS (
                    SELECT
                    CASE    
                        WHEN
                            TXT50 NOT LIKE '' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' THEN
                                TXT1 
                            END AS STAT,
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            M.*, D.*, C.*
                        FROM
                            DD D
                            INNER JOIN M_STATUSPM M ON D.MAINTENANCE_ORDER = M.AUFNR
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where_kunnr $where
                        ),
                    LY AS (
                        SELECT
                            *,
                        CASE
                                WHEN LFGSA = 'C' THEN 'Delivered'
                                WHEN STAT LIKE 'FRA' THEN
                                'Ready To Deliver Return As is Conditiion' 
                                WHEN STAT LIKE 'FSB' THEN
                                'Ready To Deliver Serviceable Condition' 
                                WHEN STAT LIKE 'F%' THEN
                                'Ready To Deliver BER' 
                                WHEN STAT LIKE 'WR' THEN
                                'Approval Received / Repair Started' 
                                WHEN STAT LIKE 'UR' THEN
                                'Under Repair' 
                                WHEN STAT LIKE 'SRMC' THEN
                                'Waiting Supply Materials From Customer' 
                                WHEN STAT LIKE 'SRAM' THEN
                                'Waiting Materials' 
                                WHEN STAT LIKE 'SROA' THEN
                                'Quotation Provided / Awaiting Approval' 
                                WHEN STAT LIKE 'SR%' THEN
                                'Waiting' 
                                WHEN STAT IS NULL THEN
                                'Undefined' 
                                WHEN STAT LIKE 'CRTD' 
                                OR STAT LIKE 'RELD' THEN
                                    'Unit Under Inspection' 
                                    WHEN STAT LIKE 'HOLD' THEN
                                    'Release Hold' 
                                    WHEN STAT LIKE 'HSHP' THEN
                                    'Hold Shipment' ELSE 'Unit Under Inspection' 
                                END AS TXT_STAT 
                            FROM
                                AA 
                        WHERE
                    ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = YEAR ( getdate ( ) ) ) ),
                AH AS (SELECT *,  ROW_NUMBER() OVER ($order) as RowNum FROM LY WHERE AUART = 'GA03' $where_status $paid)
                SELECT * FROM AH WHERE AUART = 'GA03' $rownum      
                            ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

     public function select_retail2_count($param, $ext=null)
    {
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';

        if (isset($param['search'])) {
            // code...
            // print_r($param['search']);
            $keyword = "'%".strtolower($param['search']['value'])."%'";

            // $rownum = " AND RowNum > {$param['start']}
            //     AND RowNum < {$param['end']}";
        }

        if (isset($param['start']) and isset($param['end'])) {
            // code...
            // print_r($param['search']);
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }


        if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        $paid = '';
        $where_status = '';
        if (isset($param['column'])) // here order processing
        {
            if (($param['column']['15']['search']['value'])!= ''){
                $pay = substr($param['column']['15']['search']['value'], 1, -1);
                $paid = "AND BILLING_STATUS LIKE '$pay'";
            } 
            if (($param['column']['12']['search']['value'])!= ''){
                $sw = "(" . $param['column']['12']['search']['value'] . ")";
                $swe = str_replace('\\', '', $sw);
                $where_status = "AND TXT_STAT IN $swe ";
            }
        }
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            $kunnr = '';
            if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            }
            else {
               $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            }
            $where = " (
                C.SALES_ORDER LIKE {$keyword} OR
                D.MAINTENANCE_ORDER LIKE {$keyword} OR
                C.PURCHASE_ORDER LIKE {$keyword} 
            )";
        }

        $sql = " WITH DD AS (
                    SELECT
                        H.AUFNR AS 'MAINTENANCE_ORDER',
                        H.MATNR AS 'PART_NUMBER',
                        H.MAKTX AS 'PART_NAME',
                        H.AUART AS 'ORDER_TYPE',
                        H.KDAUF,
                        H.IDAT2 AS 'TECO_DATE',
                    CASE
                            WHEN ( H.SERNR != '' ) THEN
                            SERNR 
                            WHEN ( H.SERIALNR != '' ) THEN
                            SERIALNR 
                        END AS 'SERIAL_NUMBER'
                    FROM M_STATUSPM B RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                    WHERE
                        H.AUART = 'GA03'
                        AND B.PRCTR LIKE 'GMFTC%'  
                        AND ( H.IDAT2 = '00000000' OR ( SUBSTRING ( H.IDAT2, 5, 4 ) ) = YEAR ( getdate ( ) ) ) 
                        ),
                    C1 AS (
                    SELECT DISTINCT
                        VBELN,
                        BSTNK,
                        AUDAT,
                        KUNNR,
                        NAME1,
                        VBELN_BILL,
                        FKSAA,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, FKSAA DESC ) AS RowNum 
                    FROM
                        DD D
                        LEFT JOIN M_SALESORDER S ON D.KDAUF = S.VBELN
                    WHERE KUNNR LIKE '%{$kunnr}%'
                    ),
                    CD AS (
                        SELECT
                            CG.VBELN AS 'SALES_ORDER',
                            CG.BSTNK AS 'PURCHASE_ORDER',
                            CG.AUDAT AS 'RECEIVED_DATE',
                            CG.KUNNR AS 'KUNNR',
                            CG.NAME1 AS 'CUSTOMER',
                            CG.VBELN_BILL,
                            CG.FKSAA,
                            SB.RFBSK
                        FROM
                            C1 CG LEFT JOIN M_SALESBILLING SB ON CG.VBELN_BILL = SB.VBELN
                        WHERE
                            RowNum = 1 
                        ),
                    C2 AS (
                    SELECT DISTINCT
                        VBELN,
                        BWART,
                        LFGSA,
                        LFDAT,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, LFSTA DESC ) AS RowNum
                    FROM
                        CD C11
                        LEFT JOIN M_SALESORDER S ON C11.SALES_ORDER = S.VBELN 
                    WHERE
                        BWART LIKE '601' 
                    ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum = 1 ),
                    CC AS ( SELECT C4.*, C3.LFGSA, C3.LFDAT, C3.BWART
                            FROM CD C4
                            LEFT JOIN CF C3 ON C3.VBELN = C4.SALES_ORDER ),
                    AA AS (
                    SELECT
                    CASE    
                        WHEN
                            TXT50 NOT LIKE '' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' THEN
                                TXT1 
                            END AS STAT,
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            M.*, D.*, C.*
                        FROM
                            DD D
                            INNER JOIN M_STATUSPM M ON D.MAINTENANCE_ORDER = M.AUFNR
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where_kunnr $where
                        ),
                    LY AS (
                        SELECT
                            *,
                        CASE
                                WHEN LFGSA = 'C' THEN 'Delivered'
                                WHEN STAT LIKE 'FRA' THEN
                                'Ready To Deliver Return As is Conditiion' 
                                WHEN STAT LIKE 'FSB' THEN
                                'Ready To Deliver Serviceable Condition' 
                                WHEN STAT LIKE 'F%' THEN
                                'Ready To Deliver BER' 
                                WHEN STAT LIKE 'WR' THEN
                                'Approval Received / Repair Started' 
                                WHEN STAT LIKE 'UR' THEN
                                'Under Repair' 
                                WHEN STAT LIKE 'SRMC' THEN
                                'Waiting Supply Materials From Customer' 
                                WHEN STAT LIKE 'SRAM' THEN
                                'Waiting Materials' 
                                WHEN STAT LIKE 'SROA' THEN
                                'Quotation Provided / Awaiting Approval' 
                                WHEN STAT LIKE 'SR%' THEN
                                'Waiting' 
                                WHEN STAT IS NULL THEN
                                'Undefined' 
                                WHEN STAT LIKE 'CRTD' 
                                OR STAT LIKE 'RELD' THEN
                                    'Unit Under Inspection' 
                                    WHEN STAT LIKE 'HOLD' THEN
                                    'Release Hold' 
                                    WHEN STAT LIKE 'HSHP' THEN
                                    'Hold Shipment' ELSE 'Unit Under Inspection' 
                                END AS TXT_STAT 
                            FROM
                                AA 
                        WHERE
                    ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = YEAR ( getdate ( ) ) ) )
                SELECT count(*) as 'JUMLAH' FROM LY WHERE AUART = 'GA03' $where_status $paid
                            ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    
     public function select_retail_where_status2($param, $ext=null)
    {
        
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';

        if (isset($param['search'])) {
            // code...
            // print_r($param['search']);
            $keyword = "'%".strtolower($param['search']['value'])."%'";

            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }

        if (isset($param['order'])) // here order processing
        {
           
            $order = "ORDER BY {$this->column_order[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            $kunnr = '';
            if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            }
            else {
               $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            }
            $where = " (
                C.SALES_ORDER LIKE {$keyword} OR
                D.MAINTENANCE_ORDER LIKE {$keyword} OR
                C.PURCHASE_ORDER LIKE {$keyword} 
            )";
        }

        $sql = " WITH DD AS (
                    SELECT
                        H.AUFNR AS 'MAINTENANCE_ORDER',
                        H.MATNR AS 'PART_NUMBER',
                        H.MAKTX AS 'PART_NAME',
                        H.AUART AS 'ORDER_TYPE',
                        H.KDAUF,
                        H.IDAT2 AS 'TECO_DATE',
                    CASE
                            WHEN ( H.SERNR != '' ) THEN
                            SERNR 
                            WHEN ( H.SERIALNR != '' ) THEN
                            SERIALNR 
                        END AS 'SERIAL_NUMBER'
                    FROM M_STATUSPM B RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                    WHERE
                        H.AUART = 'GA03'
                        AND B.PRCTR LIKE 'GMFTC%'  
                        AND ( H.IDAT2 = '00000000' OR ( SUBSTRING ( H.IDAT2, 5, 4 ) ) = YEAR ( getdate ( ) ) ) 
                        ),
                    C1 AS (
                    SELECT DISTINCT
                        VBELN,
                        BSTNK,
                        AUDAT,
                        KUNNR,
                        NAME1,
                        VBELN_BILL,
                        FKSAA,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, FKSAA DESC ) AS RowNum 
                    FROM
                        DD D
                        LEFT JOIN M_SALESORDER S ON D.KDAUF = S.VBELN
                    WHERE KUNNR LIKE '%{$kunnr}%'
                    ),
                    CD AS (
                        SELECT
                            CG.VBELN AS 'SALES_ORDER',
                            CG.BSTNK AS 'PURCHASE_ORDER',
                            CG.AUDAT AS 'RECEIVED_DATE',
                            CG.KUNNR AS 'KUNNR',
                            CG.NAME1 AS 'CUSTOMER',
                            CG.VBELN_BILL,
                            CG.FKSAA,
                            SB.RFBSK
                        FROM
                            C1 CG LEFT JOIN M_SALESBILLING SB ON CG.VBELN_BILL = SB.VBELN
                        WHERE
                            RowNum = 1 
                        ),
                    C2 AS (
                    SELECT DISTINCT
                        VBELN,
                        BWART,
                        LFGSA,
                        LFDAT,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, LFSTA DESC ) AS RowNum
                    FROM
                        CD C11
                        LEFT JOIN M_SALESORDER S ON C11.SALES_ORDER = S.VBELN 
                    WHERE
                        BWART LIKE '601' 
                    ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum = 1 ),
                    CC AS ( SELECT C4.*, C3.LFGSA, C3.LFDAT, C3.BWART
                            FROM CD C4
                            LEFT JOIN CF C3 ON C3.VBELN = C4.SALES_ORDER ),
                    AA AS (
                    SELECT
                    CASE    
                        WHEN
                            TXT50 NOT LIKE '' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' THEN
                                TXT1 
                            END AS STAT,
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            M.*, D.*, C.*
                        FROM
                            DD D
                            INNER JOIN M_STATUSPM M ON D.MAINTENANCE_ORDER = M.AUFNR
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where_kunnr $where
                        ),
                    LY AS (
                        SELECT
                            *,
                        CASE
                                WHEN LFGSA = 'C' THEN 'Delivered'
                                WHEN STAT LIKE 'FRA' THEN
                                'Ready To Deliver Return As is Conditiion' 
                                WHEN STAT LIKE 'FSB' THEN
                                'Ready To Deliver Serviceable Condition' 
                                WHEN STAT LIKE 'F%' THEN
                                'Ready To Deliver BER' 
                                WHEN STAT LIKE 'WR' THEN
                                'Approval Received / Repair Started' 
                                WHEN STAT LIKE 'UR' THEN
                                'Under Repair' 
                                WHEN STAT LIKE 'SRMC' THEN
                                'Waiting Supply Materials From Customer' 
                                WHEN STAT LIKE 'SRAM' THEN
                                'Waiting Materials' 
                                WHEN STAT LIKE 'SROA' THEN
                                'Quotation Provided / Awaiting Approval' 
                                WHEN STAT LIKE 'SR%' THEN
                                'Waiting' 
                                WHEN STAT IS NULL THEN
                                'Undefined' 
                                WHEN STAT LIKE 'CRTD' 
                                OR STAT LIKE 'RELD' THEN
                                    'Unit Under Inspection' 
                                    WHEN STAT LIKE 'HOLD' THEN
                                    'Release Hold' 
                                    WHEN STAT LIKE 'HSHP' THEN
                                    'Hold Shipment' ELSE 'Unit Under Inspection' 
                                END AS TXT_STAT 
                            FROM
                                AA 
                        WHERE
                    ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = YEAR ( getdate ( ) ) ) ),
                AH AS (SELECT *,  ROW_NUMBER() OVER ($order) as RowNum FROM LY WHERE AUART = 'GA03' AND TXT_STAT LIKE '{$param['status']}%' )
                SELECT * FROM AH WHERE AUART = 'GA03' $rownum      
                            ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
        // $sql =" 
        //         SELECT 
        //             RowNum, *
        //         FROM
        //                 (
        //                     SELECT *, ROW_NUMBER() OVER (ORDER BY STAT) as RowNum
        //                     FROM  TC_V_RETAIL
        //                     WHERE 
        //                         $where
        //                         TXT_STAT LIKE '{$param['status']}'        
        //                 ) AS PAGE_TB_RETAIL
        //         WHERE
        //             $where
        //             $rownum
        //     ";
        // $query  = $this->db->query($sql);
        
        // $result = $query->result_array();
        // return $result;
    }

     public function select_retail_where_status2_count($param, $ext=null)
    {
        
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';

        if (isset($param['search'])) {
            $keyword = "'%".strtolower($param['search']['value'])."%'";
        }

        if (isset($param['start']) and isset($param['end'])) {
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }

        if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            $kunnr = '';
            if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            }
            else {
               $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            }
            $where = " (
                C.SALES_ORDER LIKE {$keyword} OR
                D.MAINTENANCE_ORDER LIKE {$keyword} OR
                C.PURCHASE_ORDER LIKE {$keyword} 
            )";
        }

        $sql = " WITH DD AS (
                    SELECT
                        H.AUFNR AS 'MAINTENANCE_ORDER',
                        H.MATNR AS 'PART_NUMBER',
                        H.MAKTX AS 'PART_NAME',
                        H.AUART AS 'ORDER_TYPE',
                        H.KDAUF,
                        H.IDAT2 AS 'TECO_DATE',
                    CASE
                            WHEN ( H.SERNR != '' ) THEN
                            SERNR 
                            WHEN ( H.SERIALNR != '' ) THEN
                            SERIALNR 
                        END AS 'SERIAL_NUMBER'
                    FROM M_STATUSPM B RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                    WHERE
                        H.AUART = 'GA03'
                        AND B.PRCTR LIKE 'GMFTC%'  
                        AND ( H.IDAT2 = '00000000' OR ( SUBSTRING ( H.IDAT2, 5, 4 ) ) = YEAR ( getdate ( ) ) ) 
                        ),
                    C1 AS (
                    SELECT DISTINCT
                        VBELN,
                        BSTNK,
                        AUDAT,
                        KUNNR,
                        NAME1,
                        VBELN_BILL,
                        FKSAA,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, FKSAA DESC ) AS RowNum 
                    FROM
                        DD D
                        LEFT JOIN M_SALESORDER S ON D.KDAUF = S.VBELN
                    WHERE KUNNR LIKE '%{$kunnr}%'
                    ),
                    CD AS (
                        SELECT
                            CG.VBELN AS 'SALES_ORDER',
                            CG.BSTNK AS 'PURCHASE_ORDER',
                            CG.AUDAT AS 'RECEIVED_DATE',
                            CG.KUNNR AS 'KUNNR',
                            CG.NAME1 AS 'CUSTOMER',
                            CG.VBELN_BILL,
                            CG.FKSAA,
                            SB.RFBSK
                        FROM
                            C1 CG LEFT JOIN M_SALESBILLING SB ON CG.VBELN_BILL = SB.VBELN
                        WHERE
                            RowNum = 1 
                        ),
                    C2 AS (
                    SELECT DISTINCT
                        VBELN,
                        BWART,
                        LFGSA,
                        LFDAT,
                        ROW_NUMBER ( ) OVER ( PARTITION BY VBELN ORDER BY VBELN, LFSTA DESC ) AS RowNum
                    FROM
                        CD C11
                        LEFT JOIN M_SALESORDER S ON C11.SALES_ORDER = S.VBELN 
                    WHERE
                        BWART LIKE '601' 
                    ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum = 1 ),
                    CC AS ( SELECT C4.*, C3.LFGSA, C3.LFDAT, C3.BWART
                            FROM CD C4
                            LEFT JOIN CF C3 ON C3.VBELN = C4.SALES_ORDER ),
                    AA AS (
                    SELECT
                    CASE    
                        WHEN
                            TXT50 NOT LIKE '' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' THEN
                                TXT1 
                            END AS STAT,
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            M.*, D.*, C.*
                        FROM
                            DD D
                            INNER JOIN M_STATUSPM M ON D.MAINTENANCE_ORDER = M.AUFNR
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where_kunnr $where
                        ),
                    LY AS (
                        SELECT
                            *,
                        CASE
                                WHEN LFGSA = 'C' THEN 'Delivered'
                                WHEN STAT LIKE 'FRA' THEN
                                'Ready To Deliver Return As is Conditiion' 
                                WHEN STAT LIKE 'FSB' THEN
                                'Ready To Deliver Serviceable Condition' 
                                WHEN STAT LIKE 'F%' THEN
                                'Ready To Deliver BER' 
                                WHEN STAT LIKE 'WR' THEN
                                'Approval Received / Repair Started' 
                                WHEN STAT LIKE 'UR' THEN
                                'Under Repair' 
                                WHEN STAT LIKE 'SRMC' THEN
                                'Waiting Supply Materials From Customer' 
                                WHEN STAT LIKE 'SRAM' THEN
                                'Waiting Materials' 
                                WHEN STAT LIKE 'SROA' THEN
                                'Quotation Provided / Awaiting Approval' 
                                WHEN STAT LIKE 'SR%' THEN
                                'Waiting' 
                                WHEN STAT IS NULL THEN
                                'Undefined' 
                                WHEN STAT LIKE 'CRTD' 
                                OR STAT LIKE 'RELD' THEN
                                    'Unit Under Inspection' 
                                    WHEN STAT LIKE 'HOLD' THEN
                                    'Release Hold' 
                                    WHEN STAT LIKE 'HSHP' THEN
                                    'Hold Shipment' ELSE 'Unit Under Inspection' 
                                END AS TXT_STAT 
                            FROM
                                AA 
                        WHERE
                    ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = YEAR ( getdate ( ) ) ) ),
                AH AS (SELECT *,  ROW_NUMBER() OVER ($order) as RowNum FROM LY WHERE AUART = 'GA03' AND TXT_STAT LIKE '{$param['status']}' )
                SELECT count(*) as 'JUMLAH' FROM AH WHERE AUART = 'GA03' $rownum      
                            ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_retail_where_id($ID_RETAIL)
    {
        $sql = "
            WITH BB AS (
                SELECT DISTINCT
                    AUFNR,
                    KDAUF,
                    AUART,
                    ILART,
                    ERNAM,
                    ERDAT,
                    PHAS0,
                    PHAS1,
                    PHAS2,
                    PHAS3 
                FROM
                    M_PMORDER 
                WHERE
                    AUART = 'GA03' 
                    AND AUFNR LIKE $ID_RETAIL 
                    ),
                    CC AS ( SELECT DISTINCT VBELN, VBELN_BILL, AUDAT, KUNNR, NAME1, BOLNR FROM BB B RIGHT JOIN M_SALESORDER S ON B.KDAUF = S.VBELN),
                    AA AS (
                SELECT
                    V.NAME1 AS 'SHIPTOPARTY',
                    E.MATNR,
                    E.MAKTX,
                    CONVERT ( FLOAT, D.NETWR ) AS 'NETWR',
                    CONVERT ( FLOAT, D.MWSBK ) AS 'MWSBK',
                    D.WAERK,
                    (
                    CONVERT ( FLOAT, D.NETWR ) + CONVERT ( FLOAT, D.MWSBK )) AS 'TOTAL',
                CASE
                    
                    WHEN ( D.RFBSK IS NULL OR D.RFBSK != 'C' ) THEN
                    'UNPAID' ELSE 'PAID' 
                    END AS BILLING_STATUS,
                    A.*,
                    C.*,
                    ROW_NUMBER () OVER ( ORDER BY D.RFBSK DESC ) AS ROWS 
                FROM
                    M_PMORDERH E
                    LEFT JOIN BB A ON E.AUFNR = A.AUFNR
                    LEFT JOIN CC C ON E.KDAUF = C.VBELN
                    LEFT JOIN M_SALESBILLING D ON C.VBELN_BILL = D.VBELN
                    LEFT JOIN TC_V_SALESORDER V ON D.KUNAG = V.KUNNR
                WHERE
                    A.AUART = 'GA03' 
                    -- AND D.NETWR NOT LIKE '0.00'
                    AND A.AUFNR LIKE $ID_RETAIL
                    ) SELECT
                    * 
                FROM
                AA;";

        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function getlistOutstanding() {
        $kunnr = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $where = " KUNAG LIKE '{$kunnr}' AND RFBSK != 'C' ";
        }
        else {
            $where = " RFBSK != 'C' ";
        }
        $sql = "
            SELECT
                *,
                CASE    
                WHEN RANGE_DATE_NOW < 30 THEN
                1
                WHEN RANGE_DATE_NOW >= 30 AND RANGE_DATE_NOW <= 60 THEN
                2 
                WHEN RANGE_DATE_NOW > 60 THEN
                3 
            END AS CATEGORY  
            FROM
                (
                SELECT
                    *,
                    DATEDIFF( DAY, CONVERT ( DATE, FKDAT, 104 ), CONVERT ( DATE, getdate( ), 104 ) ) AS RANGE_DATE_NOW 
                FROM
                    M_SALESBILLING A 
                WHERE
                    $where
                    ) ag     

        ";
        return $this->db->query($sql)->result();
    }
}
