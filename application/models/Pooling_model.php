<?php
class Pooling_model extends CI_Model{

	private $tb_name = "TC_TR_POOLING_ORDER";
	private $tb_contract = "TC_CONTRACT";
	private $tb_contract_fleet = "TC_CONTRACT_FLEET";
	private $tb_contract_pn = "TC_CONTRACT_PN";

	private $tb_master_pn = "MS_PART_NUMBER";
	private $tb_master_fleet = "MS_AIRCRAFT_REGISTRATION";
	private $ms_requirement_category = "TC_POOL_REQUIREMENT_CATEGORY";
	private $order_sort = array('ID_POOLING_ORDER' => 'asc');
	private $kolom = array(null, 
						'REFERENCE',
						'REQUIREMENT_CATEGORY_NAME',
						'ID_POOLING_ORDER',
						'ID_PART_NUMBER',
						'DESCRIPTION',
						'DELIVERY_POINT_CODE',
						'AIRCRAFT_REGISTRATION',
						'CUSTOMER_PO',
						'CUSTOMER_PO',
						'ORDER_REQUESTED_BY',
						'REQUEST_ORDER_DATE',
						'REQUESTED_DELIVERY_DATE',
						'STATUS_ORDER_NAME',
						'STATUS_ORDER',
						'SERVICE_STATUS',
						'UNSERVICE_STATUS');

	public function get_counter()
	{
		$this->db->select('*');
		$this->db->from('TC_TR_POOLING_ORDER');
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function get_counter_tr()
	{
		$this->db->select('*');
		$this->db->from($this->tb_name);
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return $query->num_rows();
	}



	// Listing Other

	public function get_contract_list($id)
	{
		// $this->db->order_by('iduserrole', 'ASC');
		$this->db->where('ID_CUSTOMER', $id);
		$data = $this->db->get($this->tb_contract);
		return $data->result();
	}
	public function get_part_number_contract_description($id)
	{
		// $this->db->order_by('iduserrole', 'ASC');

		$this->db->where('PART_NUMBER', $id);
		return $this->db->get('TC_CONTRACT_PN')->row_object();
		// $data = $this->db->get('TC_CONTRACT_PN');
		// return $data->result();
	}
	public function get_fleet_list_by_contract($id)
	{
		// $this->db->order_by('iduserrole', 'ASC');
		$this->db->select($this->tb_contract_fleet.".REGISTRATION AS AIRCRAFT_REGISTRATION_NAME", $this->tb_contract_fleet.".ID");
		$this->db->from($this->tb_contract_fleet);
		// $this->db->join($this->tb_master_fleet, $this->tb_master_fleet.".ID_AIRCRAFT_REGISTRATION = ".$this->tb_contract_fleet.".AIRCRAFT_REGISTRATION");
		$this->db->where('ID_CONTRACT', $id);

		$data = $this->db->get();

		// echo $this->db->last_query();
		return $data->result();
	}
	public function get_pn_list_by_contract($id)
	{
		$this->db->select($this->tb_contract_pn.".PART_NUMBER AS PART_NUMBER");
		$this->db->from($this->tb_contract_pn);
		// $this->db->join($this->tb_master_pn, $this->tb_master_pn.".ID_PN = ".$this->tb_contract_pn.".ID_PN");
		$this->db->where('ID_CONTRACT', $id);
		$data = $this->db->get();
		return $data->result();
	}
	public function get_part_number_list()
	{
		// $this->db->order_by('iduserrole', 'ASC');
		$data = $this->db->get('ms_part_number');
		return $data->result();
	}

	public function get_requirement_category_list()
	{
		$data = $this->db->get('TC_POOL_REQUIREMENT_CATEGORY');
		return $data->result();
	}

	public function get_aircraft_registration_list()
	{
		$data = $this->db->get('ms_aircraft_registration');
		return $data->result();
	}

	public function get_delivery_point_list()
	{
		$data = $this->db->get('ms_delivery_point');
		return $data->result();
	}

	public function get_data_pooling($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		$fltr = '';
		if (isset($param['search'])) {
            
            $keyword = "'%".strtolower($param['search']['value'])."%'";
        }

        if (isset($param['start']) and isset($param['end'])) {
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }
         if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->kolom[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        if (isset($param['column'])) // here order processing
        {
        	// print_r($param['column']);
        	$i = 0;
        	$ii = 0;
      		foreach ($param['column'] as $item){
      			// echo $this->kolom[$i];
	            if( ($param['column'][$i]['search']['value']) != '') // if datatable send POST for search
	            {
	            	if ($ii != 0) {
      					$fltr .= " OR";
      				}
      				else if ($ii == 0){
      					$fltr .= "AND (";
      				}
	               $fltr .= " ";
	               $fltr .= $this->kolom[$i];
	               $fltr .= " like '%{$param['column']["$i"]['search']['value']}%'";
	            $ii++;
	            }
	         $i++;
        	}
        }
        if ($fltr != '') {
        	$fltr .= ")";
        }
       	 // echo $fltr;

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				ID_PART_NUMBER LIKE {$keyword} OR
				DESCRIPTION LIKE {$keyword} OR
				DELIVERY_POINT_CODE LIKE {$keyword} OR
				REQUESTED_DELIVERY_DATE LIKE {$keyword} OR
				REQUIREMENT_CATEGORY_NAME LIKE {$keyword} OR
				AIRCRAFT_REGISTRATION LIKE {$keyword} OR
				CUSTOMER_PO LIKE {$keyword} OR
				STATUS_ORDER LIKE {$keyword} OR
				STATUS_ORDER_NAME LIKE {$keyword} OR
				REQUEST_ORDER_DATE LIKE {$keyword}
			)";
		}
		$sql = " SELECT
						*
					FROM
						(
							SELECT
								ROW_NUMBER () OVER ( ORDER BY ID_POOLING_ORDER ) AS RowNum,
								REFERENCE,
								ID_PART_NUMBER,
								ID_POOLING_ORDER,
								DESCRIPTION,
								DELIVERY_POINT_CODE,
								REQUESTED_DELIVERY_DATE,
								REQUIREMENT_CATEGORY_NAME,
								MS_AIRCRAFT_REGISTRATION.AIRCRAFT_REGISTRATION,
								CUSTOMER_PO,
								STATUS_ORDER,
								STATUS_ORDER_NAME,
								REQUEST_ORDER_DATE,
								SERVICE_STATUS,
								UNSERVICE_STATUS 
							FROM
								TC_TR_POOLING_ORDER -- INNER JOIN TC_TR_POOLING_ORDER ON TC_TR_POOLING_ORDER.REFERENCE = tr_pooling.REFERENCE
								-- LEFT JOIN ms_part_number ON ms_part_number.ID_PN = TC_TR_POOLING_ORDER.ID_PART_NUMBER
								LEFT JOIN TC_POOL_REQUIREMENT_CATEGORY ON TC_POOL_REQUIREMENT_CATEGORY.ID_REQUIREMENT_CATEGORY = TC_TR_POOLING_ORDER.ID_REQUIREMENT_CATEGORY
								LEFT JOIN ms_aircraft_registration ON ms_aircraft_registration.ID_AIRCRAFT_REGISTRATION = TC_TR_POOLING_ORDER.ID_AIRCRAFT_REGISTRATION
								LEFT JOIN ms_status_order ON ms_status_order.ID_STATUS_ORDER = TC_TR_POOLING_ORDER.STATUS_ORDER
								LEFT JOIN ms_delivery_point ON ms_delivery_point.ID_DELIVERY_POINT = TC_TR_POOLING_ORDER.ID_DELIVERY_POINT
						) tb
					WHERE
						$where
						$rownum
						$fltr
						
						";
		$query = $this->db->query($sql);

		return $query->result_array();
	}
	// ./ Listing Other



	// Get Function JSON

	public function get_part_number_description($ID_PN)
	{
		$this->db->where('PART_NUMBER', $ID_PN);
		return $this->db->get('TC_CONTRACT_PN')->row_object();
	}

	public function get_delivery_point_address($ID_DP)
	{
		$this->db->where('ID_DELIVERY_POINT',$ID_DP);
        return $this->db->get('ms_delivery_point')->row_object();
	}

	public function get_delivery_point($ID_CONTRACT)
	{
		$this->db->where('ID_CONTRACT',$ID_CONTRACT);
        return $this->db->get('ms_delivery_point')->result();
	}

	// Get Function JSON


	public function select_request_where_no_account($NO_ACCOUNT)
	{	
		// $sql = " SELECT O.*, C.COMPANY_NAME, S.STATUS_ORDER_NAME
		// 		 FROM"

		$this->db->select('*');
		$this->db->from('TC_TR_POOLING_ORDER');
		// $this->db->join('TC_TR_POOLING_ORDER', 'TC_TR_POOLING_ORDER.REFERENCE = tr_pooling.REFERENCE','INNER');
		// $this->db->join('ms_part_number', 'ms_part_number.ID_PN = TC_TR_POOLING_ORDER.ID_PART_NUMBER','LEFT');
		$this->db->join('TC_POOL_REQUIREMENT_CATEGORY', 'TC_POOL_REQUIREMENT_CATEGORY.ID_REQUIREMENT_CATEGORY = TC_TR_POOLING_ORDER.ID_REQUIREMENT_CATEGORY','LEFT');
		$this->db->join('ms_aircraft_registration', 'ms_aircraft_registration.ID_AIRCRAFT_REGISTRATION = TC_TR_POOLING_ORDER.ID_AIRCRAFT_REGISTRATION','LEFT');
		$this->db->join('ms_status_order', 'ms_status_order.ID_STATUS_ORDER = TC_TR_POOLING_ORDER.STATUS_ORDER','LEFT');
		$this->db->join('ms_delivery_point', 'ms_delivery_point.ID_DELIVERY_POINT = TC_TR_POOLING_ORDER.ID_DELIVERY_POINT','LEFT');
		$this->db->join('CUSTOMER', 'CUSTOMER.ID_CUSTOMER = TC_TR_POOLING_ORDER.ID_CUSTOMER','LEFT');
		$this->db->where('TC_TR_POOLING_ORDER.ID_POOLING_ORDER', $NO_ACCOUNT);
				// $this->db->where('year(submited)=',date('Y'));
				// $this->db->where('month(submited)=',date('m'));

		$query = $this->db->get();
		return $query->row_object();
	}

	public function insert_pooling($REFERENCE)
	{
		$insert = array(
			'REFERENCE' 			=> $REFERENCE
		);
		return $this->db->insert('tr_pooling', $insert);
	}

	public function insert_pooling_order($insert)
	{
		$this->db->insert('TC_TR_POOLING_ORDER', $insert);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	public function update_status($id, $data)
	{
		
		$this->db->where('ID_POOLING_ORDER', $id);
		return $this->db->update('TC_TR_POOLING_ORDER', $data);
	}
	
	public function update_unserviceable($id, $data)
	{
		
		$this->db->where('ID_POOLING_ORDER', $id);
		return $this->db->update('TC_TR_POOLING_ORDER', $data);
	}

	public function update_serviceable($id, $data)
	{
		
		$this->db->where('ID_POOLING_ORDER', $id);
		return $this->db->update('TC_TR_POOLING_ORDER', $data);
	}
}
